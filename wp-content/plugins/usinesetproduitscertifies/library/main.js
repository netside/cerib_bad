$(document).ready(function()
{
    $(document).foundation();

    $(document).on('open.fndtn.reveal', '[data-reveal]', function () {
        $('.result pre').hide();
    });

    //Close Modal
    $('.close-modal').click(function(e)
    {
        e.stopPropagation();
        e.preventDefault();

        $('a.close-reveal-modal').trigger('click');
        $('.result pre').hide();
    });

    $('#nf-form').submit(function(e)
    {
        //$('.result pre').hide();
        $('.result .loader').fadeIn();

        /*$('#nf-form button').addClass('disabled').prop('disabled', true);

        e.stopPropagation();
        e.preventDefault();

        console.log('submit NF');

        var formData = new FormData();

        formData.append('action', 'update_database');
        formData.append('type', 'nf');

        $.each($('#nf-declinaisons')[0].files, function(i, file)
        {
           formData.append('nf-declinaisons-'+i, file);
        });

        $.each($('#nf-usines')[0].files, function(i, file)
        {
            formData.append('nf-usines-'+i, file);
        });

        $.each($('#nf-certifications')[0].files, function(i, file)
        {
            formData.append('nf-certifications-'+i, file);
        });

        $.each($('#nf-certificats')[0].files, function(i, file)
        {
            formData.append('nf-certificats-'+i, file);
        });

        $.ajax(
        {
            url: ajaxurl,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data)
            {
                //console.log(data);
                $('.result .loader').hide();
                $('.result pre').html(data).fadeIn();
                $('#nf-form button').removeClass('disabled').prop('disabled', false);
            },
            error: function(error)
            {
                $('.result .loader').hide();
                $('.result pre').html(error).fadeIn();
                $('#ce-form button').removeClass('disabled').prop('disabled', false);
            }
        });*/
    });

    $('#ce-form').submit(function(e)
    {
        //$('.result pre').hide();
        $('.result .loader').fadeIn();

        /*$('#ce-form button').addClass('disabled').prop('disabled', true);

        e.stopPropagation();
        e.preventDefault();

        var formData = new FormData();

        formData.append('action', 'update_database');
        formData.append('type', 'ce');

        $.each($('#ce-declinaisons')[0].files, function(i, file)
        {
            formData.append('ce-declinaisons-'+i, file);
        });

        $.each($('#ce-usines')[0].files, function(i, file)
        {
            formData.append('ce-usines-'+i, file);
        });

        $.each($('#ce-marques')[0].files, function(i, file)
        {
            formData.append('ce-marques-'+i, file);
        });

        $.each($('#ce-marquage')[0].files, function(i, file)
        {
            formData.append('ce-marquage-'+i, file);
        });

        $.ajax(
        {
            url: ajaxurl,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data)
            {
                //console.log(data);
                $('.result .loader').hide();
                $('.result pre').html(data).fadeIn();
                $('#ce-form button').removeClass('disabled').prop('disabled', false);
            },
            error: function(error)
            {
                $('.result .loader').hide();
                $('.result pre').html(error).fadeIn();
                $('#ce-form button').removeClass('disabled').prop('disabled', false);
            }
        });*/
    });

    $('#nf-restore').submit(function(e)
    {
        $('.result pre').hide();
        $('.result .loader').fadeIn();

        $('#nf-restore button').addClass('disabled').prop('disabled', true);

        e.stopPropagation();
        e.preventDefault();

        $.post
        (
            ajaxurl,
            {
                'action': 'restore_db',
                'type'  : 'nf'
            },
            function(data)
            {
                $('.result .loader').hide();
                $('.result pre').html(data).fadeIn();
                $('#nf-restore button').removeClass('disabled').prop('disabled', false);
            }
        );
    });

    $('#ce-restore').submit(function(e)
    {
        $('.result pre').hide();
        $('.result .loader').fadeIn();

        $('#ce-restore button').addClass('disabled').prop('disabled', true);

        e.stopPropagation();
        e.preventDefault();

        $.post
        (
            ajaxurl,
            {
                'action': 'restore_db',
                'type'  : 'ce'
            },
            function(data)
            {
                $('.result .loader').hide();
                $('.result pre').html(data).fadeIn();
                $('#ce-restore button').removeClass('disabled').prop('disabled', false);
            }
        );
    });
});