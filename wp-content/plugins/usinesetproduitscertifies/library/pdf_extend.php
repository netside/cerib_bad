<?php
	#
	# Construction de l'ent�te et du pied de page
	#
	# On surcharge la classe PDF qui elle m�me surcharge la classe FPDF ;-)
	#
	
	class PDF_SURCHARGE extends PDF
	{
		var		$rootImg, $sTitre, $sAuteur;
		function PDF_SURCHARGE($orientation='l',$unit='mm',$format='A4', $sTitre='header', $sAuteur='footer')
		{
			//Appel au constructeur parent
			$this->PDF($orientation,$unit,$format);
			
			//Initialisation variable
			$this->titre = $sTitre;
			$this->auteur = $sAuteur;
		}
		
		// Ent�te
		function Header()
		{
			$this->SetLeftMargin(10);
			$this->SetRightMargin(10);
			
			//Titre (Police Arial gras 32 Bleu #003366)
			$this->SetFont("Arial", "", 9);
			$this->SetTextColor(84, 111, 162);
			
			//Couleur de fond
			$this->SetFillColor(255,255,255);
			$this->Cell(0,3,"Date :".date('d-m-Y'),0,0,'R',1);
			//Saut de ligne
		    //$this->Ln(30);
		}
		
		function Footer()
		{
			//global $iPage;
			$iPage = "Page ".($this->PageNo());
			
		    //Positionnement � 1,5 cm du bas
			$this->SetLeftMargin(10);
			$this->SetRightMargin(10);
		    
			$this->SetFont('Arial', '', 9);
			$this->SetTextColor(84, 111, 162);
			
			$this->SetY(-15);
			
			$this->SetFillColor(255,255,255);
			
			$this->Cell(0,10,$iPage,0,0,'R',1);
			
			/*if ($iPage) {
				$tSize = getimagesize($this->rootImg."logo.png");
				//$coef = 0.375;
				$coef = 0.1;
				$width = $tSize[0] * $coef;	// Conversion en mm
				$height = $tSize[1] * $coef;	// Conversion en mm
				
				$this->Image($this->rootImg."logo.png", 10, 280, $width, $height, "PNG", "");
			}*/
		}
		
		function TitreRubrique($sStr){
			if ($sStr) {		
				//Titre (Police Arial gras 32 Bleu #003366)
				$this->SetFont("Arial", "B", 26);
				$this->SetTextColor(255, 0, 0);
				//Couleur de fond
				$this->SetFillColor(192,192,192);
				$this->Cell(10,20,$sStr,1,0,'C',1);
			    //Saut de ligne
			    $this->Ln(25);
			}
		}
		
		function TitreChapitre($sStr, $sStr2=null){
			if ($sStr) {
				$this->Ln(5);	
				//Arial 12
				$this->SetFont('Arial','B',16);
				$this->SetTextColor(255, 0, 0);
				//Couleur de fond
				$this->SetFillColor(255,255,255);
				//Titre
				
				if ($sStr2) {
					// bordure
					$this->Cell(10,20,"",1,0,'C',1);
					$this->Ln(1);
					// titre				
					$this->Cell(10,10,$sStr,0,0,'C',0);
					$this->Ln(8);
					$this->SetFont('Arial','BI',15);
					$this->Cell(10,10,$sStr2,0,0,'C',0);
				} else
					$this->Cell(10,10,$sStr,1,0,'C',1);
				
				//Saut de ligne
				$this->Ln(15);
			}
		}
		
		function ChapeauChapitre($sStr, $size=13){
			if ($sStr) {
				//Arial 12
				$this->SetFont('Arial','B',$size);
				$this->SetTextColor(0, 0, 0);
				//Titre
				$this->Cell(10,10,$sStr,0,0,'C',0);
				//Saut de ligne
				$this->Ln(10);
			}
		}
			
		function CorpsChapitre($sStr, $size=13){
			if ($sStr) {
				$this->SetFont('Arial','',$size);
				$this->SetTextColor(0, 0, 0);
				//Sortie du texte justifi�		
				$this->WriteHTML(10,5,$sStr);
				$this->Ln(10);
			}
		}
		
		//Tableau generique
		function FancyTable($header,$data)
		{
			$iMemX = $this->GetX();
		    $iMemY = $this->GetY();
		    //Couleurs, �paisseur du trait et police grasse
		    $this->SetFillColor(154,177,185);
		    $this->SetTextColor(255);
		    $this->SetDrawColor(0,0,0);
		    $this->SetLineWidth(.1);
		    $this->SetFont('Arial','',6);
		    //En-t�te
		    $iStartSubRow = 0;
			
			$iHauteur = 1;
			$iCount = 0;
			$iTmpNbCell = $iTmpWidth = $iSumCellChild = 0;
			for($i=0;$i<count($header);$i++){
				$tPartHeader = null;
				if ($header[$i]['type']=="multi"){
					$iTmpWidth = 0;
					$tPartHeader = explode("\n",$header[$i]['val']);
					for ($j=0;$j<count($tPartHeader);$j++){
						if ($iTmpWidth<$this->GetStringWidth($tPartHeader[$j]))
							$iTmpWidth = $this->GetStringWidth($tPartHeader[$j])+2;
					}
					$tPartHeader = null;
					$iTmpNbCell = $header[$i]['w'];
					$iSumCellChild = 0;
					//echo $iTmpWidth."-".$iTmpNbCell."<br>";
				}
				if ($header[$i]['w']==1 && $header[$i]['type']=="simple"){
					$tPartHeader = explode("\n",$header[$i]['val']);
					for ($j=0;$j<count($tPartHeader);$j++){
						if (!isset($tMax[$iCount]) || $tMax[$iCount]<$this->GetStringWidth($tPartHeader[$j]))
							$tMax[$iCount] = $this->GetStringWidth($tPartHeader[$j])+2;
					}
					//$tMax[$iCount]=$this->GetStringWidth($header[$i]['val']);
					if (count($tPartHeader)>1)
					    $iHauteur = 2;
					
					if ($iTmpNbCell){
						$iTmpNbCell--;
						$iSumCellChild+=$tMax[$iCount];
						//echo $iTmpNbCell."-".$iSumCellChild."<br>";
					}
					if (!$iTmpNbCell && $iSumCellChild){
						if ($iSumCellChild<$iTmpWidth)
							$tMax[$iCount] = $iTmpWidth-$iSumCellChild+$tMax[$iCount];
					}
					$iCount++;
				}
			}
			for ($i=0;$i<count($data);$i++){
				$tPartHeader = null;
		       	for ($iCount=0;$iCount<count($data[$i]);$iCount++){
					$tPartHeader = explode("\n",$data[$i][$iCount]);
					for ($j=0;$j<count($tPartHeader);$j++){
						if ($tMax[$iCount]<$this->GetStringWidth($tPartHeader[$j]))
							$tMax[$iCount] = $this->GetStringWidth($tPartHeader[$j])+2;
					}
		        	//$tMax[$iCount] = (($this->GetStringWidth($data[$i][$iCount]))>$tMax[$iCount]) ? $this->GetStringWidth($data[$i][$iCount]) : $tMax[$iCount];
		    	}
			}
			
			/*echo "<pre>";
			print_r($tMax);
			echo "</pre>";*/
			
			$iCount=0;		    
			for($i=0;$i<count($header);$i++){
				if ($header[$i]['type']=="multi") {
					$iHauteur = 2;
					$iSizeOfCell = 0;
					for ($j=0; $j<$header[$i]['w']; $j++)
						$iSizeOfCell += $tMax[$iCount+$j];
					$this->Cell($iSizeOfCell,4*$header[$i]['h'],$header[$i]['val'],1,0,'L',1);
				    
					$this->SetX($this->GetX()-$iSizeOfCell);
					$this->SetY($this->GetY()+4);
					$iStartSubRow = $header[$i]['w']+1;
				} else {
					if ($header[$i]['h']==2)
						$iHauteur = 2;
					$this->Cell($tMax[$iCount],4*$header[$i]['h'],$header[$i]['val'],1,0,'L',1);
					$iCount++;
				}
				
				$iStartSubRow--;
				if (!$iStartSubRow && $i<count($header))
					$this->SetY($this->GetY()-4);
			}
		    
			//$this->Ln();
			$this->SetX($iMemX);
			$this->SetY($iMemY+4*$iHauteur);
			
			//if (!$iStartSubRow)
			//	$this->SetY($this->GetY()+7);
				
		    //Restauration des couleurs et de la police
		    $this->SetFillColor(224,235,255);
		    $this->SetTextColor(0);
		    $this->SetFont('Arial','',5);
		    //Donn�es
		    $fill=0;
		    for ($i=0;$i<count($data);$i++){
		       for ($j=0;$j<count($data[$i]);$j++)
		        	$this->Cell($tMax[$j],6,$data[$i][$j],'LRTB',0,'L',$fill);
		        $this->Ln();
				$this->SetX($iMemX);
			
		        $fill=!$fill;
		    }
		    //$this->Cell(array_sum($tMax),0,'','T');
			$this->Ln();
		}

	}
?>