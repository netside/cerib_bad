<?php

include_once('/home/cerib/www/wp-load.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/fpdf/fpdf.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/fpdf/html2pdf.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/pdf_extend.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/domxml-php4-to-php5.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/xml.inc.php');
include_once('/home/cerib/www/wp-content/plugins/usinesetproduitscertifies/library/htmlheader.php');

$tHeader = null;
$iHTML = 0;

error_reporting (E_ERROR);

function minusculesSansAccents($texte)
{
    $texte = strtolower($texte);
    $texte = str_replace(
        array(
            '�', '�', '�', '�', '�', '�',
            '�', '�', '�', '�',
            '�', '�', '�', '�', '�', '�',
            '�', '�', '�', '�',
            '�', '�', '�', '�',
            '�', '�', '�',
        ),
        array(
            'a', 'a', 'a', 'a', 'a', 'a',
            'i', 'i', 'i', 'i',
            'o', 'o', 'o', 'o', 'o', 'o',
            'u', 'u', 'u', 'u',
            'e', 'e', 'e', 'e',
            'c', 'y', 'n',
        ),
        $texte
    );

    return $texte;
}

function stripFileName($file) {
    $newFile = "";

    $i = 0;
    while (isset($file{$i}) && $file{$i}) {
        $c = ord($file{$i});

        if (($c >= ord("0")) && $c <= ord("9"))
            $newFile .= $file{$i};
        else if (($c >= ord("A")) && $c <= ord("Z"))
            $newFile .= $file{$i};
        else if (($c >= ord("a")) && $c <= ord("z"))
            $newFile .= $file{$i};
        else if (($c == ord("_")) || ($c == ord("-")) || ($c == ord(".")))
            $newFile .= $file{$i};
        else if ($c == ord(" "))
            $newFile .= "_";

        $i++;
    }

    return $newFile;
}

function sortPays($a,$b){
    if ($a['dpt']>$b['dpt'])
        return 1;
    else if ($a['dpt']==$b['dpt']){
        if ($a['id_usine']>$b['id_usine'])
            return -1;
        else if ($a['id_usine']==$b['id_usine']) {
            if ($a['num_lng']>$b['num_lng'])
                return 1;
            else if ($a['num_lng']==$b['num_lng'])
                return 0;
            else
                return -1;
        } else
            return 1;
    } else
        return -1;
}

function setHTML($node, $nodeRupture, $famille, $numRow, $niv = 0) {
    global $tNumCrt, $sRemarque, $tHeader, $iHTML;
    $sHeader = "";
    $sHeader2 = "";
    $countNode=0;
    foreach ($node->child_nodes() as $cnode){
        if ($cnode->node_type()==XML_TEXT_NODE){
            if ($niv && !trim(utf8_decode($cnode->node_value()))){
            } else {
                $sHeader .= trim(utf8_decode($cnode->node_value()));
            }
        } else if ($cnode->node_type()==XML_ELEMENT_NODE) {
            if ($cnode->node_name() !== "remarque"){
                if ($famille != $cnode->node_name() && $cnode->has_attributes())
                    $tNumCrt[] = $cnode->get_attribute("num");

                $nodeText = setHTML($cnode, $nodeRupture, $famille, $numRow, $niv+1);
                $nodeText = utf8_decode($nodeText);
                $iFirstSpacePos = (strlen($nodeText)>10) ? strpos($nodeText," ",10) : 0;
                if ($niv==1 && $famille == $cnode->node_name()) {
                    $countNode = count_node($cnode->parent_node())-1;
                    $tHeader[$iHTML]['h']=1;
                    $tHeader[$iHTML]['w']=$countNode;
                    $tHeader[$iHTML]['type']="multi";
                    $tHeader[$iHTML++]['val']= (strlen($nodeText)>10 && $iFirstSpacePos) ? substr($nodeText, 0, $iFirstSpacePos)."\n".substr($nodeText, $iFirstSpacePos+1) : $nodeText;
                    $tHeader[$iHTML]['val'] = utf8_decode($tHeader[$iHTML]['val']);
                    //$tHeader[$iHTML++]['val'] = $nodeText;
                } else if ($niv==1){
                    $tHeader[$iHTML]['h']=1;
                    $tHeader[$iHTML]['w']=1;
                    $tHeader[$iHTML]['type']="simple";
                    $tHeader[$iHTML++]['val']= (strlen($nodeText)>10 && $iFirstSpacePos) ? substr($nodeText, 0, $iFirstSpacePos)."\n".substr($nodeText, $iFirstSpacePos+1) : $nodeText;
                    $tHeader[$iHTML]['val'] = utf8_decode($tHeader[$iHTML]['val']);
                    //$tHeader[$iHTML++]['val'] = $nodeText;
                } else if ($cnode->node_name() != $nodeRupture) {
                    $tHeader[$iHTML]['h']=2;
                    $tHeader[$iHTML]['w']=1;
                    $tHeader[$iHTML]['type']="simple";
                    $tHeader[$iHTML++]['val']= (strlen($nodeText)>10 && $iFirstSpacePos) ? substr($nodeText, 0, $iFirstSpacePos)."\n".substr($nodeText, $iFirstSpacePos+1) : $nodeText;
                    $tHeader[$iHTML]['val'] = utf8_decode($tHeader[$iHTML]['val']);
                    //$tHeader[$iHTML++]['val'] = $nodeText;
                }
            } else {
                $nodeText = setHTML($cnode, $nodeRupture, $famille, $numRow, $niv+1);
                $sRemarque = trim($nodeText);
            }

        }
    }

    if ($numRow==0)
        return $sHeader;
    else
        return $sHeader2;
}


function generate_pdf_nf()
{
    global $wpdb;

    //Cr�ation de Zip (date dans le fichier) et sauvegarde dans pdfgen
    $pdfGenDir = "/home/cerib/www/wp-content/pdfgen/";
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Europe/Paris'));
    $zip = new ZipArchive();

    $fileZIPName = 'NF_'.$date->format('Y-m-d_H-i-s').'.zip';

    $zip->open($pdfGenDir.$fileZIPName, ZipArchive::CREATE);

    $queryCertif = $wpdb->get_results("SELECT * FROM qlx_certif", ARRAY_A);

    foreach($queryCertif as $tCertif)
    {
        $pdf=new PDF_SURCHARGE();

        $pdf->Open();
        $pdf->SetCreator("HTML2PDF - FPDF 1.53");

        $pdf->SetFont('Arial','',12);
        $pdf->SetFillColor(255,255,255);

        $tListing = null;
        $column = $field = null;
        global $tHeader, $tNumCrt, $iHTML;
        $tHeader = null;

        $sRemarque = null;


        //R�cup�ration des Colonnes
        $iHTML=0;
        $tCertif['caracteristiques_xml'] = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>".$tCertif['caracteristiques_xml'];
        $domXml = domxml_open_mem($tCertif['caracteristiques_xml']);
        $tXml = $domXml->get_elements_by_tagname("caracteristiques");
        $sBufferLine = $sBufferLine2 = null;
        $t_numCrtKey = null;
        $tNum = $tNumCrt = null;

        foreach ($tXml as $parentNode) {

            $nodeChild = $parentNode->first_child();
            $sBufferLine = setHTML($parentNode,'famille','libelle',0);
            $tNum = $tNumCrt;
            if ($sBufferLine)
                $sBufferLine = "<tr valign='top'>" . $sBufferLine . "</tr>";
            //$sBufferLine2 = setHTML($parentNode,'famille','libelle',1);
            //if ($sBufferLine2)
            $sBufferLine .= "<tr valign='top'>" . $sBufferLine2 . "</tr>";
        }

            $sSQLSiege = "SELECT QLX_siege.* FROM
                     qlx_etablissements as QLX_usine,
                     qlx_entreprises as QLX_siege,
                     qlx_nn_etablissements_produits_certif as QLX_nnepc
                     WHERE
                     QLX_nnepc.id_certif = '%s' AND
                     QLX_nnepc.id_etablissements = QLX_usine.id AND
                     QLX_usine.id_entreprises = QLX_siege.id
					 GROUP BY QLX_usine.id
                     ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";

            $sSQLUsine = "SELECT QLX_usine.*, QLX_nnepc.id_produits, QLX_nnepc.num_lng, QLX_nnepc.filename, QLX_nnepc.fdes FROM
                 qlx_etablissements as QLX_usine,
                 qlx_entreprises as QLX_siege,
                 qlx_nn_etablissements_produits_certif as QLX_nnepc
                 WHERE
                 QLX_nnepc.id_certif = '%s' AND
                 QLX_nnepc.id_etablissements = QLX_usine.id AND
                 QLX_usine.id_entreprises = QLX_siege.id
                 GROUP BY QLX_usine.id
                 ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";

            $sSQL = "SELECT QLX_siege.*, QLX_usine.*, QLX_nnepc.id_produits, QLX_nnepc.num_lng, QLX_nnepc.filename, QLX_nnepc.fdes FROM
                     qlx_etablissements as QLX_usine,
                     qlx_entreprises as QLX_siege,
                     qlx_nn_etablissements_produits_certif as QLX_nnepc
                     WHERE
                     QLX_nnepc.id_certif = '%s' AND
                     QLX_nnepc.id_etablissements = QLX_usine.id AND
                     QLX_usine.id_entreprises = QLX_siege.id
                     ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";

            $wpdb->show_errors();
            $preparedQuery = $wpdb->prepare($sSQL, $tCertif['id']);

            $listValeurs = $wpdb->get_results($preparedQuery);

            $listValeursByUsine = array();

            //Tab � deux dimensions
            for($i = 0; $i < count($listValeurs); $i++)
            {
                $objUsine = $listValeurs[$i];

                if(gettype($listValeursByUsine[$objUsine->id]) == 'object')
                {
                    $listValeursByUsine[$objUsine->id]->add_product($objUsine->id_produits);
                }
                else
                {
                    //Si existe pas, cr�ation de l'objet Usine
                    $usine = new Usine();
                    $usine->add_product($objUsine->id_produits);

                    $listValeursByUsine[$objUsine->id] = $usine;
                }
            }

            $wpdb->show_errors();
            $preparedQuery = $wpdb->prepare($sSQLSiege, $tCertif['id']);
            $listSieges = $wpdb->get_results($preparedQuery);


            $preparedQuery = $wpdb->prepare($sSQLUsine, $tCertif['id']);
            $listUsines = $wpdb->get_results($preparedQuery);

            //Liste D�partement
            $arrayDpt = array();
            $arrayPays = array();
            for($i = 0; $i < count($listUsines); $i++)
            {
                $cp = $listUsines[$i]->cp;
                $pays = $listUsines[$i]->pays;

                if(trim(strtolower($pays)) == "france")
                {
                    $dpt = substr($listUsines[$i]->cp, 0, 2);
                    $listUsines[$i]->dpt = $dpt;

                    if(!in_array($dpt, $arrayDpt))
                        array_push($arrayDpt, $dpt);
                }
                else
                {
                    $pays = trim($listUsines[$i]->pays);
                    $listUsines[$i]->dpt = $pays;

                    if(!in_array($pays, $arrayPays))
                        array_push($arrayPays, $pays);
                }
            }
            $arrayFilter = array_merge($arrayDpt, $arrayPays);

            //Pour chaque usine, build page PDF
            for($i = 0; $i < count($listUsines); $i++)
            {

                $listUsines[$i]->adr_2 = utf8_decode(trim($listUsines[$i]->adr_2));
                $listUsines[$i]->adr_3 = utf8_decode(trim($listUsines[$i]->adr_3));
                $listUsines[$i]->tel = utf8_decode(trim($listUsines[$i]->tel));
                $listUsines[$i]->fax = utf8_decode(trim($listUsines[$i]->fax));

                $listSieges[$i]->adr_2 = utf8_decode(trim($listSieges[$i]->adr_2));
                $listSieges[$i]->adr_3 = utf8_decode(trim($listSieges[$i]->adr_3));
                $listSieges[$i]->tel = utf8_decode(trim($listSieges[$i]->tel));
                $listSieges[$i]->fax = utf8_decode(trim($listSieges[$i]->fax));

                //Construction TABLEAUX
                $listingUsineSiege = NULL;
                $listingValeurs = array();
                $tData = null;
                unset($tData);

                //USINE
                $listingUsineSiege  = "<b>Usine</b> : <br>";
                $listingUsineSiege .= $listUsines[$i]->nom . "<br>";

                if($listUsines[$i]->logo == 'true' && $tCertif['logo'] == 'true')
                    $listingUsineSiege .= "<img src='/home/cerib/www/wp-content/themes/Mexin_child_theme/images/fdes.jpg' width=100 height=39 border=0><br><br><br>";

                $listingUsineSiege .= $listUsines[$i]->adr_1 . "<br>";

                if(!empty($listUsines[$i]->adr_2))
                    $listingUsineSiege .= $listUsines[$i]->adr_2 . "<br>";

                if(!empty($listUsines[$i]->adr_3))
                    $listingUsineSiege .= $listUsines[$i]->adr_3 . "<br>";

                $listingUsineSiege .= $listUsines[$i]->cp . "<br>";

                $listingUsineSiege .= $listUsines[$i]->ville . "<br>";

                $listingUsineSiege .= $listUsines[$i]->pays . "<br>";

                if(!empty($listUsines[$i]->tel))
                    $listingUsineSiege .= "TEL : " . $listUsines[$i]->tel . "<br>";

                if(!empty($listUsines[$i]->fax))
                    $listingUsineSiege .= "FAX : " . $listUsines[$i]->fax . "<br>";

                //SIEGE
                $listingUsineSiege .= "<br><b>SIEGE : </b><br>";
                $listingUsineSiege .= $listSieges[$i]->nom . "<br>";

                $listingUsineSiege .= $listSieges[$i]->adr_1 . "<br>";

                if(!empty($listSieges[$i]->adr_2))
                    $listingUsineSiege .= $listSieges[$i]->adr_2 . "<br>";

                if(!empty($listSieges[$i]->adr_3))
                    $listingUsineSiege .= $listSieges[$i]->adr_3 . "<br>";

                $listingUsineSiege .= $listSieges[$i]->cp . "<br>";

                $listingUsineSiege .= $listSieges[$i]->ville . "<br>";

                $listingUsineSiege .= $listSieges[$i]->pays . "<br>";

                if(!empty($listSieges[$i]->tel))
                    $listingUsineSiege .= "TEL : " . $listSieges[$i]->tel . "<br>";

                if(!empty($listSieges[$i]->fax))
                    $listingUsineSiege .= "FAX : " . $listSieges[$i]->fax . "<br>";

                if(gettype($listValeursByUsine[$listUsines[$i]->id]) == 'object')
                {
                    $objValeurs = $listValeursByUsine[$listUsines[$i]->id];

                    for ($j = 0; $j < count($objValeurs->getArrayProduct()); $j++) {
                        $idProduct = $objValeurs->getProductId($j);

                        $sSQLValeurs = "SELECT * FROM qlx_valeurs WHERE id_produits = %d ORDER BY id";
                        $preparedQuery = $wpdb->prepare($sSQLValeurs, $idProduct);
                        $listValeurs = $wpdb->get_results($preparedQuery, ARRAY_A);

                        $tValeur=null;

                        for ($k = 0; $k < count($listValeurs); $k++) {
                            $tValeur[$listValeurs[$k]['num_crt']] = chop($listValeurs[$k]['valeur']);
                        }

                        for ($iNumCrt = 0; $iNumCrt < sizeOf($tNum); $iNumCrt++) {
                            if (!isset($tValeur[$tNum[$iNumCrt]]) || $tValeur[$tNum[$iNumCrt]] == "")
                                $tValeur[$tNum[$iNumCrt]] = " ";



                            $iFirstSpacePos = (strlen($tValeur[$tNum[$iNumCrt]])>10) ? strpos($tValeur[$tNum[$iNumCrt]]," ",10) : 0;
                            $iFirstSpacePos = (strlen($tValeur[$tNum[$iNumCrt]])>40) ? strpos($tValeur[$tNum[$iNumCrt]]," ",40) : $iFirstSpacePos;

                            $tData[$j][$iNumCrt] = ($iFirstSpacePos) ? substr($tValeur[$tNum[$iNumCrt]], 0, $iFirstSpacePos)."\n".substr($tValeur[$tNum[$iNumCrt]], $iFirstSpacePos+1) : $tValeur[$tNum[$iNumCrt]];

                            $tData[$j][$iNumCrt] = utf8_decode($tData[$j][$iNumCrt]);
                        }
                    }
                }

                $pdf->AddPage();
                $pdf->CorpsChapitre("<b>Produit certifi� :</b>".utf8_decode($tCertif['nom'])."<br>",15);
                $pdf->SetFont('Arial','',9);
                $pdf->SetFillColor(255,255,255);

                $iMemY = $pdf->GetY();

                $pdf->SetTextColor(64, 64, 64);
                $pdf->WriteHTML($pdf->GetX(),6,"<b>".str_replace("\n","",$listUsines[$i]->dpt)."</b>");

                $pdf->SetTextColor(128, 128, 128);
                $pdf->WriteHTML($pdf->GetX()+10,6,str_replace("\n","",$listingUsineSiege));
                $iMemYSuiv = $pdf->GetY();

                $pdf->Line($pdf->GetX()-5,$iMemY,$pdf->GetX()-5,$pdf->GetY());
                $pdf->Rect($pdf->GetX(),$iMemY,65,$pdf->GetY()-$iMemY);

                $pdf->SetX($pdf->GetX()+70);
                $pdf->SetY($iMemY);

                //Valeurs
                $pdf->FancyTable($tHeader,$tData);


                $pdf->Ln();
                if ($pdf->GetY()<$iMemYSuiv){
                    $pdf->SetY($iMemYSuiv);
                }

            }

        $sFileComplete = '/home/cerib/www/wp-content/pdfgen/' . utf8_decode($tCertif['nom']) . '_' . $date->format('Y-m-d') . '.pdf';



        $pdf->Output($sFileComplete, "F");
        $pdf->Close();

        $zip->addFile($pdfGenDir . utf8_decode($tCertif['nom']) . '_' . $date->format('Y-m-d') .'.pdf', ucfirst(minusculesSansAccents(utf8_decode($tCertif['nom']))) . '_' . $date->format('Y-m-d') .'.pdf');

        $pdf = null;
        unset($pdf);
    }

    $zip->close();

    //TO DO Insertion lien back office (table uepc_notif !!) + Rajouter Lien page Liste Produits NF
	$tableHistoName = $wpdb->prefix . "uepc_histo";
	
	$preparedQuery = "UPDATE ".$tableHistoName." SET urlzip = '".$fileZIPName."' WHERE type_produit = 'nf' ORDER BY id_histo DESC LIMIT 1";
	$wpdb->query($preparedQuery);
	
	
    return $fileZIPName;
}

generate_pdf_nf();