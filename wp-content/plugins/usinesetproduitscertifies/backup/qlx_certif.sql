8	1	Entrevous	\N	<caracteristiques>\
\	\	\	\	\	<remarque>PS : Porteur Simple / TCI : Porteur à table de compression incorporée</remarque>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>type</libelle>\
\	\	\	\	\	\	<critere num="277">coffrage</critere>\
\	\	\	\	\	\	<critere num="279">porteur</critere>\
\	\	\	\	\	\	<critere num="278">T.C.I.</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="132">\
\	\	\	\	\	hauteur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="137">\
\	\	\	\	\	largeur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="133">\
\	\	\	\	\	longueur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="138">\
\	\	\	\	\	dimensions feuillure\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="0" type="RBTN"><libelle>type</libelle>\
<valeur num_crt="277">COFFRAGE</valeur>\
<valeur num_crt="279">PORTEUR</valeur>\
<valeur num_crt="278">TCI</valeur></champs>\
\	\	\	\	</formulaire>	false
1	2	Blocs granulat courant	\N	<caracteristiques>\
\	\	\	\	\	<critere num="861" visible="false">\
\	\	\	\	\	FDES\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="147">\
\	\	\	\	\	Catégorie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="155">\
\	\	\	\	\	Classe de résistance\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="131">\
\	\	\	\	\	Epaisseur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="132">\
\	\	\	\	\	Hauteur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="133">\
\	\	\	\	\	Longueur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="641">\
\	\	\	\	\	Tolérances dimensionnelles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="134">\
\	\	\	\	\	Nombre de lames d'air\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="583">\
\	\	\	\	\	Mv bloc (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="585">\
\	\	\	\	\	Mv béton (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1421">\
\	\	\	\	\	Sismique\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="861">FDES :</valeur></champs>\
\	\	\	\	<champs num_crt="147" type="LST"><libelle>Catégorie</libelle>\
<valeur>CREUX</valeur>\
<valeur>PERFORE</valeur>\
<valeur>PLEIN</valeur></champs>\
\	\	\	\	<champs num_crt="155" type="LST-EXP"><libelle>Classe de résistance</libelle>\
<valeur>L25</valeur>\
<valeur>L35</valeur>\
<valeur>L40</valeur>\
<valeur>L45</valeur>\
<valeur>L70</valeur>\
<valeur>B40</valeur>\
<valeur>B40 ET B60</valeur>\
<valeur>B40 ET B60 ET B80</valeur>\
<valeur>B60</valeur>\
<valeur>B80</valeur>\
<valeur>B80 ET B120</valeur>\
<valeur>B80 ET B120 ET B160</valeur>\
<valeur>B120</valeur>\
<valeur>B160</valeur></champs>\
\	\	\	\	<champs num_crt="131" type="LST"><libelle>Epaisseur</libelle>\
<valeur>45</valeur>\
<valeur>50</valeur>\
<valeur>75</valeur>\
<valeur>100</valeur>\
<valeur>125</valeur>\
<valeur>150</valeur>\
<valeur>175</valeur>\
<valeur>200</valeur>\
<valeur>225</valeur>\
<valeur>250</valeur>\
<valeur>275</valeur>\
<valeur>300</valeur></champs>\
\	\	\	\	<champs num_crt="641" type="LST"><libelle>Catégorie de tolérances dimensionnelles</libelle>\
<valeur>D1</valeur>\
<valeur>D2</valeur>\
<valeur>D3</valeur>\
<valeur>D4</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="1421">Sismique</valeur></champs>\
\	\	\	\	</formulaire>	true
3	5	Blocs en béton cellulaire	\N	<caracteristiques>\
\	\	\	\	\	<critere num="147">\
\	\	\	\	\	Catégorie (C: à coller M: à maçonner)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="145">\
\	\	\	\	\	MVn (Kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="146">\
\	\	\	\	\	Rcn (MPa)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="149">\
\	\	\	\	\	Lambda utile (W/m.°C)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Dimensions (mm)</libelle>\
\	\	\	\	\	\	<critere num="131">Epaisseur</critere>\
\	\	\	\	\	\	<critere num="132">Hauteur</critere>\
\	\	\	\	\	\	<critere num="133">Longueur</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Emboîtement</libelle>\
\	\	\	\	\	\	<critere num="300">Simple</critere>\
\	\	\	\	\	\	<critere num="301">Double</critere>\
\	\	\	\	\	\	<critere num="302">Poignées</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="145" type="LST"><libelle>Masse volumique :</libelle>\
<valeur>350</valeur>\
<valeur>400</valeur>\
<valeur>450</valeur>\
<valeur>500</valeur>\
<valeur>550</valeur>\
<valeur>600</valeur></champs>\
\	\	\	\	</formulaire>	false
23	6	Carreaux à liant ciment	\N	<caracteristiques>\
\	\	\	\	\	<critere num="362">\
\	\	\	\	\	Désignation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="140">\
\	\	\	\	\	Dimensions (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="131">\
\	\	\	\	\	Epaisseur (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="381">\
\	\	\	\	\	Etat de surface\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="293">\
\	\	\	\	\	Classement UPEC\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="364">\
\	\	\	\	\	Caractéristiques optionnelles\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
22	8	Eléments pour clôtures en béton	\N	<caracteristiques>\
\	\	\	\	\	<critere num="413">\
\	\	\	\	\	Types d'éléments\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="414">\
\	\	\	\	\	Section (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="415">\
\	\	\	\	\	Plage de longueurs utiles (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="416">\
\	\	\	\	\	Classe de résistance mécanique\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="417">\
\	\	\	\	\	Absorption d'eau E+\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="418">\
\	\	\	\	\	G (gel/dégel)\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
24	20	Adjuvants	\N	<caracteristiques>\
\	\	\	\	\	<critere num="167">\
\	\	\	\	\	Classes\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="236">\
\	\	\	\	\	Appellations commerciales\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="305">\
\	\	\	\	\	Plage/dosage d'utilisation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="306">\
\	\	\	\	\	Centre de production\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="307">\
\	\	\	\	\	Centre de distribution\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="167" type="LST"><libelle>Type :</libelle>\
<valeur>ACCELERATEUR DE DURCISSEMENT</valeur>\
<valeur>ACCELERATEUR DE PRISE</valeur>\
<valeur>RETENTEUR D'EAU</valeur>\
<valeur>RETARDATEUR DE PRISE</valeur>\
<valeur>ENTRAINEUR D'AIR</valeur>\
<valeur>HYDROFUGE DE MASSE</valeur>\
<valeur>PLASTIFIANT REDUCTEUR D'EAU</valeur>\
<valeur>PLASTIFIANT REDUCTEUR D'EAU ACCELERATEUR DE PRISE</valeur>\
<valeur>PLASTIFIANT REDUCTEUR D'EAU RETARDATEUR DE PRISE</valeur>\
<valeur>SUPERPLASTIFIANT HAUT REDUCTEUR D'EAU RETARDATEUR DE PRISE</valeur>\
<valeur>SUPERPLASTIFIANT HAUT REDUCTEUR D'EAU</valeur>\
<valeur>PRODUIT DE CURE</valeur>\
<valeur>SUPERPLASTIFIANT HAUT REDUCTEUR D'EAU ACCELERATEUR DE PRISE</valeur></champs>\
\	\	\	\	</formulaire>	false
12	21	Chambres	\N	<caracteristiques>\
\	\	\	\	\	<critere num="273">\
\	\	\	\	\	Type\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="235">\
\	\	\	\	\	Classe\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="170">\
\	\	\	\	\	Modéles monobloc\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="171">\
\	\	\	\	\	Modéles à radier à reconstituer\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="802">\
\	\	\	\	\	France Télécom\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="803">\
\	\	\	\	\	Autre opérateur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="804">\
\	\	\	\	\	Non équipée\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="805">\
\	\	\	\	\	Destinée à être équipée d'un cadre\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="806">\
\	\	\	\	\	Nature du matériau constitutif des chambres\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="273" type="LST"><libelle>Type :</libelle>\
<valeur>K1</valeur>\
<valeur>K2</valeur>\
<valeur>K3</valeur>\
<valeur>L0</valeur>\
<valeur>L1</valeur>\
<valeur>L2</valeur>\
<valeur>1/2 L4</valeur>\
<valeur>L3</valeur>\
<valeur>M1</valeur>\
<valeur>M2</valeur>\
<valeur>L4</valeur>\
<valeur>M3</valeur>\
<valeur>P1</valeur>\
<valeur>L5</valeur>\
<valeur>P2</valeur>\
<valeur>L6</valeur></champs>\
\	\	\	\	<champs num_crt="235" type="LST"><libelle>Utilisation :</libelle>\
<valeur>TROTTOIR (T)</valeur>\
<valeur>CHAUSSEE (C)</valeur>\
<valeur>CHAUSSEE (modèle spécial)</valeur>\
<valeur>ESPACE VERT DOMESTIQUE (Vd)</valeur>\
<valeur>TEST V2</valeur>\
<valeur>TROTTOIR (gamme MORPHEE)</valeur>\
<valeur>CHAUSSEE (gamme MORPHEE)</valeur>\
<valeur>CHAUSSEE (gamme STRADEASY)</valeur>\
<valeur>TROTTOIR (gamme STRADEASY)</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="170">Monobloc :</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="171">A radier à reconstituer :</valeur></champs>\
\	\	\	\	</formulaire>	false
14	22	Produits d'aménagement	\N	<caracteristiques>\
\	\	\	\	\	<critere num="248">\
\	\	\	\	\	Type de produit\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="196">\
\	\	\	\	\	Famille\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="248" type="LST"><libelle>Type de produits :</libelle>\
<valeur>Bancs</valeur>\
<valeur>Bordures de défense</valeur>\
<valeur>Bordures et murets jardinières</valeur>\
<valeur>Bordures hautes</valeur>\
<valeur>Bordure BH</valeur>\
<valeur>Bordure tram</valeur>\
<valeur>Bornes</valeur>\
<valeur>Cendriers</valeur>\
<valeur>Colonne d'affichage</valeur>\
<valeur>Corbeilles</valeur>\
<valeur>Eléments d'aménagement des sols</valeur>\
<valeur>Eléments de soutènement empilables</valeur>\
<valeur>Eléments dissuasifs</valeur>\
<valeur>Eléments modulaires de protection</valeur>\
<valeur>Giratoires</valeur>\
<valeur>Grille d'arbres à usage strictement piétonnier</valeur>\
<valeur>Ilots de protection</valeur>\
<valeur>Jardinières</valeur>\
<valeur>Marche et contre marche d'habillage</valeur>\
<valeur>Muret de stabilisation des sols (ht inférieure ou égale à 1m)</valeur>\
<valeur>Murs de soutènement monolithiques</valeur>\
<valeur>panneau d'affichage</valeur>\
<valeur>Platelage pour passage a niveau</valeur>\
<valeur>Ralentisseurs</valeur>\
<valeur>Socles à vélos</valeur>\
<valeur>Tables</valeur>\
<valeur>Tables de ping pong</valeur>\
<valeur>Tables de pique-nique</valeur>\
<valeur>Eléments de Protection Urbains</valeur>\
<valeur>AUTONOR</valeur>\
<valeur>Bordures et séparateurs de voies</valeur>\
<valeur>Dalles gazon</valeur>\
<valeur>Pas japonais</valeur>\
<valeur>Marche et contre marche d'habillage</valeur>\
<valeur>Blocs à vélo</valeur>\
<valeur>Sièges</valeur>\
<valeur>Bacs à fleurs</valeur>\
<valeur>Columbariums</valeur>\
<valeur>Tombes à urnes</valeur>\
<valeur>Bassin</valeur>\
<valeur>Murets</valeur>\
<valeur>Jardinières</valeur>\
<valeur>Statues</valeur>\
<valeur>Colonnes</valeur>\
<valeur>Éléments décoratifs pour usage intérieur</valeur>\
<valeur>Abris conteneurs</valeur>\
<valeur>Poubelles</valeur>\
<valeur>GUID'BUS</valeur>\
<valeur>Bordure Quai de bus</valeur>\
<valeur>Bordure TU</valeur>\
<valeur>Bordure P3</valeur></champs>\
\	\	\	\	</formulaire>	false
16	23	Têtes d'aqueduc	\N	<caracteristiques>\
\	\	\	\	\	<critere num="250">\
\	\	\	\	\	Modéles et diamètres d'appellation (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="136">\
\	\	\	\	\	Diamètre d'utilisation (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="136" type="LST"><libelle>Diamètre d'utilisation (mm) :</libelle>\
<valeur>300</valeur>\
<valeur>300/400</valeur>\
<valeur>400</valeur>\
<valeur>500</valeur>\
<valeur>500/600</valeur>\
<valeur>600</valeur>\
<valeur>800</valeur></champs>\
\	\	\	\	</formulaire>	false
9	24	Tuyaux	\N	<caracteristiques>\
\	\	\	\	\	<critere num="242">\
\	\	\	\	\	Nature\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="241">\
\	\	\	\	\	Diamètre nominal\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="244">\
\	\	\	\	\	Spécificité\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="243">\
\	\	\	\	\	Longueur utile (m)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Classe de résistance</libelle>\
\	\	\	\	\	\	<critere num="269">90</critere>\
\	\	\	\	\	\	<critere num="270">135</critere>\
\	\	\	\	\	\	<critere num="271">165</critere>\
\	\	\	\	\	\	<critere num="272">200</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="188">\
\	\	\	\	\	Type(s) de joints\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="242" type="LST"><libelle>Nature :</libelle>\
<valeur>ARME</valeur>\
<valeur>NON ARME</valeur>\
<valeur>FIBRE ACIER</valeur>\
<valeur>FIBRE</valeur></champs>\
\	\	\	\	<champs num_crt="241" type="LST"><libelle>Diamètre nominal :</libelle>\
<valeur>300</valeur>\
<valeur>400</valeur>\
<valeur>500</valeur>\
<valeur>600</valeur>\
<valeur>800</valeur>\
<valeur>1000</valeur>\
<valeur>1200</valeur>\
<valeur>1400</valeur>\
<valeur>1500</valeur>\
<valeur>1600</valeur>\
<valeur>1800</valeur>\
<valeur>2000</valeur>\
<valeur>2200</valeur>\
<valeur>2500</valeur>\
<valeur>2600</valeur>\
<valeur>2800</valeur>\
<valeur>3000</valeur>\
<valeur>3200</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><libelle>Classe de résistance</libelle>\
<valeur num_crt="269">Classe 90 :</valeur>\
<valeur num_crt="270">Classe 135 :</valeur>\
<valeur num_crt="271">Classe 165 :</valeur>\
<valeur num_crt="272">Classe 200 :</valeur></champs>\
\	\	\	\	</formulaire>	false
10	24	Regards de visite	\N	<caracteristiques>\
\	\	\	\	\	<critere num="245">\
\	\	\	\	\	Type d'élément\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="189">\
\	\	\	\	\	Echelons\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="246">\
\	\	\	\	\	Spécificité\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="190">\
\	\	\	\	\	Diamètre Principal (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="132">\
\	\	\	\	\	Hauteur (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="191">\
\	\	\	\	\	Diamètre de sortie (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="247">\
\	\	\	\	\	Nature du matériau de sortie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="188">\
\	\	\	\	\	Joint pour la colonne\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="281">\
\	\	\	\	\	Joint pour sortie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="193">\
\	\	\	\	\	Complément de gamme avec usine N°\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="245" type="LST"><libelle>Type d'élément :</libelle>\
<valeur>DALLE REDUCTRICE</valeur>\
<valeur>ELEMENT DE COURONNEMENT</valeur>\
<valeur>ELEMENT DE FOND</valeur>\
<valeur>ELEMENT DE FOND A CAROTTER</valeur>\
<valeur>ELEMENT DROIT</valeur>\
<valeur>ELEMENT DROIT ARME</valeur>\
<valeur>ELEMENTS COLLES</valeur>\
<valeur>REHAUSSE SOUS CADRE</valeur>\
<valeur>TETE REDUCTRICE</valeur>\
<valeur>TUYAU/REGARD</valeur>\
<valeur>ELEMENT DE FOND PLAT</valeur></champs>\
\	\	\	\	</formulaire>	false
11	24	Boites de branchement	\N	<caracteristiques>\
\	\	\	\	\	<critere num="245">\
\	\	\	\	\	Type d'élément\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="246">\
\	\	\	\	\	Spécificité\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="192">\
\	\	\	\	\	Section principale de colonne (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="132">\
\	\	\	\	\	Hauteur utile (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="191">\
\	\	\	\	\	Diamètre de sortie (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="247">\
\	\	\	\	\	Nature du matériau de sortie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="188">\
\	\	\	\	\	Joint pour colonne\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="281">\
\	\	\	\	\	Joint pour sortie\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="245" type="LST"><libelle>Type d'élément</libelle>\
<valeur>SUPPORT DE TAMPON</valeur>\
<valeur>ELEMENT DE FOND</valeur>\
<valeur>ELEMENT DROIT</valeur>\
<valeur>TAMPON</valeur></champs>\
\	\	\	\	<champs num_crt="192" type="LST"><libelle>Section principale :</libelle>\
<valeur>300</valeur>\
<valeur>300X300</valeur>\
<valeur>400</valeur>\
<valeur>400X400</valeur>\
<valeur>600</valeur>\
<valeur>600X600</valeur></champs>\
\	\	\	\	</formulaire>	false
17	25	Tuiles	\N	<caracteristiques>\
\	\	\	\	\	<critere num="236">\
\	\	\	\	\	Appellation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="154">\
\	\	\	\	\	Type\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="161">\
\	\	\	\	\	Longueur sous tenon (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="162">\
\	\	\	\	\	Largeur utile (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="163">\
\	\	\	\	\	Masse (kg)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="164">\
\	\	\	\	\	Classe montagne\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="154" type="LST"><libelle>Type :</libelle>\
<valeur>Plane</valeur>\
<valeur>Plate</valeur>\
<valeur>Profilée</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="164">Classe montagne :</valeur></champs>\
\	\	\	\	</formulaire>	false
2	27	Blocs apparents	\N	<caracteristiques>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Destination</libelle>\
\	\	\	\	\	\	<critere num="234">M ou C</critere>\
\	\	\	\	\	\	<critere num="181">I ou E</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="147">\
\	\	\	\	\	Catégorie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="155">\
\	\	\	\	\	Classe de résistance\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Dimensions d'appellation</libelle>\
\	\	\	\	\	\	<critere num="182">Epaisseur (mm)</critere>\
\	\	\	\	\	\	<critere num="183">Hauteur (mm)</critere>\
\	\	\	\	\	\	<critere num="184">Longueur (mm)</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="185">\
\	\	\	\	\	Alvéoles débouchantes\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="186">\
\	\	\	\	\	Face(s) de parement\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="187">\
\	\	\	\	\	Etat de surface\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
18	28	Caveaux Autonomes	\N	<caracteristiques>\
\	\	\	\	\	<critere num="154">\
\	\	\	\	\	Type\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="198">\
\	\	\	\	\	Nombre de places\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="249">\
\	\	\	\	\	Plage de dimensions nominales\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="199">\
\	\	\	\	\	Code filtre\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="200">\
\	\	\	\	\	Ouverture\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="201">\
\	\	\	\	\	Garniture(s) d'étanchéité\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="154" type="LST"><libelle>Type :</libelle>\
<valeur>Eléments séparés</valeur>\
<valeur>Enfeu</valeur>\
<valeur>Monobloc</valeur>\
<valeur>Monobloc tronconique</valeur></champs>\
\	\	\	\	<champs num_crt="198" type="LST"><libelle>Nombre de places :</libelle>\
<valeur>1</valeur>\
<valeur>2</valeur>\
<valeur>3</valeur>\
<valeur>4</valeur>\
<valeur>6</valeur>\
<valeur>8</valeur>\
<valeur>9</valeur>\
<valeur>2/3</valeur>\
<valeur>4/6</valeur>\
<valeur>6/9</valeur></champs>\
\	\	\	\	<champs num_crt="200" type="LST"><libelle>Ouverture :</libelle>\
<valeur>Frontale</valeur>\
<valeur>Supérieure-Frontale</valeur>\
<valeur>Supérieure</valeur>\
<valeur>Mixte</valeur></champs>\
\	\	\	\	</formulaire>	false
21	30	Bornes pavillonnaires	\N	<caracteristiques>\
\	\	\	\	\	<critere num="239">\
\	\	\	\	\	Désignation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>largeur</libelle>\
\	\	\	\	\	\	<critere num="174">int.</critere>\
\	\	\	\	\	\	<critere num="294">ext.</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>hauteur</libelle>\
\	\	\	\	\	\	<critere num="175">int.</critere>\
\	\	\	\	\	\	<critere num="176">ext.</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Profondeur</libelle>\
\	\	\	\	\	\	<critere num="295">int.</critere>\
\	\	\	\	\	\	<critere num="296">ext.</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Equipement</libelle>\
\	\	\	\	\	\	<critere num="297">France Télécom</critere>\
\	\	\	\	\	\	<critere num="298">autres utilisations</critere>\
\	\	\	\	\	\	<critere num="299">non équipée</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="0" type="CHK"><libelle>Equipement</libelle>\
<valeur num_crt="297">France Télécom :</valeur>\
<valeur num_crt="298">Autres utilisations :</valeur>\
<valeur num_crt="299">Sans équipement :</valeur></champs>\
\	\	\	\	</formulaire>	false
42	35	Appuis de fenêtre	\N	<caracteristiques>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>famille d'appuis</libelle>\
\	\	\	\	\	\	<critere num="461">largeur (cm)</critere>\
\	\	\	\	\	\	<critere num="463">plage des longueurs utiles (cm)</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Caractéristiques de forme</libelle>\
\	\	\	\	\	\	<critere num="464">rejingots latéraux</critere>\
\	\	\	\	\	\	<critere num="465">rejingot arrière débordant</critere>\
\	\	\	\	\	\	<critere num="468">avec oreilles</critere>\
\	\	\	\	\	\	<critere num="469">cranté</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="470">\
\	\	\	\	\	aspect de surface\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="471">\
\	\	\	\	\	teinte\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="472">\
\	\	\	\	\	durabilité vis-à-vis du gel/dégel\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
81	241	Bordures	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1281" visible="false">\
\	\	\	\	\	FDES\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="482">\
\	\	\	\	\	Profils\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Classe de résistance</libelle>\
\	\	\	\	\	\	<critere num="483">U</critere>\
\	\	\	\	\	\	<critere num="484">T</critere>\
\	\	\	\	\	\	<critere num="485">S</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance renforcée aux agressions climatiques</libelle>\
\	\	\	\	\	\	<critere num="486">B</critere>\
\	\	\	\	\	\	<critere num="487">D</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance à l'abrasion</libelle>\
\	\	\	\	\	\	<critere num="488">H</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>longueur</libelle>\
\	\	\	\	\	\	<critere num="489">cm</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="1281">FDES :</valeur></champs>\
\	\	\	\	<champs num_crt="482" type="LST"><libelle>Profil :</libelle>\
<valeur>A1</valeur>\
<valeur>A2</valeur>\
<valeur>P1</valeur>\
<valeur>P2</valeur>\
<valeur>P4</valeur>\
<valeur>T1</valeur>\
<valeur>T2</valeur>\
<valeur>T3</valeur>\
<valeur>T4</valeur>\
<valeur>CS1</valeur>\
<valeur>CS2</valeur>\
<valeur>CS3</valeur>\
<valeur>CS4</valeur>\
<valeur>CC1</valeur>\
<valeur>CC2</valeur>\
<valeur>I1</valeur>\
<valeur>I2</valeur>\
<valeur>I3</valeur>\
<valeur>I4</valeur>\
<valeur>CS1-SC</valeur>\
<valeur>CS2-SC</valeur>\
<valeur>CC1-SC</valeur>\
<valeur>CC2-SC</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><libelle>Classe de résistance</libelle>\
<valeur num_crt="483">Classe de résistance U :</valeur>\
<valeur num_crt="484">Classe de résistance T :</valeur>\
<valeur num_crt="485">Classe de résistance S :</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="486">Résistance renforcée aux agressions climatiques (marquage B) :</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="487">Résistance renforcée aux agressions climatiques (marquage D) :</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="488">Résistance à l'abrasion (marquage H) :</valeur></champs>\
\	\	\	\	</formulaire>	true
121	281	Paves	\N	<caracteristiques>\
\	\	\	\	\	<critere num="523">\
\	\	\	\	\	Appellations commerciales\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="524">\
\	\	\	\	\	Classes d'appellation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="525">\
\	\	\	\	\	Dimensions de fabrication\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance renforcée aux agressions climatiques</libelle>\
\	\	\	\	\	\	<critere num="526">B</critere>\
\	\	\	\	\	\	<critere num="527">D</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance à l'abrasion</libelle>\
\	\	\	\	\	\	<critere num="528">H</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="524" type="LST"><libelle>Classe d'utilisation (fonction de l'intensité du trafic) :</libelle>\
<valeur>T3-4</valeur>\
<valeur>T5</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><libelle>Résistance renforcée aux agressions climatiques</libelle>\
<valeur num_crt="526">B :</valeur>\
<valeur num_crt="527">D :</valeur></champs>\
\	\	\	\	</formulaire>	true
141	302	Conduits de fumée	\N	<caracteristiques>\
\	\	\	\	\	<remarque>Ru= résistance utile de la paroi du conduit, c'est à direson isolation, sa résistance au passage de la chaleur / FC= résistance au feu de cheminée</remarque>\
\	\	\	\	\	<critere num="544">\
\	\	\	\	\	Type de conduit\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="545">\
\	\	\	\	\	Destination\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="546">\
\	\	\	\	\	Parois\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="547">\
\	\	\	\	\	Ø hydraulique (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="548">\
\	\	\	\	\	Dimensions transversales extérieures (cm x cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="549">\
\	\	\	\	\	Section intérieure (cm x cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="550">\
\	\	\	\	\	Hauteur utile (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="552">\
\	\	\	\	\	Classe de pression\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="551">\
\	\	\	\	\	Classe de température\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Classes de résistance à la</libelle>\
\	\	\	\	\	\	<critere num="553">Condensation</critere>\
\	\	\	\	\	\	<critere num="554">Corrosion</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="555">\
\	\	\	\	\	Résistant au feu de cheminée\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="556">\
\	\	\	\	\	Ru (m².°C/W)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="557">\
\	\	\	\	\	Spécificité\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="546" type="LST"><libelle>Type de Paroi :</libelle>\
<valeur>Alvéolée</valeur>\
<valeur>Pleine</valeur></champs>\
\	\	\	\	<champs num_crt="547" type="LST"><libelle>Diamètre hydraulique :</libelle>\
<valeur>16,0</valeur>\
<valeur>16,5</valeur>\
<valeur>18,0</valeur>\
<valeur>18,5 + 21,0</valeur>\
<valeur>19,0</valeur>\
<valeur>20,0</valeur>\
<valeur>20,0 + 17,5</valeur>\
<valeur>20,0 + 20,0</valeur>\
<valeur>21,0</valeur>\
<valeur>23,0</valeur>\
<valeur>24,0</valeur>\
<valeur>25,0</valeur>\
<valeur>26,0</valeur>\
<valeur>26,5</valeur>\
<valeur>27,0</valeur>\
<valeur>28,0</valeur>\
<valeur>29,5</valeur>\
<valeur>30,0</valeur>\
<valeur>34,0</valeur>\
<valeur>34,5</valeur>\
<valeur>35,0</valeur>\
<valeur>36,0</valeur>\
<valeur>37,0</valeur>\
<valeur>37,5</valeur></champs>\
\	\	\	\	<champs num_crt="551" type="LST"><libelle>Classe de température :</libelle>\
<valeur>T300</valeur>\
<valeur>T450</valeur></champs>\
\	\	\	\	<champs num_crt="555" type="LST"><libelle>Classe de résistance au feu de cheminée :</libelle>\
<valeur>O(100)</valeur>\
<valeur>G(100)</valeur></champs>\
\	\	\	\	</formulaire>	false
161	321	Dalles	\N	<caracteristiques>\
\	\	\	\	\	<critere num="504">\
\	\	\	\	\	Appellations commerciales\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="506">\
\	\	\	\	\	Classes d'appellation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance renforcée aux agressions climatiques</libelle>\
\	\	\	\	\	\	<critere num="508">B</critere>\
\	\	\	\	\	\	<critere num="509">D</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance à l'abrasion</libelle>\
\	\	\	\	\	\	<critere num="510">H</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="511">\
\	\	\	\	\	Dimensions de fabrication\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="506" type="LST"><libelle>Classe d'utilisation (fonction de l'intensité du trafic et de la charge par roue) :</libelle>\
<valeur>S4</valeur>\
<valeur>T7</valeur>\
<valeur>T11</valeur>\
<valeur>U14</valeur>\
<valeur>U25</valeur>\
<valeur>U30</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><libelle>Résistance renforcée aux agressions climatiques</libelle>\
<valeur num_crt="508">B :</valeur>\
<valeur num_crt="509">D :</valeur></champs>\
\	\	\	\	</formulaire>	true
265	421	Dallage pour sols intérieurs et extérieurs et abords de piscine	\N	<caracteristiques>\
\	\	\	\	\	<critere num="807">\
\	\	\	\	\	Famille d'aspect de surface\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="808">\
\	\	\	\	\	Appelation commerciale\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Classes</libelle>\
\	\	\	\	\	\	<critere num="810">d'épaisseur</critere>\
\	\	\	\	\	\	<critere num="813">d'emploi</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Résistance aux agressions</libelle>\
\	\	\	\	\	\	<critere num="814">climatiques (avec sels)</critere>\
\	\	\	\	\	\	<critere num="809">climatiques (sans sel)</critere>\
\	\	\	\	\	\	<critere num="815">chimiques</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="816">\
\	\	\	\	\	Résistance à l'abrasion\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="818">\
\	\	\	\	\	Résistance au poinçonnement\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
302	423	Eléments de structure linéaires en béton armé et béton précontraint	\N	<caracteristiques>\
\	\	\	\	\	<critere num="906">\
\	\	\	\	\	Type de produit\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="907">\
\	\	\	\	\	Profil\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="908">\
\	\	\	\	\	Béton armé ou précontraint\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="909">\
\	\	\	\	\	Dénomination commerciale\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Gamme de dimensions nominales (mm)</libelle>\
\	\	\	\	\	\	<critere num="910">Largeur hors-tout</critere>\
\	\	\	\	\	\	<critere num="911">Largeur âme</critere>\
\	\	\	\	\	\	<critere num="912">Hauteur</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="913">\
\	\	\	\	\	Résistance (N/mm²) garantie du béton à 28 jours\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="914">\
\	\	\	\	\	Durabilité : Classes d'exposition du béton\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="915">\
\	\	\	\	\	Parasismique\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
281	482	Séparateurs de boues et de liquides légers	\N	<caracteristiques>\
\	\	\	\	\	<critere num="828">\
\	\	\	\	\	Désignation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="829">\
\	\	\	\	\	Taille Nominale (TN)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="830">\
\	\	\	\	\	Classe d'efficacité\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="831">\
\	\	\	\	\	Dimensions (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="832">\
\	\	\	\	\	Dispositif de séparation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="833">\
\	\	\	\	\	Capacité de stockage des liquides légers (l)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="834">\
\	\	\	\	\	Capacité de stockage des boues (l)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="835">\
\	\	\	\	\	Catégorie d'utilisation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="836">\
\	\	\	\	\	Nature du matériau constitutif\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="837">\
\	\	\	\	\	Système de dérivation intégré\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
282	482	Séparateurs de boues et  de graisses	\N	<caracteristiques>\
\	\	\	\	\	<critere num="821">\
\	\	\	\	\	Désignation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="822">\
\	\	\	\	\	Taille Nominale (TN)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="823">\
\	\	\	\	\	Dimensions (cm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="824">\
\	\	\	\	\	Capacité de stockage des graisses (l)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="834">\
\	\	\	\	\	Capacité de stockage des boues (l)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="826">\
\	\	\	\	\	Catégorie d'utilisation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="827">\
\	\	\	\	\	Nature du matériau constitutif\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
402	571	Prédalles en béton armé et béton précontraint	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1042">\
\	\	\	\	\	Dénomination commerciale\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1043">\
\	\	\	\	\	Béton armé ou précontraint\
\	\	\	\	\	</critere>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Gamme de dimensions nominales (mm)</libelle>\
\	\	\	\	\	\	<critere num="1044">Epaisseur</critere>\
\	\	\	\	\	\	<critere num="1045">Largeur hors-tout</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<critere num="1046">\
\	\	\	\	\	Crantage surface supérieure\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1049">\
\	\	\	\	\	Parasismique\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
481	621	Blocs béton légers	\N	<caracteristiques>\
\	\	\	\	\	<critere num="147">\
\	\	\	\	\	Catégorie\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="155">\
\	\	\	\	\	Classe de résistance\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="131">\
\	\	\	\	\	Epaisseur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="132">\
\	\	\	\	\	Hauteur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="133">\
\	\	\	\	\	Longueur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="641">\
\	\	\	\	\	Tolérances dimensionnelles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="134">\
\	\	\	\	\	Nombre de Lames d'air\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="583">\
\	\	\	\	\	Mv bloc (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="585">\
\	\	\	\	\	Mv béton (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1406">\
\	\	\	\	\	Sismique\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1404">\
\	\	\	\	\	Lambda utile (W/m.K)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1405">\
\	\	\	\	\	Rth utile (m².K/W)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1581">\
\	\	\	\	\	NF Th\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="147" type="LST"><libelle>Catégorie</libelle>\
<valeur>CREUX</valeur>\
<valeur>PERFORE</valeur>\
<valeur>PLEIN</valeur></champs>\
\	\	\	\	<champs num_crt="155" type="LST-EXP"><libelle>Classe de résistance</libelle>\
<valeur>L25</valeur>\
<valeur>L30</valeur>\
<valeur>L35</valeur>\
<valeur>L40</valeur>\
<valeur>L45</valeur>\
<valeur>L50</valeur>\
<valeur>L60</valeur>\
<valeur>L70</valeur></champs>\
\	\	\	\	<champs num_crt="131" type="LST"><libelle>Epaisseur</libelle>\
<valeur>45</valeur>\
<valeur>50</valeur>\
<valeur>75</valeur>\
<valeur>100</valeur>\
<valeur>125</valeur>\
<valeur>150</valeur>\
<valeur>175</valeur>\
<valeur>200</valeur>\
<valeur>225</valeur>\
<valeur>250</valeur>\
<valeur>275</valeur>\
<valeur>300</valeur></champs>\
\	\	\	\	<champs num_crt="641" type="LST"><libelle>Catégorie de tolérances dimensionnelles</libelle>\
<valeur>D1</valeur>\
<valeur>D2</valeur>\
<valeur>D3</valeur>\
<valeur>D4</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="1406">Sismique</valeur></champs>\
\	\	\	\	<champs num_crt="0" type="CHK"><valeur num_crt="1581">NF Th</valeur></champs>\
\	\	\	\	</formulaire>	false
521	641	NF Escaliers 1	\N	<caracteristiques>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Modèle</libelle>\
\	\	\	\	\	\	<critere num="1081">Type</critere>\
\	\	\	\	\	\	<critere num="1082">Forme</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	\	<famille>\
\	\	\	\	\	\	<libelle>Dimensions maximales</libelle>\
\	\	\	\	\	\	<critere num="1083">Emmarchement (cm)</critere>\
\	\	\	\	\	\	<critere num="1084">Encombrement (cm)</critere>\
\	\	\	\	\	\	<critere num="1085">Hauteur à monter (cm)</critere>\
\	\	\	\	\	</famille>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="1081" type="LST"><libelle>Type :</libelle>\
<valeur>Monobloc</valeur>\
<valeur>A marche indépendante</valeur>\
<valeur>Plateau de marche indépendant</valeur></champs>\
\	\	\	\	<champs num_crt="1082" type="LST"><libelle>Forme :</libelle>\
<valeur>Droit</valeur>\
<valeur>Hélicoïdal</valeur>\
<valeur>Balancé avec fût ou mur</valeur>\
<valeur>Balancé sans fût ni mur</valeur>\
<valeur>Sur limon central ou crémaillère</valeur></champs>\
\	\	\	\	</formulaire>	false
601	701	Eléments architecturaux	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1201">\
\	\	\	\	\	Etat de surface\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1202">\
\	\	\	\	\	Résistance(s) caractéristique(s) garantie(s) à 28 jours (MPa)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1203">\
\	\	\	\	\	Durabilité : classe(s) d'exposition\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>	<formulaire>\
\	\	\	\	<champs num_crt="1201" type="LST"><libelle>Etat de surface :</libelle>\
<valeur>Acidé</valeur>\
<valeur>Bouchardé</valeur>\
<valeur>Brut démoulage immédiat</valeur>\
<valeur>Brut durci moule</valeur>\
<valeur>Désactivé</valeur>\
<valeur>Destiné à être peint ou revêtu</valeur>\
<valeur>Lisse</valeur>\
<valeur>Poli</valeur>\
<valeur>Revêtu</valeur>\
<valeur>Sablé</valeur>\
<valeur>Matricé</valeur>\
<valeur>Brut dessus de moule</valeur>\
<valeur>Dressé</valeur>\
<valeur>Taloché</valeur>\
<valeur>Lissé</valeur>\
<valeur>Feutré</valeur>\
<valeur>Avec empreinte</valeur>\
<valeur>Brossé</valeur>\
<valeur>Lavé</valeur>\
<valeur>Lavé fin</valeur>\
<valeur>Clivé</valeur>\
<valeur>Flammé</valeur>\
<valeur>Poncé</valeur>\
<valeur>Meulé</valeur>\
<valeur>Grésé</valeur>\
<valeur>Scié</valeur>\
<valeur>Hydrodécapé</valeur>\
<valeur>Grenaillé</valeur>\
<valeur>Vieilli</valeur>\
<valeur>Buriné</valeur></champs>\
\	\	\	\	</formulaire>	false
781	821	PTR_Tab_2	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1443">\
\	\	\	\	\	Désignation\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1445">\
\	\	\	\	\	Hauteur nominale h\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1446">\
\	\	\	\	\	Largeur talon (mm)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1447">\
\	\	\	\	\	Largeur feuillure\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1448">\
\	\	\	\	\	Hauteur feuillure\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1453">\
\	\	\	\	\	Rugosité\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1449">\
\	\	\	\	\	T5,2-2160-TBR\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1482">\
\	\	\	\	\	T6,85-2060-TBR\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1450">\
\	\	\	\	\	Raidisseur\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1451">\
\	\	\	\	\	Renfort\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1441">\
\	\	\	\	\	Rc à 28 jours (MPa)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1442">\
\	\	\	\	\	Classes exposition\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1486">\
\	\	\	\	\	Mrk\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1487">\
\	\	\	\	\	Mrd\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
821	881	Blocs de coffrage	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1521">\
\	\	\	\	\	Classe de résistance\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1541">\
\	\	\	\	\	Long.\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1542">\
\	\	\	\	\	Larg.\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1543">\
\	\	\	\	\	Haut.\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1561">\
\	\	\	\	\	Sismique\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1544">\
\	\	\	\	\	Tolérances dimensionnelles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1545">\
\	\	\	\	\	Nbre alvéoles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1546">\
\	\	\	\	\	Long. Alvéoles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1547">\
\	\	\	\	\	Larg. Alvéoles\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1548">\
\	\	\	\	\	Surface évidement (mm²)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1549">\
\	\	\	\	\	Epaiss. Entretoise\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1550">\
\	\	\	\	\	Epaiss. Parois\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1551">\
\	\	\	\	\	Mv béton (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1552">\
\	\	\	\	\	Mv bloc (kg/m³)\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
841	901	EMS_Tab_1	\N	<caracteristiques>\
\	\	\	\	\	<critere num="1661">\
\	\	\	\	\	Gamme\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1681">\
\	\	\	\	\	Modèle ou libellé\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1662">\
\	\	\	\	\	Profil\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1663">\
\	\	\	\	\	Hauteur élément standard (m)\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1664">\
\	\	\	\	\	Longueur élément standard\
\	\	\	\	\	</critere>\
\	\	\	\	\	<critere num="1665">\
\	\	\	\	\	Longueur semelle élément standard (m)\
\	\	\	\	\	</critere>\
\	\	\	\	</caracteristiques>		false
