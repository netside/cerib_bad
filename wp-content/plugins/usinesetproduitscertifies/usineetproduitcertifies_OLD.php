<?php
/*
Plugin Name: Usines et Produits Certifies
Plugin URI: http://tilbury-consulting.com/
Description: Plugin permettant la gestion des Usines et Produits certifies du site du CERIB
Version: 1.0.0
Author: David Tea
Author URI: http://tilbury-consulting.com/
License: GPL2
*/

@ini_set('post_max_size', '1000M');
@ini_set('upload_max_filesize', '1000M');
@ini_set( 'max_execution_time', '300' );
@ini_set('allow_url_include', '1');

error_reporting(-1);

if( !class_exists('uepc') ):

class Uepc
{
    var $process;
    var $tempDir;
    var $backupDir;

    /*
     * constructor
     *
     * This function will construct all the neccessary
     *
     * @version 1.0.0
     */
    function __construct()
    {
        $this->tempDir = WP_CONTENT_DIR . "/ftpimport/";
        $this->backupDir = dirname(__FILE__) . "/backup/";
        $this->pdfDir = WP_CONTENT_DIR . "/pdf/";

        add_action('init', array($this, 'init'), 1);
    }

    /*
     * init
     *
     * Initialisation du Plugin
     *
     */
    function init()
    {
        //Custom Post Type
        $arrayCPT = array(
            'name' => 'Certification NF',
            'singular_name' => 'Certification NF'
        );

        register_post_type('certification',
            array(
                'labels' => $arrayCPT,
                'public' => true,
                'has_archive' => true
            ));

        $arrayCPT = array(
            'name' => 'Marquage CE',
            'singular_name' => 'Marquage CE'
        );

        register_post_type('marquage',
            array(
                'labels' => $arrayCPT,
                'public' => true,
                'has_archive' => true
            ));

        register_activation_hook(__FILE__, array($this, 'sql_request'));

        add_action('admin_menu', array($this, 'admin_menu'));

        add_filter( 'wp_mail_content_type', array($this, 'set_content_type'));
        //add_action( 'wp_ajax_update_database', array($this, 'update_database'));
        add_action( 'wp_ajax_restore_db', array($this, 'restore_db'));

        //TEST
        add_action('wp_ajax_update_database_v2', array($this, 'update_database_v2'));
    }

    /*
     * sql_request
     *
     * Create Table on activation
     */
    function sql_request()
    {
        //SQL
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        //Table upec_notif
        $table_name = $wpdb->prefix . "uepc_notif";
        $sql_notif = "CREATE TABLE $table_name (
          id_notif mediumint(9) NOT NULL AUTO_INCREMENT,
          type_produit VARCHAR(2) DEFAULT '' NOT NULL,
          mail text NOT NULL,
          UNIQUE KEY id (id_notif)
        ) $charset_collate;";

        //Table upec_histo
        $table_name = $wpdb->prefix . "uepc_histo";
        $sql_histo = "CREATE TABLE $table_name (
          id_histo mediumint(9) NOT NULL AUTO_INCREMENT,
          type_produit VARCHAR(2) DEFAULT '' NOT NULL,
          status tinyint(1) NOT NULL,
          author VARCHAR(100) DEFAULT '' NOT NULL,
          date datetime NOT NULL,
          text text DEFAULT NULL,
          isActive tinyint(1) NOT NULL,
          urlzip VARCHAR(100) NOT NULL,
          UNIQUE KEY id (id_histo)
        ) $charset_collate";

        //Table Jointure uepc_post_type_id
        $table_name = $wpdb->prefix . "uepc_post_type_id";
        $sql_post_type = "CREATE TABLE $table_name(
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          type_produit VARCHAR(2) DEFAULT '' NOT NULL,
          id_certif mediumint(9) NOT NULL,
          id_post_type mediumint(9) NOT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate";

        //Tables Qualitix
        //qlx_produits
        $table_name = "qlx_produits";
        $sql_produits = "CREATE TABLE $table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          ref_prod_cerib VARCHAR(255) DEFAULT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate";

        //qlx_valeurs
        $table_name = "qlx_valeurs";
        $sql_valeurs = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          id_produits int(10) NOT NULL DEFAULT 0,
          num_crt int(10) DEFAULT NULL,
          valeur VARCHAR(255) DEFAULT NULL,
          UNIQUE KEY id(id)
        ) $charset_collate";

        //qlx_nn_etablissements_produits_certif
        $table_name = "qlx_nn_etablissements_produits_certif";
        $sql_nn_etablissements = "CREATE TABLE $table_name (
          id_produits int(10) NOT NULL,
          id_etablissements int(10) NOT NULL,
          id_certif int(10) NOT NULL,
          num_lng int(10) NOT NULL,
          filename VARCHAR(255) DEFAULT '' NOT NULL,
          fdes tinyint(1) NOT NULL
        ) $charset_collate";

        //qlx_entreprises
        $table_name = "qlx_entreprises";
        $sql_entreprises = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          nom VARCHAR(255) DEFAULT NULL,
          adr_1 VARCHAR(255) DEFAULT NULL,
          adr_2 VARCHAR(255) DEFAULT NULL,
          adr_3 VARCHAR(255) DEFAULT NULL,
          cp VARCHAR(12) DEFAULT NULL,
          ville VARCHAR(255) DEFAULT NULL,
          pays VARCHAR(255) DEFAULT NULL,
          tel VARCHAR(32) DEFAULT NULL,
          fax VARCHAR(32) DEFAULT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate";

        //qlx_etablissements
        $table_name = "qlx_etablissements";
        $sql_etablissements = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          id_entreprises int(10) DEFAULT 0 NOT NULL,
          nom VARCHAR(255) DEFAULT NULL,
          adr_1 VARCHAR(255) DEFAULT NULL,
          adr_2 VARCHAR(255) DEFAULT NULL,
          adr_3 VARCHAR(255) DEFAULT NULL,
          cp VARCHAR(12) DEFAULT NULL,
          ville VARCHAR(255) DEFAULT NULL,
          pays VARCHAR(255) DEFAULT NULL,
          tel VARCHAR(32) DEFAULT NULL,
          fax VARCHAR(32) DEFAULT NULL,
          web VARCHAR(255) DEFAULT NULL,
          logo ENUM('true', 'false') NOT NULL DEFAULT 'false',
          UNIQUE KEY id(id)
        ) $charset_collate";

        //qlx_famille_certif
        $table_name = "qlx_famille_certif";
        $sql_famille_certif = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          nom VARCHAR(255) DEFAULT NULL,
          numero VARCHAR(32) DEFAULT NULL,
          date_maj int(11) NOT NULL DEFAULT 0,
          UNIQUE KEY id(id)
        ) $charset_collate";

        //qlx_certif
        $table_name = "qlx_certif";
        $sql_certif = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          id_famille_certif int(10) NOT NULL DEFAULT 0,
          nom VARCHAR(255) DEFAULT NULL,
          description VARCHAR(255),
          caracteristiques_xml text DEFAULT NULL,
          formulaire_xml text DEFAULT NULL,
          logo enum('true', 'false') NOT NULL DEFAULT 'false',
          UNIQUE KEY id(id)
        ) $charset_collate";

        //marquagece_entreprises
        $table_name = "marquagece_entreprises";
        $sql_ce_entreprises = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          nom VARCHAR(255) DEFAULT NULL,
          adr_1 VARCHAR(255) DEFAULT NULL,
          adr_2 VARCHAR(255) DEFAULT NULL,
          adr_3 VARCHAR(255) DEFAULT NULL,
          cp VARCHAR(12) DEFAULT NULL,
          ville VARCHAR(255) DEFAULT NULL,
          pays VARCHAR(255) DEFAULT NULL,
          tel VARCHAR(32) DEFAULT NULL,
          fax VARCHAR(32) DEFAULT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate";

        //marquagece_etablissements
        $table_name = "marquagece_etablissements";
        $sql_ce_etablissements = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          id_entreprises int(10) DEFAULT 0 NOT NULL,
          nom VARCHAR(255) DEFAULT NULL,
          adr_1 VARCHAR(255) DEFAULT NULL,
          adr_2 VARCHAR(255) DEFAULT NULL,
          adr_3 VARCHAR(255) DEFAULT NULL,
          cp VARCHAR(12) DEFAULT NULL,
          ville VARCHAR(255) DEFAULT NULL,
          pays VARCHAR(255) DEFAULT NULL,
          tel VARCHAR(32) DEFAULT NULL,
          fax VARCHAR(32) DEFAULT NULL,
          web VARCHAR(255) DEFAULT NULL,
          logo ENUM('true', 'false') NOT NULL DEFAULT 'false',
          UNIQUE KEY id(id)
        ) $charset_collate";

        //marquagece_produits
        $table_name = "marquagece_produits";
        $sql_ce_produits = "CREATE TABLE $table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          ref_prod_cerib VARCHAR(255) DEFAULT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate";

        //marquagece_nn_etablissements_produits_certif
        $table_name = "marquagece_nn_etablissements_produits_certif";
        $sql_ce_nn_etablissements = "CREATE TABLE $table_name (
          id_produits int(10) NOT NULL,
          id_etablissements int(10) NOT NULL,
          id_certif int(10) NOT NULL,
          filename VARCHAR(255) DEFAULT '' NOT NULL
        ) $charset_collate";

        //marquagece_famille_produit
        $table_name = "marquagece_famille_produit";
        $sql_ce_famille_produit = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          nom VARCHAR(255) DEFAULT NULL,
          UNIQUE KEY id(id)
        ) $charset_collate";

        //marquagece_certif
        $table_name = "marquagece_certif";
        $sql_ce_certif = "CREATE TABLE $table_name (
          id int(10) NOT NULL AUTO_INCREMENT,
          id_famille_produit int(10) NOT NULL DEFAULT 0,
          nom VARCHAR(255) DEFAULT NULL,
          UNIQUE KEY id(id)
        ) $charset_collate";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        dbDelta($sql_notif);
        dbDelta($sql_histo);
        dbDelta($sql_post_type);
        dbDelta($sql_produits);
        dbDelta($sql_valeurs);
        dbDelta($sql_nn_etablissements);
        dbDelta($sql_entreprises);
        dbDelta($sql_etablissements);
        dbDelta($sql_famille_certif);
        dbDelta($sql_certif);

        dbDelta($sql_ce_entreprises);
        dbDelta($sql_ce_etablissements);
        dbDelta($sql_ce_produits);
        dbDelta($sql_ce_nn_etablissements);
        dbDelta($sql_ce_famille_produit);
        dbDelta($sql_ce_certif);

        //Insertion Data
        global $wpdb;
        $tableNotifName = $wpdb->prefix . "uepc_notif";
        $wpdb->query("INSERT INTO $tableNotifName VALUES (1, 'nf', '')");
        $wpdb->query("INSERT INTO $tableNotifName VALUES (2, 'ce', '')");
    }

    /*
     * admin_menu
     *
     * Ajout Menu sur le panneau latéral à gauche
     *
     */
    function admin_menu()
    {
        $menu = add_menu_page('Usines et produits certifiés', 'Usines et produits certifiés', 'manage_options', 'uepc', array($this, 'admin_page'), false, '6');
        $submenu = add_submenu_page('uepc', '[NF] Journal des mises à jour', '[NF] Journal des mises à jour', 'manage_options', 'nf-journal', array($this, 'nf_journal_page'));
        $submenu2 = add_submenu_page('uepc', '[CE] Journal des mises à jour', '[CE] Journal des mises à jour', 'manage_options', 'ce-journal', array($this, 'ce_journal_page'));
        $submenu3 = add_submenu_page('uepc', 'Liste des pages produits', 'Liste des pages produits', 'manage_options', 'liste-produit', array($this, 'liste_produit_page'));

        add_action( "admin_print_styles-$menu", array($this, 'init_styles_script') );
        add_action( "admin_print_styles-$submenu", array($this, 'init_styles_script') );
        add_action( "admin_print_styles-$submenu2", array($this, 'init_styles_script') );
        add_action( "admin_print_styles-$submenu3", array($this, 'init_styles_script') );
        add_filter( 'auth_cookie_expiration', array($this, 'keep_me_logged_in_for_1_year'));
    }

    function keep_me_logged_in_for_1_year( $expirein )
    {
        return 31556926; // 1 year in seconds
    }

    /*
     * admin_page
     *
     * Rendu page plugin
     *
     */
    function admin_page()
    {
        global $wpdb;
        $wpdb->show_errors();

        $tableNotifName = $wpdb->prefix . "uepc_notif";
        $tableHistoName = $wpdb->prefix . "uepc_histo";

        //Récupération date dernière mise à jour
        $queryLastUpdateNF = $wpdb->get_row("SELECT date FROM $tableHistoName WHERE status = 1 AND type_produit = 'nf' ORDER BY date DESC LIMIT 1");
        $queryLastUpdateCE = $wpdb->get_row("SELECT date FROM $tableHistoName WHERE status = 1 AND type_produit = 'ce' ORDER BY date DESC LIMIT 1");

        if(!is_null($queryLastUpdateNF))
            $queryLastUpdateNF->date = new DateTime($queryLastUpdateNF->date);

        if(!is_null($queryLastUpdateCE))
            $queryLastUpdateCE->date = new DateTime($queryLastUpdateCE->date);

        //Sauvegarde Liste Mail
        if(isset($_POST) && !empty($_POST))
        {
            $type = $_POST['type'];
            $mailList = $_POST['mail-list'];
            $action = $_POST['action'];
			
			//var_dump($_POST);

            if($_POST['action'] == 'update-database')
            {
                $resultUpdate = $this->update_database_v2();
            }

            if($_POST['action'] == 'update-mail')
            {
                $queryUpdate = $wpdb->query("UPDATE $tableNotifName SET mail = '$mailList' WHERE type_produit = '$type'");
            }
        }

        $mailListNF = $wpdb->get_results("SELECT * FROM $tableNotifName WHERE type_produit = 'nf'");
        $mailListCE = $wpdb->get_results("SELECT * FROM $tableNotifName WHERE type_produit = 'ce'");

        ?>

        <header>
            <h1>Usines et produits certifiés</h1>
        </header>

        <div class="row">
            <div class="small-8 small-offset-2 columns">
                <?php if(!empty($_POST) && $_POST['action'] == 'update-mail' && $queryUpdate == 1): ?>
                    <div data-alert class="alert-box success radius">
                        Personne(s) à notifier enregistrés
                        <a href="#" class="close">&times;</a>
                    </div>
                <?php endif; ?>

                <?php if(!empty($_POST) && $_POST['action'] == 'update-database'): ?>
                    <div data-alert class="alert-box secondary radius">
                        <?php echo $resultUpdate; ?>
                        <a href="#" class="close">&times;</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <body id="admin_page">
            <div class="row">
                <div class="small-8 small-offset-2 columns">
                    <ul class="tabs" data-tab>
                        <li class="tab-title small-6 active"><a href="#panel-nf">NF et Qualif-IB</a></li>
                        <li class="tab-title small-6"><a href="#panel-ce">Marquage CE</a></li>
                    </ul>

                    <div class="tabs-content">
                        <div class="content active" id="panel-nf">
                            <p>Personne(s) à notifier une fois les mises à jour effectuées, insérez une adresse email par ligne :</p>
                            <form method="post" action="#">
                                <textarea class="small-8 small-offset-2 ta-mail" name="mail-list" rows="10"><?php foreach($mailListNF as $mail){echo $mail->mail;} ?></textarea>
                                <input type="hidden" name="type" value="nf">
                                <input type="hidden" name="action" value="update-mail">
                                <button type="submit" class="small-8 small-offset-2">Enregistrer</button>
                            </form>
                            <button class="small-8 small-offset-2" data-reveal-id="nf-update-db">Mettre à jour la base</button>
                            <button class="small-8 small-offset-2" data-reveal-id="nf-restore-last-db">Restaurer la version précédente de la base</button>
                            <p class="small-8 small-offset-2">Dernière mise à jour : <?php if(is_null($queryLastUpdateNF)) echo '-'; else echo $queryLastUpdateNF->date->format('d/m/Y H:i:s'); ?></p>
                            <a class="small-8 small-offset-2 link-btn" href="<?php menu_page_url('nf-journal'); ?>">Journal des mises à jour</a>
                            <a class="small-8 small-offset-2 link-btn" href="<?php menu_page_url('liste-produit'); ?>&type=nf">Liste des pages produits</a>
                        </div>
                        <div class="content" id="panel-ce">
                            <p>Personne(s) à notifier une fois les mises à jour effectuées, insérez une adresse email par ligne :</p>
                            <form method="post" action="#">
                                <textarea class="small-8 small-offset-2 ta-mail" name="mail-list" rows="10"><?php foreach($mailListCE as $mail){echo $mail->mail;} ?></textarea>
                                <input type="hidden" name="type" value="ce">
                                <input type="hidden" name="action" value="update-mail">
                                <button type="submit" class="small-8 small-offset-2">Enregistrer</button>
                            </form>
                            <button class="small-8 small-offset-2" data-reveal-id="ce-update-db">Mettre à jour la base</button>
                            <button class="small-8 small-offset-2" data-reveal-id="ce-restore-last-db">Restaurer la version précédente de la base</button>
                            <p class="small-8 small-offset-2">Dernière mise à jour : <?php if(is_null($queryLastUpdateCE)) echo '-'; else echo $queryLastUpdateCE->date->format('d/m/Y H:i:s'); ?></p>
                            <a class="small-8 small-offset-2 link-btn" href="<?php menu_page_url('ce-journal'); ?>">Journal des mises à jour</a>
                            <a class="small-8 small-offset-2 link-btn" href="<?php menu_page_url('liste-produit'); ?>&type=ce">Liste des pages produits</a>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        <!--Modales -->
        <div id="nf-update-db" class="reveal-modal" data-reveal aria-labelledby="nf-update-db" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <h2>[NF et Qualif-IB] Mise à jour de la base</h2>
            <p>Assurez vous d'avoir déposé les fichiers suivants :</p>
            <ul>
                <li>declinaisons.xml</li>
                <li>usines.xml</li>
                <li>certifications.xml</li>
                <li>pdf_certificat.zip</li>
            </ul>
            <form id="nf-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="type" value="nf">
                <input type="hidden" name="action" value="update-database">
                <button class="small-4 small-offset-2 cancel-button close-modal">Annuler</button>
                <button type="submit" class="small-4 submitUpdate">Mettre à jour</button>
            </form>
            <div class="result">
                <div class="loader">
                    Mise à jour en cours, veuillez patienter, cela peut prendre quelques minutes. <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <pre>
                </pre>
            </div>
        </div>

        <div id="ce-update-db" class="reveal-modal" data-reveal aria-labelledby="ce-update-db" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <h2>[Marquage CE] Mise à jour de la base</h2>
            <p>Assurez vous d'avoir déposé les fichiers suivants :</p>
            <ul>
                <li>declinaisons_ce.xml</li>
                <li>usines_ce.xml</li>
                <li>marques_ce.xml</li>
                <li>pdf_marquage_ce.zip</li>
            </ul>
            <form id="ce-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="type" value="ce">
                <input type="hidden" name="action" value="update-database">
                <button class="small-4 small-offset-2 cancel-button close-modal">Annuler</button>
                <button type="submit" class="small-4 submitUpdate">Mettre à jour</button>
            </form>
            <div class="result">
                <div class="loader">
                    Mise à jour en cours, veuillez patienter, cela peut prendre quelques minutes. <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <pre>
                </pre>
            </div>
        </div>

        <div id="nf-restore-last-db" class="reveal-modal" data-reveal aria-labelledby="restore-last-db" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <h2>[NF et Qualif-IB] Restaurer la version précédente de la base</h2>
            <p><i class="fa fa-exclamation-triangle"></i> Restaurer la version précédente ? Date de la dernière mise à jour : <?php if(is_null($queryLastUpdateNF)) echo '-'; else echo $queryLastUpdateNF->date->format('d/m/Y H:i:s'); ?></p>
            <form id="nf-restore" method="post">
                <button class="small-4 small-offset-2 cancel-button close-modal">Non</button>
                <button type="submit" class="small-4 submitUpdate">Restaurer</button>
            </form>
            <div class="result">
                <div class="loader">
                    Restauration en cours, veuillez patienter, cela peut prendre quelques minutes. <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <pre>
                </pre>
            </div>
        </div>

        <div id="ce-restore-last-db" class="reveal-modal" data-reveal aria-labelledby="restore-last-db" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <h2>[Marquage CE] Restaurer la version précédente de la base</h2>
            <p><i class="fa fa-exclamation-triangle"></i> Restaurer la version précédente ? Date de la dernière mise à jour : <?php if(is_null($queryLastUpdateCE)) echo '-'; else echo $queryLastUpdateCE->date->format('d/m/Y H:i:s'); ?></p>
            <form id="ce-restore" method="post">
                <button class="small-4 small-offset-2 cancel-button close-modal">Non</button>
                <button type="submit" class="small-4 submitUpdate">Restaurer</button>
            </form>
            <div class="result">
                <div class="loader">
                    Restauration en cours, veuillez patienter, cela peut prendre quelques minutes. <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <pre>
                </pre>
            </div>
        </div>

        <?php
    }

    /*
     * nf_journal_page
     *
     * Rendu Page Journal [NF]
     *
     */
    function nf_journal_page()
    {
        global $wpdb;

        $tableHistoName = $wpdb->prefix . "uepc_histo";

        if(isset($_POST) && !empty($_POST))
        {
            $queryUpdate = $wpdb->query("UPDATE $tableHistoName SET isActive = 0 WHERE type_produit = 'nf'");
        }

        $historyCE = $wpdb->get_results("SELECT * FROM $tableHistoName WHERE type_produit = 'nf' AND isActive = 1 ORDER BY id_histo DESC");

        ?>

        <header>
            <h1>Usines et produits certifiés - Journal des mises à jour NF et Qualif-IB</h1>
        </header>

        <div class="row">
            <div class="small-8 small-offset-2 columns">
                <?php if($queryUpdate == 1): ?>
                    <div data-alert class="alert-box success radius">
                        Journal Vidé avec succès.
                        <a href="#" class="close">&times;</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <body id="nf_journal_page">
            <div class="row">
                <div class="small-8 small-offset-2 columns">
                    <table>
                        <tr class="row">
                            <th class="small-2">Date</th>
                            <th class="small-1">Statut</th>
                            <th class="small-2">Auteur</th>
                            <th class="small-7">Détail du log</th>
                        </tr>
                        <?php foreach($historyCE as $rowHistory):

                            $rowHistory->date = new DateTime($rowHistory->date);

                            ?>
                            <tr class="row">
                                <td><?php echo $rowHistory->date->format('d/m/Y H:i:s'); ?></td>
                                <td><?php if($rowHistory->status == 1): ?><i style="color: green" class="fa fa-2x fa-check"></i><?php else: ?><i style="color: red" class="fa fa-2x fa-close"></i><?php endif; ?></td>
                                <td><?php echo $rowHistory->author; ?></td>
                                <td class="log"><?php echo $rowHistory->text; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="small-8 small-offset-2 columns">
                    <form id="nf-clear-journal" method="post" action="#">
                        <input type="hidden" name="action" value="clear">
                        <button type="submit" class="small-8 small-offset-2">Vider le journal</button>
                    </form>
                </div>
            </div>
        </body>

        <?php
    }

    /*
     * ce_journal_page
     *
     * Rendu page journal [CE]
     *
     */
    function ce_journal_page()
    {
        global $wpdb;

        $tableHistoName = $wpdb->prefix . "uepc_histo";

        if(isset($_POST) && !empty($_POST))
        {
            $queryUpdate = $wpdb->query("UPDATE $tableHistoName SET isActive = 0 WHERE type_produit = 'ce'");
        }

        $historyCE = $wpdb->get_results("SELECT * FROM $tableHistoName WHERE type_produit = 'ce' AND isActive = 1 ORDER BY id_histo DESC");

        ?>

        <header>
            <h1>Usines et produits certifiés - Journal des mises à jour Marquage CE</h1>
        </header>

        <div class="row">
            <div class="small-8 small-offset-2 columns">
                <?php if($queryUpdate == 1): ?>
                    <div data-alert class="alert-box success radius">
                        Journal Vidé avec succès.
                        <a href="#" class="close">&times;</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <body id="ce_journal_page">
            <div class="row">
                <div class="small-8 small-offset-2 columns">
                    <table>
                        <tr class="row">
                            <th class="small-2">Date</th>
                            <th class="small-1">Statut</th>
                            <th class="small-2">Auteur</th>
                            <th class="small-7">Détail du log</th>
                        </tr>
                        <?php foreach($historyCE as $rowHistory):

                            $rowHistory->date = new DateTime($rowHistory->date);

                            ?>
                            <tr class="row">
                                <td><?php echo $rowHistory->date->format('d/m/Y H:i:s'); ?></td>
                                <td><?php if($rowHistory->status == 1): ?><i style="color: green" class="fa fa-2x fa-check"></i><?php else: ?><i style="color: red" class="fa fa-2x fa-close"></i><?php endif; ?></td>
                                <td><?php echo $rowHistory->author; ?></td>
                                <td class="log"><?php echo $rowHistory->text; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="small-8 small-offset-2 columns">
                    <form id="ce-clear-journal" method="post" action="#">
                        <input type="hidden" name="action" value="clear">
                        <button type="submit" class="small-8 small-offset-2">Vider le journal</button>
                    </form>
                </div>
            </div>
        </body>

        <?php
    }

    /*
     * liste_produit_page
     *
     * Rendu liste page produit
     *
     */
    function liste_produit_page()
    {
        $type = $_GET['type'];

        $args = array('post_type' => 'certification', 'posts_per_page' => -1);
        $loopNF = new WP_Query($args);

        $args = array('post_type' => 'marquage', 'posts_per_page' => -1);
        $loopCE = new WP_Query($args);

        global $post;
        global $wpdb;

        $tableHistoName = $wpdb->prefix . "uepc_histo";

        $historyCE = $wpdb->get_results("SELECT * FROM $tableHistoName WHERE type_produit = 'nf' AND isActive = 1 ORDER BY id_histo DESC");

        $lastUpdate = $historyCE[0];

        ?>

        <header>
            <h1>Usines et produits certifiés - Page Produits</h1>
        </header>

        <body id="admin_page">
        <div class="row">
            <div class="small-8 small-offset-2 columns">
                <ul class="tabs" data-tab>
                    <li class="tab-title small-6 <?php if($type == 'nf' || is_null($type)): ?>active<?php endif; ?>"><a href="#panel-product-nf">NF et Qualif-IB</a></li>
                    <li class="tab-title small-6 <?php if($type == 'ce'): ?>active<?php endif; ?>"><a href="#panel-product-ce">Marquage CE</a></li>
                </ul>

                <div class="tabs-content">
                    <div class="content active" id="panel-product-nf">
                        <table>
                            <tr class="row">
                                <th class="small-4">Nom Produit</th>
                                <th class="small-1">ID Page</th>
                                <th class="small-6">URL Page</th>
                                <th class="small-1">ID Produit</th>
                            </tr>
                            <?php while($loopNF->have_posts()): $loopNF->the_post(); ?>
                            <tr class="row">
                                <td class="small-4"><?php the_title(); ?></td>
                                <td class="small-1"><?php the_ID(); ?></td>
                                <td class="small-6"><?php the_permalink(); ?></td>
                                <td class="small-1"><?php echo get_post_meta($post->ID, '_product', true) ?></td>
                            </tr>
                            <?php endwhile; ?>
                        </table>
                        <a class="small-4 link-btn" href="<?php echo WP_CONTENT_URL.'/pdfgen/'.$lastUpdate->urlzip;  ?>">Télécharger les PDF</a>
                    </div>
                    <div class="content" id="panel-product-ce">
                        <table>
                            <tr class="row">
                                <th class="small-4">Nom Produit</th>
                                <th class="small-1">ID Page</th>
                                <th class="small-6">URL Page</th>
                                <th class="small-1">ID Produit</th>
                            </tr>
                            <?php while($loopCE->have_posts()): $loopCE->the_post(); ?>
                                <tr class="row">
                                    <td class="small-4"><?php the_title(); ?></td>
                                    <td class="small-1"><?php the_ID(); ?></td>
                                    <td class="small-6"><?php the_permalink(); ?></td>
                                    <td class="small-1"><?php echo get_post_meta($post->ID, '_product', true); ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </body>

        <?php
    }

    /*
     * init_styles_script
     *
     * Initialisation styles CSS et scripts JS
     *
     */
    function init_styles_script()
    {
        wp_enqueue_style('foundation-css', plugin_dir_url(__FILE__).'/library/foundation-5.5.2/css/foundation.min.css');
        wp_enqueue_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
        wp_enqueue_style('css', plugin_dir_url(__FILE__).'/library/style.css');

        wp_enqueue_script('jquery-js', plugin_dir_url(__FILE__).'/library/foundation-5.5.2/js/vendor/jquery.js');
        wp_enqueue_script('modernizr-js', plugin_dir_url(__FILE__).'/library/foundation-5.5.2/js/vendor/modernizr.js');
        wp_enqueue_script('foundation-js', plugin_dir_url(__FILE__).'/library/foundation-5.5.2/js/foundation.min.js');
        wp_enqueue_script('main-js', plugin_dir_url(__FILE__).'/library/main.js');
        wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );

        require_once (dirname(__FILE__).'/library/domxml-php4-to-php5.php');
        require_once (dirname(__FILE__).'/library/htmlheader.php');
        require_once (dirname(__FILE__).'/library/xml.inc.php');
        //require_once (dirname(__FILE__).'/library/fpdf17/fpdf.php');
        require_once (dirname(__FILE__).'/library/fpdf/fpdf.php');
        require_once (dirname(__FILE__).'/library/fpdf/html2pdf.php');
        require_once (dirname(__FILE__).'/library/pdf_extend.php');
        require_once (dirname(__FILE__).'/library/pdfgen.php');
    }

    /*
     * set_content_type
     *
     * Utilisation HTML wp_mail
     */
    function set_content_type( $content_type )
    {
        return 'text/html';
    }

    /*
     * update_database_v2
     */
    function update_database_v2() {

        global $wpdb;

        $wpdb->show_errors();

        $type = $_POST['type'];

        if($type == 'nf')
            $arrayFileName = array('declinaisons.xml', 'usines.xml', 'certifications.xml', 'pdf_certificat.zip');
        else if($type == 'ce')
            $arrayFileName = array('declinaisons_ce.xml', 'usines_ce.xml', 'marques_ce.xml', 'pdf_marquage_ce.zip');

        $this->process = '<ul>';

        $backup_state = true;
        $process_state = true;
        $xml_state = true;

        if($this->backup_database($type) == true)
        {
            //Vider les tables
            $this->truncate_table($type);

            //UTF-7 to UTF-8
            /*if ($handle = opendir($this->tempDir)) {
                while (($fileName = readdir($handle)) !== false)
                    if ($fileName != '.' && $fileName != '..') {

                        //Check if xml
                        $info = pathinfo($this->tempDir . $fileName);
                        if ($info["extension"] == "xml") {
                            $array = file($this->tempDir . $fileName);
                            $array[0] = '<?xml version="1.0" encoding="UTF-8" ?>';
                            file_put_contents($this->tempDir . $fileName, implode($array));
                        }
                    }
            }*/

            foreach($arrayFileName as $fileName)
            {
                if(!file_exists($this->tempDir . $fileName))
                {
                    $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier non trouvé</li>';
                    $this->rollback_database($type);
                    $xml_state = false;
                    break;
                }
                else
                {
                    $info = pathinfo($this->tempDir . $fileName);
                    if ($info["extension"] == "xml") {
                        $array = file($this->tempDir . $fileName);

                        $array[0] = '<?xml version="1.0" encoding="UTF-8" ?>';
                        file_put_contents($this->tempDir . $fileName, implode($array));
                    }
                }
            }

            if($xml_state == true)
                if($this->extract_pdf($type) == true)
                    if ($this->process_usines($type) == true)
                        if ($this->process_declinaisons($type) == true)
                            if ($this->process_certifications_marques($type) == true) {

                            } else
                                $process_state = false;
                        else
                            $process_state = false;
                    else
                        $process_state = false;
                else
                    $process_state = false;
        }
        else
            $backup_state = false;

        $this->process .= '</ul>';

        //Insertion dans le journal des MAJ
        $status = 1;
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Paris'));
        $date = $date->format("Y-m-d H:i:s");

        global $current_user;
        get_currentuserinfo();
        $user = $current_user->display_name;

        //$date = new DateTime();
        //$fileZIPName = 'NF_'.$date->format('dMy_His').'.zip';

        $urlZIP = '';

        if($process_state == true && $backup_state == true && $xml_state == true)
            if($type == 'nf')
                $urlZIP = generate_pdf_nf();

        //BUGFIX idCertif = 1
        /*if($type = 'nf')
        {
            $query = 'UPDATE qlx_certif SET id=864 WHERE id=1';
            $wpdb->query($query);

            $query = 'UPDATE qlx_nn_etablissements_produits_certif SET id_certif=864 WHERE id_certif=1';
            $wpdb->query($query);
        }*/

        if($process_state == false || $backup_state == false || $xml_state == false)
            $status = 0;

        $tableHistoName = $wpdb->prefix . "uepc_histo";

        $wpdb->show_errors();

        $preparedQuery = $wpdb->prepare("INSERT INTO ".$tableHistoName." (type_produit, status, author, date, text, isActive, urlzip) VALUES(%s, %d, %s, '$date', %s, 1, %s)", $type, $status, $user, $this->process, $urlZIP);
        $wpdb->query($preparedQuery);


        //Envoi de Mail
        $tableNotifName = $wpdb->prefix . "uepc_notif";
        $mailList = $wpdb->get_row("SELECT * FROM $tableNotifName WHERE type_produit = '$type'");
        $mailTo = explode("\r\n", $mailList->mail);
        $mailSubject = "[Usines et Produits Certifiés] Mise à jour de la base";

        if($type == 'nf')
            $mailContent = "Type : Mise à jour des listes usines (marque NF)<br>";
        else if($type == 'ce')
            $mailContent = "Type : Mise à jour des listes usines (marquage CE)<br>";

        $mailContent .= "Date : " . $date . "<br>";
        $mailContent .= $this->process;

        $mailSend = wp_mail($mailTo, $mailSubject, $mailContent);


        //Suprimmer les fichiers temporaires
        foreach ($arrayFileName as $fileName)
            unlink($this->tempDir . $fileName);

        return $this->process;

        //die();
    }

    /*
     * update_database
     *
     * Mise à jour de la Base de Données
     *
     */
    /*function update_database() {

        global $wpdb;

        $type = $_POST['type'];

        $this->process = '<ul>';

        $backup_state = true;

        //BACKUP (TO DO)
        if($this->backup_database($type) == true)
        {
            //Vider les tables
            $this->truncate_table($type);

            $this->process .= '<li>Upload des fichiers sur le serveur</li>';

            $upload_state = true;

            //Sauvegarde Fichiers Temporaires
            foreach($_FILES as $key => $value)
            {
                if(!move_uploaded_file($value['tmp_name'], $this->tempDir.$value['name']))
                {
                    $this->process .= '<li style="color: red">Upload des fichiers sur le serveur : KO</li>';
                    $upload_state = false;
                    break;
                }
                else
                {
                    if($value['type'] == 'text/xml')
                    {
                        $array = file($this->tempDir.$value['name']);
                        $array[0] = '<?xml version="1.0" encoding="UTF-8" ?>';
                        file_put_contents($this->tempDir.$value['name'], implode($array));
                    }
                }
            }

            if($upload_state == true)
            {
                $process_state = true;

                $this->process .= '<li style="color: green">Upload des fichiers sur le serveur : OK</li>';

                if($this->process_usines($type) == true)
                    if($this->process_declinaisons($type) == true)
                        if($this->process_certifications_marques($type) == true)
                        {

                        }
                        else
                            $process_state = false;
                    else
                        $process_state = false;
                else
                    $process_state = false;
            }
        }
        else
            $backup_state = false;

        $this->process .= '</ul>';


        //Insertion dans le journal des MAJ
        $status = 1;
        $date = new DateTime();
        $date = $date->format("Y-m-d H:i:s");

        global $current_user;
        get_currentuserinfo();
        $user = $current_user->display_name;

        if($process_state == false || $upload_state == false || $backup_state == false)
            $status = 0;

        $tableHistoName = $wpdb->prefix . "uepc_histo";

        $wpdb->show_errors();

        $preparedQuery = $wpdb->prepare("INSERT INTO ".$tableHistoName." (type_produit, status, author, date, text, isActive) VALUES(%s, %d, %s, '$date', %s, 1)", $type, $status, $user, $this->process);
        $wpdb->query($preparedQuery);


        //Envoi de Mail
        $tableNotifName = $wpdb->prefix . "uepc_notif";
        $mailList = $wpdb->get_row("SELECT * FROM $tableNotifName WHERE type_produit = '$type'");
        $mailTo = explode("\r\n", $mailList->mail);
        $mailSubject = "[Usines et Produits Certifiés] Mise à jour de la base";
        $mailContent = "Type : " . $type . "<br>";
        $mailContent .= "Date : " . $date . "<br>";
        $mailContent .= $this->process;

        $mailSend = wp_mail($mailTo, $mailSubject, $mailContent);

        //var_dump($mailTo);
        //var_dump($mailSend);

        echo $this->process;

        if($handle = opendir($this->tempDir))
        {
            while(($fileName = readdir($handle)) !== false)
                if($fileName != '.' && $fileName != '..')
                {
                    unlink($this->tempDir.$fileName);
                }
            closedir($handle);
        }

        die();
    }*/

    /*
     * restore_db
     *
     * Restore les données des tables custom (Depuis l'interface d'admin)
     */
    function restore_db()
    {
        $type = $_POST['type'];

        $this->process = '<ul>';

        $this->rollback_database($type);

        $this->process .= '</ul>';

        echo $this->process;

        die();
    }

    /*
     * backup_database
     *
     * Backup les données des tables custom
     *
     */
    function backup_database($type)
    {
        global $wpdb;
        $wpdb->show_errors();

        if($type == 'nf')
        {
            $prefixNF = 'qlx_';
            $table = array('qlx_certif', 'qlx_entreprises', 'qlx_etablissements', 'qlx_famille_certif', 'qlx_nn_etablissements_produits_certif', 'qlx_produits', 'qlx_valeurs');
        }
        elseif($type == 'ce')
        {
            $prefixCE = 'marq';
            $table = array('marquagece_certif', 'marquagece_entreprises', 'marquagece_etablissements', 'marquagece_famille_produit', 'marquagece_nn_etablissements_produits_certif', 'marquagece_produits');
        }

        //Déplacement Fichiers En cas echec backup
        if($handle = opendir($this->backupDir))
        {
            while(($fileName = readdir($handle)) !== false)
                if($fileName != '.' && $fileName != '..')
                {
                    $prefixFile = substr($fileName, 0, 4);

                    if($prefixFile == $prefixNF)
                    {
                        unlink($this->backupDir . $fileName);
                    }
                    elseif($prefixFile == $prefixCE)
                    {
                        unlink($this->backupDir.$fileName);
                    }
                }
            closedir($handle);
        }

        for($i=0; $i<count($table); $i++)
        {
            $tableName = $table[$i];
            //Local Only
            //$backupName = str_replace('\\', '/', $this->backupDir.$tableName.'.sql');
            //Server Only
            $backupName = $this->backupDir.$tableName.'.sql';

            $queryBackup = $wpdb->query("SELECT * INTO OUTFILE '$backupName' FROM $tableName");

            if($queryBackup === false)
            {
                $this->process .= '<li style="color: red">Backup de la base de données : KO</li>';

                return false;
            }
        }

        $this->process .= '<li style="color: green">Backup de la base de données : OK</li>';
        return true;
    }

    /*
     * restore_database
     *
     * Restaure les données des tables custom
     */
    function restore_database($type, $rollback = false)
    {
        global $wpdb;
        //$wpdb->show_errors();

        if($type == 'nf')
            $table = array('qlx_certif', 'qlx_entreprises', 'qlx_etablissements', 'qlx_famille_certif', 'qlx_nn_etablissements_produits_certif', 'qlx_produits', 'qlx_valeurs');
        elseif($type == 'ce')
            $table = array('marquagece_certif', 'marquagece_entreprises', 'marquagece_etablissements', 'marquagece_famille_produit', 'marquagece_nn_etablissements_produits_certif', 'marquagece_produits');

        for($i=0; $i<count($table); $i++)
        {
            $tableName = $table[$i];
            //Local Only
            //$backupName = str_replace('\\', '/', $this->backupDir.$tableName.'.sql');
            //Server Only
            $backupName = $this->backupDir.$tableName.'.sql';

            if(!file_exists($backupName))
            {
                $this->process .= '<li style="color: red">Erreur lors de la restauration : Fichier non trouvé</li>';
                return false;
            }

            $wpdb->query("LOAD DATA INFILE '$backupName' INTO TABLE $tableName");
        }

        if($rollback == true)
            $this->process .= '<li style="color: green">Restauration de la dernière base de données : OK</li>';

        return true;
    }

    /*
     * rollback_database
     *
     * Reviens à A-1 si erreur lors de l'insertion
     */
    function rollback_database($type)
    {
        $this->truncate_table($type);
        $this->restore_database($type, true);
    }

    /*
     * truncate_table
     *
     * Vide tout les tables custom
     *
     */
    function truncate_table($type)
    {
        global $wpdb;

        if($type == 'nf')
        {
            $wpdb->query('TRUNCATE TABLE qlx_certif');
            $wpdb->query('TRUNCATE TABLE qlx_entreprises');
            $wpdb->query('TRUNCATE TABLE qlx_etablissements');
            $wpdb->query('TRUNCATE TABLE qlx_famille_certif');
            $wpdb->query('TRUNCATE TABLE qlx_nn_etablissements_produits_certif');
            $wpdb->query('TRUNCATE TABLE qlx_produits');
            $wpdb->query('TRUNCATE TABLE qlx_valeurs');
        }
        elseif($type == 'ce')
        {
            $wpdb->query('TRUNCATE TABLE marquagece_certif');
            $wpdb->query('TRUNCATE TABLE marquagece_entreprises');
            $wpdb->query('TRUNCATE TABLE marquagece_etablissements');
            $wpdb->query('TRUNCATE TABLE marquagece_famille_produit');
            $wpdb->query('TRUNCATE TABLE marquagece_nn_etablissements_produits_certif');
            $wpdb->query('TRUNCATE TABLE marquagece_produits');
        }
    }


    /*
     * extract_pdf
     *
     * Lance l'extraction du pdf
     */
    function extract_pdf($type)
    {
        
        if($type == 'nf')
        {
            $fileName = 'pdf_certificat.zip';
            $folder = 'nf/';
        }
        elseif($type == 'ce')
        {
            $fileName = 'pdf_marquage_ce.zip';
            $folder = 'ce/';
        }

        $this->process .= '<li>Décompression de l\'archive '.$fileName.'</li>';


        //Delete All Previous Files
        $files = glob($this->pdfDir.$folder.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }


        $zip = new ZipArchive;

        $res = $zip->open($this->tempDir.$fileName);

        if ($res === true)
        {
            $zip->extractTo($this->pdfDir.$folder);
            $zip->close();
            $this->process .= '<li style="color: green">Décompression de l\'archive '.$fileName.' effectué</li>';
            return true;
        }
        else
        {
            $this->process .= '<li style="color: red">Erreur lors de la décompression de l\'archive '.$fileName.'</li>';
            $this->rollback_database($type);
            return false;
        }
    }

    /*
     * process_usines
     *
     * Lancer l'insertion des données pour le fichier usines.xml
     *
     */
    function process_usines($type)
    {
        global $wpdb;
        $wpdb->show_errors();

        if($type == 'nf')
        {
            $fileName = 'usines.xml';
            $tableEntrepriseName = 'qlx_entreprises';
            $tableEtablissementName = 'qlx_etablissements';
        }
        elseif($type == 'ce')
        {
            $fileName = 'usines_ce.xml';
            $tableEntrepriseName = 'marquagece_entreprises';
            $tableEtablissementName = 'marquagece_etablissements';
        }

        $this->process .= '<li>Traitement fichier XML '.$fileName.'</li>';

        if(!file_exists($this->tempDir.$fileName))
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier non trouvé</li>';
            $this->rollback_database($type);
            return false;
        }

        $xml_usines = simplexml_load_string(utf8_encode(file_get_contents($this->tempDir.$fileName)));

        if($xml_usines == false)
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier invalide</li>';
            $this->rollback_database($type);
            return false;
        }

        foreach($xml_usines->children() as $entreprise)
        {
            $id_ent = $entreprise['ref_entreprise'];
            $nom_ent = $entreprise->nom;
            $adr1_ent = $entreprise->adr1;
            $adr2_ent = $entreprise->adr2;
            $adr3_ent = $entreprise->adr3;
            $cp_ent = $entreprise->cp;
            $ville_ent = $entreprise->ville;
            $pays_ent = $entreprise->pays;
            $tel_ent = $entreprise->tel;
            $fax_ent = $entreprise->fax;


            //Query Entreprises
            $preparedQuery = $wpdb->prepare('SELECT * FROM '. $tableEntrepriseName .' WHERE id = %d', $id_ent);
            if($wpdb->query($preparedQuery) == 0)
            {
                $preparedQuery = $wpdb->prepare('INSERT INTO '. $tableEntrepriseName .' VALUES(%d, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    $id_ent, $nom_ent, $adr1_ent, $adr2_ent, $adr3_ent, $cp_ent, $ville_ent, $pays_ent, $tel_ent, $fax_ent);

                if($wpdb->query($preparedQuery) == false)
                {
                    //ERREUR INSERTION ENTREPRISES + DELETE BASE ACTUEL + RESTORE
                    $this->process .= '<li style="color: red">Enregistrements insérés dans '. $tableEntrepriseName .' : KO</li>';
                    $this->rollback_database($type);
                    return false;
                }
            }

            foreach($entreprise->etablissements->children() as $etablissement)
            {
                $id_eta = $etablissement['ref_etablissement'];
                $nom_eta = $etablissement->nom;
                $adr1_eta = $etablissement->adr1;
                $adr2_eta = $etablissement->adr2;
                $adr3_eta = $etablissement->adr3;
                $cp_eta = $etablissement->cp;
                $ville_eta = $etablissement->ville;
                $pays_eta = $etablissement->pays;
                $tel_eta = $etablissement->tel;
                $fax_eta = $etablissement->fax;
                $web_eta = $etablissement->web;
                $logo_eta = $etablissement->logo;

                if(empty($logo_eta))
                    $logo = 'false';
                else
                    $logo = 'true';

                //var_dump($logo_eta);

                //Query Etablissement
                $preparedQuery = $wpdb->prepare('SELECT * FROM '. $tableEtablissementName .' WHERE id = %d', $id_eta);
                if($wpdb->query($preparedQuery) == 0)
                {
                    $preparedQuery = $wpdb->prepare('INSERT INTO '. $tableEtablissementName .' VALUES(%d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                        $id_eta, $id_ent, $nom_eta, $adr1_eta, $adr2_eta, $adr3_eta, $cp_eta, $ville_eta, $pays_eta, $tel_eta, $fax_eta, $web_eta, $logo);

                    if ($wpdb->query($preparedQuery) == false) {
                        //ERREUR INSERTION ETABLISSEMENTS + DELETE BASE ACTUEL + RESTORE
                        $this->process .= '<li style="color: red">Enregistrements insérés dans '. $tableEtablissementName .' : KO</li>';
                        $this->rollback_database($type);
                        return false;
                    }
                }
            }
        }

        $nb_entreprises = $wpdb->get_var('SELECT COUNT(*) FROM '. $tableEntrepriseName);
        $nb_etablissements = $wpdb->get_var('SELECT COUNT(*) FROM '. $tableEtablissementName);

        $this->process .= '<li style="color: green">'. $nb_entreprises .' Enregistrements insérés dans '. $tableEntrepriseName .'</li>';
        $this->process .= '<li style="color: green">'. $nb_etablissements .' Enregistrements insérés dans '. $tableEtablissementName .'</li>';

        //unlink($this->tempDir.$fileName);
        $this->process .= '<li style="color: green">Fichier XML '. $fileName .' traité et supprimé</li>';

        return true;
    }

    /*
     * process_certifications_marques
     *
     * Lancer l'insertion des données pour le fichier certifications.xml
     *
     */
    function process_certifications_marques($type)
    {
        global $wpdb;
        //$wpdb->show_errors();

        if($type == 'nf')
        {
            $fileName = 'certifications.xml';
            $tableFamilleCertifName = 'qlx_famille_certif';
            $tableCertifName = 'qlx_certif';
        }
        elseif($type == 'ce')
        {
            $fileName = 'marques_ce.xml';
            $tableFamilleCertifName = 'marquagece_famille_produit';
            $tableCertifName = 'marquagece_certif';
        }

        $this->process .= '<li>Traitement fichier XML '.$fileName.'</li>';

        if(!file_exists($this->tempDir.$fileName))
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier non trouvé</li>';
            $this->rollback_database($type);
            return false;
        }

        $xml_certifications = simplexml_load_string(utf8_encode(file_get_contents($this->tempDir.$fileName)));

        if($xml_certifications == false)
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier invalide</li>';
            $this->rollback_database($type);
            return false;
        }

        foreach($xml_certifications->children() as $famille_produit)
        {
            $id_famille_certif = $famille_produit['ref_famille'];
            $nom_famille_certif = $famille_produit->nom;

            //Query Famille Certif
            $preparedQuery = $wpdb->prepare('SELECT * FROM '.$tableFamilleCertifName.' WHERE id = %d', $id_famille_certif);
            if($wpdb->query($preparedQuery) == 0)
            {
                if($type == 'nf')
                    $preparedQuery = $wpdb->prepare('INSERT INTO '.$tableFamilleCertifName.' VALUES(%d, %s, NULL, 0)', $id_famille_certif, $nom_famille_certif);
                elseif($type == 'ce')
                    $preparedQuery = $wpdb->prepare('INSERT INTO '.$tableFamilleCertifName.' VALUES(%d, %s)', $id_famille_certif, $nom_famille_certif);

                if ($wpdb->query($preparedQuery) == false)
                {
                    //ERREUR INSERTION FAMILLE CERTIF + DELETE BASE ACTUEL + RESTORE
                    $this->process .= '<li style="color: red">Enregistrements insérés dans '.$tableFamilleCertifName.' : KO</li>';
                    $this->rollback_database($type);
                    return false;
                }
                else
                {
                    //Ajout Page Custom Post Type
                    $tablePostType = $wpdb->prefix . "uepc_post_type_id";
                    $getId = $wpdb->get_row("SELECT * FROM $tablePostType WHERE id_certif = $id_famille_certif AND type_produit = '$type'");

                    //Page Existante
                    if($getId != null)
                    {
                        if($type == 'nf')
                            $post_type = 'certification';
                        elseif($type == 'ce')
                            $post_type = 'marquage';

                        //DELETE POST DON'T UNCOMMENT
                        /*wp_insert_post(array(
                            'ID' => $getId->id_post_type,
                            'post_title' => $libelle_certif,
                            'post_type' => $post_type,
                            'post_status' => 'publish',
                            'comment_status' => 'closed',
                            'ping_status' => 'closed',
                        ));*/
                    }
                    else
                    {
                        if($type == 'nf')
                            $post_type = 'certification';
                        elseif($type == 'ce')
                            $post_type = 'marquage';

                        $id_post_type = wp_insert_post(array(
                            'post_title' => $nom_famille_certif,
                            'post_type' => $post_type,
                            'post_status' => 'publish',
                            'comment_status' => 'closed',
                            'ping_status' => 'closed',
                        ));

                        update_post_meta($id_post_type, '_product', (int) $id_famille_certif);

                        $preparedQuery = $wpdb->prepare("INSERT INTO $tablePostType (type_produit, id_certif, id_post_type) VALUES(%s, %d, %d)", $type, $id_famille_certif, $id_post_type);
                        $wpdb->query($preparedQuery);
                    }
                }
            }

            foreach($famille_produit->certifications->children() as $certification)
            {
                $id_certif = $certification['ref_certi'];
                $libelle_certif = $certification->libelle;

                if($type == 'nf')
                {
                    $caracteristiques_certif = $certification->caracteristiques->asXML();
                    $formulaire_certif = $certification->formulaire->asXML();
                    $logo_certif = $certification->logo;
                }

                //Query Certif
                $preparedQuery = $wpdb->prepare('SELECT * FROM '.$tableCertifName.' WHERE id = %d', $id_certif);
                if($wpdb->query($preparedQuery) == 0)
                {
                    if($type == 'nf')
                        $preparedQuery = $wpdb->prepare('INSERT INTO '.$tableCertifName.' VALUES(%d, %d, %s, NULL, %s, %s, %s)',
                            $id_certif, $id_famille_certif, $libelle_certif, $caracteristiques_certif, $formulaire_certif, $logo_certif);
                    elseif($type == 'ce')
                        $preparedQuery = $wpdb->prepare('INSERT INTO '.$tableCertifName.' VALUES(%d, %d, %s)', $id_certif, $id_famille_certif, $libelle_certif);

                    if ($wpdb->query($preparedQuery) == false)
                    {
                        //ERREUR INSERTION CERTIF + DELETE BASE ACTUEL + RESTORE
                        $this->process .= '<li style="color: red">Enregistrements insérés dans '.$tableCertifName.' : KO</li>';
                        $this->rollback_database($type);
                        return false;
                    }
                }
            }
        }

        $nb_famille_certif = $wpdb->get_var('SELECT COUNT(*) FROM '.$tableFamilleCertifName);
        $nb_certif = $wpdb->get_var('SELECT COUNT(*) FROM '.$tableCertifName);

        $this->process .= '<li style="color: green">'. $nb_famille_certif .' Enregistrements insérés dans '.$tableFamilleCertifName.'</li>';
        $this->process .= '<li style="color: green">'. $nb_certif .' Enregistrements insérés dans '.$tableCertifName.'</li>';

        //unlink($this->tempDir.$fileName);
        $this->process .= '<li style="color: green">Fichier XML '.$fileName.' traité et supprimé</li>';

        return true;
    }

    /*
     * process_declinaisons
     *
     * Lancer l'installation des données pour le fichier declinaisons.xml
     */
    function process_declinaisons($type)
    {
        global $wpdb;
        //$wpdb->show_errors();

        if($type == 'nf')
        {
            $fileName = 'declinaisons.xml';
            $tableProduitsName = 'qlx_produits';
            $tableNNProduitsName = 'qlx_nn_etablissements_produits_certif';
        }
        elseif($type == 'ce')
        {
            $fileName = 'declinaisons_ce.xml';
            $tableProduitsName = 'marquagece_produits';
            $tableNNProduitsName = 'marquagece_nn_etablissements_produits_certif';
        }

        $this->process .= '<li>Traitement fichier XML '.$fileName.'</li>';

        if(!file_exists($this->tempDir.$fileName))
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier non trouvé</li>';
            $this->rollback_database($type);
            return false;
        }

        $xml_declinaisons = simplexml_load_string(utf8_encode(file_get_contents($this->tempDir.$fileName)));

        if($xml_declinaisons == false)
        {
            $this->process .= '<li style="color: red">Erreur traitement fichier '.$fileName.' : Fichier invalide</li>';
            $this->rollback_database($type);
            return false;
        }

        foreach($xml_declinaisons->children() as $declinaison)
        {
            if($type == 'nf')
            {
                $ref_decl = $declinaison['ref_decl'];
                $ref_fdes = $declinaison['ref_fdes'];
            }

            $ref_etablissement = $declinaison['ref_etablissement'];
            $ref_certi = $declinaison['ref_certi'];
            $ref_produit = $declinaison['ref_produit'];
            $ref_fichier = $declinaison['ref_fichier'];


            //Query Produits
            $preparedQuery = $wpdb->prepare('SELECT * FROM '. $tableProduitsName .' WHERE id = %d', $ref_produit);
            if($wpdb->query($preparedQuery) == 0)
            {
                $preparedQuery = $wpdb->prepare('INSERT INTO '. $tableProduitsName .' VALUES(%d, NULL)', $ref_produit);

                if ($wpdb->query($preparedQuery) == false)
                {
                    //ERREUR INSERTION PRODUITS + DELETE BASE ACTUEL + RESTORE
                    $this->process .= '<li style="color: red">Enregistrements insérés dans '. $tableProduitsName .' : KO</li>';
                    $this->rollback_database($type);
                    return false;
                }
            }

            //Query Nn Etablissements Produits Certif
            if($type == 'nf')
                $preparedQuery = $wpdb->prepare('INSERT INTO '. $tableNNProduitsName .' VALUES(%d, %d, %d, %d, %s, %d)',
                    $ref_produit, $ref_etablissement, $ref_certi, $ref_decl, $ref_fichier, $ref_fdes);
            elseif($type == 'ce')
                $preparedQuery = $wpdb->prepare('INSERT INTO '.$tableNNProduitsName.' VALUES(%d, %d, %d, %s)',
                    $ref_produit, $ref_etablissement, $ref_certi, $ref_fichier);

            if ($wpdb->query($preparedQuery) == false)
            {
                //ERREUR INSERTION NN ETABLISSEMENTS PRODUITS CERTIF + DELETE BASE ACTUEL + RESTORE
                $this->process .= '<li style="color: red">Enregistrements insérés dans '.$tableNNProduitsName.' : KO</li>';
                $this->rollback_database($type);
                return false;
            }

            if($type == 'nf')
                foreach($declinaison->critere as $critere )
                {
                    $num_crt = trim($critere['num_crt']);
                    $value = trim($critere);

                    //Query Valeurs
                    $wpdb->show_errors();

                    echo " ";

                    $preparedQuery = 'INSERT INTO qlx_valeurs (id_produits, num_crt, valeur) VALUES('.$ref_produit.', '.$num_crt.', "'.$value.'")';

                    if ($wpdb->query($preparedQuery) == false)
                    {
                        //ERREUR INSERTION VALEURS + DELETE BASE ACTUEL + RESTORE
                        $this->process .= '<li style="color: red">Enregistrements insérés dans qlx_valeurs : KO</li>';
                        $this->rollback_database($type);
                        return false;
                    }
                }
        }

        $nb_produits = $wpdb->get_var('SELECT COUNT(*) FROM '.$tableProduitsName);
        $nb_nn_etablissements_produits_certif = $wpdb->get_var('SELECT COUNT(*) FROM '.$tableNNProduitsName);

        $this->process .= '<li style="color: green">'. $nb_produits .' Enregistrements insérés dans '. $tableProduitsName.'</li>';
        $this->process .= '<li style="color: green">'. $nb_nn_etablissements_produits_certif .' Enregistrements insérés dans '. $tableNNProduitsName .'</li>';

        if($type == 'nf')
        {
            $nb_critere = $wpdb->get_var('SELECT COUNT(*) FROM qlx_valeurs');
            $this->process .= '<li style="color: green">'. $nb_critere .' Enregistrements insérés dans qlx_valeurs</li>';
        }

        //unlink($this->tempDir.$fileName);
        $this->process .= '<li style="color: green">Fichier XML '. $fileName .' traité et supprimé</li>';

        return true;
    }

}

function uepc()
{
    global $uepc;

    if(!isset($uepc))
    {
        $uepc = new Uepc();
    }

    return $uepc;
}

//Init
uepc();


endif; //Endif Class Exists