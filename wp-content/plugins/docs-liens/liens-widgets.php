<?php
class Liens_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct('liens_utiles', 'Liens utiles', array('description' => 'Affichage des liens utiles.'));
    }
    
    public function widget($args, $instance)
    {
        # utilisateur
        global $current_user;
        get_currentuserinfo();
        $roleUtilisateur = get_user_role();
        
        # dynamic content
        $group_value_1 = rwmb_meta( 'lien1' ); //print_r($group_value_1);
        $group_value_2 = rwmb_meta( 'lien2' ); //print_r($group_value_2);
        $group_value_3 = rwmb_meta( 'lien3' ); //print_r($group_value_3);
        $group_value_4 = rwmb_meta( 'lien4' ); //print_r($group_value_4);
        $group_value_5 = rwmb_meta( 'lien5' ); //print_r($group_value_5);
        
        if (
           (isset($group_value_1[0]["docs-liens_liennom"])&&$group_value_1[0]["docs-liens_liennom"] != "") 
        || (isset($group_value_2[0]["docs-liens_liennom"])&&$group_value_2[0]["docs-liens_liennom"] != "") 
        || (isset($group_value_3[0]["docs-liens_liennom"])&&$group_value_3[0]["docs-liens_liennom"] != "") 
        || (isset($group_value_4[0]["docs-liens_liennom"])&&$group_value_4[0]["docs-liens_liennom"] != "") 
        || (isset($group_value_5[0]["docs-liens_liennom"])&&$group_value_5[0]["docs-liens_liennom"] != "") 
        ) {
            $titleW = $instance['title'];
            $currentLang =  get_locale();
            if ($currentLang != "fr_FR") $titleW = "Links";
            
            echo $args['before_widget'];
            echo $args['before_title'];
            echo apply_filters('widget_title', $titleW);
            echo $args['after_title'];
            
            if (isset($group_value_1[0]["docs-liens_liennom"])&&$group_value_1[0]["docs-liens_liennom"] != "") {
                ?>
                <ul id="bloc-liens-1" class="bloc-docs-liens docs-liens-hide">
                    <?php
                    foreach ($group_value_1 as $value) {
                        $acces = 1;
                        
                        if (!isset($value["docs-liens_extranet"])) {
                            $acces = 1;
                        } else if ( @$value["docs-liens_extranet"] == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                            $acces = 1;
                        } else {
                            $acces = 0;
                        }
                        
                        if ($value["docs-liens_liennom"] != "" && $acces == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $value["docs-liens_url"]; ?>" target="_blank">
                                    <i class="fa fa-globe"></i> <?php echo (strlen($value["docs-liens_liennom"])>25)?substr($value["docs-liens_liennom"],0,25)."...":$value["docs-liens_liennom"]; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <?php
            }
            if (isset($group_value_2[0]["docs-liens_liennom"])&&$group_value_2[0]["docs-liens_liennom"] != "") {
                ?>
                <ul id="bloc-liens-2" class="bloc-docs-liens docs-liens-hide">
                    <?php
                    foreach ($group_value_2 as $value) {
                        $acces = 1;
                        
                        if (!isset($value["docs-liens_extranet"])) {
                            $acces = 1;
                        } else if ( @$value["docs-liens_extranet"] == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                            $acces = 1;
                        } else {
                            $acces = 0;
                        }
                        
                        if ($value["docs-liens_liennom"] != "" && $acces == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $value["docs-liens_url"]; ?>" target="_blank">
                                    <i class="fa fa-globe"></i> <?php echo (strlen($value["docs-liens_liennom"])>25)?substr($value["docs-liens_liennom"],0,25)."...":$value["docs-liens_liennom"]; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <?php
            }
            if (isset($group_value_3[0]["docs-liens_liennom"])&&$group_value_3[0]["docs-liens_liennom"] != "") {
                ?>
                <ul id="bloc-liens-3" class="bloc-docs-liens docs-liens-hide">
                    <?php
                    foreach ($group_value_3 as $value) {
                        $acces = 1;
                        
                        if (!isset($value["docs-liens_extranet"])) {
                            $acces = 1;
                        } else if ( @$value["docs-liens_extranet"] == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                            $acces = 1;
                        } else {
                            $acces = 0;
                        }
                        
                        if ($value["docs-liens_liennom"] != "" && $acces == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $value["docs-liens_url"]; ?>" target="_blank">
                                    <i class="fa fa-globe"></i> <?php echo (strlen($value["docs-liens_liennom"])>25)?substr($value["docs-liens_liennom"],0,25)."...":$value["docs-liens_liennom"]; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <?php
            }
            if (isset($group_value_4[0]["docs-liens_liennom"])&&$group_value_4[0]["docs-liens_liennom"] != "") {
                ?>
                <ul id="bloc-liens-4" class="bloc-docs-liens docs-liens-hide">
                    <?php
                    foreach ($group_value_4 as $value) {
                        $acces = 1;
                        
                        if (!isset($value["docs-liens_extranet"])) {
                            $acces = 1;
                        } else if ( @$value["docs-liens_extranet"] == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                            $acces = 1;
                        } else {
                            $acces = 0;
                        }
                        
                        if ($value["docs-liens_liennom"] != "" && $acces == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $value["docs-liens_url"]; ?>" target="_blank">
                                    <i class="fa fa-globe"></i> <?php echo (strlen($value["docs-liens_liennom"])>25)?substr($value["docs-liens_liennom"],0,25)."...":$value["docs-liens_liennom"]; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <?php
            }
            if (isset($group_value_5[0]["docs-liens_liennom"])&&$group_value_5[0]["docs-liens_liennom"] != "") {
                ?>
                <ul id="bloc-liens-5" class="bloc-docs-liens docs-liens-hide">
                    <?php
                    foreach ($group_value_5 as $value) {
                        $acces = 1;
                        
                        if (!isset($value["docs-liens_extranet"])) {
                            $acces = 1;
                        } else if ( @$value["docs-liens_extranet"] == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                            $acces = 1;
                        } else {
                            $acces = 0;
                        }
                        
                        if ($value["docs-liens_liennom"] != "" && $acces == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $value["docs-liens_url"]; ?>" target="_blank">
                                    <i class="fa fa-globe"></i> <?php echo (strlen($value["docs-liens_liennom"])>25)?substr($value["docs-liens_liennom"],0,25)."...":$value["docs-liens_liennom"]; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <?php
            }
            
            echo $args['after_widget'];
        }
    }
    
    public function form($instance)
    {
        $title = isset($instance['title']) ? $instance['title'] : '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo  $title; ?>" />
        </p>
        <?php
    }
}