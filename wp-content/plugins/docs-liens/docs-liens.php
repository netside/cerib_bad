<?php
/*
Plugin Name: Widgets Documents GED et liens utiles
Plugin URI: http://patricktang.fr
Description: Un plugin pour afficher les widgets documents GED et Liens utiles
Version: 1.0
Author: Patrick Tang
Author URI: http://patricktang.fr
License: GPL2
*/

class Docs_Liens_Plugin
{
    public function __construct()
    {
    
        function load_docs_widget () {
            register_widget('Docs_Widget');
        }
    
        function load_liens_widget () {
            register_widget('Liens_Widget');
        }
        
        include_once plugin_dir_path( __FILE__ ).'docs-widgets.php';
        add_action('widgets_init', 'load_docs_widget');
        
        include_once plugin_dir_path( __FILE__ ).'liens-widgets.php';
        add_action('widgets_init', 'load_liens_widget');
    }
}

new Docs_Liens_Plugin();
