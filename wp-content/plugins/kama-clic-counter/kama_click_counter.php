<?php

/**
 * Plugin Name: Kama Click Counter
 * Plugin URI: http://wp-kama.ru/?p=430
 * Description: Подсчет загрузок файла и кликов по ссылке. Используйте в тексте шоткод <code>[download url="URL"]</code> или добавьте class <code>count</code> к ссылке - <code>&lt;a class=&quot;count&quot; href=&quot;ссылка&quot;&gt;текст&lt;/a&gt;</code>
 * Author: Kama
 * Author URI: http://wp-kama.ru/
 * Text Domain: kcc
 * Domain Path: lang
 * Version: 3.4.6
 * 
 * Bild: 109
 */

__('Подсчет загрузок файла и кликов по ссылке. Используйте в тексте шоткод <code>[download url="URL"]</code> или добавьте class <code>count</code> к ссылке - <code>&lt;a class=&quot;count&quot; href=&quot;ссылка&quot;&gt;текст&lt;/a&gt;</code>');

if( defined('WP_INSTALLING') && WP_INSTALLING ) return;

$data = get_file_data( __FILE__, array('ver'=>'Version', 'lang_dir'=>'Domain Path') );

define('KCC_VER',  $data['ver'] );
define('KCC_PATH', plugin_dir_path(__FILE__) );
define('KCC_URL',  plugin_dir_url(__FILE__) );
define('KCC_NAME', basename(KCC_PATH) ); // folder name - kama-click-counter

add_action('plugins_loaded', array('KCC', 'instance') );

register_activation_hook( __FILE__, create_function('', 'KCC::instance()->activation();') );


class KCC {	
	const OPT_NAME  = 'kcc_options';
	const COUNT_KEY = 'kcccount';
	const PID_KEY   = 'kccpid';
	
	protected $admin_access;
	
	public $opt;
		
	public static $inst; 
	
	public static function instance(){
		if( is_null( self::$inst ) ){
			self::$inst = is_admin() ? new KCC_Admin() : new self;
		}
		return self::$inst;
	}
	
	function __construct(){
		if( ! is_null( self::$inst ) ) return self::$inst;
        
		global $wpdb;
			
		$this->opt = get_option( self::OPT_NAME );
		
		// access
		// set it here in order to use in front
		$this->admin_access = apply_filters('kcc_admin_access', null );
		if( $this->admin_access === null ){
			$this->admin_access = current_user_can('manage_options');
			
			if( ! $this->admin_access && ! empty($this->opt['access_roles']) ){
				foreach( wp_get_current_user()->roles as $role ){
					if( in_array($role, $this->opt['access_roles'] ) ){
						$this->admin_access = true;
						break;
					}
				}
			}	
		}
				
		// set table name
		$wpdb->tables[]   = 'kcc_clicks';
		$wpdb->kcc_clicks = $wpdb->prefix . 'kcc_clicks';
		
		// локализация
		if( ($locale = get_locale()) && ($locale != 'ru_RU') )
			$res = load_textdomain('kcc', dirname(__FILE__) . '/lang/'. $locale . '.mo' );
		
		// Рабочая часть
		if( $this->opt['links_class'] )
			add_filter('the_content', array(&$this, 'modify_links') );
		
		// admin_bar
		if( $this->opt['toolbar_item'] )
			add_action('admin_bar_menu', array(&$this, 'add_toolbar_menu'), 90 );
			
		add_action('wp_footer', array(&$this, 'add_script_to_footer'), 99);
		add_action('wp_footer', array(&$this, 'enqueue_jquery_if_need'), 0);
		
		//add_action('wp_enqueue_scripts', create_function('', 'wp_enqueue_script("jquery");'), 0 ); // early jquery enqueue, in order it could be changed
		
		// добавляем шоткод загрузок
		add_shortcode('download', array(&$this, 'download_shortcode') );
		
		// событие редиректа
		add_filter('init', array(&$this, 'redirect'), 0);
		
		// Добавляем Виджет
		if( $this->opt['widget'] )
			require_once KCC_PATH . 'widget.php';		
	}
	
	function enqueue_jquery_if_need(){
		if( ! wp_script_is( 'jquery', 'enqueued' ) )
			wp_enqueue_script('jquery');
	}
	
	## jQuery добавка для подсчета ссылок на всем сайте
	function add_script_to_footer(){
		$kcc_url_patt = $this->get_kcc_url( '{url}', '{in_post}', '{download}' );
		
		ob_start();
		?>
		<!-- Kama Click Counter -->
		<script type="text/javascript">
			try{
				var kcckey  = '<?php echo self::COUNT_KEY ?>',
					pidkey  = '<?php echo self::PID_KEY ?>',
					urlpatt = '<?php echo $kcc_url_patt ?>';
				
				// add kcc url to 'count' links
				jQuery('a.<?php echo $this->opt['links_class'] ?>').each(function(){
					var $a   = jQuery(this),
						href = $a.attr('href'),
						pid  = $a.data( pidkey ),
						url;
					
					if( href.indexOf(kcckey) !== -1 ) return; // only for not modified links
					
					url = urlpatt.replace('{in_post}', (pid ? pid : '') );
					url = url.replace('{download}', ( !! $a.data('kccdownload') ? 1 : '') );
					url = url.replace('{url}', href );
										
					$a.attr('data-kcc', 1).click(function(){ this.href = url; }); 
				});
				
				// hide ugly kcc url
				jQuery('a[href*="'+ kcckey +'"]').each(function(){
					var $a   = jQuery(this),
						href = $a.attr('href'),
						re   = new RegExp( kcckey +'=(.*)' ),
						url;
					
					if( url = href.match( re )[1] ){
						if( !! parseInt(url) ) url = '/#download'+ url;
						//console.log( !! parseInt(url), parseInt(url) );
						$a.attr('data-kcc', 1).attr('href', url ).click(function(){ this.href = href; });
					}
					
				});
			}
			catch(e){}
		</script>
		<?php
		$scr = ob_get_clean();
		$scr = preg_replace('~[^:]//[^\n]+|[\t\n\r]~', '', $scr ); // remove: comments, \t\r\n
		$scr = preg_replace('~[ ]{2,}~', ' ', $scr );
		echo $scr ."\n";
	}
	
	## получает ссылку по которой будут считаться клики
	function get_kcc_url( $url = '', $in_post = 0, $download = 0 ){
		// порядок имеет значение...
		$args = array(
			'download'      => $download,
			self::PID_KEY   => $in_post,
			self::COUNT_KEY => $url,
		);
		
		if( ! $this->opt['in_post'] )
			unset( $args[ self::PID_KEY ] );
		
		$kcc_url = array();
		foreach( $args as $key => $val )
			if( $val )
				$kcc_url[] = $key .'='. trim($val);
		
		$kcc_url = home_url() .'?'.  implode( '&', $kcc_url );
		
		if( $this->opt['hide_url'] ){
			$kcc_url = $this->hide_link_url( $kcc_url );
		}

		return apply_filters('get_kcc_url', $kcc_url );
	}
	
	#### counting part --------------------------------------
	## add clicks by given url
	function do_count( $kcc_url, $count = true ){
		global $wpdb;
		
		$parsed = $this->parce_kcc_url( $kcc_url );
		
		$args = array(
			'link_url'  => $parsed[ self::COUNT_KEY ], 
			'in_post'   => (int) $parsed[ self::PID_KEY ], 
			'downloads' => @ $parsed['download'] ? 'yes' : '',
			'kcc_url'   => $kcc_url,
			'count'     => $count,
		);
		
		$link_url = & $args['link_url'];
		$curr_time = current_time('mysql');
		
		if( is_numeric($link_url) )
			$WHERE = 'link_id = '. $link_url;
		else{
			$AND = '';
			if( $this->opt['in_post'] ) $AND  = ' AND in_post = '. (int) $args['in_post'];
			if( $args['downloads'] )    $AND .= $wpdb->prepare(' AND downloads = %s', $args['downloads'] );
			
			$WHERE = $wpdb->prepare('link_url = %s ', $link_url ) . $AND;
		}
		
		$sql = "UPDATE $wpdb->kcc_clicks SET link_clicks = (link_clicks + 1), last_click_date = '". $curr_time ."' WHERE $WHERE LIMIT 1";
		//$wpdb->prepare(); // юзать не катит потому возвращается false, если передатется ссылка с закодированной кириллицей, почему-то в ней используются % - /%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82...
		
		// костыль: обновим дубли... Возможно надо сделать блокировку таблицы при записи...
		if( $more_links = $wpdb->get_results("SELECT * FROM $wpdb->kcc_clicks WHERE $WHERE LIMIT 1,999") ){
			$up_link_id = $wpdb->get_var("SELECT link_id FROM $wpdb->kcc_clicks WHERE $WHERE LIMIT 1");
			foreach( $more_links as $link ){
				$wpdb->query("UPDATE $wpdb->kcc_clicks SET link_clicks = (link_clicks + 1) WHERE link_id = $up_link_id;");
				$wpdb->query("DELETE FROM $wpdb->kcc_clicks WHERE link_id = $link->link_id;");
			}
		}
		
		
		$data = array(); // данные добавляемой в БД ссылки
		
		do_action_ref_array('kcc_count_before', array($args, & $sql, & $data) );
		
		// пробуем обновить данные
		$updated = $wpdb->query( $sql );

		// счетчик обновлен
		if( $updated ){
			$return = true;
		}
		// добавляем данные
		else{
			// добавляем данные в бд...
			$data = array_merge( array(
				'attach_id'        => 0,
				'in_post'          => $args['in_post'],
				'link_clicks'      => $args['count'] ? 1 : 0, // Для загрузок, когда запись добавляется просто при просмотре, все равно добавляется 1 первый просмотр, чтобы добавить запись в бД
				'link_name'        => untrailingslashit( $this->is_file($link_url) ? basename($link_url) : preg_replace('~(https?|ftp)://~', '', $link_url ) ),
				'link_title'       => '', // устанавливается отдлеьно ниже
				'link_description' => '',
				'link_date'        => $curr_time,
				'last_click_date'  => $curr_time,
				'link_url'         => $link_url,
				'file_size'        => self::file_size( $link_url ),
				'downloads'        => $args['downloads'],
			), $data );
			
			// если кирилический домен
			if( false !== stripos( $data['link_name'], 'xn--') ){
				$host = parse_url( $data['link_url'], PHP_URL_HOST );
				//die( var_dump($host) );
				
				require_once KCC_PATH .'php-punycode/idna_convert.class.php';
				$ind = new idna_convert();
				
				$data['link_name'] = str_replace( $host, $ind->decode($host), $data['link_name'] );
			}
			//die( print_r($data) );
			
			$title = & $data['link_title']; // easy life
			
			// is_attach?
			if( $attach = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type = 'attachment' AND guid = %s", $link_url ) ) ){
				$title                    = $attach->post_title;
				$data['attach_id']        = $attach->ID;
				$data['link_description'] = $attach->post_content;
			}
			
			// get link_title from url			
			if( ! $title ){
				if( $this->is_file($link_url) ){
					$title = preg_replace('~[.][^.]+$~', '', $data['link_name'] ); // delete ext
					$title = preg_replace('~[_-]~', ' ', $title);
					$title = ucwords( $title );
				}
				else{
					$title = $this->get_html_title( $link_url );
				}
			}
			// если так и не удалось определить
			if( ! $title )
				$title = $data['link_name'];
			
			$data = apply_filters('kcc_insert_link_data', $data, $args );

			//$wpdb->query("LOCK TABLES $wpdb->kcc_clicks");
			$return = $wpdb->insert( $wpdb->kcc_clicks, $data ) ? $wpdb->insert_id : false;
			//$wpdb->query("UNLOCK TABLES $wpdb->kcc_clicks");
		}
		
		do_action('kcc_count_after', $args, $updated, $data );
		
		$this->clear_link_cache( $kcc_url ); // чистим кэш...
		
		return $return;		
	}
	
	/**
	 * Прячет оригинальную ссылку под ID ссылки. Ссылка должна существовать в БД.
	 * @param  string $kcc_url URL плагина для подсчета ссылки.
	 * @return string URL со спрятанной ссылкой.
	 */
	function hide_link_url( $kcc_url ){		
		$parsed = $this->parce_kcc_url( $kcc_url );
		//print_r( $parsed );
		// не прячем если это простая ссылка
		// не прячем если урл уже спрятан
		if( ! @ $parsed['download'] || is_numeric( @ $parsed[ KCC::COUNT_KEY ]) )
			return $kcc_url;
		
		// не прячем если ссылки нет в БД
		if( ! $link = $this->get_link( $kcc_url ) )
			return $kcc_url;
		
		return str_replace( $parsed[ KCC::COUNT_KEY ], $link->link_id, $kcc_url );
	}

	/**
	 * Разибирает KСС УРЛ.
	 * Конвертирует относительный путь "/blog/dir/file" в абсолютный (от корня сайта) и чистит УРЛ
	 * РАсчитан на прием грязных/неочищенных URL.
	 * 
	 * @param  string $kcc_url Kcc УРЛ.
	 * @return array параметры переданой строки
	 */
	function parce_kcc_url( $kcc_url ){
		preg_match('~\?(.+)$~', $kcc_url, $m ); // get kcc url query args
		$kcc_query = $m[1]; // parse_url( $kcc_url, PHP_URL_QUERY );
		
		// cut URL from $query, because - there could be query args (&) that is why cut it
		$split = preg_split( '~&?'. self::COUNT_KEY .'=~', $kcc_query );
		$query = & $split[0];
		$url   = & $split[1];
		
		if( ! $url )
			return array();
		
		// parse other query part
		parse_str( $query, $query_args );
		
		$url = preg_replace('/#.*$/', '', $url ); // delete #anchor

		if( $url{0} == '/' )
			$url = rtrim( home_url(), '/' ) . $url;
		
		return array(
			self::COUNT_KEY => $url, // no esc_url()
			self::PID_KEY   => (int) @ $query_args[ self::PID_KEY ],
			'download'      => !! @ $query_args['download'], //array_key_exists('download', $query_args ), // isset null не берет
		);
		
		$the_cache = apply_filters('parce_kcc_url', $the_cache );
		
		return $the_cache;
	}

	function is_file( $url ){
		// replace method work
		$return = apply_filters('kcc_is_file', null );
		if( null !== $return )
			return $return;
		
		if( ! preg_match('~\.([a-zA-Z0-9]{1,8})(?=$|\?.*)~', $url, $m ) )
			return false;
		
		$f_ext = $m[1];
		
		$not_supported_ext = array('html', 'htm', 'xhtml', 'xht', 'php');
		
		if( in_array( $f_ext, $not_supported_ext ) )
			return false;
		
		return true; // any other ext - is true		
	}

	/**
	 * return title of a (local or remote) webpage
	 * @param  string $url URL title we get to
	 * @return string   title
	 */
	function get_html_title( $url ){
		$file = @ file_get_contents( $url, false, null, 0, 10000 );
		if( $file && preg_match('@<title>(.*)</title>@is', $file, $m ) )
			return $m[1];
			
		return '';
	}
	
	## Получает размер файла по сылке
	static function file_size( $url ){
		//$url = urlencode( $url );

		$size = null;
			
		// direct. considers WP subfolder install
		if( ! $size && (false !== strpos( $url, home_url() )) ) {
			$path_part = str_replace( home_url(), '', $url );
			$file = wp_normalize_path( ABSPATH . $path_part );
			// если вп во вложенной папке...
			if( ! file_exists( $file ) )
				$file = wp_normalize_path( dirname(ABSPATH) . $path_part );
			$size = @ filesize( $file );
		}
		// curl enabled
		if( ! $size && function_exists('curl_version') ){
			$size = self::curl_get_file_size( $url );
		}
		// get_headers
		if( ! $size && function_exists('get_headers') ){
			$headers = @ get_headers( $url, 1 );
			$size = @ $headers['Content-Length'];
		}
		
		$size = (int) $size;
		
		if( ! $size )
			return 0;
			
		$i = 0;
		$type = array("B", "KB", "MB", "GB");
		while( ( $size/1024 ) > 1 ){
			$size = $size/1024;
			$i++;
		}
		return substr( $size, 0, strpos($size,'.')+2 ) .' '. $type[ $i ];
	}
	
	/**
	 * Returns the size of a file without downloading it.
	 *
	 * @param $url - The location of the remote file to download. Cannot
	 * be null or empty.
	 *
	 * @return The size of the file referenced by $url, or false if the size
	 * could not be determined.
	 */
	static function curl_get_file_size( $url ) {
		$curl = curl_init( $url );

		// Issue a HEAD request and follow any redirects.
		curl_setopt( $curl, CURLOPT_NOBODY, true );
		curl_setopt( $curl, CURLOPT_HEADER, true );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );

		$data = curl_exec( $curl );
		curl_close( $curl );

		$result = false; // Assume failure.
			
		if( ! $data )
		  return $result;

		$content_length = "unknown";
		$status = "unknown";

		if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) )
		  $status = (int) $matches[1];

		if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) )
		  $content_length = (int)$matches[1];

		// http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		if( $status == 200 || ($status > 300 && $status <= 308) )
		  $result = $content_length;
	//die( var_dump($size) );
		return $result;
	}
		
	#### text replacement part --------------------------------------
	## change links that have special class in given content
	function modify_links( $content ){
		if( false === strpos( $content, $this->opt['links_class'] ) )
			return $content;
		
		return preg_replace_callback("@<a ([^>]*class=['\"][^>]*{$this->opt['links_class']}(?=[\s'\"])[^>]*)>(.+?)</a>@", array( & $this, 'do_simple_link' ), $content );
	}
		
	## parses string to detect and process pairs of tag="value"
	function do_simple_link( $match ){
		global $post;
		
		$link_attrs  = $match[1];
		$link_anchor = $match[2];
		preg_match_all('~[^=]+=([\'"])[^\1]+?\1~', $link_attrs, $args );
		foreach( $args[0] as $pair ){
			list($tag, $value) = explode('=', $pair, 2);
			$value = trim( trim($value, '"\'') );
			$args[ trim($tag) ] = $value;
		}
		unset($args[0]);
		unset($args[1]);
		
		$args['data-'. self::PID_KEY ] = $post->ID;
		if( $this->opt['add_hits'] ){
			$link = $this->get_link( $args['href'] );
			
			if( $link && $link->link_clicks ){			
				if ( $this->opt['add_hits'] == 'in_title' )
					$args['title'] = "(". __('кликов:','kcc') ." {$link->link_clicks})". $args['title'];
				else 
					$after = ($this->opt['add_hits']=='in_plain') ? ' <span class="hitcounter">('. __('кликов:','kcc') .' '. $link->link_clicks .')</span>' : '';
			}
		}

		$link_attrs = '';
		foreach( $args as $key => $value )
			$link_attrs .= "$key=\"$value\" ";
			
		$link_attrs = trim($link_attrs);
		
		return '<a '. $link_attrs .'>'. $link_anchor .'</a>'. @ $after;
	}
	
	## получает ссылку на картинку иконки g расширению переданной ссылки
	function get_url_icon( $url ){		
		$url_path = parse_url( $url, PHP_URL_PATH );
		
		if( preg_match('~\.([a-zA-Z0-9]{1,8})(?=$|\?.*)~', $url_path, $m ) )
			$icon_name = $m[1] .'.png';
		else
			$icon_name = 'default.png';
		
		$icon_name  = file_exists( KCC_PATH . "icons/$icon_name") ? $icon_name : 'default.png';
		
		$icon_url = KCC_URL . "icons/$icon_name";
		
		return apply_filters('get_url_icon', $icon_url, $icon_name );
	}
	
	function download_shortcode( $attr = array() ){
		global $post;
		
		$kcc_url = $this->get_kcc_url( @ $attr['url'], $post->ID, 1 );
		
		// записываем данные в БД
		if( ! $link = $this->get_link( $kcc_url ) ){
			$this->do_count( $kcc_url, $count = false ); // для проверки, чтобы не считать эту операцию
			$link = $this->get_link( $kcc_url );
		}
		
		$tpl = str_replace('[link_url]', esc_url($kcc_url), $this->opt['download_tpl'] );
		
		return $this->tpl_replace_shortcodes( $tpl, $link );
	}
	
	## заменяет шоткоды в шаблоне на реальные данные
	## $link - (объект) данные ссылки из БД
	function tpl_replace_shortcodes( $tpl, $link ){
		$tpl = str_replace('[icon_url]', $this->get_url_icon( $link->link_url ), $tpl );
		$tpl = str_replace('[edit_link]', $this->edit_link_url( $link->link_id ), $tpl ); 
		
		if( preg_match('@\[link_date:([^\]]+)\]@', $tpl, $date) )
			$tpl = str_replace( $date[0], apply_filters('get_the_date', mysql2date($date[1], $link->link_date) ), $tpl );
		
		// меняем все остальные шоткоды
		preg_match_all('@\[([^\]]+)\]@', $tpl, $match );
		foreach( $match[1] as $data ){
			$tpl = str_replace("[$data]", $link->$data, $tpl );
		}
		
		return $tpl;
	}
	
	function clear_link_cache( $kcc_url ){
		$this->get_link( $kcc_url, 'clear_cache' );
	}

	/**
	 * Получает данные уже существующие ссылки из БД.
	 * 
	 * Если не удалось получить ссылку кэш не устанавливается.
	 * @param  string/int $kcc_url            URL или ID ссылки, или kcc_URL
	 * @param  boolean [$clear_cache = false] Когда нужно очистить кэш ссылки.
	 * @return object/NULL   null при очистке кэша или если не удалось получить данные.
	 */	
	function get_link( $kcc_url, $clear_cache = false ){
		static $cache;
		
		if( $clear_cache ){
			unset($cache[$kcc_url]);
			return;
		}
		
		if( isset($cache[$kcc_url]) )
			return $cache[$kcc_url];
		
		// тут кэш юзать можно только со сбросом в нужном месте...
		global $wpdb;
		
		// если это прямая ссылка а не kcc_url
		if( is_numeric($kcc_url) || false === strpos( $kcc_url, self::COUNT_KEY ) )
			$link_url = $kcc_url;
		else{
			$parsed = $this->parce_kcc_url( $kcc_url );

			$link_url = $parsed[ self::COUNT_KEY ];
			$pid      = $parsed[ self::PID_KEY ];
		}
		
		// передан ID ссылки, а не URL
		if( is_numeric($link_url) )
			$WHERE = $wpdb->prepare("link_id = %d", $link_url );
		else{
			$in_post = @ $pid ? $wpdb->prepare(' AND in_post = %d', $pid ) : '';
			$WHERE = $wpdb->prepare("link_url = %s ", $link_url ) . $in_post;
		}
		
		$link_data = $wpdb->get_row("SELECT * FROM $wpdb->kcc_clicks WHERE $WHERE");
		
		if( $link_data )		
			$cache[$kcc_url] = $link_data;
		
		return $link_data;
	}
	
	## возвращает УРЛ на редактирование ссылки в админке
	function edit_link_url( $link_id, $edit_text = '' ){
		if( ! $this->admin_access ) return '';
			
		if( ! $edit_text ) $edit_text = '✎'; //__('редактировать', 'kcc');
		
		return '<a class="kcc-edit-link" href="'. admin_url('admin.php?page='. KCC_NAME .'&edit_link='. $link_id ) .'">'. $edit_text .'</a>';
	}
	
	## toolbar link
	function add_toolbar_menu( $toolbar ) {		
		$toolbar->add_menu( array(
			'id'    => 'kcc',
			'title' => __('Статистика KCC','kcc'),
			'href'  => admin_url('admin.php?page='. KCC_NAME ),
		) );
	}
	
	### redirect ---------------------------------------------
	function redirect(){
		if( ! isset($_GET[ self::COUNT_KEY ]) )
			return;
		
		$url = $_GET[ self::COUNT_KEY ];
			
		// для переопределения функции
		if( apply_filters('kcc_redefine_redirect', false, $this ) )
			return;
		
		//if( ! $_SERVER['HTTP_REFERER'] ) // not works in IE
		//	die( __('Запрещается прямое использвьание этой ссылки', 'kcc') );
		
		// считаем
        if( apply_filters('kcc_do_count', true, $this ) )
            $this->do_count( $_SERVER['REQUEST_URI'] );
		
		if( is_numeric($url) ){
            if( ! $link_data = $this->get_link( $_SERVER['REQUEST_URI'] ) )
                return;
            
			$url = $link_data->link_url;
		}
			
		// перенаправляем
		if( headers_sent() )
			print "<script>location.replace('". esc_url($url) ."');</script>";
		else{
            wp_redirect( $url, 303 );
            
//            global $is_IIS;
//            
//            $status = 303;
//            
//            if ( !$is_IIS && PHP_SAPI != 'cgi-fcgi' )
//                status_header($status); // This causes problems on IIS and some FastCGI setups
//            
//            $url = wp_sanitize_redirect( $url );
//			header("Location: $url", true, $status ); // wp_redirect() не подходит...
        }
		
		exit;
	}

}

class KCC_Admin extends KCC {

	function __construct(){
		parent::__construct();

		// no access
		if( ! $this->admin_access )
			return;
		
		require KCC_PATH . 'admin_functions.php';
		
		add_action('admin_menu',        array(&$this, 'admin_menu') );
		
		add_action('delete_attachment', array(&$this, 'delete_link_by_attach_id') );	
		add_action('edit_attachment',   array(&$this, 'update_link_with_attach') );
		
		add_filter('plugin_action_links_'. plugin_basename(__FILE__), array(&$this, 'plugins_page_links') );
		
		// MCE
		include KCC_PATH . 'mce/mce.php';
	}
	
	## Ссылки на страницы статистики и настроек со страницы плагинов
	function plugins_page_links( $actions ){			
		$actions[] = '<a href="admin.php?page='. KCC_NAME .'&options">'. __('Настройки', 'kcc') .'</a>'; 
		$actions[] = '<a href="admin.php?page='. KCC_NAME  .'">'. __('Статистика', 'kcc') .'</a>'; 
		return $actions; 
	}	
	
	function admin_menu(){
		if( ! $this->admin_access ) return; // just in case...
		
		// открываем для всех, сюда не должно доходить, если нет доступа!....
		$hookname = add_options_page('Kama Click Counter', 'Kama Click Counter', 'read', KCC_NAME, array( & $this, 'options_page_output' ));
		add_action('load-'. $hookname, array( & $this, 'options_page_load' ) );
	}
	
	function options_page_load(){
		if( ! $this->admin_access ) return; // just in case...
		
		$this->upgrade();
		
		if( isset($_POST['save_options']) ){
			if( ! wp_verify_nonce($_POST['_wpnonce'], 'save_options') )
				return $this->msg = 'error: nonce failed';
				
			$opt = array();
			
			$_POST = wp_unslash( $_POST );

			foreach( $this->get_def_options() as $k => $v )
				$opt[$k] = isset($_POST[$k]) ? $_POST[$k] : '';
			
			update_option( self::OPT_NAME, $opt );
			
			if( $this->opt = get_option( self::OPT_NAME ) )
				$this->msg = __('Настройки сохранены.', 'kcc');
			else
				$this->msg = __('Ошибка: не удалось обновить настройки!', 'kcc');
		}
		elseif( isset($_POST['reset']) ){
			if( ! wp_verify_nonce($_POST['_wpnonce'], 'save_options') )
				return $this->msg = 'error: nonce failed';
			
			$this->set_def_options();
			$this->msg = __('Настройки сброшены на начальные!', 'kcc');
		}
		// update_link
		elseif( isset($_POST['update_link']) ){
			if( ! wp_verify_nonce($_POST['_wpnonce'], 'update_link') )
				return $this->msg = 'error: nonce failed';
			
			$data = wp_unslash( $_POST['up'] );
			$id   = (int) $data['link_id'];
			//unset( $data['update_link'], $data['local_referer'] );
			
			$this->msg = $this->update_link( $id, $data ) ? __('Ссылка обновлена!', 'kcc') : 'error: ' . __('Не удалось обновить ссылку!', 'kcc');
		}
		elseif( isset($_POST['delete_link_id']) ){
			if( ! wp_verify_nonce($_POST['_wpnonce'], 'bulk_action') )
				return $this->msg = 'error: nonce failed';
			
			if( $this->delete_links($_POST['delete_link_id']) )
				$this->msg = __('Выбранные ссылки удалены!', 'kcc');
			else
				$this->msg = __('Ничего <b>не удалено</b>!', 'kcc');
		}
		// delete single link handler
		elseif( isset($_GET['delete_link']) ){
			if( ! wp_verify_nonce($_GET['_wpnonce'], 'delete_link') )
				return $this->msg = 'error: nonce failed';
			
			if( $this->delete_links( $_GET['delete_link'] ) )
				wp_redirect( remove_query_arg( array( 'delete_link', '_wpnonce') ) );
			else
				$this->msg = __('Ничего <b>не удалено</b>!', 'kcc');
		}
	}
	
	function admin_page_url(){
		return admin_url('admin.php?page='. KCC_NAME );
	}
	
	function options_page_output(){		
		include KCC_PATH . 'admin.php';
	}
	
	function set_def_options(){
		update_option( self::OPT_NAME, $this->get_def_options() );
		
		return $this->opt = get_option( self::OPT_NAME );
	}
	
	function get_def_options(){
		$array = array(
			'download_tpl' => '
				<div class="kcc_block" title="Скачать" onclick="document.location.href=\'[link_url]\'">
					<img class="alignleft" src="[icon_url]" alt="" />
					<a class="kcc-link" href="[link_url]" title="[link_name]">Скачать: [link_title]</a>
					<div class="description">[link_description]</div>
					<small>Скачано: [link_clicks], размер: [file_size], дата: [link_date:d.M.Y]</small> 
					<b><!-- clear --></b>
					[edit_link]
				</div>

				<style type="text/css">
					.kcc_block{ position:relative; padding:15px 10px; margin-bottom:20px; cursor:pointer; transition:background-color 0.4s; }
					.kcc_block:before{ clear:both; }
					.kcc_block a{ border-bottom:0; }
					.kcc_block a.kcc-link{ text-decoration:none; display:block; font-size:150%; line-height:1.2; } 
					.kcc_block img{ width:55px; height:auto; float:left; margin:0 25px 0 5px;border:0px!important; box-shadow:none!important; } 
					.kcc_block .description{ color:#666; } 
					.kcc_block small{ color:#ccc; } 
					.kcc_block b{ display:block; clear:both; }
					.kcc_block:hover{ outline:2px solid rgba(125, 125, 125, 0.18); }
					.kcc_block:hover a{ text-decoration:none; }
					.kcc_block .kcc-edit-link{ position:absolute; top:0; right:0.2em; }
				</style>',
			'links_class'  => 'count', // проверять class в простых ссылках
			'add_hits'     => '',         // may be: '', 'in_title' or 'in_plain' (for simple links)
			'in_post'      => 1,
			'hide_url'     => false,      // прятать ссылку или нет?
			'widget'       => 1,          // включить виджет для WordPress
			'toolbar_item' => 1,          // выводить ссылку на статистику в Админ баре?
			'access_roles' => array(),    // Название ролей, кроме администратора, которым доступно упраление плагином.
		);
		
		$array['download_tpl'] = trim( preg_replace('~^\t{4}~m', '', $array['download_tpl']) );
		
		return $array;
	}	
	
	function update_link( $id, $data ){
		global $wpdb;
		
		if( $id = (int) $id )
			$query = $wpdb->update( $wpdb->kcc_clicks, $data, array('link_id' => $id) );
		
		// обновление вложения, если оно есть
		if( $data['attach_id'] > 0 ){
			$wpdb->update( $wpdb->posts,
				array('post_title' => $data['link_title'], 'post_content' => $data['link_description']),
				array('ID' => $data['attach_id'])
			);
		}
		
		return $query;
	}
	
	function delete_link_url( $link_id ){
		return esc_url( add_query_arg( array( 'delete_link'=>$link_id, '_wpnonce'=>wp_create_nonce('delete_link') ) ) );
	}
	
	/**
	 * Удаление ссылок из БД по переданному массиву ID или ID ссылки
	 * @param  array/int [$array_ids = array()] ID ссылок котоыре нужно удалить
	 * @return boolean  Удалено ли
	 */
	function delete_links( $array_ids = array() ){
		global $wpdb;
		
		$array_ids = array_filter( array_map('intval', (array) $array_ids ) );
		
		if( ! $array_ids )
			return false;
		
		return $wpdb->query( "DELETE FROM $wpdb->kcc_clicks WHERE link_id IN (". implode(',', $array_ids) .")" );
	}
	
	## Удаление ссылки по ID вложения
	function delete_link_by_attach_id( $attach_id ){
		global $wpdb;
		
		if( ! $attach_id )
			return false;
		
		return $wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->kcc_clicks WHERE attach_id = %d", $attach_id ) );
	}
	
	## Обновление ссылки, если обновляется вложение
	function update_link_with_attach( $attach_id ){
		global $wpdb;
		
		$attdata = wp_get_single_post( $attach_id );
		
		$new_data = wp_unslash( array(
			'link_description' => $attdata->post_content,
			'link_title' => $attdata->post_title,
			'link_date' => $attdata->post_date,
		) );
		
		return $wpdb->update( $wpdb->kcc_clicks, $new_data, array( 'attach_id' => $attach_id ) );
	}
	
	function activation(){
		global $wpdb;
		
		$charset_collate  = (! empty( $wpdb->charset )) ? "DEFAULT CHARSET=$wpdb->charset" : '';
		$charset_collate .= (! empty( $wpdb->collate )) ? " COLLATE $wpdb->collate" : '';

		// Создаем таблицу если такой еще не существует
		$sql = "CREATE TABLE $wpdb->kcc_clicks (
			link_id           bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			attach_id         bigint(20) UNSIGNED NOT NULL default 0,
			in_post           bigint(20) UNSIGNED NOT NULL default 0,
			link_clicks       bigint(20) UNSIGNED NOT NULL default 1,
			link_name         varchar(255)        NOT NULL,
			link_title        varchar(255)        NOT NULL,
			link_description  varchar(255)        NOT NULL,
			link_date         date                NOT NULL default '0000-00-00',
			last_click_date   date                NOT NULL default '0000-00-00',
			link_url          varchar(255)        NOT NULL,
			file_size         varchar(255)        NOT NULL,
			downloads         ENUM('','yes')      NOT NULL default '',
			PRIMARY KEY  (link_id), 
			KEY in_post (in_post),
			KEY downloads (downloads),
			KEY link_url (link_url)
		) $charset_collate";

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );
		//wp_die();
		
		$this->upgrade();
		
		if( ! get_option( self::OPT_NAME ) )
			$this->set_def_options();
	}
	
	function upgrade(){
		$prev_ver = get_option('kcc_version');

		if( $prev_ver == KCC_VER || ! $prev_ver ) return;

		update_option('kcc_version', KCC_VER );

		global $wpdb;

		// обнволение структуры таблиц
		//require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		//$doe = dbDelta( dem_get_db_schema() );
		//wp_die(print_r($doe));

		$fields = $wpdb->get_results("SHOW COLUMNS FROM $wpdb->kcc_clicks");
		$fields = wp_list_pluck( $fields, 'Field' );
		
		// up to 3.0 ver
		if( version_compare( $prev_ver, '3.0', '<') ){
			// $wpdb->query("UPDATE $wpdb->posts SET post_content=REPLACE(post_content, '[download=', '[download url=')");
			// обновим таблицу
			$charset_collate  = 'CHARACTER SET ' . ( (! empty( $wpdb->charset )) ? $wpdb->charset : 'utf8' );
			$charset_collate .= ' COLLATE ' . ( (! empty( $wpdb->collate )) ? $wpdb->collate : 'utf8_general_ci' );
			
			// добавим поле: дата последнего клика
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks ADD `last_click_date` DATE NOT NULL default '0000-00-00' AFTER link_date");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks ADD `downloads` ENUM('','yes') NOT NULL default ''");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks ADD INDEX  `downloads` (`downloads`)");
			
			// обновим существующие поля
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_date`  `link_date` DATE NOT NULL default  '0000-00-00'");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_id`    `link_id`   BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `attach_id`  `attach_id` BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '0'");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `in_post`    `in_post`   BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '0'");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_clicks`  `link_clicks` BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '0'");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks DROP  `permissions`");

			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_name`   `link_name`  VARCHAR( 255 ) $charset_collate NOT NULL");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_title`  `link_title` VARCHAR( 255 ) $charset_collate NOT NULL");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_url`    `link_url`   VARCHAR( 255 ) $charset_collate NOT NULL");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `link_description`  `link_description` VARCHAR( 255 ) $charset_collate NOT NULL");
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks CHANGE  `file_size`   `file_size`  VARCHAR( 255 ) $charset_collate NOT NULL");
		}
		
		// 3.4.0
		if( version_compare( $prev_ver, '3.4.0', '<') ){
			$wpdb->query("ALTER TABLE $wpdb->kcc_clicks ADD INDEX link_url (link_url(191))");
		}

		
	}
}