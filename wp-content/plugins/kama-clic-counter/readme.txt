=== Plugin Name ===
Stable tag: 3.4.6
Contributors: Tkama
Tags: analytics, statistics, count, count clicks, clicks, counter, download, downloads, link, kama
Requires at least: 3.6
Tested up to: 4.5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Count click on any link all over the site. Also, creates download block in content for file download. Has widget for top downloads.


== Description ==

Using this plugin you will have statistics on clicks on your files or any other link (not file).

Plugin don't have any additional instruments for you to physically uploads files - it's no need! All files uploaded using standard wordpress media uploader. To create download block, file URL are used.

In additional, plugin has:

* Button in visual editor, to insert download file block easely.
* Customizable widget, that allows output "Top Downloads" list.


Localisation: English, Russian



== TODO ==
* set filename in shortcode itself.
* detail statistic on each day (PRO version)
* tiny mce button click show url field and button to select file from media library
* Когда пользователь нажимает на кнопку DW, появляющаяся адресная строка вводит любого пользователя в ступор, в итоге все пользуются стандартной кнопкой, а плагин неиспользуется вообще.. Диалог редактирования ссылки из настроек прикрутить бы к кнопке DW в редакторе.. И в самом диалоге прикрутить стандартный диалог прикрепления файла (в нем же можно и с локального компьютера и из медиатеки цеплять - пользователи же уже привыкли).. Страница статистики расположенная в Настройках - нелогичное решение, несмотря на то, что там и настройки тоже есть. Ее место либо вообще в главном меню (чего я сам не люблю), либо в Инструменты или Медиафайлы. И сама аббревиатура DW на кнопке неинтуитивная, иконку бы, могу поискать..

hotlink protection
<IfModule mod_rewrite.c>
RewriteEngine on
RewriteBase /
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} !^https?://.*wptest\.ru/ [NC]
RewriteRule \.(zip|7z|exe)$ - [NC,F,L]
</IfModule>



== Installation ==
Add and activate the plugin through the 'Plugins' menu in WordPress


== Frequently Asked Questions ==

= How can I customize download block with CSS? =

Just customize CSS styles in plugin options page. Also you can add css styles into your style.css file.



== Screenshots ==
1. Statistics page.
2. Plugin settings page.
3. Single link edit page.
4. TinyMce visual editor downloads button.



== Upgrade Notice ==

In Order to upgrade to version 3.0 or higher you need update content shortcodes from [download=<url>] to [download url=<url>]. Do this with that simple sql query, for it do once this PHP code: <?php global $wpdb; $wpdb->query("UPDATE $wpdb->posts SET post_content=REPLACE(post_content, '[download=', '[download url=')"); ?>


== Changelog ==

= 3.4.6 =
ADD: 'get_url_icon' filter to manage icons.

= 3.4.5 =
ADD: Administrator option to set access to plugin to other WP roles.
ADD: Option to add link to KCC Stat in admin bar.
DEL: no HTTP_REFERER block on direct kcc url use.

= 3.4.4 =
CHANGE: is_file extention check method for url.
ADD: 'kcc_is_file' filter
ADD: widget option to set link to post instead of link to file
REMOVED: 'kcc_file_ext' filter

= 3.4.3 =
Added hooks: 'parce_kcc_url', 'kcc_count_before', 'kcc_count_after'.
Added: second parametr '$args' to 'kcc_insert_link_data' filter.
Added: punycode support. Now links filter in admin table trying to find keyword in 'link_name' db column too, not only in 'link_url'.

= 3.4.2 =
Added: 'kcc_admin_access' filter. For possibility to change access capability.
Fix: redirect protection fix.

= 3.4.1 =
Fix: parse kcc url fix.

= 3.4.0 =
Added: Hide url in download block option. See the options page.
Added: 'link_url' column index in DB for faster plugin work.
Added: 'get_kcc_url', 'kcc_redefine_redirect', 'kcc_file_ext', 'kcc_insert_link_data' hooks.
Added: Now plugin replace its ugly URL with original URL, when link hover.
Added: Replace 'edit link' text for download block to icon. It's more convenient.
Fix: Correct updates of existing URLs. In some cases there appeared duplicates, when link contain '%' symbol (it could be cyrillic url or so on...)
Fix: XSS attack protection.
Fix: Many structure fix in code.


= 3.3.2 =
Fix: php notice

= 3.3.1 =
Add: de_DE l10n, thanks to Volker Typke.

= 3.3.0 =
Add: l10n on plugin page.
Add: menu to admin page.
Fix: antivirus wrongly says that file infected.

= 3.2.34 =
Fix: Some admin css change

= 3.2.3.3 =
added: jQuery links become hidden. All jQuery affected links have #kcc anchor and onclick attr with countclick url
fixed: error with parse_url part. If url had "=" it was exploded...

= 3.2.3.2 =
fixed: didn't correctly redirected to url with " " character
added: round "clicks per day" on admin statistics page to one decimal digit

= 3.2.3.1 =
fixed: "back to stat" link on "edit link" admin page

= 3.2.3 =
fixed: redirects to https doesn't worked correctly
fixed: PHP less than 5.3 support
fixed: go back button on "edit link" admin page
fixed: localization

= 3.2.2 =
Added: "go back" button on "edit link" admin page

= 3.2.1 =
Set autoreplace old shortcodes to new in DB during update: [download=] [download url=]

= 3.2 =
Widget has been added