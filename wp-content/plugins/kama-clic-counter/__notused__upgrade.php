<?php

## Plugin Update
## Нужно вызывать на странице настроек плагина, чтобы не грузить лишний раз сервер.
function kcc_upgrade(){
	$old_ver = get_option('kcc_version');
	
	if( $old_ver == KCC_VER ) return;
	
	update_option('kcc_version', KCC_VER );
	
	// обновим таблицы БД
	global $wpdb;
	
	// обнволение структуры таблиц
    //require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	//$doe = dbDelta( dem_get_db_schema() );
	//wp_die(print_r($doe));
	
	### 
	### изменение данных таблиц
	$fields = $wpdb->get_results("SHOW COLUMNS FROM $wpdb->kcc_clicks");
	$fields = wp_list_pluck( $fields, 'Field' );
		
	// 3.1.3
	if( ! in_array('end', $fields_q ) )
		$wpdb->query("ALTER TABLE $wpdb->democracy_q ADD `end` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `added`;");
	
	if( ! in_array('note', $fields_q ) )
		$wpdb->query("ALTER TABLE $wpdb->democracy_q ADD `note` text NOT NULL;");
	
	if( in_array('current', $fields_q ) ){
		$wpdb->query("ALTER TABLE $wpdb->democracy_q CHANGE `current` `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;");
		$wpdb->query("ALTER TABLE $wpdb->democracy_q CHANGE `active` `open`    TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;");
	}
	
	// 4.1
	if( ! in_array('aids', $fields_log ) ){
		// если нет поля aids, создаем 2 поля и индексы
		$wpdb->query("ALTER TABLE $wpdb->democracy_log ADD `aids`   text NOT NULL;");
		$wpdb->query("ALTER TABLE $wpdb->democracy_log ADD `userid` bigint(20) UNSIGNED NOT NULL DEFAULT 0;");
		$wpdb->query("ALTER TABLE $wpdb->democracy_log ADD KEY userid (userid)");
		$wpdb->query("ALTER TABLE $wpdb->democracy_log ADD KEY qid (qid)");
	}
	
	// 4.2
	if( in_array('allowusers', $fields_q ) )
		$wpdb->query("ALTER TABLE $wpdb->democracy_q CHANGE `allowusers` `democratic` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';");
	
	if( ! in_array('forusers', $fields_q ) ){
		$wpdb->query("ALTER TABLE $wpdb->democracy_q ADD `forusers` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `multiple`;");
		$wpdb->query("ALTER TABLE $wpdb->democracy_q ADD `revote`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `multiple`;");
	}
    
    // 4.5.6
	if( ! in_array('expire', $fields_log ) )
		$wpdb->query("ALTER TABLE $wpdb->democracy_log ADD `expire` bigint(20) UNSIGNED NOT NULL default 0 AFTER `userid`;");
		
	// 4.9
	if( ! in_array('date', $fields_log ) )
		$wpdb->query("ALTER TABLE `$wpdb->democracy_log` ADD `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `userid`;");
	
	// 4.9.3
	if( version_compare( $old_ver, '4.9.3', '<') ){
		$wpdb->query("ALTER TABLE `$wpdb->democracy_log` CHANGE `date` `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';");
		
		$wpdb->query("ALTER TABLE `$wpdb->democracy_q` CHANGE `multiple` `multiple` tinyint(5) UNSIGNED NOT NULL DEFAULT 0;");
		
		$wpdb->query("ALTER TABLE `$wpdb->democracy_a` CHANGE `added_by` `added_by` varchar(100) NOT NULL default '';");
		$wpdb->query("UPDATE `$wpdb->democracy_a` SET added_by = '' WHERE added_by = '0'");
	}
	if( ! in_array('added_user', $fields_q ) )
		$wpdb->query("ALTER TABLE `$wpdb->democracy_q` ADD `added_user` bigint(20) UNSIGNED NOT NULL DEFAULT 0 AFTER `added`;");
	if( ! in_array('show_results', $fields_q ) )
		$wpdb->query("ALTER TABLE `$wpdb->democracy_q` ADD `show_results` tinyint(1) UNSIGNED NOT NULL default 1 AFTER `revote`;");
	
	// 5.0.4
	if( version_compare( $old_ver, '5.0.4', '<') ){
		$wpdb->query("ALTER TABLE $wpdb->democracy_log CHANGE `ip` `ip` BIGINT(11) UNSIGNED NOT NULL DEFAULT '0';");
		$wpdb->query("ALTER TABLE $wpdb->democracy_log CHANGE `qid` `qid` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0';");
		
		$wpdb->query("ALTER TABLE `$wpdb->democracy_a` CHANGE `aid` `aid` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT;");
		$wpdb->query("ALTER TABLE `$wpdb->democracy_a` CHANGE `qid` `qid` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0';");
		
		$wpdb->query("ALTER TABLE `$wpdb->democracy_q` CHANGE `id` `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT;");
	}

	// 5.1.1
	if( ! in_array('voteid', $fields_log ) ){
		$wpdb->query("ALTER TABLE `$wpdb->democracy_log` ADD `voteid` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;");
	}
	
}
