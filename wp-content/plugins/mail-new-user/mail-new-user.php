<?php
/*
Plugin Name: User Emails
Description: Changes the default user creation notification emails
Version: 1.0
Author: patrick Tang
Author URI: http://www.patricktang.fr
*/

if ( !function_exists( 'wp_new_user_notification' ) ) {
    function wp_new_user_notification( $user_id, $deprecated = null, $notify = '' ) {
        if ( $deprecated !== null ) {
            _deprecated_argument( __FUNCTION__, '4.3.1' );
        }
        // set content type to html
        add_filter( 'wp_mail_content_type', 'wpmail_content_type' );
     
        global $wpdb, $wp_hasher;
        $user = get_userdata( $user_id );
        $siteUrl = get_site_url();
     
        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
     
        $message  = sprintf(__('Nouvel utilisateur créé sur le site Cerib.com :'), $blogname) . "<br><br>";
        $message .= sprintf(__('Identifiant : %s'), $user->user_login) . "<br><br>";
        $message .= sprintf(__('Mot de passe : %s'), stripslashes($_POST["pass1"])) . "<br><br>";
        $message .= sprintf(__('E-mail : %s'), $user->user_email) . "<br><br>";
        $message .= '
        Cordialement,
        <br><br>
        L\'équipe du CERIB<br>
        <a href="'.$siteUrl.'">Cerib.com</a>
        ';
        $headers = 'From: CERIB.com <webmaster@cerib.com>';
     
        @wp_mail(get_option('admin_email'), sprintf(__('Cerib.com - Création d\'un nouvel utilisateur'), $blogname), $message, $headers);
     
        if ( 'admin' === $notify || empty( $notify ) ) {
            return;
        }
     
        // Generate something random for a password reset key.
        $key = wp_generate_password( 20, false );
     
        /** This action is documented in wp-login.php */
        do_action( 'retrieve_password_key', $user->user_login, $key );
     
        // Now insert the key, hashed, into the DB.
        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new PasswordHash( 8, true );
        }
        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

        $message = '       
        <h2>Bonjour '.$user->first_name.' '.$user->last_name.',</h2>
        <p>
            Ce message vous confirme que votre identifiant a bien été créé sur le site Cerib.com :
            <br><br>
            Identifiant : '.$user->user_login.'
            <br><br>
            Mot de passe : '.stripslashes($_POST["pass1"]).'
        </p>
         
        <p>
            Cordialement,
            <br><br>
            L\'Equipe du Cerib<br><br>
            <a href="'.$siteUrl.'">Cerib.com</a>
        </p>
        ';

        $headers = 'From: CERIB.com <webmaster@cerib.com>';
     
        wp_mail($user->user_email, sprintf(__('Cerib.com - Votre identifiant et votre mot de passe'), $blogname), $message, $headers);
    }
}

function wpmail_content_type() {
 
    return 'text/html';
}

//* Change the message/body of the email
add_filter( 'retrieve_password_message', 'rv_new_retrieve_password_message', 10, 2 );
function rv_new_retrieve_password_message( $message, $key ){
	// Bail if username or email is not entered
	if ( ! isset( $_POST['user_login'] ) )
		return;
	// Get user's data
	if ( strpos( $_POST['user_login'], '@' ) ) { # by email
		$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
	} else { # by username
		$login = trim( $_POST['user_login'] );
		$user_data = get_user_by( 'login', $login );
	}
	// Store some info about the user
	$user_fname = $user_data->user_firstname;
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
	// Assembled the URL for resetting the password
	$reset_url = add_query_arg(array(
		'action' => 'rp',
		'key' => $key,
		'login' => rawurlencode( $user_login )
	), wp_login_url() );
	// Create and return the message
	$message = sprintf( '<h2>%s</h2>', __( 'Bonjour ' ) );
	$message .= sprintf( '<p>%s</p>', __( 'Quelqu’un a demandé le renouvèlement de son mot de passe pour le compte suivant : '.home_url().'<br>' ) );
	$message .= sprintf( '<p>%s</p>', __( 'Identifiant : '.$user_login.' <br><br>S\'il s\'agit d\'une erreur, ignorez ce message et la demande ne sera pas prise en compte. Pour renouveler votre mot de passe, cliquez sur le lien suivant :' ) );
	$message .= sprintf( '<p><a href="%s">%s</a></p>', $reset_url, __( 'Réinitialiser mon mot de passe' ) );
	$message .= '<br>
	    <p>
            Cordialement,
            <br><br>
            L\'Equipe du Cerib<br><br>
            <a href="'.$siteUrl.'">Cerib.com</a>
        </p>';
	return $message;
}
