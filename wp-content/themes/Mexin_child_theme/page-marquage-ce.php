<?php
get_header();

GLOBAL $webnus_options;

$last_time = get_the_time(' F Y');


GLOBAL $webnus_page_options_meta;


$show_titlebar = null;
$titlebar_bg = null;
$titlebar_fg = null;
$have_sidebar = false;
$sidebar_pos = null;

$meta = $webnus_page_options_meta->the_meta();

if(!empty($meta)){
$show_titlebar =  isset($meta['webnus_page_options'][0]['show_page_title_bar'])?$meta['webnus_page_options'][0]['show_page_title_bar']:null;
$titlebar_bg =  isset($meta['webnus_page_options'][0]['title_background_color'])?$meta['webnus_page_options'][0]['title_background_color']:null;
$titlebar_fg =  isset($meta['webnus_page_options'][0]['title_text_color'])?$meta['webnus_page_options'][0]['title_text_color']:null;
$titlebar_fs =  isset($meta['webnus_page_options'][0]['title_font_size'])?$meta['webnus_page_options'][0]['title_font_size']:null;
$sidebar_pos =  isset($meta['webnus_page_options'][0]['sidebar_position'])?$meta['webnus_page_options'][0]['sidebar_position']:'right';
$have_sidebar = !( 'none' == $sidebar_pos )? true : false;

}
/*
if('hide' != $show_titlebar):
?>
<section id="headline" style="<?php if(!empty($titlebar_bg)) echo ' background-color:'.$titlebar_bg.';'; ?>">
    <div class="container">
      <h3 style="<?php if(!empty($titlebar_fg)) echo ' color:'.$titlebar_fg.';'; if(!empty($titlebar_fs)) echo ' font-size:'.$titlebar_fs.';';  ?>"><?php the_title(); ?></h3>
    </div>
</section>
<?php if( 1 == $webnus_options->webnus_enable_breadcrumbs() ) { ?>
      <div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<?php } ?>
<?php
endif;
*/
?>
<?php if (!is_front_page()): ?>
      <div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<?php endif; ?>
<?php if (!is_front_page()): ?>
<section id="main-content" class="container interne">
<?php else: ?>
<section id="main-content" class="container">
<?php endif; ?>
<!-- Start Page Content -->
<?php
/*
	LEFT SIDEBAR
*/

/*if( ('left' == $sidebar_pos) || ('both' == $sidebar_pos ) ) get_sidebar('left');*/
/*
if( $have_sidebar ) {
?>
<section class="<?php  echo('both' == $sidebar_pos )?'col-md-6 cntt-w':'col-md-9 cntt-w'; ?>">
	<article>
	
<?php 
}
*/	

/*

		<table id="table-nf">
			<tr class="row">
				<th class="small-3">Produit</th>
				<th class="small-3">Reference de la norme</th>
				<th class="small-3">Catégorie</th>
				<th class="small-2">Système d'attestation de conformité</th>
				<th class="small-1">Liste des Usines</th>
			</tr>
			<?php while($loopNF->have_posts()): $loopNF->the_post(); 
			
				//Récupération Type de Certification
				$preparedQuery = $wpdb->prepare("SELECT id_famille_produit FROM marquagece_certif WHERE id = %d", get_post_meta($post->ID, '_product', true));
				$idFamilleCertif = $wpdb->get_row($preparedQuery);
				$idFamilleCertif = $idFamilleCertif->id_famille_produit;
				
				$preparedQuery = $wpdb->prepare("SELECT nom FROM marquagece_famille_produit WHERE id = %d", $idFamilleCertif);
				$nameCertif = $wpdb->get_row($preparedQuery);
			
			?>
			<tr class="row">
				<td class="small-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
				<td class="small-3"><?php echo $nameCertif->nom; ?></td>
				<td class="small-3"></td>
				<td class="small-2"></td>
				<td class="small-1"><a href="<?php echo get_permalink(785); ?>?idCertif=<?php echo get_post_meta($post->ID, '_product', true) ?>"><i class="fa fa-2x fa-building"></i></a></td>
			</tr>
			<?php endwhile; ?>
		</table>
		
		<table id="table-nf">
			<tr class="row headTable">
				<th class="small-4">Nom Catégorie</th>
				<th class="small-4">Références de la norme</th>
				<th class="small-3">Système d'attestation de conformité</th>
				<th class="small-1">Liste des Usines</th>
			</tr>
			<?php while($loopNF->have_posts()): $loopNF->the_post(); 
			
				//Récupération Type de Certification
				$preparedQuery = $wpdb->prepare("SELECT id_famille_produit FROM marquagece_certif WHERE id = %d", get_post_meta($post->ID, '_product', true));
				$idFamilleCertif = $wpdb->get_row($preparedQuery);
				$idFamilleCertif = $idFamilleCertif->id_famille_produit;
				
				$preparedQuery = $wpdb->prepare("SELECT nom FROM marquagece_famille_produit WHERE id = %d", $idFamilleCertif);
				$nameCertif = $wpdb->get_row($preparedQuery);
			
			?>
			<tr class="row">
				<td class="small-4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
				<td class="small-4"></td>
				<td class="small-3"></td>
				<td class="small-1"><a class="link-usine" href="<?php echo get_permalink(785); ?>?idCertif=<?php echo get_post_meta($post->ID, '_product', true) ?>">Usines</a></td>
			</tr>
			<?php endwhile; ?>
		</table>

<!-- LOCAL --> <td class="small-1"><a href="<?php echo get_permalink(821); ?>?idCertif=<?php echo get_post_meta($post->ID, '_product', true) ?>"><i class="fa fa-2x fa-building"></i></a></td>
*/

global $wpdb;
global $post;

$args = array('post_type' => 'marquage', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC');
$loopNF = new WP_Query($args);


	echo '<div class="row-wrapper-x">';
		  /*if( $loopNF->have_posts() ): while( $loopNF->have_posts() ): $loopNF->the_post();
		  endwhile;
		  endif;*/
		  if(have_posts()): while(have_posts()): the_post();
			the_content();
		endwhile;
		endif;
		
		?>
		<?php
	echo '</div>';
wp_link_pages();
if ( comments_open() && ( $webnus_options->webnus_allow_comments_on_page() == 1) )

comments_template(); 

/*
		  if( $have_sidebar ) {
?>
	</article>
</section>	

<?php
}
*/
/*
	RIGHT SIDEBAR
*/

if( ('right' == $sidebar_pos) || ('both' == $sidebar_pos) ) get_sidebar('right');

?>

</section>

<?php get_footer(); ?>