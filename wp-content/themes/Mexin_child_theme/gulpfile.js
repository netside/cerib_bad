'use strict';

// Chargement des dépendances
var gulp        = require('gulp'),
	$           = require('gulp-load-plugins')(),
	browserSync = require('browser-sync');

// Définition des variables
var reload = browserSync.reload;
var path = {
	server:  '',
	css:    'css/',
	scss:   'scss/',
	images:  'assets/images/',
	js:      'assets/js/',
	refresh: ['**/*.html', '**/*.php']
}

// Supports navigateur
global.browser_support = [
	"ie >= 9",
	"ie_mob >= 10",
	"ff >= 30",
	"chrome >= 34",
	"safari >= 7",
	"opera >= 23",
	"ios >= 7",
	"android >= 4.4",
	"bb >= 10"
]

/**
 * STYLE
 * Task: $ gulp style
 * Compilation des fichiers scss dans le fichier main.css
 */
gulp.task('style', function() {
	return gulp.src( path.scss + '*.scss' )
		.pipe( $.plumber() )
		.pipe( $.sourcemaps.init() )
			.pipe( $.sass({ outputStyle: 'compressed' }) )
			.on( 'error', $.sass.logError )
			.pipe( $.autoprefixer({ browsers : browser_support }) )
		.pipe( $.sourcemaps.write('maps/', { addComment: false }) )
		.pipe( gulp.dest( path.css ) )
		.pipe( reload({ stream: true }) );
});
/**
 * SCRIPT
 * Task : $ gulp script
 * Compilation et minification des fichiers du dossier 'plugins' et du fichier main.js dans le main.min.js
 */
gulp.task('script', function() {
	return gulp.src([
			path.js + 'plugins/*.js',
			path.js + 'main.js'
		])
		.pipe( $.plumber() )
		.pipe( $.concat('main.min.js') )
		.pipe( $.uglify() )
		.pipe( gulp.dest( path.js ) )
		.pipe( reload({ stream:true }) );
});

/**
 * BUILD
 * Task : $ gulp build
 * Éxécution des taches style & script
 */
gulp.task('build', ['style']);

/**
 * WATCH
 * Task : $ gulp watch
 * Ecoute les changements dans les fichier scss et js
 */
gulp.task('watch', ['build'], function() {
	gulp.watch( path.refresh, reload );
	gulp.watch( path.scss + '**/*.scss', ['style'] );
});

/**
 * REV
 * Versionning des assets
 */
gulp.task('rev', function() {
	gulp.src('inc/init.inc.php')
	.pipe( $.wpRev({
		css:       path.css + 'main.css',
		cssHandle: 'main_css',
		js:        path.js + '/main.min.js',
		jsHandle:  'main_script_min'
	}))
	.pipe( gulp.dest('inc/') )
});

/**
 * SERVE
 * Task : $ gulp serve
 * Lance un serveur en local, écoute les changements et actualise le navigateur
 */
gulp.task('serve', ['watch'], function() {
	if ( path.server.lenght >= 0 ) {
		browserSync.init({ server: { baseDir:  './' }});
	}
	else {
		browserSync.init({
			proxy: path.server,
			notify: false
		});
	}
});
