<?php

/***************************************/
/*			Globalization $woptions
/***************************************/


GLOBAL $webnus_options;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
?><!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="<?php bloginfo('charset');?>">
	<title><?php

	/***************************************/
	/*			Title Generation Process
	/***************************************/
	if(is_plugin_active('all-in-one-seo-pack/all_in_one_seo_pack.php'))
	{

		wp_title( '|', true, 'right' );

	}elseif(is_plugin_active('wordpress-seo/wp-seo.php'))
	{

		wp_title( '|', true, 'right' );

	}else{

		global $page, $paged;

		wp_title( '|', true, 'right' );

		bloginfo( 'name' );

		$site_description = get_bloginfo( 'description', 'display' );

		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page' ,WEBNUS_TEXT_DOMAIN) . ' %s', max( $paged, $page ) );
	}
		/***************************************/
	/*			End Title Generation Process
	/***************************************/


?></title>
<?php

if(is_plugin_inactive('all-in-one-seo-pack/all_in_one_seo_pack.php') && is_plugin_inactive('wordpress-seo/wp-seo.php'))
{
?>
	<meta name="description" content="<?php
	/***************************************/
	/*			Description Meta Tag Generator
	/***************************************/

		$page_seo_meta = false;
		global $page_seo_meta;

		$seo_meta = !empty($page_seo_meta)?$page_seo_meta->the_meta():null;

		if( !empty( $seo_meta ) && !empty( $seo_meta['webnus_seo_options'][0]['seo_desc'] ) )
		{

			echo($seo_meta['webnus_seo_options'][0]['seo_desc']);

		}else{


			if ( is_single() ) {

				single_post_title('', true);

			} else {

				bloginfo('name'); echo " - "; bloginfo('description');

			}


		}

/***************************************/
/*			End Description Meta Tag Generator
/***************************************/

?>">
<meta name="keywords" content="<?php

/***************************************/
/*			Keywords Meta Tag Generator
/***************************************/

	global $page_seo_meta;

	$seo_meta = !empty($page_seo_meta)?$page_seo_meta->the_meta():null;

	if( !empty($seo_meta) && !empty($seo_meta['webnus_seo_options'][0]['seo_keyword']) )
	{

		echo($seo_meta['webnus_seo_options'][0]['seo_keyword']);

	}

/***************************************/
/*			End Keywords Meta Tag Generator
/***************************************/


?>" />
<?php } //end of all in one seo installation check ?>
<meta name="author" content="<?php

if( !is_single() )
	echo get_bloginfo('name');
else {
	global $post;

	if(isset($post) && is_object($post))
	{


		$flname = get_the_author_meta('first_name',$post->post_author). ' ' . get_the_author_meta('last_name',$post->post_author);
		$flname = trim($flname);
		if (empty($flname))
			the_author_meta('display_name',$post->post_author);
		else
			echo $flname;

	}

}


?>">


	<!-- Mobile Specific Metas
  ================================================== -->
<?php

$responsive = $webnus_options->webnus_enable_responsive();

if(1 == $responsive){
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php } else { ?>
<meta name="viewport" content="width=1200,user-scalable=yes">
<?php } ?>


	<!-- Modernizer
  ================================================== -->

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.11889.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/respond.js" type="text/javascript"></script>
	<![endif]-->
		<!-- HTML5 Shiv events (end)-->
	<!-- MEGA MENU -->


	<!-- Favicons
  ================================================== -->
<?php if($webnus_options->webnus_fav_icon()): ?>
<link rel="shortcut icon" href="<?php echo $webnus_options->webnus_fav_icon(); ?>">
<?php else: ?>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
<?php endif; ?>

	<!-- CSS + JS
  ================================================== -->
<?php wp_head();  ?>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/select-multiple.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo get_stylesheet_directory_uri(); ?>/styles-certification.css" media="screen" rel="stylesheet" type="text/css">
</head>

<?php
/** Codes for transparent header **/
$transparent_header = '';
if( is_page())
{
GLOBAL $webnus_page_options_meta;
$transparent_header_meta = isset($webnus_page_options_meta)?$webnus_page_options_meta->the_meta():null;
$transparent_header =(isset($transparent_header_meta) && is_array($transparent_header_meta) && isset($transparent_header_meta['webnus_page_options'][0]['webnus_transparent_header']))?$transparent_header_meta['webnus_page_options'][0]['webnus_transparent_header']:null;
}
$transparent_header_class = ($transparent_header=='light')?'transparent-header-w':'';
$transparent_header_class .= ($transparent_header=='dark')?'transparent-header-w t-dark-w':'';

$postshow_class='';
if (is_single()){
		global $blogpost_meta;
		$post_meta = $blogpost_meta->the_meta();
			if(!empty($post_meta)){
				if($post_meta['style_type']=="postshow1" && $thumbnail_id = get_post_thumbnail_id()){
					$postshow_class = "postshow1-hd transparent-header-w";
				} elseif ( $post_meta['style_type']=="postshow2" && $thumbnail_id = get_post_thumbnail_id() ) {
					$postshow_class = "postshow2-hd";
				}
			}
		}
?>






<body>
