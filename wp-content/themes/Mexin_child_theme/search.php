<?php
get_header(); ?>
<?php
# utilisateur
global $current_user;
get_currentuserinfo();
$roleUtilisateur = get_user_role();

# langue
$currentLang =  get_locale();
if ($currentLang == "fr_FR") {
    $accueil = "Accueil";
    $recherche = "Recherche";
    $titre = "Résultats de recherche pour :";
} else {
    $accueil = "Home";
    $recherche = "Search";
    $titre = "Search results for:";
}
?>
<div class="breadcrumbs-w"><div class="container"><div id="crumbs"><a href="<?php echo home_url('/'); ?>"><?php echo $accueil; ?></a> &gt; <span class="current"><?php echo $recherche; ?></span></div></div></div>
<section class="blox   bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
    <div class="max-overlay" style="background-color:"></div>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="container search-results">
            <div class="vc_col-sm-12 bloc-content-full wpb_column vc_column_container white-bg">
                <div class="wpb_wrapper">
                    <div class="wpb_row vc_row-fluid full-row">
                        <div class="vc_col-sm-12">
                            <h1><?php echo $titre; ?> <strong><?php printf( '%s', get_search_query() ); ?></strong></h1>
                        </div>
                    </div>
                    <div class="wpb_row vc_row-fluid full-row">
                        <div class="vc_col-sm-8 search-results-main">
                         <?php
                    	 if(have_posts()):
                    		while( have_posts() ): the_post();
                    		
                        		# accès extranet
                                $acces = 1;
                                $extranet = get_post_meta ( get_the_ID(), '_wpac_is_members_only', true);
                                if ($extranet == "") {
                                    $acces = 1;
                                } else if ($extranet == "true" && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                                    $acces = 1;
                                } else {
                                    $acces = 0;
                                }
                                if ($acces == 1) {
                    			    get_template_part('parts/blogloop','search');
                                }
                                
                    		endwhile;
                    	 else:
                    		get_template_part('parts/blogloop-none');
                    	 endif;
                    	 ?>
                           
                          <br class="clear">
                          <?php 
                    		if(function_exists('wp_pagenavi'))
                    		{
                    			wp_pagenavi();
                    		}
                    	  ?>
                          <div class="white-space"></div>
                        </div>
                        <div class="vc_col-sm-4 search-results-ged">
                          <?php echo do_shortcode( '[resultats_ged]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>