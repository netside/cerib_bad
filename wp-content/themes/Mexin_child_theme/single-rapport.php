 <?php
/******************/
/**  Single Post
/******************/
?>
<?php
# accès extranet
# utilisateur
global $current_user;
get_currentuserinfo();
$roleUtilisateur = get_user_role();

# langue
$currentLang =  get_locale();
if ($currentLang == "fr_FR") {
    $retourListe = "Retour à la liste";
    $telecharger = "Télécharger";
    $keywordss = "Mots-clés :";
    $partager = "Partager";
    $par = "Par";
    $urlRetour = "http://www.cerib.com/rapport/";
} else {
    $retourListe = "Back to the list";
    $telecharger = "Download";
    $keywordss = "Keywords:";
    $partager = "Share";
    $par = "By";
    $urlRetour = "http://www.cerib.com/rapport/?lang=en";
}

$acces = 1;
$extranet = rwmb_meta('rapport_extranet');
if ($extranet == 0) {
    $acces = 1;
} else if ( @$extranet == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
    $acces = 1;
} else {
    $acces = 0;
}
if ($acces == 1):
    ?>
    <?php
    get_header();
    if (is_single()){
    	global $blogpost_meta;
    	$post_meta = $blogpost_meta->the_meta();
    	if(!empty($post_meta)){
    		if($post_meta['style_type']=="postshow1" && $thumbnail_id = get_post_thumbnail_id()){
    			$background = wp_get_attachment_image_src( $thumbnail_id, 'full' ); ?>
    			<div class="postshow1" style="background-image: url(<?php echo $background[0]; ?> );">
    				<div class="postshow-overlay"></div>
    				<div class="container"><h1 class="post-title-ps1"><?php the_title() ?></h1></div>
    			</div> <?php
    		}
    	}
    } ?>
    <div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
    <!-- Start Page Content -->
    <div class="row-wrapper-x">
    	<div class="vc_col-md-12 wpb_column vc_column_container ">
    		<div class="wpb_wrapper">
    			
    	<div class="wpb_text_column wpb_content_element ">
    		<div class="wpb_wrapper">
    			
    		</div> 
    	</div> 
    		</div> 
    	</div> 
    </div></section>
    <section id="carousel-interne" class="wpb_row vc_row-fluid full-row">
    	<div class="vc_col-md-12 wpb_column vc_column_container ">
    		<div class="wpb_wrapper">            
                <?php echo do_shortcode( '[layerslider id="24"]' ); ?>
    		</div> 
    	</div> 
    </section>
    <section class="blox bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
        <div class="max-overlay" style="background-color:"></div>
        <div class="wpb_row vc_row-fluid full-row">
            <div class="container">
                <div class="vc_col-md-3 bloc-sidebar-left wpb_column vc_column_container">
                    <aside class="col-md-3 sidebar leftside">
                        <?php 
                        dynamic_sidebar( 'Rapport' );
                        ?>
                		<?php if( 1 == $webnus_options->webnus_blog_social_share() ) { ?>	
                		<div class="widget widget-full">
                    		<h4 class="subtitle"><?php echo $partager; ?></h4>
                    		<?php
                            $linkS = urlencode(get_the_permalink());
                            $titleS = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
                            ?>
                    		<ul class="social-share-icons">
                        		<li><a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $linkS ?>&amp;t=<?php echo $titleS ?>" target="blank"><i class="fa-facebook"></i></a></li>
                        		<li><a class="google" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php echo $linkS ?>" target="_blank"><i class="fa-google"></i></a></li>
                        		<li><a class="twitter" href="https://twitter.com/intent/tweet?original_referer=<?php echo $linkS ?>&amp;text=<?php echo $titleS ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?><?php echo isset( $twitter_user ) ? '&amp;via='.$twitter_user : ''; ?>" target="_blank"><i class="fa-twitter"></i></a></li>
                        		<li><a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $linkS ?>&amp;title=<?php echo $titleS ?>&amp;source=<?php bloginfo( 'name' ); ?>" target="blank"><i class="fa-linkedin"></i></a></li>
                        		<li><a class="email" href="mailto:?subject=<?php echo get_the_title() ?>&amp;body=<?php echo $linkS ?>"><i class="fa-envelope"></i></a></li>
                    		</ul>
                		</div>
                		<?php } ?>
            		</aside>
                </div>
                <div class="vc_col-md-9 wpb_column vc_column_container">
                    <div class="wpb_wrapper">
                        <div class="wpb_tabs wpb_content_element  onglet-nombre-4" data-interval="0">
                            <div class="wpb_wrapper webnus-tabs wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">
                                <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                    <?php if ($currentLang == "fr_FR") { ?>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-etudes-recherches" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/etudes-recherches/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Études &amp; Recherches</a></li>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-2015" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/programme/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">Programme E&amp;R</a></li>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-doctoral" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/theses/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">Thèses</a></li>
                                    <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tab-rapports-er" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true"><a href="http://www.cerib.com/rapport/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">Rapports E&amp;R</a></li>
                                    <?php } else { ?>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-etudes-recherches" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/development-and-projects/research-development/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Research & Development</a></li>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-2015" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/development-and-projects/rd-programme/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">R&D programme</a></li>
                                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-doctoral" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/development-and-projects/topics/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">Topics</a></li>
                                    <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tab-rapports-er" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true"><a href="http://www.cerib.com/rapport/?lang=en" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">R&D reports</a></li>
                                    <?php } ?>
                                </ul>
                                <div id="tab-rapports-er" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="false" style="display: block;">
    
                                    <div class="wpb_row vc_row-fluid full-row">
                                        <div class="col-md-6 rapport-retour-liste">
                                            <a href="<?php echo $urlRetour; ?>"><i class="fa-chevron-left"></i> <?php echo $retourListe; ?></a>
                                        </div>
                                        <div class="col-md-6 rapport-telecharger-liste">
                                            <a href="<?php echo do_shortcode('[download url="'.rwmb_meta('rapport_documentged').'"]'); ?>" target="_blank"><?php echo $telecharger; ?> <i class="fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                    <hr class="vertical-space">
                    		<?php if( have_posts() ): while( have_posts() ): the_post();  ?>
                          <article class="blog-single-post row-rapport">
                    	  
                    		<?php
                    			
                    			setViews(get_the_ID());
                    			GLOBAL $webnus_options;
                    			
                    			
                    		$post_format = get_post_format(get_the_ID());
                    	
                    		$content = get_the_content(); ?>
                    		<div class="post-trait-w"> <?php
                    		if(!isset($background)) { ?>
                    		 <h4><?php the_title() ?></h4> <?php
                    		}
                    	if(  $webnus_options->webnus_blog_sinlge_featuredimage_enable() && !isset($background) ){
                    	
                    		global $featured_video;
                    		
                    		$meta_video = $featured_video->the_meta();
                    		
                    		if( 'video'  == $post_format || 'audio'  == $post_format)
                    		{
                    			
                    		$pattern =
                    		  '\\['                              // Opening bracket
                    		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                    		. "(video|audio)"                     // 2: Shortcode name
                    		. '(?![\\w-])'                       // Not followed by word character or hyphen
                    		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                    		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                    		.     '(?:'
                    		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                    		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                    		.     ')*?'
                    		. ')'
                    		. '(?:'
                    		.     '(\\/)'                        // 4: Self closing tag ...
                    		.     '\\]'                          // ... and closing bracket
                    		. '|'
                    		.     '\\]'                          // Closing bracket
                    		.     '(?:'
                    		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                    		.             '[^\\[]*+'             // Not an opening bracket
                    		.             '(?:'
                    		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                    		.                 '[^\\[]*+'         // Not an opening bracket
                    		.             ')*+'
                    		.         ')'
                    		.         '\\[\\/\\2\\]'             // Closing shortcode tag
                    		.     ')?'
                    		. ')'
                    		. '(\\]?)';  			
                    		
                    			
                    			preg_match('/'.$pattern.'/s', $post->post_content, $matches);
                    			
                    			
                    			if( (is_array($matches)) && (isset($matches[3])) && ( ($matches[2] == 'video') || ('audio'  == $post_format)) && (isset($matches[2])))
                    			{
                    				$video = $matches[0];
                    				
                    				echo do_shortcode($video);
                    				
                    				$content = preg_replace('/'.$pattern.'/s', '', $content);
                    				
                    			}else				
                    			if( (!empty( $meta_video )) && (!empty($meta_video['the_post_video'])) )
                    			{
                    				echo do_shortcode($meta_video['the_post_video']);
                    			}
                    				
                    	
                    			
                    			
                    		}else
                    		if( 'gallery'  == $post_format)
                    		{
                    			
                    						
                    			$pattern =
                    		  '\\['                              // Opening bracket
                    		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                    		. "(gallery)"                     // 2: Shortcode name
                    		. '(?![\\w-])'                       // Not followed by word character or hyphen
                    		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                    		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                    		.     '(?:'
                    		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                    		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                    		.     ')*?'
                    		. ')'
                    		. '(?:'
                    		.     '(\\/)'                        // 4: Self closing tag ...
                    		.     '\\]'                          // ... and closing bracket
                    		. '|'
                    		.     '\\]'                          // Closing bracket
                    		.     '(?:'
                    		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                    		.             '[^\\[]*+'             // Not an opening bracket
                    		.             '(?:'
                    		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                    		.                 '[^\\[]*+'         // Not an opening bracket
                    		.             ')*+'
                    		.         ')'
                    		.         '\\[\\/\\2\\]'             // Closing shortcode tag
                    		.     ')?'
                    		. ')'
                    		. '(\\]?)';  			
                    
                    			preg_match('/'.$pattern.'/s', $post->post_content, $matches);
                    			
                    			
                    			if( (is_array($matches)) && (isset($matches[3])) && ($matches[2] == 'gallery') && (isset($matches[2])))
                    			{
                    				
                    				$atts = shortcode_parse_atts($matches[3]);
                    				
                    				$ids = $gallery_type = '';
                    				
                    				if(isset($atts['ids']))
                    				{
                    					$ids = $atts['ids'];
                    				}
                    				if(isset($atts['webnus_gallery_type']))
                    				{
                    					$gallery_type = $atts['webnus_gallery_type'];
                    				}
                    
                    					echo do_shortcode('[vc_gallery img_size= "full" type="flexslider_fade" interval="3" images="'.$ids.'" onclick="link_image" custom_links_target="_self"]');
                    				
                    				$content = preg_replace('/'.$pattern.'/s', '', $content);
                    			}
                    				
                    	
                    			
                    			
                    		}else
                    		if( (!empty( $meta_video )) && (!empty($meta_video['the_post_video'])) )
                    		{
                    			echo do_shortcode($meta_video['the_post_video']);
                    		}
                    		else
                    			get_the_image( array( 'meta_key' => array( 'Full', 'Full' ), 'size' => 'Full' ) ); 
                    	}
                    			
                    		?>
                    
                    		</div>
                    	    <div <?php post_class('post'); ?>>
                              <?php
                        		# auteur
                                $arrAllAut = get_the_terms(get_the_ID(), 'rapport_auteur', 'fields=names');
                                $arrAut = array();
                                if ($arrAllAut[0]->name != "") foreach ($arrAllAut as $aut) $arrAut[] = $aut->name;
                                # numéro
                                $arrAllNum = get_the_terms(get_the_ID(), 'rapport_numero', 'fields=names');
                                $arrNum = array();
                                if ($arrAllNum[0]->name != "") foreach ($arrAllNum as $num) $arrNum[] = $num->name;
                                # tags
                                $arrAllTags = get_the_terms(get_the_ID(), 'rapport_tag', 'fields=names');
                                $arrTags = array();
                                if ($arrAllTags[0]->name != "") foreach ($arrAllTags as $tag) $arrTags[] = $tag->name;
                                ?>
                               <div class="postmetadata">
                        			<h6 class="blog-time">
                            			<?php if ($currentLang == "fr_FR") { ?>
                            			    <?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?> 
                            			 <?php } else { ?>
                            			    <?php the_time('m') ?>/<?php the_time('d') ?>/<?php the_time('Y') ?> 
                            			 <?php } ?>
                                    </h6>
                        			<?php if ($arrAllNum[0]->name != "") { ?><h6 class="blog-number"><strong>réf. </strong> <?php echo implode(", ", $arrNum); ?> </h6><?php } ?>
                        			<?php if ($arrAllAut[0]->name != "") { ?><h6 class="blog-author"><strong><?php echo $par; ?> </strong> <?php echo implode(", ", $arrAut); ?> </h6><?php } ?>
                        	  </div>
                              
                              
                    		    <a href="<?php echo rwmb_meta('rapport_documentged'); ?>" target="_blank">
                    		        <img src="http://admin.verticrawl.com/search/screenshot.php?DATABASE=3f4e781dbe09a35fcfbfb0c08de85684&amp;url=<?php echo rwmb_meta('rapport_documentged'); ?>" align="right" hspace="20">
                    		    </a>
                    			<?php 
                    			
                    			if( 'quote' == $post_format  ) echo '<blockquote>';
                    			echo apply_filters('the_content',$content); 
                    			if( 'quote' == $post_format  ) echo '</blockquote>';
                    			
                    			?>
                    			<?php if ($arrAllTags[0]->name != "") { ?><strong><?php echo $keywordss; ?></strong> <?php echo implode(", ", $arrTags); ?><?php } ?>
                              <br class="clear">
    
                            </div>
                    		<?php 
                    		
                    		  ?>
                    		
                          </article>
                          <?php 
                           endwhile;
                    		 endif;
                          ?>
                            <!-- end-main-conten -->
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
    get_footer();
else:
    ?>
    <script>
        window.location.href = '<?php echo home_url(); ?>';
    </script>
    <?php
    exit;
endif;
?>