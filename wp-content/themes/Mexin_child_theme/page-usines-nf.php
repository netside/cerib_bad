<?php

require_once dirname(__FILE__) . "/lib/domxml-php4-to-php5.php";
require_once dirname(__FILE__).'/lib/htmlheader.php';
require_once dirname(__FILE__).'/lib/xml.inc.php';
error_reporting(0);

//Custom Class
/*class Usine
{
	private $products;
	
	public function __construct()
	{
		$this->products = array();
	}
	
	public function add_product($idProduct)
	{
		array_push($this->products, $idProduct);
	}
	
	public function getArrayProduct()
	{
		return $this->products;
	}
	
	public function getProductId($i)
	{
		return $this->products[$i];
	}
}

$pdfDir = WP_CONTENT_URL . "/pdf/";

//Custom Functions
function count_node($node){
	$i=0;
	foreach ($node->child_nodes() as $cnode){
		if ($cnode->node_name() != "#text")
			$i++;                
		$cnode = $cnode->next_sibling();
	}
	return $i;
}

function setHtmlTableHeader($node, $nodeRupture, $famille, $numRow, $niv = 0) {
	global $tNumCrt, $tCrtVisible, $sRemarque;
	$sHeader = "";
	$sHeader2 = "";
	$countNode=0;
	foreach ($node->child_nodes() as $cnode){
		if ($cnode->node_type()==XML_TEXT_NODE){
			if ($niv && !trim(utf8_decode($cnode->node_value()))){
				$sHeader .= "";
				$sHeader2 .= "";
			} else {
				$sHeader .= trim(utf8_decode($cnode->node_value()));
				$sHeader2 .= trim(utf8_decode($cnode->node_value()));
			}
		} else if ($cnode->node_type()==XML_ELEMENT_NODE) {
			if ($cnode->node_name() !== "remarque"){
				$bIsVisible = true;
				if ($famille != $cnode->node_name() && $cnode->has_attributes()){
					$tNumCrt[] = $cnode->get_attribute("num");
					
					$tCrtVisible[] = true;                    
					if ($cnode->get_attribute("visible")=="false"){
						$bIsVisible = false;
						$tCrtVisible[sizeOf($tNumCrt)-1] = false;
					}
				}
					
				if ($bIsVisible) {
					$nodeText = setHtmlTableHeader($cnode, $nodeRupture, $famille, $numRow, $niv+1);
					
					if ($niv==1 && $famille == $cnode->node_name()) {
						$countNode = count_node($cnode->parent_node())-1;
						$sHeader .= "<td colspan='$countNode'>".trim($nodeText)."</td>\r\n";
					} else if ($niv==1){
						$sHeader2 .= "<td>".trim($nodeText)."</td>\r\n";                    
					} else if ($cnode->node_name() != $nodeRupture) {
						$sHeader .= "<td rowspan='2'>".trim($nodeText)."</td>\r\n";
					} else {
						$sHeader .= trim($nodeText);
						$sHeader2 .= trim($nodeText);
					}
				}
			} else {
				$nodeText = setHtmlTableHeader($cnode, $nodeRupture, $famille, $numRow, $niv+1);
				$sRemarque = trim($nodeText);
			}
			
		}
	}
	if ($numRow==0)
		return $sHeader;
	else
		return $sHeader2;
}

function setHtmlForm($node, $sKey, $sType = null, $sName = null) {    
	$sForm = "";
	//foreach ($node->child_nodes() as $cnode){
	$nodeChild = $node->first_child();
	while ($nodeChild) {
		if ($nodeChild->node_type()==XML_ELEMENT_NODE) {
		
			// on chope le type et on passe sur le parent
			if ($nodeChild->node_name() == "champs"){
				$sType = $nodeChild->get_attribute('type');
				$sNumCrt = $nodeChild->get_attribute('num_crt');
				$iNbFils = count_node($nodeChild)-1;
				// on pointe sur les fils
				$nodeChildChild = $nodeChild->first_child();
				$i=0;
				$isLibelle = false;
			}
			$fieldName = null;
			while ($nodeChildChild) {
				if ($nodeChildChild->node_type()==XML_ELEMENT_NODE) {
					if ($nodeVal = $nodeChildChild->first_child())
						$nodeText = html_entity_decode(utf8_decode($nodeVal->node_value()));
					$nodeName = $nodeChildChild->node_name();
					
					// libelle
					if ($nodeName == "libelle"){
					
						$sForm .= "\r\n
						<tr>
							<td><b>".$nodeText."</b></td>";
						$fieldName = $nodeText;
						$isLibelle = true;
						
					} else if ( ($sType == "LST" || $sType == "LST-EXP") && $nodeName == "valeur" ){
					
						if (!$i)
							$sForm .= "<td class='normal'>\r\n<select name=\"".$sType."_".$sNumCrt."\" class='normal'><option value=''></option>\r\n";
						$sForm .= "\t<option value=\"".$nodeText."\">".$nodeText."</option>\r\n";
						$i++;
						if ($i==$iNbFils)
							$sForm .= "</select></td></tr>\r\n";
							
					} else if ($sType == "CHK" && $nodeName == "valeur" ){
					
						$sNumCrt = $nodeChildChild->get_attribute('num_crt');
						$sValueCrt = "X";
						if (!$sNumCrt){
							$sNumCrt = $nodeChild->get_attribute('num_crt');
							$sValueCrt = $nodeText;
						}
						if ($fieldName) {
							if ($isLibelle)
								$sForm .= "<td class='normal'>".$nodeText."<input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
							else
								$sForm .= "<tr><td>&nbsp;</td><td class='normal'>".$nodeText."<input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
							$isLibelle = false;
						} else
							$sForm .= "<tr><td colspan='2' align='right' class='normal'>".$nodeText."<input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
						
					} else if ($sType == "RBTN" && $nodeName == "valeur" ){
					
						$sNumCrt = $nodeChildChild->get_attribute('num_crt');
						if ($isLibelle && !$i)
							$sForm .= "<td class='normal'>";
						else if (!$i)
							$sForm .= "<tr><td>&nbsp;</td><td class='normal'>";
						
						$sForm .= "<input type='radio' name='RBTN_".$nodeChild->get_attribute('num_crt')."' value='$sNumCrt'>".$nodeText;
						$isLibelle = false;
						$i++;
						if ($i==$iNbFils)
							$sForm .= "</td></tr>\r\n";
						
					} else
						$sForm .= $nodeText;
				}
				$nodeChildChild = $nodeChildChild->next_sibling();
			}
			
		}
		$nodeChild = $nodeChild->next_sibling();
	}
	return $sForm;
}*/

if(!isset($_GET["idCertif"]) && empty($_GET["idCertif"]))
{
	wp_redirect(home_url());
	exit;
}
else
{
	global $wpdb;
	global $post;
	
	$wpdb->show_errors();
				
	$idCertif = $_GET['idCertif'];
	
	$preparedQuery = $wpdb->prepare("SELECT id_famille_certif FROM qlx_certif WHERE id = %s", $idCertif);
	$idFamilleCertif = $wpdb->get_row($preparedQuery);
	
	$idFamilleCertif = $idFamilleCertif->id_famille_certif;
				
	$tablePostTypeIdName = $wpdb->prefix . "uepc_post_type_id";
	$preparedQuery = $wpdb->prepare("SELECT id_post_type FROM $tablePostTypeIdName WHERE type_produit = 'nf' AND id_certif = '%d'", $idFamilleCertif);
	$idPost = $wpdb->get_row($preparedQuery);
	
	$sSQL = "SELECT qlx_certif.id, qlx_certif.nom, count(distinct QLX_nnepc.id_etablissements) as nbUsine FROM 
		 qlx_certif, qlx_nn_etablissements_produits_certif as QLX_nnepc 
		 WHERE 
		 qlx_certif.id_famille_certif = $idFamilleCertif AND 
		 QLX_nnepc.id_certif = qlx_certif.id 
		 GROUP BY qlx_certif.nom";
		 
	$listUsines = $wpdb->get_results($sSQL);
	
	$nameProduit = '';
	
	if(count($listUsines) > 1)
		foreach($listUsines as $certifObj)
			if($certifObj->id == $idCertif)
			{
				$nameProduit = ' - '.$certifObj->nom;
				break;
			}
	
	
	if(is_null($idPost))
	{
		wp_redirect(home_url());
		exit();
	}
}

get_header();

$pdfDir = WP_CONTENT_URL . "/pdf/";
$pdfDirFile = WP_CONTENT_DIR . "/pdf/";

GLOBAL $webnus_options;

$last_time = get_the_time(' F Y');


GLOBAL $webnus_page_options_meta;


$show_titlebar = null;
$titlebar_bg = null;
$titlebar_fg = null;
$have_sidebar = false;
$sidebar_pos = null;

$meta = $webnus_page_options_meta->the_meta();

if(!empty($meta)){
$show_titlebar =  isset($meta['webnus_page_options'][0]['show_page_title_bar'])?$meta['webnus_page_options'][0]['show_page_title_bar']:null;
$titlebar_bg =  isset($meta['webnus_page_options'][0]['title_background_color'])?$meta['webnus_page_options'][0]['title_background_color']:null;
$titlebar_fg =  isset($meta['webnus_page_options'][0]['title_text_color'])?$meta['webnus_page_options'][0]['title_text_color']:null;
$titlebar_fs =  isset($meta['webnus_page_options'][0]['title_font_size'])?$meta['webnus_page_options'][0]['title_font_size']:null;
$sidebar_pos =  isset($meta['webnus_page_options'][0]['sidebar_position'])?$meta['webnus_page_options'][0]['sidebar_position']:'right';
$have_sidebar = !( 'none' == $sidebar_pos )? true : false;

}
/*
if('hide' != $show_titlebar):
?>
<section id="headline" style="<?php if(!empty($titlebar_bg)) echo ' background-color:'.$titlebar_bg.';'; ?>">
    <div class="container">
      <h3 style="<?php if(!empty($titlebar_fg)) echo ' color:'.$titlebar_fg.';'; if(!empty($titlebar_fs)) echo ' font-size:'.$titlebar_fs.';';  ?>"><?php the_title(); ?></h3>
    </div>
</section>
<?php if( 1 == $webnus_options->webnus_enable_breadcrumbs() ) { ?>
      <div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<?php } ?>
<?php
endif;
*/
?>
<?php if (!is_front_page()): ?>
      <!--<div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>-->
<?php endif; ?>
<?php if (!is_front_page()): ?>
<section id="main-content" class="container interne-cerfif">
<?php else: ?>
<section id="main-content" class="container">
<?php endif; ?>
<!-- Start Page Content -->
<?php
/*
	LEFT SIDEBAR
*/

if( ('left' == $sidebar_pos) || ('both' == $sidebar_pos ) ) get_sidebar('left');
/*
if( $have_sidebar ) {
?>
<section class="<?php  echo('both' == $sidebar_pos )?'col-md-6 cntt-w':'col-md-9 cntt-w'; ?>">
	<article>
	
<?php 
}
*/				 
	//Récupérer les colonnes
	$sSQLCertif = "SELECT * FROM qlx_certif WHERE id='%s'";
	$preparedQuery = $wpdb->prepare($sSQLCertif, $idCertif);
	$tCertif = $wpdb->get_row($preparedQuery, ARRAY_A);
	
	$tCertif['caracteristiques_xml'] = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>".$tCertif['caracteristiques_xml'];
            $domXml = domxml_open_mem($tCertif['caracteristiques_xml']);
            $tXml = $domXml->get_elements_by_tagname("caracteristiques");

			$remarque = $domXml->get_elements_by_tagname("remarque");



			if(!is_null($remarque[0]))
				$remarque = utf8_decode($remarque[0]->node_value());
			else
				$remarque = '';

            $sBufferLine = null;
            $t_numCrtKey = null;
			
            foreach ($tXml as $parentNode) {
				
                $nodeChild = $parentNode->first_child();

                $sBufferLine = setHtmlTableHeader($parentNode,'famille','libelle',0);
                $tNum = $tNumCrt;
                if ($sBufferLine)
                    $sBufferLine = "<tr valign='top'>".$sBufferLine."</tr>";
                $sBufferLine2 = setHtmlTableHeader($parentNode,'famille','libelle',1);
                //if ($sBufferLine2)
                    $sBufferLine .= "<tr valign='top'>".$sBufferLine2."</tr>";
            }
			
	//List Usines (A FILTRER AVEC DONNEES EN POST)
	
	if(isset($_POST) && !empty($_POST))
	{
		$sListVars = $_POST;
		$i=0;
		for ($g=0; $g < count($sListVars); $g++){
            list($sMyCrt, $sValue) = each($sListVars);
			
            if ( $sValue && ( substr_count($sMyCrt,"LST") || 
                 substr_count($sMyCrt,"RBTN") || 
                 substr_count($sMyCrt,"CHK") ) ) {
                //echo $sMyCrt." ".$sValue."<br>";
                $sParamDpt .= "&".$sMyCrt."=".urlencode(str_replace("\'","'",$sValue));
                $t_Crt = explode("_",$sMyCrt);
                if ($t_Crt[0]=="LST" || $t_Crt[0]=="CHK"){
                    $t_Critere[$i]['type'] = "";
                    $t_Critere[$i]['nbCrt']=0;
                    $t_Critere[$i]['num_crt']=$t_Crt[1];
                    $t_Critere[$i]['valeur'] = " AND (valeur = \"".str_replace(",",".",$sValue)."\" OR  valeur = \"".$sValue."\") ";
                    $t_Critere[$i]['val'] = $sValue;
                    $i++;
                } else if ($t_Crt[0]=="RBTN"){
                    $t_Critere[$i]['type'] = "RBTN";
                    $t_Critere[$i]['nbCrt']=0;
                    $t_Critere[$i]['num_crt']=$sValue;
                    $t_Critere[$i]['valeur'] = " AND valeur = \"X\" ";
                    $t_Critere[$i]['val'] = "X";
                    $i++;
                } else if ($t_Crt[0]=="LST-EXP"){
                    $sValue = str_replace(",",".",$sValue);
                    if (substr_count($sValue," à ")){
                        $t_Critere[$i]['type'] = "LST-EXP_A";
                        $t_Critere[$i]['nbCrt']=0;
                        $t_Critere[$i]['num_crt']=$t_Crt[1];
                        $t_Critere[$i]['valeur'] = " AND valeur BETWEEN ".str_replace(" à "," AND ",$sValue)." ";
                        $t_Critere[$i]['val'] = $sValue;
                        $i++;
                    } else if (substr_count($sValue," ET ")){
                        $bModeSearch = 1;
                        $tVal = explode(" ET ",$sValue);
                        for ($j=0; $j<sizeOf($tVal); $j++){
                            $t_Critere[$i]['type'] = "LST-EXP_ET";
                            $t_Critere[$i]['nbCrt'] = sizeOf($tVal);
                            $t_Critere[$i]['num_crt'] = $t_Crt[1];
                            $t_Critere[$i]['valeur'] = " AND QLX_val.valeur = \"".$tVal[$j]."\" ";
                            $t_Critere[$i]['val'] = $sValue;
                            $i++;
                        }
                    } else if (substr_count($sValue," OU ")){
                        $bModeSearch = 1;
                        $tVal = explode(" OU ",$sValue);
                        for ($j=0; $j<sizeOf($tVal); $j++){
                            $t_Critere[$i]['type'] = "LST-EXP_OU";
                            $t_Critere[$i]['nbCrt'] = sizeOf($tVal);
                            $t_Critere[$i]['num_crt'] = $t_Crt[1];
                            $t_Critere[$i]['valeur'] = " AND QLX_val.valeur = \"".$tVal[$j]."\" ";
                            $t_Critere[$i]['val'] = $sValue;
                            $i++;
                        }
                    } else {
                        $t_Critere[$i]['type'] = "LST";
                        $t_Critere[$i]['nbCrt'] = 0;
                        $t_Critere[$i]['num_crt'] = $t_Crt[1];
                        $t_Critere[$i]['valeur'] = " AND valeur = \"".$sValue."\" ";
                        $t_Critere[$i]['val'] = $sValue;
                        $i++;
                    }
                }
            }
        }
		
		$sSqlAdd = null;
        
        if (isset($t_Critere) && !empty($t_Critere)) {
            $sNumCrt = sizeOf($t_Critere);
            $sSqlAdd = null;
			
			//var_dump($sNumCrt);
            
            // on est en recherche avec une expréssion régulière dans un des critères
            if ($bModeSearch) {
                $sSqlCrtUsineAdd = null;
                $sFrom = null;
                $sWhere = null;
                for ($j=0; $j<$sNumCrt; $j++){
                    if ($t_Critere[$j]['type']!="LST-EXP_ET" && $t_Critere[$j]['type']!="LST-EXP_OU") {
                        $sFrom.=", qlx_valeurs as QLX_v".$j;
                        $sWhere.=" AND QLX_v".$j.".id_produits = QLX_val.id_produits AND QLX_v".$j.".num_crt = ".$t_Critere[$j]['num_crt']." AND QLX_v".$j.".valeur = \"".$t_Critere[$j]['val']."\"";
                    }
                }
                for ($j=0; $j<$sNumCrt; $j++){
                    if ($t_Critere[$j]['type']=="LST-EXP_ET" || $t_Critere[$j]['type']=="LST-EXP_OU") {
                        // on récupère tous les établissement correspondant au critères ( un élément de la lst-exp + les autres critère )
                        $sSqlUsine = "SELECT QLX_nnepc.id_etablissements
                        FROM qlx_nn_etablissements_produits_certif AS QLX_nnepc, qlx_valeurs as QLX_val $sFrom
                        WHERE QLX_val.num_crt  = ".$t_Critere[$j]['num_crt']." ".$t_Critere[$j]['valeur']." AND 
                        QLX_nnepc.id_produits = QLX_val.id_produits $sWhere
                        GROUP BY QLX_nnepc.id_etablissements";
                        //echo $sSqlUsine;
                        $resultUsine = $wpdb->get_results($sSqlUsine, ARRAY_A);
                        //while($myrowUsine=db_fetch_array($resultUsine)){
						for($r=0; $r < count($resultUsine); $r++){
							
							$myrowUsine = $resultUsine[$r];
                            
                            if ($t_Critere[$j]['type']=="LST-EXP_ET"){
                                if (isset($t_usine[$myrowUsine['id_etablissements']]))
                                    $t_usine[$myrowUsine['id_etablissements']]++;
                                else
                                    $t_usine[$myrowUsine['id_etablissements']]=1;
                            // si les établissements sont présent pour les n critères de la lst-exp
                                // on fabrique la liste des bons établissements
                                if ($t_usine[$myrowUsine['id_etablissements']] == $t_Critere[$j]['nbCrt'])
                                    $sSqlCrtUsineAdd .= (!$sSqlCrtUsineAdd) ? $myrowUsine['id_etablissements'] : ", ".$myrowUsine['id_etablissements'];
                            } else
                                $sSqlCrtUsineAdd .= (!$sSqlCrtUsineAdd) ? $myrowUsine['id_etablissements'] : ", ".$myrowUsine['id_etablissements'];
                                
                        }
                    }
                }
                // construction du critere de recherche pour la requete de récupération des valeurs
                if ($sSqlCrtUsineAdd){
                    $sSqlCrtUsineAdd = "AND QLX_nnepc.id_etablissements in ($sSqlCrtUsineAdd)";
            
                    for ($j=0; $j<$sNumCrt; $j++){
                        if ($t_Critere[$j]['type']=="LST-EXP_ET" || $t_Critere[$j]['type']=="LST-EXP_OU"){
                            $sSqlValeurs = "SELECT QLX_val.id_produits FROM 
                            qlx_valeurs as QLX_val, qlx_nn_etablissements_produits_certif as QLX_nnepc $sFrom 
                            WHERE QLX_val.num_crt = ".$t_Critere[$j]['num_crt']." ".$t_Critere[$j]['valeur']." AND 
                            QLX_nnepc.id_produits=QLX_val.id_produits $sSqlCrtUsineAdd $sWhere
                            ORDER BY QLX_val.id";
                            $resultValeurs = $wpdb->get_results($sSqlValeurs, ARRAY_A);
                            //while($myrow=db_fetch_array($resultValeurs))
							for($r=0; $r < count($resultValeurs); $r++)
							{
								$myrow = $resultValeurs[$r];
								
                                $sSqlAdd .= (!$sSqlAdd) ? $myrow['id_produits'] : ", ".$myrow['id_produits'];	
							}
                        }
                    }
                }
            // sinon, recherche simple
            } else {
                for ($j=0; $j<$sNumCrt; $j++){
                    
                    $sSqlValeurs = "SELECT qlx_valeurs.id_produits FROM 
                    qlx_valeurs, qlx_nn_etablissements_produits_certif as QLX_nnepc WHERE 
                    num_crt = ".$t_Critere[$j]['num_crt']." ".$t_Critere[$j]['valeur']." AND 
                    QLX_nnepc.id_produits=qlx_valeurs.id_produits ORDER BY id";
					
                    $resultValeurs = $wpdb->get_results($sSqlValeurs, ARRAY_A);
					for($r=0; $r < count($resultValeurs); $r++)
					{
						$myrow = $resultValeurs[$r];
						
						$t_Prod[$myrow['id_produits']] = (isset($t_Prod[$myrow['id_produits']])) ? $t_Prod[$myrow['id_produits']]+1 : 1;
                        
                        if ($j==($sNumCrt-1)) {
                            if ( ($t_Prod[$myrow['id_produits']]==$sNumCrt))
                                $sSqlAdd .= (!$sSqlAdd) ? $myrow['id_produits'] : ", ".$myrow['id_produits'];
                        }
					}
                    /*while($myrow=db_fetch_array($resultValeurs)){
                     
                    }*/
                }
            }
            
            // construction du critere de recherche pour la requete
            if ($sSqlAdd)
                $sSqlAdd = "AND QLX_nnepc.id_produits in ($sSqlAdd)";
            else {
                $bIsModeListing = 0;
                $sMsg = "<br><span class=\"SITEError\">Aucun résultat ne correspond aux critères de recherche.</span><br><br>";
            }
        }
		
		if(empty($sSqlAdd))
		{
			?>
			<div class="no-criteria">
				Aucun Résultat pour les critères sélectionnés.
			</div>
			<?php
		}
	}
	
	$sSQLSiege = "SELECT QLX_siege.* FROM 
                     qlx_etablissements as QLX_usine, 
                     qlx_entreprises as QLX_siege, 
                     qlx_nn_etablissements_produits_certif as QLX_nnepc 
                     WHERE 
                     QLX_nnepc.id_certif = '%s' AND 
                     QLX_nnepc.id_etablissements = QLX_usine.id AND 
                     QLX_usine.id_entreprises = QLX_siege.id $sSqlAdd
					 GROUP BY QLX_usine.id
                     ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";
					 
	$sSQLUsine = "SELECT QLX_usine.*, QLX_nnepc.id_produits, QLX_nnepc.num_lng, QLX_nnepc.filename, QLX_nnepc.fdes FROM 
                 qlx_etablissements as QLX_usine, 
                 qlx_entreprises as QLX_siege, 
                 qlx_nn_etablissements_produits_certif as QLX_nnepc 
                 WHERE 
                 QLX_nnepc.id_certif = '%s' AND 
                 QLX_nnepc.id_etablissements = QLX_usine.id AND 
                 QLX_usine.id_entreprises = QLX_siege.id $sSqlAdd
                 GROUP BY QLX_usine.id 
                 ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";
				 
	$sSQL = "SELECT QLX_siege.*, QLX_usine.*, QLX_nnepc.id_produits, QLX_nnepc.num_lng, QLX_nnepc.filename, QLX_nnepc.fdes FROM 
                     qlx_etablissements as QLX_usine, 
                     qlx_entreprises as QLX_siege, 
                     qlx_nn_etablissements_produits_certif as QLX_nnepc 
                     WHERE 
                     QLX_nnepc.id_certif = '%s' AND 
                     QLX_nnepc.id_etablissements = QLX_usine.id AND 
                     QLX_usine.id_entreprises = QLX_siege.id $sSqlAdd
                     ORDER BY QLX_usine.cp, QLX_usine.id, QLX_nnepc.num_lng";
				
	 $wpdb->show_errors();
	 $preparedQuery = $wpdb->prepare($sSQL, $idCertif);
	 
	 $listValeurs = $wpdb->get_results($preparedQuery);
	 
	 $listValeursByUsine = array();
	 
	 //Tab à deux dimensions
	 for($i = 0; $i < count($listValeurs); $i++)
	 {
		$objUsine = $listValeurs[$i];
		
		if(gettype($listValeursByUsine[$objUsine->id]) == 'object')
		{
			$listValeursByUsine[$objUsine->id]->add_product($objUsine->id_produits);
		}
		else
		{
			//Si existe pas, création de l'objet Usine
			$usine = new Usine();
			$usine->add_product($objUsine->id_produits);
			
			$listValeursByUsine[$objUsine->id] = $usine;
		}
	 }
					 
					 
	$wpdb->show_errors();
	$preparedQuery = $wpdb->prepare($sSQLSiege, $idCertif);
	$listSieges = $wpdb->get_results($preparedQuery);
	
	
	$preparedQuery = $wpdb->prepare($sSQLUsine, $idCertif);
	$listUsines = $wpdb->get_results($preparedQuery);
	
	//Liste Département
	$arrayDpt = array();
	$arrayPays = array();
	for($i = 0; $i < count($listUsines); $i++)
	{	
		$cp = $listUsines[$i]->cp;
		$pays = $listUsines[$i]->pays;
		
		if(trim(strtolower($pays)) == "france")
		{
			$dpt = substr($listUsines[$i]->cp, 0, 2);
			$listUsines[$i]->dpt = $dpt;
			
			if(!in_array($dpt, $arrayDpt))
				array_push($arrayDpt, $dpt);
		}
		else
		{
			$pays = trim($listUsines[$i]->pays);
			$listUsines[$i]->dpt = $pays;
			
			if(!in_array($pays, $arrayPays))
				array_push($arrayPays, $pays);
		}
	}
	$arrayFilter = array_merge($arrayDpt, $arrayPays);

	$args = array('post_type' => 'certification', 'p' => $idPost->id_post_type);
	$query = new WP_Query($args);
	
	echo '<div class="row-wrapper-x">';
		if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post();
		
			echo '<div id="certif-title"><h3>';
			echo 'Liste des Usines - '. $post->post_title.$nameProduit;
			echo '</h3></div>';
			
			echo '<ul id="nb-usines-liste">';
	?>
			<li><a href="<?php the_permalink();?>">
	
	<?php
			echo '<div class="nb-menu">';
			echo '<i class="fa fa-2x fa-chevron-circle-left"></i> Retour aux caractéristiques';
			echo '</div>';
			echo '</a></li>';
			echo '</ul>';
			
	?>
	
		<div class="filter">
			<?php echo $remarque; ?>
			<?php if(!empty($tCertif['formulaire_xml'])): ?>
				Filtrer les usines par élément technique : <br>
				<div class="small-12">
					<form method="post" action="#">
						<?php
							//Récupérer critères 
								$tCertif['formulaire_xml'] = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>".$tCertif['formulaire_xml'];
									$domXml = domxml_open_mem($tCertif['formulaire_xml']);
									$tXml = $domXml->get_elements_by_tagname("formulaire");
									$sBufferForm = null;
									foreach ($tXml as $parentNode) {
										$nodeChild = $parentNode->first_child();
										$sBufferForm .= "
										<table id='formCertif' style=\"border:1px solid #ffffff;\" border='0' cellpadding='0' cellspacing='0'>
											" . setHtmlForm($parentNode,'nom') . "
										</table>";
									}
									
								echo $sBufferForm;
						?>
						<input type="submit" value="Appliquer les filtres">
					</form>
				</div>
			<?php endif; ?>
			<div class="small-12">
				<table id='formDept'>
					<tr>
						<td>Filtrer les résultats par département : </td>
						<td>
							<select id="dptFilterNF">
								<option value="all">Tous les départements</option>
								<?php for($d = 0; $d < count($arrayFilter); $d++): ?>
								<option value="<?php echo $arrayFilter[$d]; ?>"><?php echo $arrayFilter[$d]; ?></option>
								<?php endfor; ?>
							</select>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div id="usine-list">
			<table id="table-usine-nf">
				<?php for($i = 0; $i < count($listUsines); $i++): 
				
					$listUsines[$i]->adr_2 = trim($listUsines[$i]->adr_2);
					$listUsines[$i]->adr_3 = trim($listUsines[$i]->adr_3);
					$listUsines[$i]->tel   = trim($listUsines[$i]->tel);
					$listUsines[$i]->fax   = trim($listUsines[$i]->fax);
				
					$listSieges[$i]->adr_2 = trim($listSieges[$i]->adr_2);
					$listSieges[$i]->adr_3 = trim($listSieges[$i]->adr_3);
					$listSieges[$i]->tel   = trim($listSieges[$i]->tel);
					$listSieges[$i]->fax   = trim($listSieges[$i]->fax);
				?>
				<tr class="usine-info" data-dpt="<?php echo $listUsines[$i]->dpt; ?>">
					<td class="small-1"><?php echo $listUsines[$i]->dpt; ?></td>
					<td class="small-2"><div class="usine-detail">Usine : <br><h5 class="usine-name"><?php echo $listUsines[$i]->nom ?></h5><?php if($listUsines[$i]->fdes == 1): ?><img src='<?php echo WP_CONTENT_URL ?>/themes/Mexin_child_theme/images/fdes.jpg' class='fdesImg'><?php endif; ?><p><?php echo $listUsines[$i]->adr_1; ?><br><?php if(!empty($listUsines[$i]->adr_2)) echo $listUsines[$i]->adr_2.'<br>'; ?><?php if(!empty($listUsines[$i]->adr_3)) echo $listUsines[$i]->adr_3.'<br>'; ?><?php echo $listUsines[$i]->cp; ?> - <?php echo $listUsines[$i]->ville; ?><br><?php echo $listUsines[$i]->pays; ?><br><?php if(!empty($listUsines[$i]->tel)) echo 'TEL : '.$listUsines[$i]->tel.'<br>'; ?><?php if(!empty($listUsines[$i]->fax)) echo 'FAX : '.$listUsines[$i]->fax; ?></p></div>
					<div class="siege-detail">Siege : <br><h5><?php echo $listSieges[$i]->nom ?></h5><p><?php echo $listSieges[$i]->adr_1; ?><br><?php if(!empty($listSieges[$i]->adr_2)) echo $listSieges[$i]->adr_2.'<br>'; ?><?php if(!empty($listSieges[$i]->adr_3)) echo $listSieges[$i]->adr_3.'<br>'; ?><?php echo $listSieges[$i]->cp; ?> - <?php echo $listSieges[$i]->ville; ?><br><?php echo $listSieges[$i]->pays; ?><br><?php if(!empty($listSieges[$i]->tel)) echo 'TEL : '.$listSieges[$i]->tel.'<br>'; ?><?php if(!empty($listSieges[$i]->fax)) echo 'FAX : '.$listSieges[$i]->fax; ?></p></div>
					<?php if(!empty($listUsines[$i]->filename) && file_exists($pdfDirFile.'nf/'.$listUsines[$i]->filename)): ?><a href="<?php echo $pdfDir.'nf/'.$listUsines[$i]->filename; ?>" target="_blank"><i class="fa fa-2x fa-file-pdf-o"></i><?php endif; ?></td>
					<td class="small-9">
						<table id='tabCertif' style=\"width: 100%;border:1px solid #000000;\" border='0' cellpadding='0' cellspacing='0'>
						<?php echo $sBufferLine; ?>
						<?php
							//Get Valeurs
							if(gettype($listValeursByUsine[$listUsines[$i]->id]) == 'object')
							{
								$objValeurs = $listValeursByUsine[$listUsines[$i]->id];
								
								for($j = 0; $j < count($objValeurs->getArrayProduct()); $j++)
								{
									$idProduct = $objValeurs->getProductId($j);
									
									$sSQLValeurs = "SELECT * FROM qlx_valeurs WHERE id_produits = %d ORDER BY id";
									$preparedQuery = $wpdb->prepare($sSQLValeurs, $idProduct);
									$listValeurs = $wpdb->get_results($preparedQuery, ARRAY_A);
									
									$sTableauProduit = null;
									
									for($k=0; $k < count($listValeurs); $k++)
									{
										$tValeur[$listValeurs[$k]['num_crt']]=chop($listValeurs[$k]['valeur']);
									}
									
									for ($iNumCrt=0;$iNumCrt<sizeOf($tNum);$iNumCrt++)
									{
										if (!isset($tValeur[$tNum[$iNumCrt]]) || $tValeur[$tNum[$iNumCrt]]=="")
											$tValeur[$tNum[$iNumCrt]] = "&nbsp;";
										if ($tCrtVisible[$iNumCrt])
											$sTableauProduit .= "<td class='normal' $sBgcolor>".$tValeur[$tNum[$iNumCrt]]."</td>\r\n";
									}
									
									if($sTableauProduit)
										$sTableauProduit = "<tr>\r\n".$sTableauProduit."\r\n</tr>\r\n";
									
									echo $sTableauProduit;
								}
							}
						?>
						</table>
					</td>
				</tr>
				<?php endfor; ?>
			</table>
		</div>
	<?php
			
			endwhile;
			endif;
	echo '</div>';
		
	wp_link_pages();
	if ( comments_open() && ( $webnus_options->webnus_allow_comments_on_page() == 1) )

	comments_template();


/*
		  if( $have_sidebar ) {
?>
	</article>
</section>	

<?php
}
*/
/*
	RIGHT SIDEBAR
*/

if( ('right' == $sidebar_pos) || ('both' == $sidebar_pos) ) get_sidebar('right');

?>

</section>

<?php get_footer(); ?>