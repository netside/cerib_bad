function ajaxformsendmail() {
    jQuery("#ajaxevenement-response").hide();
    jQuery.ajax({
        type: 'POST',
        url: ajaxevenementajax.ajaxurl,
        data: {
            action: 'ajaxevenement_send_mail',
            acfidevenement: jQuery("#ajaxevenementidevenement").val(),
            acfcivilite: jQuery("#ajaxevenementcivilite").val(),
            acfnom: jQuery("#ajaxevenementnom").val(),
            acfprenom: jQuery("#ajaxevenementprenom").val(),
            acfsociete: jQuery("#ajaxevenementsociete").val(),
            acffonction: jQuery("#ajaxevenementfonction").val(),
            acfautre: jQuery("#ajaxevenementautre").val(),
            acfadresse: jQuery("#ajaxevenementadresse").val(),
            acfcodepostal: jQuery("#ajaxevenementcodepostal").val(),
            acfville: jQuery("#ajaxevenementville").val(),
            acftelephone: jQuery("#ajaxevenementtelephone").val(),
            acfemail: jQuery("#ajaxevenementemail").val(),
            acfoption1: jQuery("#ajaxevenementform input[name=ajaxevenementoption1]:checked").val(),
            acfoption2: jQuery("#ajaxevenementform input[name=ajaxevenementoption2]:checked").val(),
            acfoption3: jQuery("#ajaxevenementform input[name=ajaxevenementoption3]:checked").val(),
            acfnewsletter: jQuery("#ajaxevenementform input[name=ajaxevenementnewsletter]:checked").val(),
            acfcommentaire: jQuery("#ajaxevenementcommentaire").val()
        },
        success:function(data, textStatus, XMLHttpRequest){
            var id = '#ajaxevenement-response';
            jQuery(id).css('border', '2px solid #f7e700');
            if (data == "Votre inscription a bien été prise en compte.") {
                jQuery("#ajaxevenementform")[0].reset();
                jQuery(id).css('border', '2px solid #398f14');
            }
            jQuery(id).html('');
            jQuery(id).append(data);
            jQuery(id).show();
        },
        error: function(MLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}
