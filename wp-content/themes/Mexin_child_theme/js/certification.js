jQuery('#dptFilterCE').change(function(){
	
	jQuery('#table-usine .usine-info').show();
	
	var val = jQuery(this).val();
	
	if(val != 'all')
		jQuery('#table-usine .usine-info[data-dpt!='+val+']').hide();
});

jQuery('#dptFilterNF').change(function(){
	
	jQuery('#table-usine-nf .usine-info').show();
	
	var val = jQuery(this).val();
	
	if(val != 'all')
		jQuery('#table-usine-nf .usine-info[data-dpt!='+val+']').hide();
});

jQuery('#table-nf .link-usine').html('<i class="fa fa-2x fa-industry"></i>');