jQuery(function($){

	// Scroll to top
	$('#scrollToForm').click(function(event) {
		$('html, body').animate({
			scrollTop: 0
		}, 600)
		return false;
	});

	// Show Modal
	bioEp.init({
		cookieExp: 0,
		delay: 1,
		width: 660
	});

	$('#closeModal').click(function(event) {
		$('#bio_ep').fadeOut();
	});

});
