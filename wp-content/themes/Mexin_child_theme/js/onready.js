// affiche l'image d'entête, les docs et les liens selon l'onglet
function activeTab (id) {
    // carousel
    jQuery("span.ls-bottom-slidebuttons  a:nth-child(" + id + ")").click();
    
    // documents
    for (e=1; e<=5; e++) {
        jQuery("#bloc-docs-"+e).hide();
        jQuery("#bloc-docs-"+e).parent().hide();
    }
    if (jQuery("#bloc-docs-"+id).html() != undefined) jQuery("#bloc-docs-"+id).parent().show();
    jQuery("#bloc-docs-"+id).show();
    
    // liens
    for (e=1; e<=5; e++) {
        jQuery("#bloc-liens-"+e).hide();
        jQuery("#bloc-liens-"+e).parent().hide();
    }
    if (jQuery("#bloc-liens-"+id).html() != undefined) jQuery("#bloc-liens-"+id).parent().show();
    jQuery("#bloc-liens-"+id).show();
}

// simule le clique sur un onglet
function simulateClickTab (id) {
    jQuery(".wpb_tabs_nav li:nth-child(" + id + ") a.ui-tabs-anchor").click();

    // on ré-active pour le carousel
    var timeoutID = window.setTimeout(activeTab, 1000, id);
}

jQuery(document).ready(function($){
    
    // bind - affichage des documents, liens et image d'entête
    jQuery(".ui-tabs-anchor").each(function(index) {
        jQuery(this).bind (
            "click",
            function() {
                // cas particulier (rapport)
                var urlr = jQuery(location).attr('href');
                var posR = urlr.indexOf("/innovation/etudes-et-projets/");
                var posRe = urlr.indexOf("/innovation/development-and-projects/");
                if (posR>0 && index == 3) {
                    window.location.href = 'http://www.cerib.com/rapport/';
                    return false;
                }
                if (posRe>0 && index == 3) {
                    window.location.href = 'http://www.cerib.com/rapport/?lang=en';
                    return false;
                }
                
                // activation de l'onglet
                activeTab(index+1);
            }
        );
    });
    
    // on affiche les docs 1 et liens 1 par défaut
    jQuery("#bloc-docs-1").css("display", "block");
    jQuery("#bloc-liens-1").css("display", "block");
    
    // on active l'onglet selon l'URL avec # lors du chargement de la page
    var hash = location.hash.replace('#','');
    if (hash != '') {
        var url = jQuery(location).attr('href');
        var arrURL = new Array();
        arrURL = url.split("-");
        var tabNumber = arrURL[arrURL.length-1];
        if (tabNumber>0) simulateClickTab(tabNumber);
    } else {
        simulateClickTab(1);
    }
    
    // évènement - initialise le style select
    jQuery(".filtre-label").each(function(index) {
        jQuery(this).bind (
            "click",
            function() {
                return false;
            }
        );
    });
    
    $('#filtre-departement-tous').selectMultiple();
    $("#filtre-departement-tous").change(function(){
        var arrDepartement = $("#filtre-departement-tous").val();
        if (arrDepartement == null) {
            $("#tab-tous .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-tous').selectMultiple('deselect_all');
            $("#tab-tous .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-tous .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-tous').selectMultiple();
    $("#filtre-theme-tous").change(function(){
        var arrTheme = $("#filtre-theme-tous").val();
        if (arrTheme == null) {
            $("#tab-tous .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-tous").selectMultiple('deselect_all');
            $("#tab-tous .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-tous .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-departement-matinales').selectMultiple();
    $("#filtre-departement-matinales").change(function(){
        var arrDepartement = $("#filtre-departement-matinales").val();
        if (arrDepartement == null) {
            $("#tab-matinales .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-matinales').selectMultiple('deselect_all');
            $("#tab-matinales .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-matinales .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-matinales').selectMultiple();
    $("#filtre-theme-matinales").change(function(){
        var arrTheme = $("#filtre-theme-matinales").val();
        if (arrTheme == null) {
            $("#tab-matinales .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-matinales").selectMultiple('deselect_all');
            $("#tab-matinales .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-matinales .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-departement-petits-dejeuners').selectMultiple();
    $("#filtre-departement-petits-dejeuners").change(function(){
        var arrDepartement = $("#filtre-departement-petits-dejeuners").val();
        if (arrDepartement == null) {
            $("#tab-petits-dejeuners .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-petits-dejeuners').selectMultiple('deselect_all');
            $("#tab-petits-dejeuners .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-petits-dejeuners .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-petits-dejeuners').selectMultiple();
    $("#filtre-theme-petits-dejeuners").change(function(){
        var arrTheme = $("#filtre-theme-petits-dejeuners").val();
        if (arrTheme == null) {
            $("#tab-petits-dejeuners .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-petits-dejeuners").selectMultiple('deselect_all');
            $("#tab-petits-dejeuners .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-petits-dejeuners .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-departement-soirees').selectMultiple();
    $("#filtre-departement-soirees").change(function(){
        var arrDepartement = $("#filtre-departement-soirees").val();
        if (arrDepartement == null) {
            $("#tab-soirees .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-soirees').selectMultiple('deselect_all');
            $("#tab-soirees .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-soirees .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-soirees').selectMultiple();
    $("#filtre-theme-soirees").change(function(){
        var arrTheme = $("#filtre-theme-soirees").val();
        if (arrTheme == null) {
            $("#tab-soirees .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-soirees").selectMultiple('deselect_all');
            $("#tab-soirees .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-soirees .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-departement-salons-conferences').selectMultiple();
    $("#filtre-departement-salons-conferences").change(function(){
        var arrDepartement = $("#filtre-departement-salons-conferences").val();
        if (arrDepartement == null) {
            $("#tab-salons-conferences .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-salons-conferences').selectMultiple('deselect_all');
            $("#tab-salons-conferences .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-salons-conferences .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-salons-conferences').selectMultiple();
    $("#filtre-theme-salons-conferences").change(function(){
        var arrTheme = $("#filtre-theme-salons-conferences").val();
        if (arrTheme == null) {
            $("#tab-salons-conferences .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-salons-conferences").selectMultiple('deselect_all');
            $("#tab-salons-conferences .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-salons-conferences .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-departement-evenementiel').selectMultiple();
    $("#filtre-departement-evenementiel").change(function(){
        var arrDepartement = $("#filtre-departement-evenementiel").val();
        if (arrDepartement == null) {
            $("#tab-evenementiel .evenement-list-bloc").show();
        } else if (arrDepartement.length>0) {
            $('#filtre-theme-evenementiel').selectMultiple('deselect_all');
            $("#tab-evenementiel .evenement-list-bloc").hide();
            for (d=0; d<arrDepartement.length; d++) {
                $('#tab-evenementiel .evenement-list-bloc.departement-'+arrDepartement[d]).show();
            }
        }
    });
    $('#filtre-theme-evenementiel').selectMultiple();
    $("#filtre-theme-evenementiel").change(function(){
        var arrTheme = $("#filtre-theme-evenementiel").val();
        if (arrTheme == null) {
            $("#tab-evenementiel .evenement-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#filtre-departement-evenementiel").selectMultiple('deselect_all');
            $("#tab-evenementiel .evenement-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-evenementiel .evenement-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    
    // formation - initialise le style select
    $('#filtre-theme-prochaines').selectMultiple();
    $("#filtre-theme-prochaines").change(function(){
        var arrTheme = $("#filtre-theme-prochaines").val();
        if (arrTheme == null) {
            $(".formation-list-bloc").show();
        } else if (arrTheme.length>0) {
            $(".formation-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('.formation-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    $('#filtre-theme-en-avant').selectMultiple();
    $("#filtre-theme-en-avant").change(function(){
        var arrTheme = $("#filtre-theme-en-avant").val();
        if (arrTheme == null) {
            $("#tab-nos-offres .formation-list-bloc").show();
        } else if (arrTheme.length>0) {
            $("#tab-nos-offres .formation-list-bloc").hide();
            for (d=0; d<arrTheme.length; d++) {
                $('#tab-nos-offres .formation-list-bloc.'+arrTheme[d]).show();
            }
        }
    });
    
    // équipe - initialise le style select
    $('#filtre-departement-equipe').selectMultiple({
        afterSelect: function(values){
            $('#filtre-departement-equipe').selectMultiple('deselect_all');
            $("#label-departement").html('Par département');
            if (values == null) {
                $(".equipe-list-bloc").hide();
            } else {
                $(".equipe-list-bloc").hide();
                $('.equipe-list-bloc.'+values).show();
            }
            var strOUT = values.toString().replace("departement-", "Département ");
            $("#label-departement").html(strOUT);
        }
    });
    
    
    // formulaire d'inscription
    jQuery("#ajaxevenement-response").hide();
    jQuery("#ajaxformation-response").hide();
    
    
    // espace client
    jQuery(".active-form-connexion").each(function(index) {
        jQuery(this).bind (
            "click",
            function() {
                for (a=0; a<6; a++) {
                    jQuery(".onglet-vertical li:nth-child("+(a+1)+") a").removeClass("current");
                    jQuery(".list-form-connexion div:nth-child("+(a+1)+")").removeClass("current");
                }
                jQuery(".onglet-vertical li:nth-child("+(index+1)+") a").addClass("current");
                jQuery(".list-form-connexion div:nth-child("+(index+1)+")").addClass("current");
                
                // valeur du type d'accès pour le formulaire de demande
                var typeValue = "";
                switch (index) {
                    case 0:
                        typeValue = "Extranet";
                    break;
                    case 2:
                        typeValue = "My Cerib";
                    break;
                    case 3:
                        typeValue = "Plateforme E&R";
                    break;
                    case 4:
                        typeValue = "QualiB";
                    break;
                    case 5:
                        typeValue = "Memento";
                    break;
                }
                jQuery("#type-acces").val(typeValue);
                
                return false;
            }
        );
    });
    jQuery("#form-extranet-login").submit(function() {
        if (jQuery("#extranet_log").val() == "" || jQuery("#extranet_pwd").val() == "") {
            return false;
        }
    });
    jQuery("#form-apegib-login").submit(function() {
        if (jQuery("#apegib_log").val() == "" || jQuery("#apegib_pwd").val() == "") {
            return false;
        }
    });
    
    
    // recherche
    jQuery(".bouton-recherche").click(function() {
        jQuery("#searchform").submit();
        return false;
    });
    jQuery(".bouton-recherche-rapport").click(function() {
        jQuery("#searchformrapport").submit();
        return false;
    });
    
    // plan du site
    jQuery(".wsp-pages-list > li.page_item_has_children > a").each(function(index) {
        jQuery(this).bind (
            "click",
            function() {
                return false;
            }
        );
    });
    
    
    // scroll move for sidebar
    if (jQuery('.leftside').length) {
        var stickyNavTop = jQuery('.leftside').offset().top;
        var stickyNavBottom = jQuery('.leftside').offset().top+jQuery('.leftside').height();
        var stickyNavHeight = jQuery('.leftside').height();
        var tabsHeight = jQuery('.wpb_tabs').height();
        var noTabsHeight = jQuery('.bloc-content-menu-tab').height();
        var noTabsHeightMenu = jQuery('.bloc-content-withmenu-tab').height();
        
        if (noTabsHeight>0) tabsHeight = noTabsHeight;
        if (noTabsHeightMenu>0) tabsHeight = noTabsHeightMenu;
        
        var stickyNav = function(){
            if (stickyNavHeight < tabsHeight) {
                var scrollTop = jQuery(window).scrollTop();
                var stickyMapTop = jQuery('#social-bar').offset().top;
                
                if (scrollTop > stickyNavTop) { 
                    jQuery('.leftside').addClass('sticky');
                } else {
                    jQuery('.leftside').removeClass('sticky');
                }
                if (scrollTop > (stickyMapTop-stickyNavHeight-25)) {
                    jQuery('.leftside').removeClass('sticky');
                }
            }
        };
        
        stickyNav();
         
        jQuery(window).scroll(function() {
            stickyNav();
        });
    }
    
    // forms fonction autres
    jQuery("#fonction").change(function() {
        if (this.value == 'Autre (veuillez préciser)') {
            jQuery("#fonction-autre").show();
        } else {
            jQuery("#fonction-autre").hide();
            jQuery("#autre").val('');
        }
    });
    jQuery("#ajaxevenementfonction").change(function() {
        if (this.value == 'Autre (veuillez préciser)') {
            jQuery("#eve-fonction-autre").show();
        } else {
            jQuery("#eve-fonction-autre").hide();
            jQuery("#ajaxevenementautre").val('');
        }
    });
    
});
