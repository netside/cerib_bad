function ajaxformsendmail_formation() {
    jQuery("#ajaxformation-response").hide();
    jQuery.ajax({
        type: 'POST',
        url: ajaxformationajax.ajaxurl,
        data: {
            action: 'ajaxformation_send_mail',
            acfidformation: jQuery("#ajaxformationidformation").val(),
            acfstagiairenom: jQuery("#ajaxformationstagiairenom").val(),
            acfstagiaireprenom: jQuery("#ajaxformationstagiaireprenom").val(),
            acfstagiairefonction: jQuery("#ajaxformationstagiairefonction").val(),
            acfstagiaireemail: jQuery("#ajaxformationstagiaireemail").val(),
            acfstagiaireintitule: jQuery("#ajaxformationstagiaireintitule").val(),
            acfstagiairereference: jQuery("#ajaxformationstagiairereference").val(),
            acfstagiairelieu: jQuery("#ajaxformationstagiairelieu").val(),
            acfstagiairedate: jQuery("#ajaxformationstagiairedate").val(),
            acfsocieteraisonsociale: jQuery("#ajaxformationsocieteraisonsociale").val(),
            acfsocieteadresse: jQuery("#ajaxformationsocieteadresse").val(),
            acfsocietetelephone: jQuery("#ajaxformationsocietetelephone").val(),
            acfsocieteemail: jQuery("#ajaxformationsocieteemail").val(),
            acfsocietetva: jQuery("#ajaxformationsocietetva").val(),
            acfnewsletter: jQuery("#ajaxformationform input[name=ajaxformationnewsletter]:checked").val()
        },
        success:function(data, textStatus, XMLHttpRequest){
            var id = '#ajaxformation-response';
            jQuery(id).css('border', '2px solid #f7e700');
            if (data == "Votre pré-inscription a bien été prise en compte.") {
                jQuery("#ajaxformationform")[0].reset();
                jQuery(id).css('border', '2px solid #398f14');
            }
            jQuery(id).html('');
            jQuery(id).append(data);
            jQuery(id).show();
        },
        error: function(MLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}
