<?php
class GED_db {
	
	public 
	  $external     = false,
	  $arbo         = array(),
	  $profils      = array()
	;
	
	private 
	  $primaryRootPath = '',
	  $db
	;
	
	public function __construct($rootFolder='ged', $user_folder='', $external=false, $jPathRoot=''){
		$root = $jPathRoot ? $jPathRoot : JPATH_ROOT;
		
		$rootPath = $root.$rootFolder.$user_folder;
		
		$this->primaryRootPath = $rootPath;
		$this->db = $this->connect();
		
		$this->external = $external;
		$this->arbo     = $this->getArbo($rootPath);
		$this->profils  = $this->getProfils();
		
		@mysql_close($this->db);
	}
	
	private function connect(){
        $newdb = new wpdb(DB_USER_GED, DB_PASSWORD_GED, DB_NAME_GED, DB_HOST_GED);
        $newdb->show_errors();

		return $newdb;
	}
	
	private function getArbo($rootPath, $id_parent=0){
		$folders = $this->folders($rootPath, $id_parent);
		$files   = $this->files($rootPath, $id_parent);
		
		$results = array();
		
		if ( !empty($folders) ) {
			$results['folders'] = $folders;
			foreach($results['folders'] as $folder){
				$children = $this->getArbo($folder->fullpath, $folder->id_folder);
				if ( !empty($children['files']) || !empty($children['folders']) ){
					$folder->children = $children;
				}
			}
		}
		
		if ( !empty($files) ){
			$results['files'] = $files;
		}
		
		return $results;
	}
	private function folders($currentPath, $id_parent){
		$db = $this->db;
		
		$query  = " SELECT id_folder, id_parent, name, title ";
		$query .= " FROM jos_ged_folder ";
		$query .= " WHERE id_parent='".$id_parent."'";
		$query .= " AND (name<>'actualites' AND name<>'emailing')";
		$query .= " ORDER BY title ASC ";
		// etq($query);
		$data = $db->get_results($query);
		
		$results = array();
		if (count($data)>0) {
    		foreach($data as $key => $result){
    			$result->fullpath = $currentPath."/".$result->name;
    			$result->shortpath = str_replace($this->primaryRootPath, '', $result->fullpath);
    			$results[$result->id_folder] = $result;
    		}
		}
		
		return $results;
	}
	private function files($currentPath, $id_folder){
		$db = $this->db;
		
		$query  = " SELECT id_file, id_folder, name, type, size, title, metas ";
		$query .= " FROM jos_ged_file ";
		$query .= " WHERE id_folder='".$id_folder."'";
		$query .= " ORDER BY title ASC ";
		// etq($query);
		$data = $db->get_results($query);
		
		$results = array();
		if (count($data)>0) {
    		foreach($data as $result){
    			$result->fullpath  = $currentPath."/".$result->name;
    			$result->shortpath = str_replace($this->primaryRootPath, '', $result->fullpath);
    			// ets($result->fullpath);
    			$results[$result->id_file] = $result;
    		}
		}
		
		return $results;
	}
	
	private function getProfils(){
		$db = $this->db;
		
		$query  = " SELECT id_profil, name ";
		$query .= " FROM jos_ged_profil ";
		$query .= " ORDER BY name ASC ";
		// etq($query);
		$data = $db->get_results($query);
		return $data;
	}
}


function getDocumentsGED() 
{
    $gedData = new GED_db('ged', '', true, 'http://www.produits-beton.com/DM/');
    $profils = $gedData->profils;
    $arbo    = $gedData->arbo;
    
	$documents = new stdClass;
	$documents->profils = $gedData->profils;
	$documents->arbo    = $gedData->arbo;
	$documents->html    = getDocuments_parseFolders($documents->arbo['folders']);
	
	return $documents;
}
function getDocuments_clean($folders){
	for($i=0, $n=count($folders); $i<$n; $i++){
		if ( !empty($folders[$i]->children['folders']) ){
			$folders[$i]->children['folders'] = getDocuments_clean($folders[$i]->children['folders']);
		}
		if ( empty($folders[$i]->children['folders']) && empty($folders[$i]->children['files']) ){
			unset($folders[$i]);
		}
	}
	return $folders;
}
function getDocuments_parseFolders($folders, $level=0, $offset=''){
	$html = '';
	$arr = array();
	
	if ( !empty($folders) ){
		$level++;
		
		$data = '';
		foreach($folders as $folder){
			if ( !empty($folder->children['folders']) || !empty($folder->children['files']) ){
				$fo = ( !empty($folder->children['folders']) ) ? getDocuments_parseFolders($folder->children['folders'], $level, $offset.'  ') : '';
				$fi = ( !empty($folder->children['files']) )   ? getDocuments_parseFiles($folder->children['files'], $level, $offset)     : '';
				
				if ( !empty($fo) || !empty($fi) ){
					if ( $level==1 ){
						//$data .= $offset.'<h3>'.$folder->title.'</h3>' . "\n";
						if ( $fo ) $data .= $fo;
						if ( $fi ) $data .= $fi;
					}
					else {
						//$data .= $offset.' <li>'."\n";
						//$data .= $offset.'  <span class="isFolder">'.$folder->title.'</span>' . "\n";
						if ( $fo ) $data .= $fo;
						if ( $fi ) $data .= $fi;
						//$data .= $offset.' </li>' . "\n";
					}
				}
			}
		}
		
		if ( $data ){
			if ( $level==1 ){
				$html .= $offset.$data;
			}
			else {
				//$html .= $offset.'<ul class="level_'.$level.'">' . "\n";
				$html .= $data;
				//$html .= $offset.'</ul>' . "\n";
			}
		}
	}
	
	return $html;
}
function getDocuments_parseFiles($files, $level, $offset=''){
	$html = '';
	
	if ( !empty($files) ){
		$data = '';
		foreach($files as $file){
			//$data .= $offset.'   <li><a href="'.$file->fullpath.'" target="_blank"><span class="see_internal_files isFile" id="sf-'.$file->id_file.'">'.$file->shortpath.'</span></a></li>' . "\n";
			$data .= $offset.'|||'.$file->id_file.'|'.$file->fullpath.'|'.$file->shortpath.'';
		}
		
		if ( $data ){
			//$html .= $offset.'  <ul class="level_'.$level.' isFileList">' . "\n";
			$html .= $data;
			//$html .= $offset.'  </ul>' . "\n";
		}
	}
	
	return $html;
}
# lancement seulement en back-office
if (is_admin()) {
    global $htmlGED;
    #$documentsGED = getDocumentsGED();
    #$htmlGED = $documentsGED->html;
}
?>