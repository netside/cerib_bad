<?php
# affiche la sidebar left
add_shortcode('sidebar_left', 'display_sidebar_left');
function display_sidebar_left ($atts, $content) {
    get_sidebar('left');
}