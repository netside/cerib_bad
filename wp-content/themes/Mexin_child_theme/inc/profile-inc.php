<?php

# admin - redirection du back-office vers front si autre que administrateur et CERIB
add_action('admin_init', 'gkp_restrict_access_administration');	
function gkp_restrict_access_administration(){
    if (is_admin() 
    && !current_user_can('administrator') 
    && !current_user_can('cerib') 
    && !current_user_can('rh') 
    && !current_user_can('rapports') 
    && !current_user_can('author') 
    && !( defined( 'DOING_AJAX' ) && DOING_AJAX )) {
    	wp_redirect( get_bloginfo('url') );
    	exit();
    }
}

# admin - cache la toolbar si autre que administrateur et CERIB
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !current_user_can('cerib')) {
        show_admin_bar(false);
    }
}

# admin - ajoute les champs supplémentaires au profil utilisateur
function my_new_contactmethods( $contactmethods ) {
    unset( $contactmethods['url'] );
    unset( $contactmethods['googleplus'] );
    unset( $contactmethods['twitter'] );
    unset( $contactmethods['facebook'] );
    
    $contactmethods['fonction'] = 'Fonction';
    $contactmethods['telephone'] = 'Téléphone';
    $contactmethods['siret'] = 'SIRET';
    $contactmethods['siren'] = 'SIREN';
    $contactmethods['naf'] = 'Code NAF';
    $contactmethods['raisonsociale'] = 'Raison sociale';
    $contactmethods['adresse'] = 'Adresse';
    $contactmethods['codepostal'] = 'Code postal';
    $contactmethods['ville'] = 'Ville';
    $contactmethods['adhesion'] = 'Adhésion Groupe (APEGIB)';
    $contactmethods['paiement'] = 'Mode de paiement (APEGIB)';
    
    return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);

# redirige vers la page d'accueil si erreur de login
add_action( 'wp_login_failed', 'pippin_login_fail' );  // hook failed login
function pippin_login_fail( $username ) {
    wp_redirect(home_url() . '/?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
    exit;
}

# protection
add_filter('login_errors',create_function('$a', "return null;"));
define('DISALLOW_FILE_EDIT',true);

# admin - enlève des éléments de menu
function remove_menu_pages() {
    if (current_user_can('administrator')) {
    	remove_menu_page('edit.php?post_type=portfolio');
    	remove_menu_page('edit.php?post_type=faq');
    	remove_menu_page('edit.php?post_type=employee');
    	remove_menu_page('edit-comments.php');
	} else if (current_user_can('cerib')) {
    	remove_menu_page('edit.php?post_type=portfolio');
    	remove_menu_page('edit.php?post_type=faq');
    	remove_menu_page('edit.php?post_type=employee');
    	remove_menu_page('themepunch-google-fonts');
    	remove_menu_page('edit-comments.php');
	}
}
add_action( 'admin_menu', 'remove_menu_pages' );

# page de login
function my_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . home_url('/') . 'wp-content/themes/Mexin_child_theme/login/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');
function my_login_logo_url() {
return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
return 'Cerib.com';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function login_checked_remember_me() {
    add_filter( 'login_footer', 'rememberme_checked' );
}
add_action( 'init', 'login_checked_remember_me' );

function rememberme_checked() {
    echo "<script>document.getElementById('rememberme').checked = true;</script>";
}

