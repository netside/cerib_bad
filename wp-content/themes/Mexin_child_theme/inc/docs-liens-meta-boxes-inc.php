<?php

# custom meta box
add_filter( 'rwmb_meta_boxes', 'docs_liens_box' );
function docs_liens_box( $meta_boxes )
{
    
    # connexion à la GED
    global $htmlGED;
    
    $arrFiles = explode("|||", $htmlGED);
    $arrFilesSelect = array();
    $arrTMP = array();
    foreach ($arrFiles as $arrVal) {
        $arrTMP = explode("|", $arrVal);
        if (isset($arrTMP[1])) $arrFilesSelect[$arrTMP[1]] = $arrTMP[2];
    }
    //print_r($arrFilesSelect); die();
    
    
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'docs-liens_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'docs-liens-1',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Documents et liens : Onglet 1', 'docs-liens-1' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page', 'evenements', 'formation' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'id'     => 'document1',
				'name'   => __( 'Documents', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}documentnom",
        				'type'  => 'text',
        			),
        			// SELECT ADVANCED BOX
        			array(
        				'name'        => __( 'URL GED', 'docs-liens' ),
        				'id'          => "{$prefix}documentged",
        				'type'        => 'select_advanced',
        				// Array of 'value' => 'Label' pairs for select box
        				'options'     => $arrFilesSelect,
        				// Select multiple values, optional. Default is false.
        				'multiple'    => false,
        				// 'std'         => 'value2', // Default value, optional
        				'placeholder' => __( 'Choisir un document', 'docs-liens' ),
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
			array(
				'id'     => 'lien1',
				'name'   => __( 'Liens', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}liennom",
        				'type'  => 'text',
        			),
        			// URL
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'URL', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}url",
        				'type'  => 'url',
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
		),
	);

	// 2nd meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'docs-liens-2',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Documents et liens : Onglet 2', 'docs-liens-2' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'id'     => 'document2',
				'name'   => __( 'Documents', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}documentnom",
        				'type'  => 'text',
        			),
        			// SELECT ADVANCED BOX
        			array(
        				'name'        => __( 'URL GED', 'docs-liens' ),
        				'id'          => "{$prefix}documentged",
        				'type'        => 'select_advanced',
        				// Array of 'value' => 'Label' pairs for select box
        				'options'     => $arrFilesSelect,
        				// Select multiple values, optional. Default is false.
        				'multiple'    => false,
        				// 'std'         => 'value2', // Default value, optional
        				'placeholder' => __( 'Choisir un document', 'docs-liens' ),
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
			array(
				'id'     => 'lien2',
				'name'   => __( 'Liens', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}liennom",
        				'type'  => 'text',
        			),
        			// URL
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'URL', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}url",
        				'type'  => 'url',
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
		),
	);

	// 3rd meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'docs-liens-3',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Documents et liens : Onglet 3', 'docs-liens-3' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'id'     => 'document3',
				'name'   => __( 'Documents', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}documentnom",
        				'type'  => 'text',
        			),
        			// SELECT ADVANCED BOX
        			array(
        				'name'        => __( 'URL GED', 'docs-liens' ),
        				'id'          => "{$prefix}documentged",
        				'type'        => 'select_advanced',
        				// Array of 'value' => 'Label' pairs for select box
        				'options'     => $arrFilesSelect,
        				// Select multiple values, optional. Default is false.
        				'multiple'    => false,
        				// 'std'         => 'value2', // Default value, optional
        				'placeholder' => __( 'Choisir un document', 'docs-liens' ),
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
			array(
				'id'     => 'lien3',
				'name'   => __( 'Liens', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}liennom",
        				'type'  => 'text',
        			),
        			// URL
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'URL', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}url",
        				'type'  => 'url',
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
		),
	);

	// 4th meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'docs-liens-4',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Documents et liens : Onglet 4', 'docs-liens-4' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'id'     => 'document4',
				'name'   => __( 'Documents', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}documentnom",
        				'type'  => 'text',
        			),
        			// SELECT ADVANCED BOX
        			array(
        				'name'        => __( 'URL GED', 'docs-liens' ),
        				'id'          => "{$prefix}documentged",
        				'type'        => 'select_advanced',
        				// Array of 'value' => 'Label' pairs for select box
        				'options'     => $arrFilesSelect,
        				// Select multiple values, optional. Default is false.
        				'multiple'    => false,
        				// 'std'         => 'value2', // Default value, optional
        				'placeholder' => __( 'Choisir un document', 'docs-liens' ),
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
			array(
				'id'     => 'lien4',
				'name'   => __( 'Liens', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}liennom",
        				'type'  => 'text',
        			),
        			// URL
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'URL', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}url",
        				'type'  => 'url',
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
		),
	);

	// 5th meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'docs-liens-5',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Documents et liens : Onglet 5', 'docs-liens-5' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'id'     => 'document5',
				'name'   => __( 'Documents', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}documentnom",
        				'type'  => 'text',
        			),
        			// SELECT ADVANCED BOX
        			array(
        				'name'        => __( 'URL GED', 'docs-liens' ),
        				'id'          => "{$prefix}documentged",
        				'type'        => 'select_advanced',
        				// Array of 'value' => 'Label' pairs for select box
        				'options'     => $arrFilesSelect,
        				// Select multiple values, optional. Default is false.
        				'multiple'    => false,
        				// 'std'         => 'value2', // Default value, optional
        				'placeholder' => __( 'Choisir un document', 'docs-liens' ),
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
			array(
				'id'     => 'lien5',
				'name'   => __( 'Liens', 'rwmb' ),
				'type'   => 'group', // Group type
				'clone'  => true,    // Can be cloned?

				// List of child fields
				'fields' => array(
        			// TEXT
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'Nom', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}liennom",
        				'type'  => 'text',
        			),
        			// URL
        			array(
        				// Field name - Will be used as label
        				'name'  => __( 'URL', 'docs-liens' ),
        				// Field ID, i.e. the meta key
        				'id'    => "{$prefix}url",
        				'type'  => 'url',
        			),
        			// CHECKBOX
        			array(
        				'name' => __( 'Profil extranet', 'docs-liens' ),
        				'id'   => "{$prefix}extranet",
        				'type' => 'checkbox',
        			),
				),
			),
		),
	);

	return $meta_boxes;
}
