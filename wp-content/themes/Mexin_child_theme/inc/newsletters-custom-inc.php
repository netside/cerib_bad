<?php
    
# initialisation du custom post type
function custom_post_newsletter() {
  $labels = array(
    'name'               => _x( 'Newsletter', 'post type general name' ),
    'singular_name'      => _x( 'Newsletter', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter une newsletter' ),
    'edit_item'          => __( 'Modifier une newsletter' ),
    'new_item'           => __( 'Nouvelle newsletter' ),
    'all_items'          => __( 'Toutes les newsletters' ),
    'view_item'          => __( 'Voir la newsletter' ),
    'search_items'       => __( 'Rechercher dans les newsletters' ),
    'not_found'          => __( 'Aucune newsletter trouvée' ),
    'not_found_in_trash' => __( 'Aucune newsletter trouvée dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Newsletters'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des newsletters',
    'public'        => true,
    'menu_position' => 16,
    'menu_icon'     => 'dashicons-email-alt',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'newsletters', $args ); 
}
add_action( 'init', 'custom_post_newsletter' );

# custom message
function updated_messages_newsletter( $messages ) {
  global $post, $post_ID;
  $messages['newsletters'] = array(
    0 => '', 
    1 => sprintf( __('Newsletter mise à jour. <a href="%s">Voir la newsletter</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Newsletter mise à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Newsletter restaurée à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Newsletter publiée. <a href="%s">Voir la newsletter</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Newsletter enregistrée.'),
    8 => sprintf( __('Newsletter envoyée. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Newsletter planifiée pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_newsletter' );

# custom categories
function taxonomies_newsletter() {
  $labels = array(
    'name'              => _x( 'Catégorie Newsletter', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'newsletters_categorie', 'newsletters', $args );
}
add_action( 'init', 'taxonomies_newsletter', 0 );

# prise en charge RUL pour les archives
add_action('init', 'newsletter_archive_rewrite');
function newsletter_archive_rewrite(){
   add_rewrite_rule('^newsletters/([0-9]{4})/([0-9]{2})/?','index.php?post_type=newsletters&year=$matches[1]&monthnum=$matches[2]','top');
   add_rewrite_rule('^newsletters/([0-9]{4})/?','index.php?post_type=newsletters&year=$matches[1]','top');
}
