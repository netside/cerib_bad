<?php

# shortcode : affichage du bloc recherche topbar
add_shortcode('recherche-form', 'display_recherche_form');
function display_recherche_form ($atts, $content) {
    $currentLang =  get_locale();
    if ($currentLang == "fr_FR") {
        $placeholder = "Entrez vos mots-clés";
        $urlFrom = "http://www.cerib.com/";
    } else {
        $placeholder = "Enter your keywords";
        $urlFrom = "http://www.cerib.com/?lang=en";
        $langInput = '<input type="hidden" id="lang" name="lang" value="en" >';
    }
    $content = '
    <div class="wpb_row vc_row-fluid full-row header-contact">
        <div class="col-md-12 form-recherche">
            <form id="searchform" role="search" action="' . $urlFrom . '" method="get">
                '.$langInput.'
                <input type="text" id="s" name="s" placeholder="'.$placeholder.'">
                <a href="#" class="bouton-recherche"><i class="fa fa-search"></i></a>
            </form>
        </div>
    </div>
    ';
    return $content;
}

# shortcode : affichage du bloc recherche rapport widget
add_shortcode('rapport-recherche-form', 'display_recherche_rapport_form');
function display_recherche_rapport_form ($atts, $content) {
    $currentLang =  get_locale();
    if ($currentLang == "fr_FR") {
        $placeholder = "Entrez vos mots-clés";
        $urlFrom = "http://www.cerib.com/rapport/";
    } else {
        $placeholder = "Enter your keywords";
        $urlFrom = "http://www.cerib.com/rapport/?lang=en";
        $langInput = '<input type="hidden" id="lang" name="lang" value="en" >';
    }
    $content = '
    <div class="form-recherche-rapport">
        <form id="searchformrapport" action="' . $urlFrom . '" method="get">
                '.$langInput.'
            <input type="text" id="r" name="r" placeholder="'.$placeholder.'">
            <a href="#" class="bouton-recherche-rapport"><i class="fa fa-search"></i></a>
        </form>
    </div>
    ';
    return $content;
}

# shortcode : affichage du résultats de recherche du site
add_shortcode('resultats_ged', 'display_resultats_ged');
function display_resultats_ged ($atts, $content) {
    include_once("ged/verticrawl.class.php");
    $_GET["q"] = $_GET["s"];
    $verticrawl = new verticrawl;
    $URL = $verticrawl->getResults();
    
    $currentLang =  get_locale();
    if ($currentLang == "fr_FR") {
        $titre = "Base documentaire";
    } else {
        $titre = "Documents";
    }
    
	$results=array();
	for($i=1, $n=count($URL); $i<=$n; $i++){
		$row =& $URL[$i];
		
		$row['body']      = utf8_encode($row['body']);
		$row['url-title'] = utf8_encode($row['url-title']);
		$row['url']       = utf8_encode($row['url']);
		
		$result = new stdClass;
		$result->title    = $row['url-title'];
		$result->extract  = $row['body'];
		$result->url      = $row['url'];
		$results[] = $result;
	}
	$countDoc = 0;
	if (count($results)>0) {
        $dbGed = new wpdb(DB_USER_GED, DB_PASSWORD_GED, DB_NAME_GED, DB_HOST_GED);
        $dbGed->show_errors();
    	?>
        <h4><i class="fa fa-file-o"></i> <?php echo $titre; ?></h4>
        <ul class="document-ged-liste">
    	<?php
    	foreach ($results as $result) {
        	$filename = "";
        	$arrTMP = explode("/", $result->url);
        	$filename = $arrTMP[count($arrTMP)-1];
    		$query  = " SELECT title ";
    		$query .= " FROM jos_ged_file ";
    		$query .= " WHERE name LIKE '".$filename."' ";
    		$query .= " LIMIT 1 ";
    		$data = $dbGed->get_results($query);
    		if ($data[0]->title != "") {
        		# on bloque les résultats du dossier "client"
                # utilisateur
                global $current_user;
                get_currentuserinfo();
                $roleUtilisateur = get_user_role();
                
        		# accès extranet
                $acces = 1;
                $pos = strpos($result->url, "www.produits-beton.com/DM/ged/client/");
                $posApegib = strpos($result->url, "www.produits-beton.com/DM/ged/client/apegib/");
                if ($pos == "") {
                    $acces = 1;
                } else if ($posApegib == 7 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib") ) {
                    $acces = 1;
                } else if ($pos == 7 && $posApegib == "" && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                    $acces = 1;
                } else {
                    $acces = 0;
                }
                if ($acces == 1) {
                    ?>
                    <li><a href="<?php echo $result->url; ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php echo $data[0]->title; ?></a></li>
                    <?php
                    $countDoc++;
                }
            }
        }
        ?>
        </ul>
        <?php
        if ($countDoc == 0) echo "Aucun document trouvé";
        unset($dbGed);
    }
}
