<?php
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
date_default_timezone_set('Europe/Paris');

if (isset($_GET["export"]) && $_GET["export"] == 'download_csv' && isset($_GET["exportkey"]) && $_GET["exportkey"] == 'ne-v920b23--z72d0ei73-0eonvp42-gn39') {
    // output headers so that the file is downloaded rather than displayed
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".date("YmdHis")."_formation_inscriptions.csv\";" );
    header("Content-Transfer-Encoding: binary");
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('inscription_id', 'Formation', 'Date', 'Nom du stagiaire', utf8_decode('Prénom du stagiaire'), 'Fonction du stagiaire', 'E-mail du stagiaire', utf8_decode('Intitulé'), utf8_decode('Référence'), 'Lieu', utf8_decode('Dates prévues'), utf8_decode('Raison sociale de la société'), utf8_decode('Adresse de la société'), utf8_decode('Téléphone de la société'), utf8_decode('E-mail de la société'), utf8_decode('N° TVA intra')), ";");
    
    // fetch the data
    $connect = mysql_connect("localhost", "root", 'iS4qjVrrqbcC');
    mysql_select_db("cerib");
    $result = mysql_query("SELECT f.inscription_id, p.post_title, DATE_FORMAT(f.date, '%d/%m/%Y %H:%i:%s'), f.stagiaire_nom, f.stagiaire_prenom, f.stagiaire_fonction, f.stagiaire_email, f.stagiaire_intitule, f.stagiaire_reference, f.stagiaire_lieu, f.stagiaire_dates_prevues, f.societe_raison_sociale, f.societe_adresse, f.societe_telephone, f.societe_email, f.societe_tva FROM pzebfiv_formations_inscription f, pzebfiv_posts p WHERE f.formation_id = p.ID ORDER BY f.date DESC ");
    while ($row = mysql_fetch_assoc($result)) {
        fputcsv($output, $row, ";");
    }

} else {
    exit;
}
?>