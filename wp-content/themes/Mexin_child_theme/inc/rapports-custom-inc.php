<?php

# initialisation du custom post type
function custom_post_rapport() {
  $labels = array(
    'name'               => _x( 'Rapports', 'post type general name' ),
    'singular_name'      => _x( 'Rapport', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter un nouveau rapport' ),
    'edit_item'          => __( 'Modifier un rapport' ),
    'new_item'           => __( 'Nouveau rapport' ),
    'all_items'          => __( 'Tous les rapports' ),
    'view_item'          => __( 'Voir le rapport' ),
    'search_items'       => __( 'Rechercher dans les rapports' ),
    'not_found'          => __( 'Aucun rapport trouvé' ),
    'not_found_in_trash' => __( 'Aucun rapport trouvé dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Rapports'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des rapports',
    'public'        => true,
    'menu_position' => 14,
    'menu_icon'     => 'dashicons-analytics',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
	'capability_type' => 'rapport',
	'capabilities' => array(
		'publish_posts' => 'publish_rapports',
		'edit_posts' => 'edit_rapports',
		'edit_others_posts' => 'edit_others_rapports',
		'read_private_posts' => 'read_private_rapports',
		'edit_post' => 'edit_rapport',
		'delete_post' => 'delete_rapport',
		'read_post' => 'read_rapport',
	),
  );
  register_post_type( 'rapport', $args ); 
}
add_action( 'init', 'custom_post_rapport' );

# custom message
function updated_messages_rapport( $messages ) {
  global $post, $post_ID;
  $messages['rapport'] = array(
    0 => '', 
    1 => sprintf( __('Rapport mis à jour. <a href="%s">Voir le rapport</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Rapport mis à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Rapport restauré à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Rapport publié. <a href="%s">Voir le rapport</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Rapport enregistré.'),
    8 => sprintf( __('Rapport envoyé. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Rapport planifié pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_rapport' );

# custom categories
/*
function taxonomies_rapport() {
  $labels = array(
    'name'              => _x( 'Catégories Rapports', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'rapport_category', 'rapport', $args );
}
add_action( 'init', 'taxonomies_rapport', 0 );
*/

# custom tags
function taxonomies_auteur_rapport() {
  $labels = array(
    'name'              => _x( 'Auteurs Rapports', 'taxonomy general name' ),
    'singular_name'     => _x( 'Auteur', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les auteurs' ),
    'all_items'         => __( 'Tous les auteurs' ),
    'parent_item'       => __( 'Auteur parent' ),
    'parent_item_colon' => __( 'Auteur parent :' ),
    'edit_item'         => __( 'Modifier un auteur' ), 
    'update_item'       => __( 'Mettre à jour un auteur' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvel auteur' ),
    'menu_name'         => __( 'Auteurs' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'capabilities' => array(
    	'manage_terms' => 'manage_rapport',
    	'edit_terms' => 'edit_rapport',
    	'delete_terms' => 'delete_rapport',
    	'assign_terms' => 'assign_rapport'
    ),
  );
  register_taxonomy( 'rapport_auteur', 'rapport', $args );
}
add_action( 'init', 'taxonomies_auteur_rapport', 0 );

function taxonomies_numero_rapport() {
  $labels = array(
    'name'              => _x( 'Numéros Rapports', 'taxonomy general name' ),
    'singular_name'     => _x( 'Numéro', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les numéros' ),
    'all_items'         => __( 'Tous les numéros' ),
    'parent_item'       => __( 'Numéro parent' ),
    'parent_item_colon' => __( 'Numéro parent :' ),
    'edit_item'         => __( 'Modifier un numéro' ), 
    'update_item'       => __( 'Mettre à jour un numéro' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouveau numéro' ),
    'menu_name'         => __( 'Numéros' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'capabilities' => array(
    	'manage_terms' => 'manage_rapport',
    	'edit_terms' => 'edit_rapport',
    	'delete_terms' => 'delete_rapport',
    	'assign_terms' => 'assign_rapport'
    ),
  );
  register_taxonomy( 'rapport_numero', 'rapport', $args );
}
add_action( 'init', 'taxonomies_numero_rapport', 0 );

function taxonomies_tag_rapport() {
  $labels = array(
    'name'              => _x( 'Mots-clés Rapports', 'taxonomy general name' ),
    'singular_name'     => _x( 'Mots-clés', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les mots-clés' ),
    'all_items'         => __( 'Tous les mots-clés' ),
    'parent_item'       => __( 'Mot-clé parent' ),
    'parent_item_colon' => __( 'Mot-clé parent :' ),
    'edit_item'         => __( 'Modifier un mot-clé' ), 
    'update_item'       => __( 'Mettre à jour un mot-clé' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouveau mot-clé' ),
    'menu_name'         => __( 'Mots-clés' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'capabilities' => array(
    	'manage_terms' => 'manage_rapport',
    	'edit_terms' => 'edit_rapport',
    	'delete_terms' => 'delete_rapport',
    	'assign_terms' => 'assign_rapport'
    ),
  );
  register_taxonomy( 'rapport_tag', 'rapport', $args );
}
add_action( 'init', 'taxonomies_tag_rapport', 0 );

# custom meta box
add_filter( 'rwmb_meta_boxes', 'rapport_box' );
function rapport_box( $meta_boxes )
{
    
    # connexion à la GED
    global $htmlGED;
    $documentsGED = getDocumentsGED();
    $htmlGED = $documentsGED->html;
    
    $arrFiles = explode("|||", $htmlGED);
    $arrFilesSelect = array();
    $arrTMP = array();
    foreach ($arrFiles as $arrVal) {
        $arrTMP = explode("|", $arrVal);
        if (isset($arrTMP[1])) {
            #$pos = strpos($arrTMP[2], "/rapports/");
            #if ($pos == 7) 
            $arrFilesSelect[$arrTMP[1]] = $arrTMP[2];
        }
    }
    //print_r($arrFilesSelect); die();
    
    
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'rapport_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Document et profil', 'rapport' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'rapport' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// SELECT ADVANCED BOX
			array(
				'name'        => __( 'URL GED', 'rapport' ),
				'id'          => "{$prefix}documentged",
				'type'        => 'select_advanced',
				// Array of 'value' => 'Label' pairs for select box
				'options'     => $arrFilesSelect,
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				// 'std'         => 'value2', // Default value, optional
				'placeholder' => __( 'Choisir un document', 'rapport' ),
			),
			// CHECKBOX
			array(
				'name' => __( 'Profil extranet', 'rapport' ),
				'id'   => "{$prefix}extranet",
				'type' => 'checkbox',
			),
		),
	);

	return $meta_boxes;
}

# prise en charge RUL pour les archives
add_action('init', 'rapport_archive_rewrite');
function rapport_archive_rewrite(){
   add_rewrite_rule('^rapport/([0-9]{4})/([0-9]{2})/?','index.php?post_type=rapport&year=$matches[1]&monthnum=$matches[2]','top');
   add_rewrite_rule('^rapport/([0-9]{4})/?','index.php?post_type=rapport&year=$matches[1]','top');
}

# shortcode : affichage des rapports
add_shortcode('etudes_recherche', 'display_etudes_recherche');
function display_etudes_recherche ($atts, $content) {
    # utilisateur
    global $current_user;
    get_currentuserinfo();
    $roleUtilisateur = get_user_role();
    
    # langue
    $currentLang =  get_locale();
    if ($currentLang == "fr_FR") {
        $lireLaSuite = "Lire la suite";
        $telecharger = "Télécharger";
        $keywordss = "Mots-clés :";
        $resultats = "Résultats de recherche pour :";
        $archives = "Archives pour :";
        $archivesA = "Archives pour l'année :";
        $par = "Par";
    } else {
        $lireLaSuite = "See more";
        $telecharger = "Download";
        $keywordss = "Keywords:";
        $resultats = "Search results for:";
        $archives = "Archives for:";
        $archivesA = "Archives by Year for:";
        $par = "By";
    }
    
    # moteur de recherche
    ?>
    <?php //echo do_shortcode( '[searchandfilter id="776"]' ); ?>
    <?php //echo do_shortcode( '[searchandfilter id="776" show="results"]' ); ?>
    <?php
    /**/
    # liste des rapports
    $args = array( 'post_type' => 'rapport', 'posts_per_page' => 10 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
    
		# accès extranet
        $acces = 1;
        $extranet = rwmb_meta('rapport_extranet');
        if ($extranet == 0) {
            $acces = 1;
        } else if ( @$extranet == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
            $acces = 1;
        } else {
            $acces = 0;
        }
        
        //if ($acces == 1 && date("Y") == get_the_date("Y")) {
        if ($acces == 1) {
    		# auteur
            $arrAllAut = get_the_terms(get_the_ID(), 'rapport_auteur', 'fields=names');
            $arrAut = array();
            if ($arrAllAut[0]->name != "") foreach ($arrAllAut as $aut) $arrAut[] = $aut->name;
            # numéro
            $arrAllNum = get_the_terms(get_the_ID(), 'rapport_numero', 'fields=names');
            $arrNum = array();
            if ($arrAllNum[0]->name != "") foreach ($arrAllNum as $num) $arrNum[] = $num->name;
            # tags
            $arrAllTags = get_the_terms(get_the_ID(), 'rapport_tag', 'fields=names');
            $arrTags = array();
            if ($arrAllTags[0]->name != "") foreach ($arrAllTags as $tag) $arrTags[] = $tag->name;
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post rapport-list'); ?>>
                <div class="wpb_row vc_row-fluid full-row">
                    <div class="col-md-12">
                        <h4><?php the_title() ?></h4>
                    </div>
                </div>
                <div class="wpb_row vc_row-fluid full-row">
                    <div class="col-md-2">
                        <img src="http://admin.verticrawl.com/search/screenshot.php?DATABASE=3f4e781dbe09a35fcfbfb0c08de85684&amp;url=<?php echo rwmb_meta('rapport_documentged'); ?>">
                    </div>
                    <div class="col-md-10">
            			<h6 class="blog-time">
                			<?php if ($currentLang == "fr_FR") { ?>
                			    <?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?> 
                			 <?php } else { ?>
                			    <?php the_time('m') ?>/<?php the_time('d') ?>/<?php the_time('Y') ?> 
                			 <?php } ?>
                        </h6>
            			<?php if ($arrAllNum[0]->name != "") { ?><h6 class="blog-number"><strong>réf. </strong> <?php echo implode(", ", $arrNum); ?> </h6><?php } ?>
            			<?php if ($arrAllAut[0]->name != "") { ?><h6 class="blog-author"><strong><?php echo $par; ?> </strong> <?php echo implode(", ", $arrAut); ?> </h6><?php } ?>
            			<p>
                        <?php echo mb_substr( strip_tags( preg_replace( '/\[[^\]]+\]/', '', get_the_content()) ), 0, 150, "UTF-8" ).'&hellip;'; ?>
            			</p>
                    </div>
                </div>
                <div class="wpb_row vc_row-fluid full-row">
                    <div class="col-md-6 mots-cles"><?php if ($arrAllTags[0]->name != "") { ?><strong><?php echo $keywordss; ?></strong> <?php echo implode(", ", $arrTags); ?><?php } ?></div>
                    <div class="col-md-6 lire-suite">
                        <ul>
                            <li><a href="<?php the_permalink(); ?>"><?php echo $lireLaSuite; ?> <i class="fa-chevron-right"></i></a></li>
                            <li><a href="<?php echo do_shortcode('[download url="'.rwmb_meta('rapport_documentged').'"]'); ?>" target="_blank"><?php echo $telecharger; ?> <i class="fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <hr class="vertical-space">
            </article>
            <?php
        }
    endwhile;
    /**/
}

# shortcode : affichage des archives des rapports
add_shortcode('etudes_recherche_archive', 'display_etudes_recherche_archive');
function display_etudes_recherche_archive ($atts, $content) {
    global $wpdb;
    
    # utilisateur
    global $current_user;
    get_currentuserinfo();
    $roleUtilisateur = get_user_role();
    
    # langue
    $currentLang =  get_locale();
    
    if ($currentLang == "fr_FR") {
        $results = $wpdb->get_results("SELECT ID, YEAR(post_date) as ANNEE FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'rapport' ORDER BY post_date DESC");
    } else {
        $results = $wpdb->get_results("SELECT p.ID, YEAR(p.post_date) as ANNEE FROM $wpdb->posts as p, ".$wpdb->prefix."icl_translations as i WHERE i.element_id = p.ID AND i.language_code = 'en' AND p.post_status = 'publish' AND p.post_type = 'rapport' ORDER BY p.post_date DESC");
    }
    $arrAnnee = array();
    foreach($results as $result): 
        $acces = 1;
        $extranet = rwmb_meta('rapport_extranet', '', $result->ID);
        if ($extranet == 0) {
            $acces = 1;
        } else if ( @$extranet == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
            $acces = 1;
        } else {
            $acces = 0;
        }
        if ($acces == 1) {
            $arrAnnee[] = $result->ANNEE;
        }
    endforeach;
    $arrAnnee = array_unique($arrAnnee);
    
    ob_start();
    
    $content = '<ul class="archives-link">';
    foreach ($arrAnnee as $annee) {
        $content.= '<li><a href="'.get_year_link('rapport/'.$annee).'">'.$annee.'</a></li>';
    }
    $content.= '</ul>';
    
    echo $content;
        
    $output = ob_get_clean();
    return $output;
}

