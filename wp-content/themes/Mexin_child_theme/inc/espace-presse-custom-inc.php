<?php

# initialisation du custom post type
function custom_post_presse() {
  $labels = array(
    'name'               => _x( 'Espace presse', 'post type general name' ),
    'singular_name'      => _x( 'Espace presse', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter un nouveau communiqué' ),
    'edit_item'          => __( 'Modifier un communiqué' ),
    'new_item'           => __( 'Nouveau communiqué' ),
    'all_items'          => __( 'Tous les communiqués' ),
    'view_item'          => __( 'Voir le communiqué' ),
    'search_items'       => __( 'Rechercher dans les communiqués' ),
    'not_found'          => __( 'Aucun communiqué trouvé' ),
    'not_found_in_trash' => __( 'Aucun communiqué trouvé dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Espace presse'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion de l\'espace presse',
    'public'        => true,
    'menu_position' => 13,
    'menu_icon'     => 'dashicons-clipboard',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'espace-presse', $args ); 
}
add_action( 'init', 'custom_post_presse' );

# custom message
function updated_messages_presse( $messages ) {
  global $post, $post_ID;
  $messages['espace-presse'] = array(
    0 => '', 
    1 => sprintf( __('Communiqué mis à jour. <a href="%s">Voir le communiqué</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Communiqué mis à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Communiqué restauré à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Communiqué publié. <a href="%s">Voir le communiqué</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Communiqué enregistré.'),
    8 => sprintf( __('Communiqué envoyé. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Communiqué planifié pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_presse' );

# custom categories
function taxonomies_presse() {
  $labels = array(
    'name'              => _x( 'Catégories Presse', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'presse_category', 'espace-presse', $args );
}
add_action( 'init', 'taxonomies_presse', 0 );

# custom meta box
add_filter( 'rwmb_meta_boxes', 'presse_box' );
function presse_box( $meta_boxes )
{
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'espace-presse_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Document associé', 'espace-presse' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'espace-presse' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE ADVANCED (WP 3.5+)
			array(
				'name'             => __( '', 'espace-presse' ),
				'id'               => "{$prefix}document",
				'type'             => 'file_advanced',
				'max_file_uploads' => 1,
				'mime_type'        => '', // Leave blank for all file types
			),
		),
	);

	return $meta_boxes;
}

# prise en charge RUL pour les archives
add_action('init', 'presse_archive_rewrite');
function presse_archive_rewrite(){
   add_rewrite_rule('^espace-presse/([0-9]{4})/([0-9]{2})/?','index.php?post_type=espace-presse&year=$matches[1]&monthnum=$matches[2]','top');
   add_rewrite_rule('^espace-presse/([0-9]{4})/?','index.php?post_type=espace-presse&year=$matches[1]','top');
}

