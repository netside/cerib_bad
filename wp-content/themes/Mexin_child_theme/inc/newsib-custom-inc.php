<?php
    
# initialisation du custom post type
function custom_post_newsib() {
  $labels = array(
    'name'               => _x( 'News IB', 'post type general name' ),
    'singular_name'      => _x( 'News IB', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter une news IB' ),
    'edit_item'          => __( 'Modifier une news IB' ),
    'new_item'           => __( 'Nouvelle news IB' ),
    'all_items'          => __( 'Toutes les news IB' ),
    'view_item'          => __( 'Voir la news IB' ),
    'search_items'       => __( 'Rechercher dans les news IB' ),
    'not_found'          => __( 'Aucune news IB trouvée' ),
    'not_found_in_trash' => __( 'Aucune news IB trouvée dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'News IB'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des news IB',
    'public'        => true,
    'menu_position' => 17,
    'menu_icon'     => 'dashicons-format-aside',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'newsib', $args ); 
}
add_action( 'init', 'custom_post_newsib' );

# custom message
function updated_messages_newsib( $messages ) {
  global $post, $post_ID;
  $messages['newsib'] = array(
    0 => '', 
    1 => sprintf( __('News IB mise à jour. <a href="%s">Voir la news IB</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('News IB mise à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('News IB restaurée à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('News IB publiée. <a href="%s">Voir la newsletter</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('News IB enregistrée.'),
    8 => sprintf( __('News IB envoyée. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('News IB planifiée pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_newsib' );

# custom categories
function taxonomies_newsib() {
  $labels = array(
    'name'              => _x( 'Catégorie News IB', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'newsib_categorie', 'newsib', $args );
}
add_action( 'init', 'taxonomies_newsib', 0 );

# prise en charge RUL pour les archives
add_action('init', 'newsib_archive_rewrite');
function newsib_archive_rewrite(){
   add_rewrite_rule('^newsib/([0-9]{4})/([0-9]{2})/?','index.php?post_type=newsib&year=$matches[1]&monthnum=$matches[2]','top');
   add_rewrite_rule('^newsib/([0-9]{4})/?','index.php?post_type=newsib&year=$matches[1]','top');
}
