<?php
    
# initialisation du custom post type
function custom_post_formation() {
  $labels = array(
    'name'               => _x( 'Formations', 'post type general name' ),
    'singular_name'      => _x( 'Formation', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter une nouvelle formation' ),
    'edit_item'          => __( 'Modifier une formation' ),
    'new_item'           => __( 'Nouvelle formation' ),
    'all_items'          => __( 'Toutes les formations' ),
    'view_item'          => __( 'Voir la formation' ),
    'search_items'       => __( 'Rechercher dans les formations' ),
    'not_found'          => __( 'Aucune formation trouvée' ),
    'not_found_in_trash' => __( 'Aucune formation trouvée dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Formations'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des formations',
    'public'        => true,
    'menu_position' => 12,
    'menu_icon'     => 'dashicons-welcome-learn-more',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'formation', $args ); 
}
add_action( 'init', 'custom_post_formation' );

# custom message
function updated_messages_formation( $messages ) {
  global $post, $post_ID;
  $messages['formation'] = array(
    0 => '', 
    1 => sprintf( __('Formation mise à jour. <a href="%s">Voir la formation</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Formation mise à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Formation restaurée à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Formation publiée. <a href="%s">Voir la formation</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Formation enregistrée.'),
    8 => sprintf( __('Formation envoyée. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Formation planifiée pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_formation' );

# custom categories
function taxonomies_formation() {
  $labels = array(
    'name'              => _x( 'Catégories Formation', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'formation_category', 'formation', $args );
}
add_action( 'init', 'taxonomies_formation', 0 );

# custom meta box
add_filter( 'rwmb_meta_boxes', 'formation_box' );
function formation_box( $meta_boxes )
{
    
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'formation_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Informations', 'formation' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'formation' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// DATE
			array(
				'name'       => __( 'Date de début', 'formation' ),
				'id'         => "{$prefix}datedebut",
				'type'       => 'date',

				// jQuery date picker options. See here http://api.jqueryui.com/datepicker
				'js_options' => array(
					'appendText'      => __( ' (aaaa/mm/dd)', 'formation' ),
					'dateFormat'      => __( 'yy/mm/dd', 'formation' ),
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
			),
			// DATE
			array(
				'name'       => __( 'Date de fin', 'formation' ),
				'id'         => "{$prefix}datefin",
				'type'       => 'date',

				// jQuery date picker options. See here http://api.jqueryui.com/datepicker
				'js_options' => array(
					'appendText'      => __( ' (aaaa/mm/dd)', 'formation' ),
					'dateFormat'      => __( 'yy/mm/dd', 'formation' ),
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Contact', 'formation' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}contact",
				'type'  => 'text',
			),
			// EMAIL
			array(
				'name' => __( 'E-mail de contact', 'formation' ),
				'id'   => "{$prefix}email_contact",
				'type' => 'email',
				// Field description (optional)
				'desc'  => __( 'Si vide, e-mail de l\'administrateur par défaut', 'formation' ),
			),
		),
	);

	// 2nd meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard2',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Mise en avant', 'formation' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'formation' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// RADIO BUTTONS
			array(
				'name'    => __( '', 'formation' ),
				'id'      => "{$prefix}enavant",
				'type'    => 'radio',
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'0' => __( 'non', 'formation' ),
					'1' => __( 'oui', 'formation' ),
				),
				'std'    => '0',
			),
		),
	);

	return $meta_boxes;
}

# shortcode : affichage des évènements
add_shortcode('formation', 'display_formation');
function display_formation ($atts, $content) {
    
    $args = array( 'post_type' => 'formation', 'posts_per_page' => 100, 'meta_key'=>'formation_datedebut', 'orderby' => 'formation_datedebut', 'order' => 'ASC' );
    $loop = new WP_Query( $args );
    $arrThemeCode = array();
    $arrThemeName = array();
    while ( $loop->have_posts() ) : $loop->the_post();
        # données date
        $datedebut = rwmb_meta('formation_datedebut');
        $datefin = rwmb_meta('formation_datefin');
        $datenowF = date("Y/m/d");
        $classPasse = 0;
        if ($datenowF != $datefin) {
            $arrDate = array($datefin, $datenowF);
            sort($arrDate);
            if ($arrDate[1] == $datenowF) $classPasse = 1;
        }
        $enavant = rwmb_meta('formation_enavant');
        //if ($classPasse == 0 && (($atts["type"] == "en-avant" && $enavant == 1) || ($atts["type"] == "prochaines"))) {
        if ((($atts["type"] == "en-avant" && $enavant == 1) || ($atts["type"] == "prochaines"))) {
            # données thèmes
            $arrAllTags = array();
            $arrAllTags = get_the_terms(get_the_ID(), 'formation_category', 'fields=names');
            if ($arrAllTags != "") {
                foreach ($arrAllTags as $tag) {
                    $arrThemeCode[] = "theme-".$tag->slug;
                    $arrThemeName[] = $tag->name;
                }
            }
        }
    endwhile;
    $arrThemeCode = array_unique($arrThemeCode);
    $arrThemeName = array_unique($arrThemeName);
    sort($arrThemeCode);
    sort($arrThemeName);
    # filtre
    ?>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="col-md-12 formation-filtres">
            <ul>
                <li>FILTRER</li>
                <li>
                    <a href="#" class="filtre-label">Par thème <i class="fa-chevron-down"></i></a>
                    <select name="filtre-theme-<?php echo $atts["type"]; ?>[]" id="filtre-theme-<?php echo $atts["type"]; ?>" multiple="multiple">
                        <?php for ($c=0; $c<count($arrThemeCode); $c++) { ?>
                        <option value="<?php echo $arrThemeCode[$c]; ?>"><?php echo $arrThemeName[$c]; ?></option>
                        <?php } ?>
                    </select>
                </li>
            </ul>
            <div class="formation-separateur"></div>
        </div>
    </div>
    <?php
    # liste
    while ( $loop->have_posts() ) : $loop->the_post();
        # données thèmes
        $arrAllTags = array();
        $arrAllTags = get_the_terms(get_the_ID(), 'formation_category', 'fields=names');
        $arrTagsCode = array();
        $arrTagsName = array();
        if ($arrAllTags != "") {
            foreach ($arrAllTags as $tag) {
                $arrTagsCode[] = "theme-".$tag->slug;
                $arrTagsName[] = $tag->name;
            }
        }
        
        # données date
        $datedebut = rwmb_meta('formation_datedebut');
        $datefin = rwmb_meta('formation_datefin');
        $datenowF = date("Y/m/d");
        $classPasse = 0;
        if ($datenowF == $datedebut) {
            $classPasse = 1;
        } else {
            $arrDate = array($datedebut, $datenowF);
            sort($arrDate);
            if ($arrDate[1] == $datenowF) $classPasse = 1;
        }
        # si mis en avant
        $enavant = rwmb_meta('formation_enavant');
        if ($classPasse == 0 && (($atts["type"] == "en-avant" && $enavant == 1) || ($atts["type"] == "prochaines"))) {
        //if ((($atts["type"] == "en-avant" && $enavant == 1) || ($atts["type"] == "prochaines"))) {
            ?>
            <div class="col-md-6 formation-list-bloc <?php echo implode(" ", $arrTagsCode); ?>">
                <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post formation-list clearfix'); ?>>
                    <div class="wpb_row vc_row-fluid full-row">
                        <div class="col-xs-3 formation-date formation-date-list">
                        <span class="jour"><?php echo date("d", strtotime($datedebut)); ?></span>
                        <span class="mois"><?php echo date("m", strtotime($datedebut)); ?></span>
                        </div>
                        <div class="col-xs-9 formation-item">
                            <h4>
                                <?php the_title() ?>
                            </h4>
                			<p class="formation-theme-lieu">
                    			<span class="formation-theme-label">Thème</span> <?php echo implode(", ", $arrTagsName); ?>
                            </p>
                        </div>
                    </div>
                    <div class="wpb_row vc_row-fluid full-row">
                        <div class="formation-lien">
                            <?php /*if (@$_GET["passe"] != 1) { ?>
                            <a href="#?" class="col-xs-6">Pré-inscription</a>
                            <?php }*/ ?>
                            <a href="<?php echo get_post_permalink(); ?>" class="col-xs-<?php echo (@$_GET["passe"] == 1)?"12":"12" ?>">Voir le programme</a>
                        </div>
                    </div>
                </article>
                <hr class="vertical-space1">
            </div>
            <?php
        }
    endwhile;
}

# formulaire inscription
function ajaxformation_enqueuescripts() {
    wp_enqueue_script('ajaxformation', '/wp-content/themes/Mexin_child_theme/js/ajax-formation-inscription.js', array('jquery'));
    wp_localize_script( 'ajaxformation', 'ajaxformationajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts', 'ajaxformation_enqueuescripts');

function ajaxformation_show_inscription($idformation) {
    $post_formation = get_post($idformation); 
    ?>
    <form id="ajaxformationform" action="" method="post"enctype="multipart/form-data">
        <input type="hidden" id="ajaxformationidformation" name="ajaxformationidformation" value="<?php echo $idformation; ?>">
        <div id="ajaxformation-text">
            <h3>
                Pré-inscription pour  :
                <?php echo $post_formation->post_title; ?>
            </h3>
            <div class="wpb_row vc_row-fluid full-row">
                Ce formulaire constitue une pré-inscription.<br>
                Nous vous confirmerons votre inscription définitive dès réception d’un bulletin d’inscription complété et signé.
            </div>
            <div class="wpb_row vc_row-fluid full-row">
                <hr class="vertical-space">
                <div class="col-md-6">
                    <h5>Le stagiaire</h5>
                    <ul class="bloc-left">
                        <li><input type="text" id="ajaxformationstagiairenom" name="ajaxformationstagiairenom" placeholder="* Nom"></li>
                        <li><input type="text" id="ajaxformationstagiaireprenom" name="ajaxformationstagiaireprenom" placeholder="* Prénom"></li>
                        <li><input type="text" id="ajaxformationstagiairefonction" name="ajaxformationstagiairefonction" placeholder="Fonction"></li>
                        <li><input type="text" id="ajaxformationstagiaireemail" name="ajaxformationstagiaireemail" placeholder="* E-mail"></li>
                        <li><input type="text" id="ajaxformationstagiaireintitule" name="ajaxformationstagiaireintitule" placeholder="Intitulé"></li>
                        <li><input type="text" id="ajaxformationstagiairereference" name="ajaxformationstagiairereference" placeholder="* Référence"></li>
                        <li><input type="text" id="ajaxformationstagiairelieu" name="ajaxformationstagiairelieu" placeholder="* Lieu"></li>
                        <li><input type="email" id="ajaxformationstagiairedate" name="ajaxformationstagiairedate" placeholder="* Dates prévues"></li>
                        <li><label><input type="checkbox" id="ajaxformationnewsletter" name="ajaxformationnewsletter" value="1"> Je souhaite recevoir toutes les informations du Cerib</label></li>
                    </ul>
                    <span class="small">* Champs obligatoires</span>
                </div>
                <div class="col-md-6">
                    <h5>La société</h5>
                    <ul class="bloc-right">
                        <li><input type="text" id="ajaxformationsocieteraisonsociale" name="ajaxformationsocieteraisonsociale" placeholder="* Raison sociale"></li>
                        <li><input type="text" id="ajaxformationsocietetva" name="ajaxformationsocietetva" placeholder="N° TVA intra"></li>
                        <li><textarea id="ajaxformationsocieteadresse" name="ajaxformationsocieteadresse" rows="4" placeholder="Adresse"></textarea></li>
                        <li><input type="text" id="ajaxformationsocietetelephone" name="ajaxformationsocietetelephone" placeholder="* Téléphone"></li>
                        <li><input type="text" id="ajaxformationsocieteemail" name="ajaxformationsocieteemail" placeholder="* E-mail"></li>
                    </ul>
                </div>
            </div>
            <hr class="vertical-space">
            <div class="wpb_row vc_row-fluid full-row">
                <div class="col-md-6">
                    <div id="ajaxformation-response"></div>
                </div>
                <div class="col-md-6">
                    <a onclick="ajaxformsendmail_formation();" class="submit-button" style="cursor: pointer">Envoyer et valider mon inscription</a>
                </div>
            </div>
        </div>
    </form>
    <?php
}

function ajaxformation_send_mail() {
    global $wpdb;
    $result = $wpdb->get_results( "SELECT COUNT(*) AS presence FROM pzebfiv_formations_inscription WHERE formation_id = '".$_POST["acfidformation"]."' AND stagiaire_email = '".$_POST["acfstagiaireemail"]."'" );
    #
    $post_formation = get_post($_POST["acfidformation"]);
    #
    $idformation = $_POST["acfidformation"];
    $stagiairenom = stripslashes($_POST["acfstagiairenom"]);
    $stagiaireprenom = stripslashes($_POST["acfstagiaireprenom"]);
    $stagiairefonction = stripslashes($_POST["acfstagiairefonction"]);
    $stagiaireemail = stripslashes($_POST["acfstagiaireemail"]);
    $stagiaireintitule = stripslashes($_POST["acfstagiaireintitule"]);
    $stagiairereference = stripslashes($_POST["acfstagiairereference"]);
    $stagiairelieu = stripslashes($_POST["acfstagiairelieu"]);
    $stagiairedate = stripslashes($_POST["acfstagiairedate"]);
    $societeraisonsociale = stripslashes($_POST["acfsocieteraisonsociale"]);
    $societeadresse = stripslashes($_POST["acfsocieteadresse"]);
    $societetelephone = stripslashes($_POST["acfsocietetelephone"]);
    $societeemail = stripslashes($_POST["acfsocieteemail"]);
    $societetva = stripslashes($_POST["acfsocietetva"]);
    $contact_email = rwmb_meta('formation_email_contact', '', $_POST["acfidformation"]);
    $admin_email = ($contact_email!="")?$contact_email:get_option('admin_email');
    #
    $results = "";
    $error = 0;
    #
    if ($result[0]->presence > 0) {
        $results = "Votre pré-inscription a déjà été prise en compte avec cette adresse e-mail.";
        $error = 1;
    } else if (!filter_var($stagiaireemail, FILTER_VALIDATE_EMAIL)) {
        $results = "L'adresse e-mail du stagiaire n'est pas valide.";
        $error = 1;
    } else if (!filter_var($societeemail, FILTER_VALIDATE_EMAIL)) {
        $results = "L'adresse e-mail de la société n'est pas valide.";
        $error = 1;
    } else if( strlen($stagiairenom) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($stagiaireprenom) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($stagiairereference) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($stagiairelieu) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($stagiairedate) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($societeraisonsociale) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($societetelephone) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($societeemail) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    }
    #
    if($error == 0) {
        if ($newsletter == 1) {
            $timeNow = mktime();
            $wpdb->insert( 
            	'pzebfiv_wysija_user', 
            	array( 
            		'wpuser_id' => 0, 
            		'email' => $stagiaireemail , 
            		'firstname' => $stagiaireprenom , 
            		'lastname' => $stagiairenom ,
            		'created_at' => $timeNow , 
            		'status' => 1
            	)
            );
            $wpdb->insert( 
            	'pzebfiv_wysija_user_list', 
            	array( 
            		'list_id' => 1, 
            		'user_id' => $wpdb->insert_id , 
            		'sub_date' => $timeNow 
            	)
            );
        }
        $result = $wpdb->insert( 
        	'pzebfiv_formations_inscription', 
        	array( 
        		'formation_id' => $idformation, 
        		'date' => current_time('mysql', 1) , 
        		'stagiaire_nom' => $stagiairenom , 
        		'stagiaire_prenom' => $stagiaireprenom , 
        		'stagiaire_fonction' => $stagiairefonction , 
        		'stagiaire_email' => $stagiaireemail , 
        		'stagiaire_intitule' => $stagiaireintitule , 
        		'stagiaire_reference' => $stagiairereference , 
        		'stagiaire_lieu' => $stagiairelieu , 
        		'stagiaire_dates_prevues' => $stagiairedate , 
        		'societe_raison_sociale' => $societeraisonsociale , 
        		'societe_adresse' => $societeadresse , 
        		'societe_telephone' => $societetelephone , 
        		'societe_email' => $societeemail , 
        		'societe_tva' => $societetva 
        	)
        );
        if ($result == 1) {
            # e-mail à l'administrateur
            $headers = 'From: CERIB <'.$admin_email.'>';
            $subject = 'Cerib.com - pré-inscription à une formation';
            $contents = "Une pré-inscription pour la formation : " . $post_formation->post_title . "<br><br>\r\n\r\n";
            $contents.= "Voici les données du formulaire, ci-dessous.<br><br>\r\n\r\n";
            $contents.= "Stagiaire : <br><br>\r\n\r\n";
        	$contents.= "Nom : " . $stagiairenom ."<br>\r\n";
        	$contents.= "Prénom : " . $stagiaireprenom ."<br>\r\n";
        	$contents.= "Fonction : " . $stagiairefonction ."<br>\r\n";
        	$contents.= "E-mail : " . $stagiaireemail ."<br>\r\n";
        	$contents.= "Intitulé : " . $stagiaireintitule ."<br>\r\n";
        	$contents.= "Référence : " . $stagiairereference ."<br>\r\n";
        	$contents.= "Lieu : " . $stagiairelieu ."<br>\r\n";
        	$contents.= "Dates prévues : " . $stagiairedate ."<br><br>\r\n\r\n";
            $contents.= "Société : <br><br>\r\n\r\n";
        	$contents.= "Raison sociale : " . $societeraisonsociale ."<br>\r\n";
        	$contents.= "Adresse : " . $societeadresse ."<br>\r\n";
        	$contents.= "Téléphone : " . $societetelephone ."<br>\r\n";
        	$contents.= "E-mail : " . $societeemail ."<br>\r\n";
        	$contents.= "TVA : " . $societetva ."<br>\r\n";
        	$contents.= "Newsletter : " . (($newsletter==1)?"oui":"non") ."<br>\r\n";
        	$contents.= "<br>\r\n--<br>\r\nCet email a été envoyé via le formulaire de pré-inscription à une formation de Cerib.com (http://www.cerib.com)<br>\r\n";
        	#
            if(wp_mail($admin_email, $subject, $contents, $headers)) {
                //$results = "*Thanks for you mail.";
            } else {
                //$results = "*The mail could not be sent.";
            }
            # e-mail à l'internaute
            $headers = 'From: CERIB <'.$admin_email.'>';
            $subject = 'Cerib.com - pré-inscription à une formation';
        	$contents = '
            Bonjour,<br><br>
            Nous avons bien reçu votre formulaire de pré-inscription pour la session : <br><br>
            '.$post_formation->post_title.'<br><br>
            Nous vous confirmerons votre inscription définitive dès réception d’un bulletin d’inscription complété et signé.<br><br>
            A très vite<br>
            Le Centre de Formation du Cerib<br>
        	';
        	#
            if(wp_mail($stagiaireemail, $subject, $contents, $headers)) {
                //$results = "*Thanks for you mail.";
            } else {
                //$results = "*The mail could not be sent.";
            }
            #
            $results = "Votre pré-inscription a bien été prise en compte.";
        } else {
            $results = "Une erreur est survenue lors de l'inscription.";
        }
    }
    
    // Return the String
    die($results);
}
add_action( 'wp_ajax_nopriv_ajaxformation_send_mail', 'ajaxformation_send_mail' );
add_action( 'wp_ajax_ajaxformation_send_mail', 'ajaxformation_send_mail' );

function ajaxformation_shortcode_func( $atts ) {
    ob_start();
    ajaxformation_show_inscription($atts["id"]);
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
add_shortcode( 'ajaxformation', 'ajaxformation_shortcode_func' );


# écran des inscriptions formation
add_action('admin_menu', 'add_admin_menu_formation');
function add_admin_menu_formation() {
    add_submenu_page('edit.php?post_type=formation', 'Pré-inscriptions', 'Pré-inscriptions', 'manage_options', 'formation_inscriptions', 'menu_html_formation');
}
function menu_html_formation() {
    global $wpdb;
    
    if (isset($_GET["idinscription"]) && $_GET["idinscription"]>0) {
        $inscription = $wpdb->get_results("SELECT * FROM pzebfiv_formations_inscription WHERE inscription_id = '".$_GET["idinscription"]."'");
        $post_formation = get_post($inscription[0]->formation_id);
        ?>
        <div class="wrap">
     
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Pré-inscription pour la formation : <?php echo $post_formation->post_title; ?></h2>
            <br><br>
            <div id="poststuff" class="metabox-holder has-right-sidebar">
                <div id="post-body-content" style="margin-right: 300px; width: auto;">
                    <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                        <div id="inboundfieldsdiv" class="postbox ">
                            <h3 class="hndle ui-sortable-handle" style="padding: 8px 12px; margin-top: 0;"><span>Données du formulaire</span></h3>
                            <div class="inside">
                                <table class="widefat message-fields" cellspacing="0">
                                    <tbody>
                                        <tr class="alt">
                                            <th colspan="2"><strong>Le Stagiaire</strong></th>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title" width="30%">Date</td>
                                            <td class="field-value"><?php echo date("d/m/Y à H:i:s", strtotime($inscription[0]->date)); ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Nom du stagiaire</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_nom; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Prénom du stagiaire</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_prenom; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Fonction du stagiaire</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_fonction; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">E-mail du stagiaire</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_email; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Intitulé</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_intitule; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Référence</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_reference; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Lieu</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_lieu; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Dates prévues</td>
                                            <td class="field-value"><?php echo $inscription[0]->stagiaire_dates_prevues; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="widefat message-fields" cellspacing="0">
                                    <tbody>
                                        <tr class="alt">
                                            <th colspan="2"><strong>La société</strong></th>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title" width="30%">Raison sociale</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe_raison_sociale; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Adresse</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe_adresse; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Téléphone</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe_telephone; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">E-mail</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe_email; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">N° TVA intra</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe_tva; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="publishing-action">
                        <a href="<?php echo home_url('/'); ?>wp-admin/edit.php?post_type=formation&page=formation_inscriptions&idformation=<?php echo $_GET["idformation"]; ?>">
                            <input name="retour" type="button" class="button-primary" id="retour" value="Retour à la liste">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        //Create an instance of our package class...
        $testListTable = new TT_Formation_List_Table();
        //Fetch, prepare, sort, and filter our data...
        $testListTable->prepare_items();
        ?>
        <div class="wrap">
     
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Pré-inscriptions aux formations</h2>       
            <br><br>
            <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
            <form id="formation-filter" method="get">
                <!-- For plugins, we also need to ensure that the form posts back to our current page -->
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <!-- Now we can render the completed list table -->
                <?php $testListTable->display() ?>
            </form>
     
        </div>
        <?php
    }
}


# liste des inscriptions 
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
 
class TT_Formation_List_Table extends WP_List_Table {
 
    function __construct() {
        global $status, $page;
 
        //Set parent defaults
        parent::__construct(array(
            'singular' => 'Nice Link', //singular name of the listed records
            'plural' => 'Nice Links', //plural name of the listed records
            'ajax' => false        //does this table support ajax?
        ));
    }
    
    /**
     * Add extra markup in the toolbars before or after the list
     * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
     */
    function extra_tablenav( $which ) {
       if ( $which == "top" ){
          //The code that goes before the table is here
          global $wpdb;
          $results = $wpdb->get_results("SELECT ID, post_title, post_date FROM pzebfiv_posts WHERE post_type = 'formation' AND post_status = 'publish' ORDER BY post_title ");
          ?>
            <div class="alignleft actions">
                <select name="idformation" id="idformation" class="global-filter">
                    <option value="0">- Toutes les formations -</option>
                    <?php foreach ($results as $item) { ?>
                    <option value="<?php echo $item->ID; ?>"<?php echo ($_GET["idformation"]==$item->ID)?" selected=\"selected\"":""; ?>><?php echo $item->post_title; ?></option>
                    <?php } ?>
                </select>
                <input type="button" name="filter_action" id="filter_action" class="button" value="Filtrer">
                <input type="button" name="export_action" id="export_action" class="button" value="Export csv">
            </div>          
            <script type="text/javascript">
                jQuery("#filter_action").click(function(){
                    var id = jQuery("#idformation").val();
                    window.location.href = '<?php echo home_url('/'); ?>wp-admin/edit.php?post_type=formation&page=formation_inscriptions&idformation='+id;
                });
                jQuery("#export_action").click(function(){
                    window.location.href = '<?php echo home_url('/'); ?>wp-content/themes/Mexin_child_theme/inc/export-formation.php?export=download_csv&exportkey=<?php echo EXP_KEY; ?>';
                });
            </script>
          <?php
       }
       if ( $which == "bottom" ){
          //The code that goes after the table is there
          //echo"Hi, I'm after the table";
       }
    }
 
    function column_default($item, $column_name) {
        $post_formation = get_post($item->formation_id);
        
        switch ($column_name) {
            case 'formation_id':
                return "<a href=\"". home_url('/') ."wp-admin/edit.php?post_type=formation&page=formation_inscriptions&idformation=".$_GET["idformation"]."&idinscription=".$item->inscription_id."\">".$post_formation->post_title."</a>";
            case 'date':
                return date("d/m/Y H:i:s", strtotime($item->$column_name));
            case 'stagiaire':
                $out = $item->stagiaire_prenom." ".$item->stagiaire_nom." / ".$item->stagiaire_email;
                return $out;
            case 'societe':
                $out = $item->societe_raison_sociale." - ".$item->societe_telephone." / ".$item->societe_email;
                return $out;
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }
 
    function get_columns() {
        return $columns = array(
            'formation_id' => __('Formation'),
            'date' => __('Date'),
            'stagiaire' => __('Stagiaire'),
            'societe' => __('Société')
        );
    }
    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
       return $sortable = array(
          'evenement_id'=>'evenement_id',
          'date'=>'date'
       );
    }
  
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries
 
           /* -- Preparing your query -- */
        $query = "SELECT * FROM pzebfiv_formations_inscription ";
        if (isset($_GET["idformation"]) && $_GET["idformation"]>0) {
            $query.= " WHERE formation_id = '" . $_GET["idformation"] . "' ";
        }
 
        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'date';
        $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : 'DESC';
        if (!empty($orderby) & !empty($order)) {
            $query.=' ORDER BY ' . $orderby . ' ' . $order;
        }
        //
 
        $totalitems = $wpdb->query($query);
 
        /**
         * First, lets decide how many records per page to show
         */
        $perpage = 5;
 
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
        //Page Number
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }
 
 
        //How many pages do we have in total?
        $totalpages = ceil($totalitems / $perpage);
        //adjust the query to take pagination into account
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query.=' LIMIT ' . (int) $offset . ',' . (int) $perpage;
        }
 
        /* -- Register the pagination -- */
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));
 
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
 
        $this->_column_headers = array($columns, $hidden, $sortable);
 
        $this->items = $wpdb->get_results($query);
    }
 
}
