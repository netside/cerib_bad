<?php

# inscription pour l'évènement JEC
add_shortcode('jecinscription', 'display_jec_inscription');
function display_jec_inscription ($atts, $content) {
    ?>
    <script language="JavaScript" type="text/javascript">
    // -----------------------------------------------------------------------------------------
    // ------------------- Verification de la validité de l'adresse mail. ------------------------
    // ------------------------------- Ne pas modifier -----------------------------------------
    // -----------------------------------------------------------------------------------------
    function verifMail(a){
    
    testm = false;
    reg = new RegExp("^[A-Za-z0-9]+([_\\.\\-\\+][A-Za-z0-9]*)*@[A-Za-z0-9]+([_\\.\\-][A-Za-z0-9]{1,})*\\.([A-Za-z]{2,}){1}$", "");
    var ResultEmail = reg.test(a);
    if (ResultEmail) testm=true;
    return testm;
    
    
    }
    
    function CheckDate(d) {
    
    // Cette fonction vérifie le format JJ/MM/AAAA saisi et la validité de la date.
    // Le séparateur est défini dans la variable separateur
    var amin=1901; // année mini
    var amax=2100; // année maxi
    var separateur="/"; // separateur entre jour/mois/annee
    var j=(d.substring(0,2));
    var m=(d.substring(3,5));
    var a=(d.substring(6));
    var ok=1;
    if ( ((isNaN(j))||(j < 1)||(j > 31)) && (ok==1) ) {
    ok=0;
    }
    if ( ((isNaN(m))||(m < 1)||(m > 12)) && (ok==1) ) {
    ok=0;
    }
    if ( ((isNaN(a))||(a < amin)||(a > amax)) && (ok==1) ) {
    ok=0;
    }
    if ( ((d.substring(2,3)!=separateur)||(d.substring(5,6)!=separateur)) && (ok==1) ) {
    alert("Les séparateurs de date doivent être des +separateur+"); ok=0;
    }
    if (ok==1) {
    
    
    var d2=new Date(a,m-1,j);
    j2=d2.getDate();
    m2=d2.getMonth()+1;
    a2=d2.getFullYear();
    if (a2 <=100) {a2=1900+a2}
    if ( (j!=j2)||(m!=m2)||(a!=a2) ) {
    alert("La date "+d+" n'existe pas !");
    ok=0;
    }
    
    
    }
    return ok;
    
    
    }
    
    // -----------------------------------------------------------------------------------------
    // -------------------- Verification des champs obligatoires -------------------------------
    // Pour rajouter des champs obligatoires, copier coller le code suivant et modifier les noms.
    // -----------------------------------------------------------------------------------------
    function valid(){
    
    var collectElements=document.forms["coupon_form"].elements;
    var MessErreur = "Veuillez corriger les problemes suivants : \n \n";
    var testUtil = 1;
    
    if(document.getElementById('email').value != ""){ 
    
    
    if (verifMail(document.getElementById('email').value) == true) // à enlever si le champs n'est pas l'e-mail
    testUtil = eval(testUtil&1); // Ne pas modifier
    else {
    MessErreur = MessErreur+"\t - Adresse E-mail invalide \n";
    testUtil = eval(testUtil&0); //Ne pas modifier
    }
    
    
    if (document.getElementById('email').value == document.getElementById('controlEmail').value) 
    testUtil = eval(testUtil&1); // Ne pas modifier
    else {
    MessErreur = MessErreur+"\t - Veuillez ressaisir votre adresse e-mail. \n";
    testUtil = eval(testUtil&0); //Ne pas modifier
    }
    
    
    }
    else{
    
    
    MessErreur = MessErreur+"\t - Remplir le champ Adresse E-mail \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    
    
    }
    if(document.coupon_form.do_field_40_11.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Civilité \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    if(document.coupon_form.do_field_41_2.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Nom \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    if(document.coupon_form.do_field_42_1.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Prénom \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    if(document.coupon_form.do_field_43_3.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Société \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    if(document.coupon_form.do_field_44_6.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Code postal \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    if(document.coupon_form.do_field_45_5.value.replace(/(^\s*)|(\s*$)/g,'') == ""){
    MessErreur = MessErreur+"\t - Remplir le champ Ville \n";
    testUtil = eval(testUtil&0); // Ne pas modifier
    }
    
    // --------- Verification de la syntaxe des champs date et des champs numeriques. -----------
    // ------------------------------- NE PAS MODIFIER -----------------------------------------
    for(i=0;i < collectElements.length;i++){
    
    
    // Verification du contenu des champs numériques
    if(collectElements[i].id.substr(0,9) == "customint" || collectElements[i].id.substr(0,12) == "fieldinteger"){
    
    
    if(collectElements[i].value != ""){
    if(isNaN(collectElements[i].value)){
    MessErreur= MessErreur+"\t - "+collectElements[i].value+" n'est pas un chiffre. \n";
    testUtil = eval(testUtil&0);
    }
    }
    
    
    }
    
    // Verification du contenu des champs date
    if(collectElements[i].id.substr(0,10) == "customdate" || collectElements[i].id == "birthdate" || collectElements[i].id.substr(0,13) == "fielddatetime"){
    
    
    if(collectElements[i].value != ""){
    if(CheckDate(collectElements[i].value) == 0){
    MessErreur= MessErreur+"\t - "+collectElements[i].value+" n'est pas sous la forme JJ/MM/AAAA. \n";
    testUtil = eval(testUtil&0);
    }
    }
    
    
    }
    
    
    }
    // -------- Fin vérification de la syntaxe des champs date et des champs numeriques. --------
    
    // ----------------------- Ne pas modifier la partie ci-dessous ----------------------------
    if(testUtil == 1){ document.coupon_form.submit(); return false;}
    else alert(MessErreur);
    
    
    }
    //--------------------------------------------------------------------------------------------------- 
    </script>
    <style>
    .jecform, .jecform th, .jecform tr, .jecform td {
        border: 0;
    }
    .jecform > tr > td > input {
        margin-bottom: 0;
    }
    </style>
    <!-- Debut champs utilisateurs -->
    <form NAME='coupon_form' METHOD='post' ACTION='http://f.info.cerib.com/cw/default.aspx' accept-charset='UTF-8' >
    <input type="hidden" name="do_ListId" value="158C">
    <input type="hidden" name="do_IdCoupon" value="4">
    <input type="hidden" name="do_typ" value="2">
    <input type="hidden" name="do_redirect" value="">
    <input type="hidden" name='do_SponsorId' value="[SPIDKC]">
    <table class="jecform">
    <tr>
    <td align="right">Adresse e-mail<font color="#FF0000"> * </font> : <input type="Text" id="email" name="do_field_39_7" style="margin-bottom: 0;"></td>
    <td>Veuillez ressaisir votre Adresse e-mail<font color="#FF0000"> * </font> : <input type="Text" id="controlEmail" name="controlEmail" onpaste="return false;" style="margin-bottom: 0;"></td>
    </tr>
    <tr>
    <td align="right">Civilité<font color="#FF0000"> * </font> : <input type="Text" id="customstr1" name="do_field_40_11" maxlength="1024" style="margin-bottom: 0;"></td>
    <td>Société<font color="#FF0000"> * </font> : <input type="Text" id="company" name="do_field_43_3" maxlength="128" style="margin-bottom: 0;"></td>
    </tr>
    <tr>
    <td align="right">Nom<font color="#FF0000"> * </font> : <input type="Text" id="lastname" name="do_field_41_2" maxlength="64" style="margin-bottom: 0;"></td>
    <td>Prénom<font color="#FF0000"> * </font> : <input type="Text" id="firstname" name="do_field_42_1" maxlength="64" style="margin-bottom: 0;"></td>
    </tr>
    <tr>
    <td align="right">Code postal<font color="#FF0000"> * </font> : <input type="Text" id="zipcode" name="do_field_44_6" maxlength="16" style="margin-bottom: 0;"></td>
    <td>Ville<font color="#FF0000"> * </font> : <input type="Text" id="city" name="do_field_45_5" maxlength="64" style="margin-bottom: 0;"></td>
    </tr>
    <tr>
    <td align="right">Téléphone : <input type="Text" id="phone" name="do_field_46_8" maxlength="32" style="margin-bottom: 0;"></td>
    <td>Fonction : <input type="Text" id="customstr77" name="do_field_47_77" maxlength="1024" style="margin-bottom: 0;"></td>
    </tr>
    
    <tr>
    <td align="right" valign="top">
        Je souhaite recevoir toutes les informations du Cerib : 
        <select id="interest" name="do_interest_20" style="margin-bottom: 0;">
        <option value="">- Faites votre choix -</option>
        <option value="78">oui</option>
        <option value="79">non</option>
        </select>
    </td>
    <td valign="top">Présence au cocktail : 
        <select id="interest" name="do_interest_21" style="margin-bottom: 0;">
        <option value="">- Faites votre choix -</option>
        <option value="80">oui</option>
        <option value="81">non</option>
        </select>
    </td>
    </tr>
    
    <tr>
    <td align="right" valign="top">
        Commentaire <textarea name='do_fieldSaisie_48' cols="50" rows="5" maxlength="500"></textarea>
    </td>
    <td valign="top">
        Les champs marqués d'un <font color='#FF0000'> * </font> sont obligatoires.
        <br><br>
        <input type="button" onclick="valid();" value="Valider">
    </td>
    </tr>
    </table>
    <font face="Helvetica" size=1 Id=StyleFont2><b>Déclaration CNIL Numéro 1896252<br>Les informations recueillies dans ce formulaire font l'objet d'un traitement informatique destiné à l'usage exclusif de CERIB.<br> Conformément à la loi "Informatique et Libertés" du 6 janvier 1978, vous bénéficiez d'un droit d'accès, de modification, de suppression et d'opposition aux données vous concernant.<br>Si vous souhaitez exercer ces droits, veuillez vous adresser à CERIB – Direction DMC – CS 10010 – 28233 ÉPERNON Cedex ou écrire à <a href="mailto:webmaster@cerib.com">webmaster@cerib.com</a>.<br>Nous nous engageons à ne pas communiquer à des tiers les informations vous concernant sauf si vous nous en donnez l'autorisation.</b></font>
    <br>
    <p> </p> <p style="text-align: center;"> <img src="http://s.info.cerib.com/5516/www/JEC-du-5-07-2016(160x45).jpg" border="0" alt="logo" title="Journée Expertise & Construction" width="105" height="30" /> </p>
    </form>
    <!-- FIN DU CODE DU FORMULAIRE -->
    <?php
}