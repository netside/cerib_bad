<?php
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
    echo '<style>
    #post-body .rwmb-select-advanced, 
    #post-body .select2-container.rwmb-select-advanced {
    	max-width: 430px;
    }
    </style>';
    }
?>