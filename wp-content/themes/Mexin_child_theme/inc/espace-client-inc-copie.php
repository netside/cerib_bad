<?php

# récupère le rôle d'un utilisateur
function get_user_role() {
	global $current_user;
	
	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);
	
	return $user_role;
}

# shortcode : affichage du bloc espace client topbar
add_shortcode('espace-client-connexion', 'display_espace_client_connexion');
function display_espace_client_connexion ($atts, $content) {
    global $current_user;
    get_currentuserinfo();
    $roleUtilisateur = get_user_role();
    /*
    echo 'Username: ' . $current_user->user_login . "\n";
    echo 'User email: ' . $current_user->user_email . "\n";
    echo 'User level: ' . $current_user->user_level . "\n";
    echo 'User first name: ' . $current_user->user_firstname . "\n";
    echo 'User last name: ' . $current_user->user_lastname . "\n";
    echo 'User display name: ' . $current_user->display_name . "\n";
    echo 'User ID: ' . $current_user->ID . "\n";
    */
    
    $content = '
    <div class="wpb_row vc_row-fluid full-row header-contact">
        <div class="col-md-12">
            <h3 class="ubermenu-widgettitle ubermenu-target">Connectez-vous à votre Espace Client :</h3>
        </div>
    </div>
    <div class="wpb_row vc_row-fluid full-row header-contact">
        <div class="col-md-4">
            <ul class="onglet-vertical">
                <li><a href="#" class="active-form-connexion current">Extranet</a></li>
                <li><a href="#" class="active-form-connexion">APEGIB</a></li>
                <li><a href="#" class="active-form-connexion">My Cerib</a></li>
                <li><a href="#" class="active-form-connexion">Plateforme E&R</a></li>
                <li><a href="#" class="active-form-connexion">QualiB</a></li>
                <li><a href="#" class="active-form-connexion">Memento</a></li>
            </ul>
        </div>
        <div class="col-md-8 list-form-connexion">
            <div id="extranet-connexion" class="form-connexion current">';
    if ( is_user_logged_in() && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
        $nameDisplay = ($current_user->user_firstname==""&&$current_user->user_lastname=="")?$current_user->display_name:$current_user->user_firstname." ".$current_user->user_lastname;
    $content.= '
                <p>
                    Bonjour '.$nameDisplay.', 
                    <br><br>
                    Vous êtes connecté à l\'espace Extranet.
                    <br><br>
                    <a href="'.wp_logout_url( home_url() ).'">
                        <input type="button" id="extranet-deconnexion" name="extranet-deconnexion" class="bouton-connexion" value="Déconnexion">
                    </a>
                </p>';
    } else {
    $content.= '
                <form id="form-extranet-login" action="' . wp_login_url(home_url()) . '" method="post">
                    <input type="hidden" name="redirect_to" value="' . home_url('/') . '">
                    <h4>Extranet</h4>
                    <h5>Accès pour les ressortissants du CERIB</h5>
                    <ul>
                        <li><input type="text" id="extranet_log" name="log" placeholder="Identifiant"></li>
                        <li><input type="password" id="extranet_pwd" name="pwd" placeholder="Mot de passe"></li>
                        '.((isset($_GET["login"])&&$_GET["login"]=="failed")?"<li class='erreur'>Erreur de connexion. Veuillez rééssayer.</li>":"").'
                        <li><input type="submit" id="extranet-connexion" name="extranet-connexion" class="bouton-connexion" value="Connexion"></li>
                        <li class="connexion-liens-misc">
                            <a href="#extranet-demande" class="lbp-inline-link-4 cboxElement">Demander un accès</a>
                            <a href="'.home_url('/').'nous-contacter/">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>';
    }
    $content.= '
            </div>
            <div id="apegib-connexion" class="form-connexion">';
    if ( is_user_logged_in() && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib") ) {
        $nameDisplay = ($current_user->user_firstname==""&&$current_user->user_lastname=="")?$current_user->display_name:$current_user->user_firstname." ".$current_user->user_lastname;
    $content.= '
                <p>
                    Bonjour '.$nameDisplay.', 
                    <br><br>
                    Vous êtes connecté à l\'espace APEGIB.
                    <br><br>
                    <a href="http://www.cerib.com/accueil-apegib/">
                        <input type="button" id="acces-apegib" name="acces-apegib" class="bouton-connexion" value="Accéder à votre espace APEGIB">
                    </a>
                    <br>
                    <a href="'.wp_logout_url( home_url() ).'">
                        <input type="submit" id="extranet-deconnexion" name="extranet-deconnexion" class="bouton-connexion" value="Déconnexion">
                    </a>
                </p>';
    } else {
    $content.= '
                <form id="form-apegib-login" action="' . wp_login_url(home_url()) . '" method="post">
                    <input type="hidden" name="redirect_to" value="' . home_url('/') . '">
                    <h4>APEGIB</h4>
                    <h5>Accès pour les ressortissants du CERIB<br>et cotisants APEGIB</h5>
                    <ul>
                        <li><input type="text" id="apegib_log" name="log" placeholder="Identifiant"></li>
                        <li><input type="password" id="apegib_pwd" name="pwd" placeholder="Mot de passe"></li>
                        '.((isset($_GET["login"])&&$_GET["login"]=="failed")?"<li class='erreur'>Erreur de connexion. Veuillez rééssayer.</li>":"").'
                        <li><input type="submit" id="apegib-connexion" name="apegib-connexion" class="bouton-connexion" value="Connexion"></li>
                        <li class="connexion-liens-misc">
                            <a href="#apegib-demande" class="lbp-inline-link-5 cboxElement">Demander un accès</a>
                            <a href="'.home_url('/').'nous-contacter/">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>';
    }
    $content.= '
            </div>
            <div id="mycerib-connexion" class="form-connexion">
                <form action="http://ged.cerib.net/dsextranet/" name="authentification" method="post" target="_blank">
                    <h4>MyCerib <span class="small">(redirection à la plaforme externe)</span></h4>
                    <ul>
                        <li><input type="submit" id="mycerib-connexion" name="mycerib-connexion" class="bouton-connexion" value="Se connecter"></li>
                        <li class="connexion-liens-misc">
                            <a href="#mycerib-demande" class="lbp-inline-link-4 cboxElement">Demander un accès</a>
                            <a href="http://ged.cerib.net/dsextranet/" target="_blank">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>
            </div>
            <div id="plateforme-er-connexion" class="form-connexion">
                <form name="login-er" method="post" action="http://portail-er.cerib.fr/Pages/Default.aspx" target="_blank">
                    <h4>Plateforme E&R<br><span class="small">(redirection à la plaforme externe)</span></h4>
                    <h5>Accès pour les pilotes d\'Études et Recherche</h5>
                    <ul>
                        <li><input type="submit" id="plateformeer-connexion" name="plateformeer-connexion" class="bouton-connexion" value="Se connecter"></li>
                        <li class="connexion-liens-misc">
                            <a href="#plateforme-er-demande" class="lbp-inline-link-4 cboxElement">Demander un accès</a>
                            <a href="http://portail-er.cerib.fr/Pages/Default.aspx" target="_blank">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>
            </div>
            <div id="qualib-connexion" class="form-connexion">
                <form name="ctl00" method="post" action="http://cerib.qualib.fr" id="ctl00" target="_blank">
                    <h4>QU@l-IB <span class="small">(redirection à la plaforme externe)</span></h4>
                    <ul>
                        <li><input type="submit" id="qualib-connexion" name="qualib-connexion" class="bouton-connexion" value="Se connecter"></li>
                        <li class="connexion-liens-misc">
                            <a href="#qualib-demande" class="lbp-inline-link-4 cboxElement">Demander un accès</a>
                            <a href="http://cerib.qualib.fr" target="_blank">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>
            </div>
            <div id="memento-connexion" class="form-connexion">
                <form name="authentification" method="post" action="http://intranoo.cerib.fr/sites/mementos" target="_blank">
                    <h4>Memento <span class="small">(redirection à la plaforme externe)</span></h4>
                    <ul>
                        <li><input type="text" id="memento_TBLogin" name="TBLogin" placeholder="Identifiant"></li>
                        <li><input type="password" id="memento_TBMdp" name="TBMdp" placeholder="Mot de passe"></li>
                        <li><input type="submit" id="memento-connexion" name="memento-connexion" class="bouton-connexion" value="Connexion"></li>
                        <li class="connexion-liens-misc">
                            <a href="#memento-demande" class="lbp-inline-link-4 cboxElement">Demander un accès</a>
                            <a href="http://intranoo.cerib.fr/sites/mementos" target="_blank">Mot de passe perdu ?</a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <div id="lbp-inline-href-4" style="padding:10px; background: #fff;">
            <h3>Demander un accès</h3>
            <div class="wpb_row vc_row-fluid full-row">
                Appel de cotisation APEGIB au titre de membre pour l\'année 2015.
            </div>
            <hr class="vertical-space">
            <div class="col-md-12 demander-acces-form">
            	'.do_shortcode( '[contact-form-7 id="1184" title="Demander un accès"]').'
            </div>
        </div>
    </div>
    <div style="display: none;">
        <div id="lbp-inline-href-5" style="padding:10px; background: #fff;">
            <h3>Demander un accès APEGIB</h3>
            <div class="wpb_row vc_row-fluid full-row">
                Appel de cotisation APEGIB au titre de membre pour l\'année 2015.
            </div>
            <hr class="vertical-space">
            <div class="col-md-12 demander-acces-apegib-form">
            	'.do_shortcode( '[contact-form-7 id="1183" title="Demander un accès APEGIB"]').'
            </div>
            <div class="wpb_row vc_row-fluid full-row">
                <div class="col-md-6">
                    (1) Adresse pour Règlement par chèque :<br>
                    APEGIB<br>
                    A l\'attention du Trésorier Stéphane LE GUIRRIEC<br>
                    CS 10010<br>
                    28233 EPERNON CEDEX
                </div>
                <div class="col-md-6">
                    (2) Références bancaires :<br>
                    Titulaire du compte : Association APEGIB<br>
                    Code Banque : 18707<br>
                    N° Compte : 30621885943<br>
                    Clé RIB : 27<br>
                    IBAN : FR76 1870 7000 0130 6218 8594 327<br>
                    Adresse SWIFT (BIC) : CCBPFRPPVER<br>
                    Domiciliation : BPVF EURE ET LOIR ENTR
                </div>
            </div>
        </div>
    </div>
    ';
    /*
    <div style="display: none;">
        <div id="lbp-inline-href-2" style="padding:10px; background: #fff;">
            <form name="lostpasswordform" id="lostpasswordform" action="http://ns37211.ovh.net/~ceribw/wp-login.php?action=lostpassword" method="post">
            	<p>
            		<label for="user_login">Identifiant ou adresse de messagerie&nbsp;:<br>
            		<input type="text" name="user_login" id="user_login" class="input" value="" size="20"></label>
            	</p>
            		<input type="hidden" name="redirect_to" value="' . home_url('/') . '">
            	<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Générer un mot de passe"></p>
            </form>
        </div>
    </div>  
    */
    return $content;
}
