<?php
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
date_default_timezone_set('Europe/Paris');

if (isset($_GET["export"]) && $_GET["export"] == 'download_csv' && isset($_GET["exportkey"]) && $_GET["exportkey"] == 'ne-v920b23--z72d0ei73-0eonvp42-gn39') {
    // output headers so that the file is downloaded rather than displayed
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".date("YmdHis")."_evenements_inscriptions.csv\";" );
    header("Content-Transfer-Encoding: binary");
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('inscription_id', utf8_decode('Évènement'), 'Date', utf8_decode('Civilité'), 'Nom', utf8_decode('Prénom'), utf8_decode('Société'), 'Fonction', 'Fonction (autre)', 'Adresse', 'Code Postal', 'Ville', utf8_decode('Téléphone'), 'E-mail', 'Commentaire', 'Option 1', 'Option 2', 'Option 3'), ";");
    
    // fetch the data
    $connect = mysql_connect("localhost", "root", 'iS4qjVrrqbcC');
    mysql_select_db("cerib");
    $result = mysql_query("SELECT e.inscription_id, p.post_title, DATE_FORMAT(e.date, '%d/%m/%Y %H:%i:%s'), e.civilite, e.nom, e.prenom, e.societe, e.fonction, e.fonction_autre, e.adresse, e.code_postal, e.ville, e.telephone, e.email, e.commentaire, e.option1, e.option2, e.option3, e.evenement_id FROM pzebfiv_evenements_inscription e, pzebfiv_posts p WHERE e.evenement_id = p.ID ORDER BY e.date DESC ");
    while ($row = mysql_fetch_assoc($result)) {
        # ville
        $resultV = mysql_query("SELECT meta_value FROM pzebfiv_postmeta WHERE post_id = '" . $row["evenement_id"] . "' AND meta_key = 'evenements_ville' ");
        $rowV = mysql_fetch_row($resultV);
        $ville = $rowV[0];
        # departement
        $resultD = mysql_query("SELECT meta_value FROM pzebfiv_postmeta WHERE post_id = '" . $row["evenement_id"] . "' AND meta_key = 'evenements_departement' ");
        $rowD = mysql_fetch_row($resultD);
        $dept = $rowD[0];
        
        $row["post_title"] = $row["post_title"] . " / ".$ville." (".$dept.")";
        $row["commentaire"] = stripslashes($row["commentaire"]);
        $row["option1"] = str_replace("|||", "", str_replace("|||0", " non", str_replace("|||1", " oui", $row["option1"])));
        $row["option2"] = str_replace("|||", "", str_replace("|||0", " non", str_replace("|||1", " oui", $row["option2"])));
        $row["option3"] = str_replace("|||", "", str_replace("|||0", " non", str_replace("|||1", " oui", $row["option3"])));
        $row["evenement_id"] = "";
        fputcsv($output, $row, ";");
    }

} else {
    exit;
}
?>