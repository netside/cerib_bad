<?php
    
# initialisation du custom post type
function custom_post_evenement() {
  $labels = array(
    'name'               => _x( 'Évènements', 'post type general name' ),
    'singular_name'      => _x( 'Évènement', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter un nouveau évènement' ),
    'edit_item'          => __( 'Modifier un évènement' ),
    'new_item'           => __( 'Nouveau évènement' ),
    'all_items'          => __( 'Tous les évènements' ),
    'view_item'          => __( 'Voir l\'évènement' ),
    'search_items'       => __( 'Rechercher dans les évènements' ),
    'not_found'          => __( 'Aucun évènement trouvé' ),
    'not_found_in_trash' => __( 'Aucun évènement trouvé dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Évènements'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des évènements',
    'public'        => true,
    'menu_position' => 11,
    'menu_icon'     => 'dashicons-calendar-alt',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'evenements', $args ); 
}
add_action( 'init', 'custom_post_evenement' );

# custom message
function updated_messages_evenement( $messages ) {
  global $post, $post_ID;
  $messages['evenements'] = array(
    0 => '', 
    1 => sprintf( __('Évènement mis à jour. <a href="%s">Voir l\'évènement</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Évènement mis à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Évènement restauré à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Évènement publié. <a href="%s">Voir l\'évènement</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Évènement enregistré.'),
    8 => sprintf( __('Évènement envoyé. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Évènement planifié pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_evenement' );

# custom categories
function taxonomies_evenement() {
  $labels = array(
    'name'              => _x( 'Catégories Évènement', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parent' ),
    'parent_item_colon' => __( 'Catégorie parent :' ),
    'edit_item'         => __( 'Modifier une catégorie' ), 
    'update_item'       => __( 'Mettre à jour une catégorie' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouvelle catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'evenement_category', 'evenements', $args );
}
add_action( 'init', 'taxonomies_evenement', 0 );

# custom categories
function themes_evenement() {
  $labels = array(
    'name'              => _x( 'Thèmes Évènement', 'taxonomy general name' ),
    'singular_name'     => _x( 'Thème', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les thèmes' ),
    'all_items'         => __( 'Toutes les thèmes' ),
    'parent_item'       => __( 'Thème parent' ),
    'parent_item_colon' => __( 'Thème parent :' ),
    'edit_item'         => __( 'Modifier un thème' ), 
    'update_item'       => __( 'Mettre à jour un thème' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouveau thème' ),
    'menu_name'         => __( 'Thèmes' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
  );
  register_taxonomy( 'evenement_theme', 'evenements', $args );
}
add_action( 'init', 'themes_evenement', 0 );

# custom meta box
add_filter( 'rwmb_meta_boxes', 'evenement_box' );
function evenement_box( $meta_boxes ) {
    global $depts;
    
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'evenements_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Date et lieu', 'evenements' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'evenements' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// DATE
			array(
				'name'       => __( 'Date de début', 'evenements' ),
				'id'         => "{$prefix}datedebut",
				'type'       => 'date',

				// jQuery date picker options. See here http://api.jqueryui.com/datepicker
				'js_options' => array(
					'appendText'      => __( ' (aaaa/mm/dd)', 'evenements' ),
					'dateFormat'      => __( 'yy/mm/dd', 'evenements' ),
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
			),
			// DATE
			array(
				'name'       => __( 'Date de fin', 'evenements' ),
				'id'         => "{$prefix}datefin",
				'type'       => 'date',

				// jQuery date picker options. See here http://api.jqueryui.com/datepicker
				'js_options' => array(
					'appendText'      => __( ' (aaaa/mm/dd)', 'evenements' ),
					'dateFormat'      => __( 'yy/mm/dd', 'evenements' ),
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
			),
			// EMAIL
			array(
				'name' => __( 'Contact', 'evenements' ),
				'id'   => "{$prefix}contact",
				'type' => 'email',
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Nom du lieu', 'evenements' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}lieu",
				'type'  => 'text',
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Ville', 'evenements' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}ville",
				'type'  => 'text',
			),
			// SELECT BOX
			array(
				'name'        => __( 'Département', 'evenements' ),
				'id'          => "{$prefix}departement",
				'type'        => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'     => $depts,
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'placeholder' => __( 'Choisir un département', 'evenements' ),
			),
			array(
				'id'            => 'map',
				'name'          => __( 'Localisation', 'evenements' ),
				'type'          => 'map',
				// Default location: 'latitude,longitude[,zoom]' (zoom is optional)
				'std'           => '48.6017571,1.6826241,14',
				// Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
				'address_field' => 'adresse',
			),
			// Map requires at least one address field (with type = text)
			array(
				'id'   => 'adresse',
				'name' => __( 'Adresse complète', 'evenements' ),
				'type' => 'text',
				'std'  => __( '1 Rue des Longs Reages, 28230 Épernon, France', 'evenements' ),
				// Field description (optional)
				'desc'  => __( 'Sert à positionner sur la Google Maps ci-dessus', 'evenements' ),
			),
		),
	);

	// 2nd meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard2',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Réservation', 'evenements' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'evenements' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// SELECT BOX
			array(
				'name'        => __( 'Bouton d\'inscription', 'evenements' ),
				'id'          => "{$prefix}btninscription",
				'type'        => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options' => array(
					'1' => __( 'Oui', 'evenements' ),
					'0' => __( 'Non', 'evenements' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std' => 1,
			),
			// EMAIL
			array(
				'name' => __( 'E-mail de contact', 'evenements' ),
				'id'   => "{$prefix}email_contact",
				'type' => 'email',
				// Field description (optional)
				'desc'  => __( 'Si vide, e-mail de l\'administrateur par défaut', 'evenements' ),
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Option 1', 'evenements' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}option1",
				'type'  => 'text',
				// Field description (optional)
				'desc'  => __( '1ère question qui apparaitra pendant l\'inscription', 'evenements' ),
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Option 2', 'evenements' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}option2",
				'type'  => 'text',
				// Field description (optional)
				'desc'  => __( '2nde question qui apparaitra pendant l\'inscription', 'evenements' ),
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Option 3', 'evenements' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}option3",
				'type'  => 'text',
				// Field description (optional)
				'desc'  => __( '3ème question qui apparaitra pendant l\'inscription', 'evenements' ),
			),
		),
	);

	return $meta_boxes;
}

# shortcode : affichage des évènements
add_shortcode('evenements', 'display_evenements');
function display_evenements ($atts, $content) {
    global $depts;
    
    $args = array( 'post_type' => 'evenements', 'evenement_category' => $atts["categorie"], 'posts_per_page' => 100, 'meta_key'=>'evenements_datedebut', 'orderby' => 'evenements_datedebut', 'order' => 'ASC' );
    $loop = new WP_Query( $args );
    $arrDepartementCode = array();
    $arrDepartementName = array();
    $arrThemeCode = array();
    $arrThemeName = array();
    while ( $loop->have_posts() ) : $loop->the_post();
        # données date
        $datedebut = rwmb_meta('evenements_datedebut');
        $datefin = rwmb_meta('evenements_datefin');
        $datenowF = date("Y/m/d");
        $classPasse = 0;
        if ($datenowF != $datefin) {
            if ($datefin != "") $arrDate = array($datefin, $datenowF);
            else                $arrDate = array($datedebut, $datenowF);
            sort($arrDate);
            if ($arrDate[1] == $datenowF) $classPasse = 1;
        }
        if ((@$_GET["passe"] == 1 && $classPasse == 1) || (@$_GET["passe"] != 1 && $classPasse == 0)) {
            # données thèmes
            $arrAllTags = array();
            $arrAllTags = get_the_terms(get_the_ID(), 'evenement_theme', 'fields=names');
            if ($arrAllTags != "") {
                foreach ($arrAllTags as $tag) {
                    $arrThemeCode[] = "theme-".$tag->slug;
                    $arrThemeName[] = $tag->name;
                }
            }
            $arrDepartementCode[] = rwmb_meta('evenements_departement');
            $arrDepartementName[] = $depts[(string)rwmb_meta('evenements_departement')];
        }
    endwhile;
    $arrDepartementCode = array_unique($arrDepartementCode);
    $arrDepartementName = array_unique($arrDepartementName);
    sort($arrDepartementCode);
    sort($arrDepartementName);
    $arrThemeCode = array_unique($arrThemeCode);
    $arrThemeName = array_unique($arrThemeName);
    sort($arrThemeCode);
    sort($arrThemeName);
    # filtre
    ?>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="col-md-12 evenement-filtres">
            <ul>
                <li>FILTRER</li>
                <li>
                    <a href="#" class="filtre-label">Par département <i class="fa-chevron-down"></i></a>
                    <select name="filtre-departement-<?php echo $atts["categorie"]; ?>[]" id="filtre-departement-<?php echo $atts["categorie"]; ?>" multiple="multiple">
                        <?php for ($c=0; $c<count($arrDepartementCode); $c++) { ?>
                        <option value="<?php echo $arrDepartementCode[$c]; ?>"><?php echo $arrDepartementName[$c]; ?></option>
                        <?php } ?>
                    </select>
                </li>
                <li>OU</li>
                <li>
                    <a href="#" class="filtre-label">Par thème <i class="fa-chevron-down"></i></a>
                    <select name="filtre-theme-<?php echo $atts["categorie"]; ?>[]" id="filtre-theme-<?php echo $atts["categorie"]; ?>" multiple="multiple">
                        <?php for ($c=0; $c<count($arrThemeCode); $c++) { ?>
                        <option value="<?php echo $arrThemeCode[$c]; ?>"><?php echo $arrThemeName[$c]; ?></option>
                        <?php } ?>
                    </select>
                </li>
                <li><a href="<?php echo home_url('/'); ?>/le-cerib/evenements-cerib/" id="evenement-a-venir-<?php echo $atts["categorie"]; ?>" class="filtre-tempo-label <?php echo (@$_GET["passe"] != 1)?"active":""; ?>">Évènements à venir</a></li>
                <li><a href="<?php echo home_url('/'); ?>/le-cerib/evenements-cerib/?passe=1" id="evenement--passes-<?php echo $atts["categorie"]; ?>" class="filtre-tempo-label <?php echo (@$_GET["passe"]==1)?"active":""; ?>">Évènements passés</a></li>
            </ul>
            <div class="evenement-separateur"></div>
        </div>
    </div>
    <?php
    # liste
    while ( $loop->have_posts() ) : $loop->the_post();
        # données thèmes
        $arrAllTags = array();
        $arrAllTags = get_the_terms(get_the_ID(), 'evenement_theme', 'fields=names');
        $arrTagsCode = array();
        $arrTagsName = array();
        if ($arrAllTags != "") {
            foreach ($arrAllTags as $tag) {
                $arrTagsCode[] = "theme-".$tag->slug;
                $arrTagsName[] = $tag->name;
            }
        }
        
        # données date
        $datedebut = rwmb_meta('evenements_datedebut');
        $datefin = rwmb_meta('evenements_datefin');
        $datenowF = date("Y/m/d");
        $classPasse = 0;
        if ($datenowF != $datefin) {
            if ($datefin != "") $arrDate = array($datefin, $datenowF);
            else                $arrDate = array($datedebut, $datenowF);
            sort($arrDate);
            if ($arrDate[1] == $datenowF) $classPasse = 1;
        }
        if ((@$_GET["passe"] == 1 && $classPasse == 1) || (@$_GET["passe"] != 1 && $classPasse == 0)) {
            ?>
            <div class="col-md-6 evenement-list-bloc departement-<?php echo rwmb_meta('evenements_departement'); ?> <?php echo implode(" ", $arrTagsCode); ?> <?php echo $classPasse; ?>">
                <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post evenement-list clearfix'); ?>>
                    <div class="col-xs-3 evenement-date">
                        <span class="jour"><?php echo date("d", strtotime($datedebut)); ?></span>
                        <span class="mois"><?php echo date("m", strtotime($datedebut)); ?></span>
                    </div>
                    <div class="col-xs-9 evenement-item">
                        <h4>
                            <?php
                            $title = get_the_title();
                            echo (strlen($title)>100)?substr($title, 0, 100)."...":$title;
                            ?>
                        </h4>
            			<p class="evenement-theme-lieu">
                			<span class="evenement-theme-label">Thème</span> <?php echo implode(", ", $arrTagsName); ?>
                			<br>
                			<span class="evenement-lieu-label">Lieu</span> <?php echo (rwmb_meta('evenements_ville')!="")?rwmb_meta('evenements_ville'):""; ?> <?php echo (rwmb_meta('evenements_departement')!="")?"(".rwmb_meta('evenements_departement').")":""; ?>
                        </p>
                    </div>
                    <div class="evenement-lien">
                        <?php /*if (@$_GET["passe"] != 1) { ?>
                        <a href="#?" class="col-xs-6">Inscription en ligne</a>
                        <?php }*/ ?>
                        <a href="<?php echo get_post_permalink(); ?>" class="col-xs-<?php echo (@$_GET["passe"] == 1)?"12":"12" ?>">Voir le programme</a>
                    </div>
                </article>
                <hr class="vertical-space">
            </div>
            <?php
        }
    endwhile;
}


# shortcode : affichage des évènements en blocs
add_shortcode('evenements-accueil', 'display_evenements_bloc');
function display_evenements_bloc ($atts, $content) {
    $args = array( 'post_type' => 'evenements', 'posts_per_page' => 100, 'meta_key'=>'evenements_datedebut', 'orderby' => 'evenements_datedebut', 'order' => 'ASC' );
    $loop = new WP_Query( $args );
    $datenowF = date("Y/m/d");
    $nbEve = 0;
    while ( $loop->have_posts() ) : $loop->the_post();
        $datedebut = rwmb_meta('evenements_datedebut');
        $datefin = rwmb_meta('evenements_datefin');
        if ($datedebut >= $datenowF && $nbEve<3) {
        ?>
        <article id="post-<?php the_ID(); ?>" class="wpb_row vc_row-fluid full-row evenement-item-accueil">
            <div class="col-xs-3 evenement-date">
                <span class="jour"><?php echo date("d", strtotime($datedebut)); ?></span>
                <span class="mois"><?php echo date("m", strtotime($datedebut)); ?></span>
            </div>
            <div class="col-xs-9 evenement-item">
                <h4>
                    <a href="<?php echo get_post_permalink(); ?>">
                        <strong>
                            <?php
                            $title = get_the_title();
                            echo mb_substr( strip_tags( preg_replace( '/\[[^\]]+\]/', '', $title) ), 0, 56, "UTF-8" ).'&hellip;';
                            ?>
                        </strong>
                        <?php echo (rwmb_meta('evenements_ville')!="")?"<br>".rwmb_meta('evenements_ville'):""; ?>
                        <?php echo (rwmb_meta('evenements_departement')!="")?"(".rwmb_meta('evenements_departement').")":""; ?>
                    </a>
                </h4>
            </div>
        </article>
        <?php
        $nbEve++;
        }
    endwhile;
}

# formulaire inscription
function ajaxevenement_enqueuescripts() {
    wp_enqueue_script('ajaxevenement', '/wp-content/themes/Mexin_child_theme/js/ajax-evenement-inscription.js', array('jquery'));
    wp_localize_script( 'ajaxevenement', 'ajaxevenementajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts', 'ajaxevenement_enqueuescripts');

function ajaxevenement_show_inscription($idevenement) {
    $post_evenement = get_post($idevenement); 
    ?>
    <form id="ajaxevenementform" action="" method="post"enctype="multipart/form-data">
        <input type="hidden" id="ajaxevenementidevenement" name="ajaxevenementidevenement" value="<?php echo $idevenement; ?>">
        <div id="ajaxevenement-text">
            <h3>
                Inscription pour :
                <?php echo $post_evenement->post_title; ?>
                <?php echo (rwmb_meta('evenements_ville')!="")?"/ ".rwmb_meta('evenements_ville'):""; ?>
                <?php echo (rwmb_meta('evenements_departement')!="")?"(".rwmb_meta('evenements_departement').")":""; ?>
            </h3>
            <div class="wpb_row vc_row-fluid full-row">
                Afin de vous inscrire, merci de compléter le formulaire ci-dessous.
            </div>
            <div class="wpb_row vc_row-fluid full-row">
                <hr class="vertical-space">
                <div class="col-md-6">
                    <ul class="bloc-left">
                        <li>
                            <select id="ajaxevenementcivilite" nom="ajaxevenementcivilite">
                                <option value="-">* Civilité -</option>
                                <option value="Mme">Madame</option>
                                <option value="M.">Monsieur</option>
                                <option value="Mlle">Mademoiselle</option>
                            </select>
                        </li>
                        <li><input type="text" id="ajaxevenementnom" name="ajaxevenementnom" placeholder="* Nom"></li>
                        <li><input type="text" id="ajaxevenementprenom" name="ajaxevenementprenom" placeholder="* Prénom"></li>
                        <li><input type="text" id="ajaxevenementsociete" name="ajaxevenementsociete" placeholder="* Société"></li>
                        <li>
                            <label>Vous êtes</label>
                            <select name="ajaxevenementfonction" id="ajaxevenementfonction">
                                <option value="">---</option>
                                <option value="Architecte">Architecte</option>
                                <option value="Bailleur">Bailleur</option>
                                <option value="Bureau d'étude">Bureau d'étude</option>
                                <option value="Bureau de contrôle">Bureau de contrôle</option>
                                <option value="Collectivité">Collectivité</option>
                                <option value="Constructeur de maisons individuelles">Constructeur de maisons individuelles</option>
                                <option value="Économiste de la construction">Économiste de la construction</option>
                                <option value="Enseignant">Enseignant</option>
                                <option value="Entreprise du bâtiment">Entreprise du bâtiment</option>
                                <option value="Entreprise de TPGC">Entreprise de TPGC</option>
                                <option value="Fabricant industriel (autre que IB)">Fabricant industriel (autre que IB)</option>
                                <option value="Industriel du béton (Ressortissant du CERIB)">Industriel du béton (Ressortissant du CERIB)</option>
                                <option value="Maître d'œuvre">Maître d'œuvre</option>
                                <option value="Maître d'ouvrage">Maître d'ouvrage</option>
                                <option value="Organisme">Organisme</option>
                                <option value="Promoteur">Promoteur</option>
                                <option value="Syndicat professionnel">Syndicat professionnel</option>
                                <option value="Autre (veuillez préciser)">Autre (veuillez préciser)</option>
                            </select>
                        </li>
                        <li id="eve-fonction-autre" style="display: none;"><input type="text" id="ajaxevenementautre" name="ajaxevenementautre" placeholder="Fonction dans la société"></li>
                        <li><input type="text" id="ajaxevenementadresse" name="ajaxevenementadresse" placeholder="* Adresse"></li>
                        <li><input type="text" id="ajaxevenementcodepostal" name="ajaxevenementcodepostal" placeholder="* Code postal"></li>
                        <li><input type="text" id="ajaxevenementville" name="ajaxevenementville" placeholder="* Ville"></li>
                        <li><input type="text" id="ajaxevenementtelephone" name="ajaxevenementtelephone" placeholder="* Téléphone"></li>
                        <li><input type="email" id="ajaxevenementemail" name="ajaxevenementemail" placeholder="* E-mail"></li>
                    </ul>
                </div>
                <?php
                $option1 = rwmb_meta('evenements_option1');
                $option2 = rwmb_meta('evenements_option2');
                $option3 = rwmb_meta('evenements_option3');
                ?>
                <div class="col-md-6">
                    <ul class="bloc-right">
                        <?php if ($option1 != "") { ?>
                        <li>
                            * <?php echo $option1; ?><br>
                            <label><input type="radio" name="ajaxevenementoption1" value="1"> oui</label>
                            <label><input type="radio" name="ajaxevenementoption1" value="0"> non</label>
                        </li>
                        <?php } ?>
                        <?php if ($option2 != "") { ?>
                        <li>
                            * <?php echo $option2; ?><br>
                            <label><input type="radio" name="ajaxevenementoption2" value="1"> oui</label>
                            <label><input type="radio" name="ajaxevenementoption2" value="0"> non</label>
                        </li>
                        <?php } ?>
                        <?php if ($option3 != "") { ?>
                        <li>
                            * <?php echo $option3; ?><br>
                            <label><input type="radio" name="ajaxevenementoption3" value="1"> oui</label>
                            <label><input type="radio" name="ajaxevenementoption3" value="0"> non</label>
                        </li>
                        <?php } ?>
                        <li><textarea id="ajaxevenementcommentaire" name="ajaxevenementcommentaire" rows="4" placeholder="Commentaire"></textarea></li>
                        <li><label><input type="checkbox" id="ajaxevenementnewsletter" name="ajaxevenementnewsletter" value="1"> Je souhaite recevoir toutes les informations du Cerib</label></li>
                    </ul>
                    <span class="small">* Champs obligatoires</span>
                    <hr class="vertical-space">
                </div>
            </div>
            <div class="wpb_row vc_row-fluid full-row">
                <div class="col-md-6">
                    <div id="ajaxevenement-response"></div>
                </div>
                <div class="col-md-6">
                    <a onclick="ajaxformsendmail();" class="submit-button" style="cursor: pointer">Envoyer et valider mon inscription</a>
                </div>
            </div>
        </div>
    </form>
    <?php
}

function ajaxevenement_send_mail() {
    global $wpdb;
    $result = $wpdb->get_results( "SELECT COUNT(*) AS presence FROM pzebfiv_evenements_inscription WHERE evenement_id = '".$_POST["acfidevenement"]."' AND email = '".$_POST["acfemail"]."'" );
    #
    $post_evenement = get_post($_POST["acfidevenement"]);
    #
    $idevenement = $_POST["acfidevenement"];
    $civilite = $_POST["acfcivilite"];
    $nom = stripslashes($_POST["acfnom"]);
    $prenom = stripslashes($_POST["acfprenom"]);
    $societe = stripslashes($_POST["acfsociete"]);
    $fonction = stripslashes($_POST["acffonction"]);
    $autre = stripslashes($_POST["acfautre"]);
    $adresse = stripslashes($_POST["acfadresse"]);
    $codepostal = stripslashes($_POST["acfcodepostal"]);
    $ville = stripslashes($_POST["acfville"]);
    $telephone = stripslashes($_POST["acftelephone"]);
    $email = stripslashes($_POST["acfemail"]);
    $optionstring1 = rwmb_meta('evenements_option1', '', $_POST["acfidevenement"]);
    $optionstring2 = rwmb_meta('evenements_option2', '', $_POST["acfidevenement"]);
    $optionstring3 = rwmb_meta('evenements_option3', '', $_POST["acfidevenement"]);
    $option1 = $_POST["acfoption1"];
    $option2 = $_POST["acfoption2"];
    $option3 = $_POST["acfoption3"];
    $newsletter = $_POST["acfnewsletter"];
    $commentaire = stripslashes($_POST["acfcommentaire"]);
    $contact_email = rwmb_meta('evenements_email_contact', '', $_POST["acfidevenement"]);
    $admin_email = ($contact_email!="")?$contact_email:get_option('admin_email');
    #
    $villeEvenement = rwmb_meta('evenements_ville', '', $_POST["acfidevenement"]);
    $departementEve = rwmb_meta('evenements_departement', '', $_POST["acfidevenement"]);
    #
    $results = "";
    $error = 0;
    #
    if ($result[0]->presence > 0) {
        $results = "Votre inscription a déjà été prise en compte avec cette adresse e-mail.";
        $error = 1;
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $results = "L'adresse e-mail n'est pas valide.";
        $error = 1;
    } else if( $civilite == "-" ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($nom) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($prenom) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($societe) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($adresse) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($codepostal) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($ville) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if( strlen($telephone) == 0 ) {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if(strlen($optionstring1)>0 && $option1 == "") {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if(strlen($optionstring2)>0 && $option2 == "") {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    } else if(strlen($optionstring3)>0 && $option3 == "") {
        $results = "Veuillez remplir tous les champs.";
        $error = 1;
    }
    #
    if($error == 0) {
        if ($newsletter == 1) {
            $timeNow = mktime();
            $wpdb->insert( 
            	'pzebfiv_wysija_user', 
            	array( 
            		'wpuser_id' => 0, 
            		'email' => $email , 
            		'firstname' => $prenom , 
            		'lastname' => $nom ,
            		'created_at' => $timeNow , 
            		'status' => 1
            	)
            );
            $wpdb->insert( 
            	'pzebfiv_wysija_user_list', 
            	array( 
            		'list_id' => 1, 
            		'user_id' => $wpdb->insert_id , 
            		'sub_date' => $timeNow 
            	)
            );
        }
        $result = $wpdb->insert( 
        	'pzebfiv_evenements_inscription', 
        	array( 
        		'evenement_id' => $idevenement, 
        		'date' => current_time('mysql', 1) , 
        		'civilite' => $civilite , 
        		'nom' => $nom , 
        		'prenom' => $prenom , 
        		'societe' => $societe , 
        		'fonction' => $fonction , 
        		'fonction_autre' => $autre , 
        		'adresse' => $adresse , 
        		'code_postal' => $codepostal , 
        		'ville' => $ville , 
        		'telephone' => $telephone , 
        		'email' => $email , 
        		'commentaire' => $commentaire , 
        		'option1' => ($optionstring1)."|||".$option1 , 
        		'option2' => ($optionstring2)."|||".$option2 , 
        		'option3' => ($optionstring3)."|||".$option3
        	)
        );
        if ($result == 1) {
            # e-mail à l'administrateur
            $headers = 'From: CERIB <'.$admin_email.'>';
            $subject = 'Cerib.com - inscription à un évènement';
            $contents = "Une inscription pour l'évènement : <br>" . $post_evenement->post_title ." / ".$villeEvenement." (".$departementEve.")" . "<br><br>\r\n\r\n";
            $contents.= "Voici les données du formulaire : <br><br>\r\n\r\n";
        	$contents.= "Civilité : " . $civilite ."<br>\r\n";
        	$contents.= "Nom : " . $nom ."<br>\r\n";
        	$contents.= "Prénom : " . $prenom ."<br>\r\n";
        	$contents.= "Société : " . $societe ."<br>\r\n";
        	$contents.= "Fonction : " . $fonction ."<br>\r\n";
        	$contents.= "Fonction (autre) : " . $autre ."<br>\r\n";
        	$contents.= "Adresse : " . $adresse ."<br>\r\n";
        	$contents.= "Code postal : " . $codepostal ."<br>\r\n";
        	$contents.= "Ville : " . $ville ."<br>\r\n";
        	$contents.= "Téléphone : " . $telephone ."<br>\r\n";
        	$contents.= "E-mail : " . $email ."<br>\r\n";
        	$contents.= "Commentaire : " . $commentaire ."<br>\r\n";
        	if ($optionstring1 != "") $contents.= "Option1 : " . ($optionstring1)." ".(($option1==1)?"oui":"non")."<br>\r\n";
        	if ($optionstring2 != "") $contents.= "Option2 : " . ($optionstring2)." ".(($option2==1)?"oui":"non")."<br>\r\n";
        	if ($optionstring3 != "") $contents.= "Option3 : " . ($optionstring3)." ".(($option3==1)?"oui":"non")."<br>\r\n";
        	$contents.= "Newsletter : " . (($newsletter==1)?"oui":"non") ."<br>\r\n";
        	$contents.= "<br>\r\n--<br>\r\nCet email a été envoyé via le formulaire d'inscription à un évènement de Cerib.com (http://www.cerib.com)<br>\r\n";
        	#
            if(wp_mail($admin_email, $subject, $contents, $headers)) {
                //$results = "*Thanks for you mail.";
            } else {
                //$results = "*The mail could not be sent.";
            }
            # e-mail à l'internaute
            $headers = 'From: CERIB <'.$admin_email.'>';
            $subject = 'Cerib.com - inscription à un évènement';
            
        	$datedebut = rwmb_meta('evenements_datedebut', '', $_POST["acfidevenement"]);
        	$datefin = rwmb_meta('evenements_datefin', '', $_POST["acfidevenement"]);
        	$periode = "";
			if ($datefin != "") {
    			if ($datedebut == $datefin) {
        			$periode = "le " . date("d/m/Y", strtotime($datedebut));
                } else {
                    $periode = "du ".date("d/m/Y", strtotime($datedebut))." au ".date("d/m/Y", strtotime($datefin));
                }
            } else {
                $periode = "le " . date("d/m/Y", strtotime($datedebut));
            }
            /*
        	$contents = '
            Bonjour,<br><br>
            Nous sommes heureux de vous confirmer votre inscription pour l\'évènement : <br><br>
            '.$post_evenement->post_title.', '.$periode.'.<br><br>
            Vous recevrez un e-mail contenant toutes les informations utiles à votre venue quelques jours avant l’événement.<br><br>
            A très vite !<br><br>
            L’Equipe de l’Action Régionale du Cerib<br>
        	';
        	*/
        	$contents = '
            Bonjour,<br><br>
            Nous sommes heureux de vous confirmer votre inscription pour l\'évènement : <br><br>
            <i>'.$post_evenement->post_title.', '.$periode.'</i>.<br><br>
            Vous retrouverez l\'ensemble des informations pratiques concernant ce « rendez-vous » sur la page dédiée de notre <a href="http://www.cerib.com/le-cerib/evenements-cerib/">site web</a>.<br><br>
            Bien cordialement<br><br>
            L\'Equipe de l\'Action Régionale<br><br>
            <a href="http://www.cerib.com/"><img src="http://www.cerib.com/wp-content/uploads/2015/08/logo-cerib-2x.png" width="150" height="61" border="0" alt="CERIB"></a>
        	';
        	#
            if(wp_mail($email, $subject, $contents, $headers)) {
                //$results = "*Thanks for you mail.";
            } else {
                //$results = "*The mail could not be sent.";
            }
            #
            $results = "Votre inscription a bien été prise en compte.";
        } else {
            $results = "Une erreur est survenue lors de l'inscription.";
        }
    }
    // Return the String
    die($results);
}
add_action( 'wp_ajax_nopriv_ajaxevenement_send_mail', 'ajaxevenement_send_mail' );
add_action( 'wp_ajax_ajaxevenement_send_mail', 'ajaxevenement_send_mail' );

function ajaxevenement_shortcode_func( $atts ) {
    ob_start();
    ajaxevenement_show_inscription($atts["id"]);
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
add_shortcode( 'ajaxevenement', 'ajaxevenement_shortcode_func' );


# écran des inscriptions évènements
add_action('admin_menu', 'add_admin_menu_evenement');
function add_admin_menu_evenement() {
    add_submenu_page('edit.php?post_type=evenements', 'Inscriptions', 'Inscriptions', 'manage_options', 'evenements_inscriptions', 'menu_html_evenement');
}
function menu_html_evenement() {
    global $wpdb;
    
    if (isset($_GET["idinscription"]) && $_GET["idinscription"]>0) {
        $inscription = $wpdb->get_results("SELECT * FROM pzebfiv_evenements_inscription WHERE inscription_id = '".$_GET["idinscription"]."'");
        $post_evenement = get_post($inscription[0]->evenement_id);
        $optionstring1 = rwmb_meta('evenements_option1', '', $post_evenement->ID);
        $optionstring2 = rwmb_meta('evenements_option2', '', $post_evenement->ID);
        $optionstring3 = rwmb_meta('evenements_option3', '', $post_evenement->ID);
        $villeEvenement = rwmb_meta('evenements_ville', '', $post_evenement->ID);
        $departementEve = rwmb_meta('evenements_departement', '', $post_evenement->ID);

        ?>
        <div class="wrap">
     
            <div id="icon-users" class="icon32"><br/></div>
            <h2>
                Inscription pour l'évènement : <br>
                <?php echo $post_evenement->post_title; ?>
                <?php echo ($villeEvenement!="")?"/ ".$villeEvenement:""; ?>
                <?php echo ($departementEve!="")?"(".$departementEve.")":""; ?>
            </h2>
            <br><br>
            <div id="poststuff" class="metabox-holder has-right-sidebar">
                <div id="post-body-content" style="margin-right: 300px; width: auto;">
                    <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                        <div id="inboundfieldsdiv" class="postbox ">
                            <h3 class="hndle ui-sortable-handle" style="padding: 8px 12px; margin-top: 0;"><span>Données du formulaire</span></h3>
                            <div class="inside">
                                <table class="widefat message-fields" cellspacing="0">
                                    <tbody>
                                        <tr class="alt">
                                            <td class="field-title" width="30%">Date</td>
                                            <td class="field-value"><?php echo date("d/m/Y à H:i:s", strtotime($inscription[0]->date)); ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Civilité</td>
                                            <td class="field-value"><?php echo $inscription[0]->civilite; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Nom</td>
                                            <td class="field-value"><?php echo $inscription[0]->nom; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Prénom</td>
                                            <td class="field-value"><?php echo $inscription[0]->prenom; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Société</td>
                                            <td class="field-value"><?php echo $inscription[0]->societe; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Fonction</td>
                                            <td class="field-value"><?php echo $inscription[0]->fonction; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Fonction (autre)</td>
                                            <td class="field-value"><?php echo $inscription[0]->fonction_autre; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Adresse</td>
                                            <td class="field-value"><?php echo $inscription[0]->adresse; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Code postal</td>
                                            <td class="field-value"><?php echo $inscription[0]->code_postal; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Ville</td>
                                            <td class="field-value"><?php echo $inscription[0]->ville; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Téléphone</td>
                                            <td class="field-value"><?php echo $inscription[0]->telephone; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">E-mail</td>
                                            <td class="field-value"><?php echo $inscription[0]->email; ?></td>
                                        </tr>
                                        <tr class="alt">
                                            <td class="field-title">Commentaire</td>
                                            <td class="field-value"><?php echo $inscription[0]->commentaire; ?></td>
                                        </tr>
                                        <?php if ($optionstring1 != "") { ?>
                                        <tr class="alt">
                                            <td class="field-title"><?php echo $optionstring1; ?></td>
                                            <td class="field-value"><?php echo (str_replace($optionstring1."|||", "", $inscription[0]->option1)==1)?"oui":"non"; ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if ($optionstring2 != "") { ?>
                                        <tr class="alt">
                                            <td class="field-title"><?php echo $optionstring2; ?></td>
                                            <td class="field-value"><?php echo (str_replace($optionstring2."|||", "", $inscription[0]->option2)==1)?"oui":"non"; ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if ($optionstring3 != "") { ?>
                                        <tr class="alt">
                                            <td class="field-title"><?php echo $optionstring3; ?></td>
                                            <td class="field-value"><?php echo (str_replace($optionstring3."|||", "", $inscription[0]->option3)==1)?"oui":"non"; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="publishing-action">
                        <a href="<?php echo home_url('/'); ?>wp-admin/edit.php?post_type=evenements&page=evenements_inscriptions&idevenement=<?php echo $_GET["idevenement"]; ?>">
                            <input name="retour" type="button" class="button-primary" id="retour" value="Retour à la liste">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        //Create an instance of our package class...
        $testListTable = new TT_Evenement_List_Table();
        //Fetch, prepare, sort, and filter our data...
        $testListTable->prepare_items();
        ?>
        <div class="wrap">
     
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Inscriptions aux évènements</h2>       
            <br><br>
            <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
            <form id="evenements-filter" method="get">
                <!-- For plugins, we also need to ensure that the form posts back to our current page -->
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <!-- Now we can render the completed list table -->
                <?php $testListTable->display(); ?>
            </form>
     
        </div>
        <?php
    }
}


# liste des inscriptions 
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
 
class TT_Evenement_List_Table extends WP_List_Table {
 
    function __construct() {
        global $status, $page;
 
        //Set parent defaults
        parent::__construct(array(
            'singular' => 'Nice Link', //singular name of the listed records
            'plural' => 'Nice Links', //plural name of the listed records
            'ajax' => false        //does this table support ajax?
        ));
    }
    
    /**
     * Add extra markup in the toolbars before or after the list
     * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
     */
    function extra_tablenav( $which ) {
       if ( $which == "top" ){
          //The code that goes before the table is here
          global $wpdb;
          $results = $wpdb->get_results("SELECT ID, post_title, post_date FROM pzebfiv_posts WHERE post_type = 'evenements' AND post_status = 'publish' ORDER BY post_title ");
          ?>
            <div class="alignleft actions">
                <select name="idevenement" id="idevenement" class="global-filter">
                    <option value="0">- Tous les évènements -</option>
                    <?php foreach ($results as $item) { ?>
                        <?php
                        $villeEvenement = rwmb_meta('evenements_ville', '', $item->ID);
                        $departementEve = rwmb_meta('evenements_departement', '', $item->ID);
                        ?>
                    <option value="<?php echo $item->ID; ?>"<?php echo ($_GET["idevenement"]==$item->ID)?" selected=\"selected\"":""; ?>><?php echo $item->post_title; ?> / <?php echo $villeEvenement; ?> (<?php echo $departementEve; ?>)</option>
                    <?php } ?>
                </select>
                <input type="button" name="filter_action" id="post-query-submit" class="button" value="Filtrer">
                <input type="button" name="export_action" id="export_action" class="button" value="Export csv">
            </div>          
            <script type="text/javascript">
                jQuery("#post-query-submit").click(function(){
                    var id = jQuery("#idevenement").val();
                    window.location.href = '<?php echo home_url('/'); ?>wp-admin/edit.php?post_type=evenements&page=evenements_inscriptions&idevenement='+id;
                });
                jQuery("#export_action").click(function(){
                    window.location.href = '<?php echo home_url('/'); ?>wp-content/themes/Mexin_child_theme/inc/export-evenement.php?export=download_csv&exportkey=<?php echo EXP_KEY; ?>';
                });
            </script>
          <?php
       }
       if ( $which == "bottom" ){
          //The code that goes after the table is there
          //echo"Hi, I'm after the table";
       }
    }
 
    function column_default($item, $column_name) {
        $post_evenement = get_post($item->evenement_id);
        $villeEvenement = rwmb_meta('evenements_ville', '', $post_evenement->ID);
        $departementEve = rwmb_meta('evenements_departement', '', $post_evenement->ID);
        
        switch ($column_name) {
            case 'evenement_id':
                return "<a href=\"". home_url('/') ."wp-admin/edit.php?post_type=evenements&page=evenements_inscriptions&idevenement=".$_GET["idevenement"]."&idinscription=".$item->inscription_id."\">".$post_evenement->post_title." / ".$villeEvenement." (".$departementEve.")"."</a>";
            case 'date':
                return date("d/m/Y H:i:s", strtotime($item->$column_name));
            case 'nom':
                $out = $item->civilite." ".$item->prenom." ".$item->nom;
                return $out;
            case 'adresse':
                $out = $item->societe."<br>".$item->adresse." ".$item->code_postal." ".$item->ville."<br>".$item->telephone."<br>".$item->email;
                return $out;
            case 'questions':
                $out = str_replace("|||", "", str_replace("|||1", " oui", str_replace("|||0", " non", $item->option1)))."<br>".str_replace("|||", "", str_replace("|||1", " oui", str_replace("|||0", " non", $item->option2)))."<br>".str_replace("|||", "", str_replace("|||1", " oui", str_replace("|||0", " non", $item->option3)));
                return $out;
            case 'commentaire':
                return $item->$column_name;
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }
 
    function get_columns() {
        return $columns = array(
            'evenement_id' => __('Évènement'),
            'date' => __('Date'),
            'nom' => __('Nom'),
            'adresse' => __('Adresse'),
            'questions' => __('Questions'),
            'commentaire' => __('Commentaire')
        );
    }
    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
       return $sortable = array(
          'evenement_id'=>'evenement_id',
          'date'=>'date'
       );
    }
  
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries
 
           /* -- Preparing your query -- */
        $query = "SELECT * FROM pzebfiv_evenements_inscription ";
        if (isset($_GET["idevenement"]) && $_GET["idevenement"]>0) {
            $query.= " WHERE evenement_id = '" . $_GET["idevenement"] . "' ";
        }
 
        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'date';
        $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : 'DESC';
        if (!empty($orderby) & !empty($order)) {
            $query.=' ORDER BY ' . $orderby . ' ' . $order;
        }
        //
        $totalitems = $wpdb->query($query);
 
        /**
         * First, lets decide how many records per page to show
         */
        $perpage = 10;
 
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
        //Page Number
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }
 
 
        //How many pages do we have in total?
        $totalpages = ceil($totalitems / $perpage);
        //adjust the query to take pagination into account
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query.=' LIMIT ' . (int) $offset . ',' . (int) $perpage;
        }
 
        /* -- Register the pagination -- */
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));
 
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
 
        $this->_column_headers = array($columns, $hidden, $sortable);
 
        $this->items = $wpdb->get_results($query);
    }
 
}

