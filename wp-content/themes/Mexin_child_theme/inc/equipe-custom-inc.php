<?php
    
# initialisation du custom post type
function custom_post_equipe() {
  $labels = array(
    'name'               => _x( 'Équipes', 'post type general name' ),
    'singular_name'      => _x( 'Équipe', 'post type singular name' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter un contact' ),
    'edit_item'          => __( 'Modifier un contact' ),
    'new_item'           => __( 'Nouveau contact' ),
    'all_items'          => __( 'Tous les contacts' ),
    'view_item'          => __( 'Voir le contact' ),
    'search_items'       => __( 'Rechercher dans les contacts' ),
    'not_found'          => __( 'Aucun contact trouvé' ),
    'not_found_in_trash' => __( 'Aucun contact trouvé dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Équipes'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gestion des équipes',
    'public'        => true,
    'menu_position' => 15,
    'menu_icon'     => 'dashicons-businessman',
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => true,
  );
  register_post_type( 'equipe', $args ); 
}
add_action( 'init', 'custom_post_equipe' );

# custom message
function updated_messages_equipe( $messages ) {
  global $post, $post_ID;
  $messages['equipe'] = array(
    0 => '', 
    1 => sprintf( __('Contact mis à jour. <a href="%s">Voir le contact</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Contact mis à jour.'),
    5 => isset($_GET['revision']) ? sprintf( __('Contact restauré à la revision %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Contact publié. <a href="%s">Voir le contact</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Contact enregistré.'),
    8 => sprintf( __('Contact envoyé. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Contact planifié pour : <strong>%1$s</strong>. <a target="_blank" href="%2$s">Prévisualiser les modifications</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Brouillon mis à jour. <a target="_blank" href="%s">Prévisualiser les modifications</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_equipe' );

# custom categories
function taxonomies_equipe() {
  $labels = array(
    'name'              => _x( 'Département', 'taxonomy general name' ),
    'singular_name'     => _x( 'Département', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher dans les départements' ),
    'all_items'         => __( 'Tous les départements' ),
    'parent_item'       => __( 'Département parent' ),
    'parent_item_colon' => __( 'Département parent :' ),
    'edit_item'         => __( 'Modifier un département' ), 
    'update_item'       => __( 'Mettre à jour un département' ),
    'add_new_item'      => __( 'Ajouter' ),
    'new_item_name'     => __( 'Nouveau département' ),
    'menu_name'         => __( 'Départements' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
  );
  register_taxonomy( 'equipe_departement', 'equipe', $args );
}
add_action( 'init', 'taxonomies_equipe', 0 );

# custom meta box
add_filter( 'rwmb_meta_boxes', 'equipe_box' );
function equipe_box( $meta_boxes )
{
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'equipe_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Informations', 'equipe' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'equipe' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Fonction', 'equipe' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}fonction",
				'type'  => 'text',
			),
			// EMAIL
			array(
				'name' => __( 'Email', 'equipe' ),
				'id'   => "{$prefix}email",
				'type' => 'email',
			),
			// URL
			array(
				'name' => __( 'Linkedin', 'equipe' ),
				'id'   => "{$prefix}linkedin",
				'type' => 'url',
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Téléphone', 'equipe' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}telephone",
				'type'  => 'text',
			),
		),
	);

	return $meta_boxes;
}

# metabox pour le shortcode
add_action('add_meta_boxes','equipe_shortcode_box');
function equipe_shortcode_box () {
    if (isset($_GET["post"]) && $_GET["post"]>0) {
        add_meta_box('equipe_shortcode_box', 'Shortcode à copier/coller pour affichage dans une page', 'equipe_shortcode_box_html', 'equipe', 'normal');
    }
}
function equipe_shortcode_box_html () {
    echo '[equipe_contact id="'.$_GET["post"].'"]';
}

# colonne avec shortcode
add_filter('manage_equipe_posts_columns' , 'add_equipe_columns');
function add_equipe_columns($columns) {
    return array_merge($columns,
          array('code' => 'Shortcode'));
}
add_action('manage_equipe_posts_custom_column' , 'equipe_custom_columns', 10, 2 );
function equipe_custom_columns( $column, $post_id ) {
    switch ( $column ) {
 
    case 'code' :
        echo '[equipe_contact id="'.$post_id.'"]';
        break;
    }
}


# shortcode : affichage d'un contact
add_shortcode('equipe_contact', 'display_equipe_contact');
function display_equipe_contact ($atts, $content) {
    if (isset($atts["id"]) && $atts["id"]>0) {
        $args = array( 'post_type' => 'equipe', 'p' => $atts["id"] );
        $loop = new WP_Query( $args );
        
        ob_start();
        
        while ( $loop->have_posts() ) : $loop->the_post();
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
            $thumb_url = $thumb_url_array[0];
            $content = '<div id="contact">';
            if ($thumb_url!="http://www.cerib.com/wp-includes/images/media/default.png") $content.= '<div style="background:url('.$thumb_url.') center center; background-size: cover; width:80%; margin: 0 auto; margin-bottom: 15px; height: 240px;"></div>';
            $content.= '
                    <p></p>
                    <h4>'.get_the_title().'</h4>
                    <h5>'.rwmb_meta('equipe_fonction').'</h5>
                    <ul id="contact-info">';
            if (rwmb_meta('equipe_email')!="")     $content.= '<li><a href="mailto:'.eae_encode_emails(rwmb_meta('equipe_email')).'"><i class="fa fa-envelope-o"></i> '.eae_encode_emails(rwmb_meta('equipe_email')).'</a></li>';
            if (rwmb_meta('equipe_telephone')!="") $content.= '<li><i class="fa fa-phone"></i> '.rwmb_meta('equipe_telephone').'</li>';
            if (rwmb_meta('equipe_linkedin')!="")  $content.= '<li><i class="fa fa-linkedin"></i> <a href="'.rwmb_meta('equipe_linkedin').'">Profil linkedin</a></li>';
            $content.= '
                    </ul>
                </div>
            ';
        endwhile;
        echo $content;
        
        $output = ob_get_clean();
        return $output;
    }
}

# shortcode : affichage des équipes
add_shortcode('equipe', 'display_equipe');
function display_equipe ($atts, $content) {
    global $depts;
    
    $args = array( 'post_type' => 'equipe', 'posts_per_page' => 100, 'orderby' => 'post_title', 'order' => 'ASC' );
    $loop = new WP_Query( $args );
    $arrDepartementCode = array();
    $arrDepartementName = array();
    while ( $loop->have_posts() ) : $loop->the_post();
        # données departement
        $arrAllTags = array();
        $arrAllTags = get_the_terms(get_the_ID(), 'equipe_departement', 'fields=names');
        if ($arrAllTags != "") {
            foreach ($arrAllTags as $tag) {
                $arrDepartementCode[] = "departement-".$tag->slug;
                $arrDepartementName[] = $tag->name;
            }
        }
    endwhile;
    $arrDepartementCode = array_unique($arrDepartementCode);
    $arrDepartementName = array_unique($arrDepartementName);
    sort($arrDepartementCode);
    sort($arrDepartementName);
    # filtre
    ?>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="col-md-12 equipe-filtres">
            <ul>
                <li>AFFICHER NOS ÉQUIPES</li>
                <li>
                    <a href="#" class="filtre-label"><span id="label-departement">Par département</span> <i class="fa-chevron-down"></i></a>
                    <select name="filtre-departement-equipe[]" id="filtre-departement-equipe" multiple="multiple">
                        <?php for ($c=0; $c<count($arrDepartementCode); $c++) { ?>
                        <option value="<?php echo $arrDepartementCode[$c]; ?>"><?php echo $arrDepartementName[$c]; ?></option>
                        <?php } ?>
                    </select>
                </li>
            </ul>
            <div class="equipe-separateur"></div>
        </div>
    </div>
    <div class="wpb_row vc_row-fluid full-row">
        <?php
        # liste
        while ( $loop->have_posts() ) : $loop->the_post();
            # données departement
            $arrAllTags = array();
            $arrAllTags = get_the_terms(get_the_ID(), 'equipe_departement', 'fields=names');
            $arrTagsCode = array();
            $arrTagsName = array();
            if ($arrAllTags != "") {
                foreach ($arrAllTags as $tag) {
                    $arrTagsCode[] = "departement-".$tag->slug;
                    $arrTagsName[] = $tag->name;
                }
            }
            ?>
            <div class="col-md-6 equipe-list-bloc <?php echo implode(" ", $arrTagsCode); ?>">
                <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post equipe-list clearfix'); ?>>
                    <div class="col-md-4 equipe-photo">
                        <? get_the_image( array( 'meta_key' => array( 'Full', 'Full' ), 'size' => 'Full' ) ); ?>
                        <?php /*
                        <h4>
                        <?php
                        $title = trim(get_the_title());
                        echo str_replace(" ", "<br>", $title);
                        ?>
                        </h4>
                        */ ?>
                    </div>
                    <div class="col-md-8 equipe-item">
                        <h4><?php the_title(); ?></h4>
                        <h5><?php echo rwmb_meta('equipe_fonction'); ?></h5>
            			<ul class="equipe-mail-phone">
            			<?php if (rwmb_meta('equipe_email')!="") { ?><li><a href="mailto:<?php echo eae_encode_emails(rwmb_meta('equipe_email')); ?>"><i class="fa fa-envelope-o"></i> <?php echo eae_encode_emails(rwmb_meta('equipe_email')); ?></a></li><?php } ?>
            			<?php if (rwmb_meta('equipe_telephone')!="") { ?><li><i class="fa fa-phone"></i> <?php echo rwmb_meta('equipe_telephone'); ?></li><?php } ?>
            			<?php if (rwmb_meta('equipe_linkedin')!="") { ?><li><i class="fa fa-linkedin"></i> <a href="<?php echo rwmb_meta('equipe_linkedin'); ?>">Profil linkedin</a></li><?php } ?>
            			</ul>
                        <?php the_content(); ?>
                    </div>
                </article>
                <hr class="vertical-space">
            </div>
            <?php
        endwhile;
        ?>
    </div>
    <?php
}

