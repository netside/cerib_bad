<?php
class Usine
{
	private $products;
	
	public function __construct()
	{
		$this->products = array();
	}
	
	public function add_product($idProduct)
	{
		array_push($this->products, $idProduct);
	}
	
	public function getArrayProduct()
	{
		return $this->products;
	}
	
	public function getProductId($i)
	{
		return $this->products[$i];
	}
}

function count_node($node){
	$i=0;
	foreach ($node->child_nodes() as $cnode){
		if ($cnode->node_name() != "#text")
			$i++;                
		$cnode = $cnode->next_sibling();
	}
	return $i;
}

function setHtmlTableHeader($node, $nodeRupture, $famille, $numRow, $niv = 0) {
	global $tNumCrt, $tCrtVisible, $sRemarque;
	$sHeader = "";
	$sHeader2 = "";
	$countNode=0;
	foreach ($node->child_nodes() as $cnode){
		if ($cnode->node_type()==XML_TEXT_NODE){
			if ($niv && !trim(utf8_decode($cnode->node_value()))){
				$sHeader .= "";
				$sHeader2 .= "";
			} else {
				$sHeader .= trim(utf8_decode($cnode->node_value()));
				$sHeader2 .= trim(utf8_decode($cnode->node_value()));
			}
		} else if ($cnode->node_type()==XML_ELEMENT_NODE) {
			if ($cnode->node_name() !== "remarque"){
				$bIsVisible = true;
				if ($famille != $cnode->node_name() && $cnode->has_attributes()){
					$tNumCrt[] = $cnode->get_attribute("num");
					
					$tCrtVisible[] = true;                    
					if ($cnode->get_attribute("visible")=="false"){
						$bIsVisible = false;
						$tCrtVisible[sizeOf($tNumCrt)-1] = false;
					}
				}
					
				if ($bIsVisible) {
					$nodeText = setHtmlTableHeader($cnode, $nodeRupture, $famille, $numRow, $niv+1);
					
					if ($niv==1 && $famille == $cnode->node_name()) {
						$countNode = count_node($cnode->parent_node())-1;
						$sHeader .= "<td colspan='$countNode'>".trim($nodeText)."</td>\r\n";
					} else if ($niv==1){
						$sHeader2 .= "<td>".trim($nodeText)."</td>\r\n";                    
					} else if ($cnode->node_name() != $nodeRupture) {
						$sHeader .= "<td rowspan='2'>".trim($nodeText)."</td>\r\n";
					} else {
						$sHeader .= trim($nodeText);
						$sHeader2 .= trim($nodeText);
					}
				}
			} else {
				$nodeText = setHtmlTableHeader($cnode, $nodeRupture, $famille, $numRow, $niv+1);
				$sRemarque = trim($nodeText);
			}
			
		}
	}
	if ($numRow==0)
		return $sHeader;
	else
		return $sHeader2;
}

function setHtmlForm($node, $sKey, $sType = null, $sName = null) {    
	$sForm = "";
	//foreach ($node->child_nodes() as $cnode){
	$nodeChild = $node->first_child();
	while ($nodeChild) {
		if ($nodeChild->node_type()==XML_ELEMENT_NODE) {
		
			// on chope le type et on passe sur le parent
			if ($nodeChild->node_name() == "champs"){
				$sType = $nodeChild->get_attribute('type');
				$sNumCrt = $nodeChild->get_attribute('num_crt');
				$iNbFils = count_node($nodeChild)-1;
				// on pointe sur les fils
				$nodeChildChild = $nodeChild->first_child();
				$i=0;
				$isLibelle = false;
			}
			$fieldName = null;
			while ($nodeChildChild) {
				if ($nodeChildChild->node_type()==XML_ELEMENT_NODE) {
					if ($nodeVal = $nodeChildChild->first_child())
						$nodeText = html_entity_decode(utf8_decode($nodeVal->node_value()));
					$nodeName = $nodeChildChild->node_name();
					
					// libelle
					if ($nodeName == "libelle"){
					
						$sForm .= "\r\n
						<tr>
							<th><b>".$nodeText."</b></th>";
						$fieldName = $nodeText;
						$isLibelle = true;
						
					} else if ( ($sType == "LST" || $sType == "LST-EXP") && $nodeName == "valeur" ){
					
						if (!$i)
							$sForm .= "<td class='normal'>\r\n<select name=\"".$sType."_".$sNumCrt."\" class='normal'><option value=''></option>\r\n";
						$sForm .= "\t<option value=\"".$nodeText."\">".$nodeText."</option>\r\n";
						$i++;
						if ($i==$iNbFils)
							$sForm .= "</select></td></tr>\r\n";
							
					} else if ($sType == "CHK" && $nodeName == "valeur" ){
					
						$sNumCrt = $nodeChildChild->get_attribute('num_crt');
						$sValueCrt = "X";
						if (!$sNumCrt){
							$sNumCrt = $nodeChild->get_attribute('num_crt');
							$sValueCrt = $nodeText;
						}
						if ($fieldName) {
							if ($isLibelle)
								$sForm .= "<td class='normal'>".$nodeText."<input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
							else
								$sForm .= "<tr><td>&nbsp;</td><td class='normal'>".$nodeText."<input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
							$isLibelle = false;
						} else
							$sForm .= "<tr><th align='right' class='normal'>".$nodeText."</th><td><input type='checkbox' name='CHK_$sNumCrt' value='$sValueCrt'></td></tr>\r\n";
						
					} else if ($sType == "RBTN" && $nodeName == "valeur" ){
					
						$sNumCrt = $nodeChildChild->get_attribute('num_crt');
						if ($isLibelle && !$i)
							$sForm .= "<td class='normal'>";
						else if (!$i)
							$sForm .= "<tr><td>&nbsp;</td><td class='normal'>";
						
						$sForm .= "<input type='radio' name='RBTN_".$nodeChild->get_attribute('num_crt')."' value='$sNumCrt'>".$nodeText;
						$isLibelle = false;
						$i++;
						if ($i==$iNbFils)
							$sForm .= "</td></tr>\r\n";
						
					} else
						$sForm .= $nodeText;
				}
				$nodeChildChild = $nodeChildChild->next_sibling();
			}
			
		}
		$nodeChild = $nodeChild->next_sibling();
	}
	return $sForm;
}