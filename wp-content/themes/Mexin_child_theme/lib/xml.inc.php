<?php
/*
 * Plateforme - XML
 * Copyright � 2005, Hippocampe
 * 
 * Fichier : xml.inc.php
 * Description : Fonction utiles pour la gestion de XML
 * Auteurs : Pincemin Nicolas <nicolas@hippocampe.fr>
 * Version : 1.0.0 (Pincemin Nicolas, 24/06/2005) - Fichier initial
 */

function getSqlFromArray($tArray){
	
	//$sSqlValuesSep = (isset($sSqlInsert)) ? " , " : null;
	
	$tKeys =array_keys($tArray);
	//if (!$sSqlInsert){
		$sSqlKey = null;
		for ($i=0;$i<sizeOf($tKeys);$i++)
			$sSqlKey .= ($i) ? ", ".$tKeys[$i] : $tKeys[$i];
		$sSqlInsert = "INSERT INTO @MYTABLE@ ($sSqlKey) VALUES ";
	//}
	
	//for ($i=0;$i<sizeOf($tArray);$i++){
		//$sSqlValuesSep = (isset($sSqlValues)) ? " , " : null;
		$sSqlValues = null;
		for ($j=0;$j<sizeOf($tKeys);$j++){
			// le champ n'est pas trouv�
			if (!isset($tArray[$tKeys[$j]]))
			    $tArray[$tKeys[$j]]=null;
			
			$sSqlValues .= ($j) ? ", " : "";
			$sSqlValues .= "'".str_replace("'","\'",$tArray[$tKeys[$j]])."'";
		}
		//$sSqlInsert .= $sSqlValuesSep."(".$sSqlValues.")";
		$sSqlInsert .= "(".$sSqlValues.")";
	//}
	return $sSqlInsert;
}
 
/*	* Fonction GetXmlNodeAsString	Transforme un flux de donn�es XML en chaine � partir d'un noeud de d�part
	*
	* @param $node	Noeud de d�part	* 
	* @return $st 	Chaine XML
*/
function GetXmlNodeAsString($node) {    
	$st = "";
	foreach ($node->child_nodes() as $cnode){
		if ($cnode->node_type()==XML_TEXT_NODE)
			$st .= str_replace("<","&lt;",trim(chop(utf8_decode($cnode->node_value()))));
		else if ($cnode->node_type()==XML_ELEMENT_NODE) {
			$st .= "<" . $cnode->node_name();
			if ($attribnodes=$cnode->attributes()) {
				$st .= " ";
				foreach ($attribnodes as $anode)
					$st .= $anode->node_name() . "='" . utf8_decode($anode->node_value()) . "' ";
			}    
			$nodeText = GetXmlNodeAsString($cnode);
			if (empty($nodeText) && !$attribnodes)
				$st .= " />";        // unary
			else
				$st .= ">" . $nodeText . "</" . utf8_decode($cnode->node_name()) . ">";
		}
	}
	$st = str_replace("\t\n","",$st);
	return str_replace("\t","",$st);
}

/*	* Fonction insertXmlDataIntoSql			Transforme un flux de donn�es XML en tableau � deux dim de [i][cl�]>valeur
	*
	* @param $domXml 				flux XML
	* @param $sPrimaryNode 			Noeud de d�part (table)
	* @param $sKeyPrimaryNode 		Identifiant de ce noeud de d�part (cl� primaire de la table)
	* @param $sSecondaryNode 		Noeud de d�part de la table secondaire (reli� par un id_secondaire)
	* @param $sKeySecondaryNode 	Cl� secondaire (id_... de la table $sSecondaryNode)
	* 
	* Si $sSecondaryNode vaut quelque chose mais pas $sKeySecondaryNode, 
	* alors la cl� $sSecondaryNode sera enlev� du tableau de r�sultat $tab
*/
function insertXmlDataIntoSql($domXml, $tabReplace, $sPrimaryNode, $sKeyPrimaryNode = "", $sSecondaryNode = "", $sKeySecondaryNode = "", $sNoField = "", $sTypeNN = ""){
	$tXml = $domXml->get_elements_by_tagname($sPrimaryNode);
	$i=0;
	$tab=array();
	foreach ($tXml as $node) {
		$sSqlInsert = null;
		if (!$sKeyPrimaryNode) {
			if ($attribnodes=$node->attributes()) {
				foreach ($attribnodes as $anode)
					$tab[$anode->node_name()] = utf8_decode($anode->node_value());
				unset($attribnodes);
				unset($anode);
			}    
		} else if ($sKeyPrimaryNode) {
		
			if ($sKeyPrimaryNode == "autoincrement") {
				$tab['id'] = $i+1;
				if ($sKeySecondaryNode)
					$tab[$sKeySecondaryNode] = $node->get_attribute($sKeySecondaryNode);
			} else if (!$sTypeNN){
				$tab['id'] = $node->get_attribute($sKeyPrimaryNode);
			} else
				$tab[$sKeyPrimaryNode] = $node->get_attribute($sKeyPrimaryNode);
				
			if ($sTypeNN == "type_nn") {
				$tab['id'] = $i+1;
			}
		
			// Traitement des noeuds enfants
			$nodeChild = $node->first_child();
			while ($nodeChild) {
				
				if ($nodeChild->node_name() != "#text") {
					
					// si on a toujours pas de cl� primaire
					if (!$tab['id'] && $sKeyPrimaryNode && $sKeyPrimaryNode != "autoincrement")
			    		$tab['id'] = $nodeChild->get_attribute($sKeyPrimaryNode);
					
					if ($sKeyPrimaryNode == "autoincrement") {
						if ($attribnodes=$nodeChild->attributes()) {
							foreach ($attribnodes as $anode)
								$tab[$anode->node_name()] = utf8_decode($anode->node_value());
						}
					}
					// cr�ation d'un tableau cl�/valeur
					$sKey = $nodeChild->node_name();
					$sValue = "";
					if ($nodeVal = $nodeChild->first_child())
						$sValue = trim(chop(utf8_decode($nodeVal->node_value())));
					
					// si groupement au sein du flux, on supprime le grp
					$tExcludeGrp = explode(",", $sNoField);					
					//if ( ($sKey == $sSecondaryNode && !$sKeySecondaryNode) || ($sKey == $sNoField && $sNoField) ){
					if ( ($sKey == $sSecondaryNode && !$sKeySecondaryNode) || (in_array($sKey,$tExcludeGrp) && $sNoField) ){
						unset($tab[$sKey]);
					} else {
						if ($nodeChild->has_child_nodes()){
							$nodeChildChild = $nodeChild->first_child();
							$nodeChildChild = $nodeChildChild->next_sibling();
							if (isset($nodeChildChild))
								$sValue = "<".$sKey.">" . GetXmlNodeAsString($nodeChild) . "</".$sKey.">";
						}
						$tab[$sKey] = $sValue;
					}
					//$tab[$sKey] = $sValue;
					
					// si groupement au sein du flux, on supprime le grp
					//if ( ($sKey == $sSecondaryNode && !$sKeySecondaryNode) || ($sKey == $sNoField && $sNoField) )
					//	unset($tab[$sKey]);
					
					if ($sKey=="critere" && isset($tab["num_crt"])) {
						if ($sKeyPrimaryNode == "autoincrement")
							$tab['id'] = $i+1;
						$sSqlInsert = getSqlFromArray($tab);
						for ($k=0;$k<sizeOf($tabReplace);$k++)
							$sSqlInsert = str_replace($tabReplace[$k]['key'], $tabReplace[$k]['value'], $sSqlInsert);
						$result = db_query($sSqlInsert);
						$i++;
					}
					
					//echo $nodeChild->node_name()."<br>";
				}
				
				$nodeChild = $nodeChild->next_sibling();
	        }
			unset($nodeChild);
			// si cl� secondaire alors, on remonte les parents pour chopper l'id_table_secondaire
			if ($sSecondaryNode && $sKeySecondaryNode) {
				// d�finition du nom de la cl� secondaire
				$sKey = 'id_'.$sSecondaryNode;
				// tant que j'ai pas trouv� ma cl� secondaire
				$nodeParent = $node;
				while( isset($nodeParent) && $nodeParent->node_name() != $sSecondaryNode ){
					$nodeParent = $nodeParent->parent_node();
				}
					
				if ( isset($nodeParent) && $nodeParent->node_name()==$sSecondaryNode) {
					$tab[$sKey] = $nodeParent->get_attribute($sKeySecondaryNode);
				}
				unset($nodeParent);
			}
			unset($node);				
		}
		
		if (!$sSqlInsert && ( (isset($tab['id']) && $tab['id']) || !$sKeyPrimaryNode ) ) { // modif 24/01/06  && $tab['id']
			$sSqlInsert = getSqlFromArray($tab);
			unset($tab);
			for ($k=0;$k<sizeOf($tabReplace);$k++)
				$sSqlInsert = str_replace($tabReplace[$k]['key'], $tabReplace[$k]['value'], $sSqlInsert);
			//echo $sSqlInsert."<br>";
			$result = db_query($sSqlInsert, false);// doublons
			$i++;
		}
	}
	unset($tXml);
	return $i;
}
/*function insertXmlDataIntoSql($domXml, $tabReplace, $sPrimaryNode, $sKeyPrimaryNode = "", $sSecondaryNode = "", $sKeySecondaryNode = ""){
	//global $candy; 
    $tXml = $domXml->get_elements_by_tagname($sPrimaryNode);
	//$root->name = '$candy';
	foreach ($tXml as $node){
		$node->name = $sPrimaryNode;
		parse_node($node);
	}
	//echo $candy;
}

function parse_node($node) { 
    if ($node->has_child_nodes()) { 
        foreach($node->child_nodes() as $n) {
			$n->name = null;
			if ($n->node_name() == '#text') {
				echo "<br>";
            	//echo $n->node_value()."<br>";
               // eval("$node->name=\"" . $n->node_value() . "\";"); 
            } else {
				//echo $n->node_name()."<br>";
                $n->name = $node->name . '->' . $n->node_name();
				echo $n->name.memory_get_usage();
                parse_node($n); 
            }
        }
		unset($node);
    } 
}*/

?>