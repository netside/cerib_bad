<?php 

GLOBAL $webnus_options; 

/***************************************/
/*			Close  head line if woocommerce available
/***************************************/		

if( isset($post) ){
if( 'product' == get_post_type( $post->ID ))
{
echo '</section>';
}
}

?>

<?php
$footer_show = 'true';
if(isset($post))
{
GLOBAL $webnus_page_options_meta;
$footer_show_meta = isset($webnus_page_options_meta)?$webnus_page_options_meta->the_meta():null;
$footer_show =(isset($footer_show_meta) && is_array($footer_show_meta) && isset($footer_show_meta['webnus_page_options'][0]['webnus_footer_show']))?$footer_show_meta['webnus_page_options'][0]['webnus_footer_show']:null;
}
$widgetNL = new WYSIJA_NL_Widget(true);


$currentLang =  get_locale();
$idNewsletter = 1;
if ($currentLang == "fr_FR") {
    $newsletterLabel = "Inscrivez-vous à la newsletter : ";
    $idNewsletter = 1;
} else {
    $newsletterLabel = "Subscribe to the newsletter :";
    $idNewsletter = 2;
}

if ($footer_show != 'false') : ?>
    <?php if (is_front_page()): ?>
    <section id="newsletter-bar">
        <div id="newsletter-bar-container" class="container">
            <label><?php echo $newsletterLabel; ?></label>
            <?php echo $widgetNL->widget(array('form' => $idNewsletter, 'form_type' => 'php')); ?>
        </div>
    </section>
    <?php endif; ?>
    <section id="social-bar">
        <div id="social-bar-container" class="container">
            <?php if (is_front_page()): ?>
            <div class="vc_col-sm-6 bloc-logo-cti">
                <span class="social-info">
                    <?php
                    $currentLang =  get_locale();
                    if ($currentLang == "fr_FR") echo "Membre du réseau";
                    else                         echo "A member of";
                    ?>
                </span>
                <a href="http://www.reseau-cti.com/" target="_blank"><img src="<?php echo site_url("/"); ?>wp-content/themes/Mexin_child_theme/images/logo-cti-2016.jpg" alt="CTI" border="0" class="social-icon-cti" height="48"></a>
            </div>
            <div class="vc_col-sm-6">
            <?php else: ?>
            <div class="vc_col-sm-12">
            <?php endif; ?>
                <span class="social-info">
                    <?php
                    $currentLang =  get_locale();
                    if ($currentLang == "fr_FR") echo "Nous suivre";
                    else                         echo "Follow us";
                    ?>
                </span>
                <?php
                if($webnus_options->webnus_top_social_icons_linkedin())
                	echo '<a href="'. $webnus_options->webnus_linkedin_ID() .'" target="_blank" class="linkedin"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-linkedin.png" alt="Linkedin" border="0" class="social-icon"></a>';	
                if($webnus_options->webnus_top_social_icons_youtube())
                	echo '<a href="'. $webnus_options->webnus_youtube_ID() .'" target="_blank" class="youtube"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-youtube.png" alt="Youtube" border="0" class="social-icon"></a>';		
                if($webnus_options->webnus_top_social_icons_google())
                	echo '<a href="'. $webnus_options->webnus_google_ID() .'" target="_blank" class="google"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-googleplus.png" alt="Google +" border="0" class="social-icon"></a>';	
                if($webnus_options->webnus_top_social_icons_twitter())
                	echo '<a href="'. $webnus_options->webnus_twitter_ID() .'" target="_blank" class="twitter"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-twitter.png" alt="Twitter" border="0" class="social-icon"></a>';
                if($webnus_options->webnus_top_social_icons_facebook())
                	echo '<a href="'. $webnus_options->webnus_facebook_ID() .'" target="_blank" class="facebook"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-facebook.png" alt="Facebook" border="0" class="social-icon"></a>';
                if($webnus_options->webnus_top_social_icons_rss())
                	echo '<a href="'. $webnus_options->webnus_rss_ID() .'" class="rss"><img src="' . site_url("/") . 'wp-content/themes/Mexin_child_theme/images/icon-rss.png" alt="RSS" border="0" class="social-icon"></a>';
                if($webnus_options->webnus_top_social_icons_dribbble())
                	echo '<a href="'. $webnus_options->webnus_dribbble_ID().'" target="_blank" class="dribble"><i class="fa-dribbble"></i></a>';
                if($webnus_options->webnus_top_social_icons_pinterest())
                	echo '<a href="'. $webnus_options->webnus_pinterest_ID() .'" target="_blank" class="pinterest"><i class="fa-pinterest"></i></a>';
                if($webnus_options->webnus_top_social_icons_vimeo())
                	echo '<a href="'. $webnus_options->webnus_vimeo_ID() .'" target="_blank" class="vimeo"><i class="fa-vimeo-square"></i></a>';
                if($webnus_options->webnus_top_social_icons_instagram())
                	echo '<a href="'. $webnus_options->webnus_instagram_ID() .'" target="_blank" class="instagram"><i class="fa-instagram"></i></a>';		
                if($webnus_options->webnus_top_social_icons_flickr())
                	echo '<a href="'. $webnus_options->webnus_flickr_ID() .'" target="_blank" class="other-social"><i class="fa-flickr"></i></a>';	
                if($webnus_options->webnus_top_social_icons_reddit())
                	echo '<a href="'. $webnus_options->webnus_reddit_ID() .'" target="_blank" class="other-social"><i class="fa-reddit"></i></a>';	
                if($webnus_options->webnus_top_social_icons_delicious())
                	echo '<a href="'. $webnus_options->webnus_delicious_ID() .'" target="_blank" class="other-social"><i class="fa-delicious"></i></a>';	
                if($webnus_options->webnus_top_social_icons_lastfm())
                	echo '<a href="'. $webnus_options->webnus_lastfm_ID() .'" target="_blank" class="other-social"><i class="fa-lastfm-square"></i></a>';
                if($webnus_options->webnus_top_social_icons_tumblr())
                	echo '<a href="'. $webnus_options->webnus_tumblr_ID() .'" target="_blank" class="other-social"><i class="fa-tumblr-square"></i></a>';
                if($webnus_options->webnus_top_social_icons_skype())
                	echo '<a href="'. $webnus_options->webnus_skype_ID() .'" target="_blank" class="other-social"><i class="fa-skype"></i></a>';        
                ?>
            </div>
        </div>
    </section>
	<footer id="footer" <?php if( 2 == $webnus_options->webnus_footer_color() ) echo 'class="litex"';?>>
	<section class="container footer-in">
	  <?php 
		
	/***************************************/
	/*			Loading footer type 1,2,3,4,5
	/*			Each footer has it's own behaviour
	/***************************************/		
		
		$footer_type = $webnus_options->webnus_footer_type();
	  
		get_template_part('parts/footer',$footer_type);
			  
	  ?>
	  </section>
	<!-- end-footer-in -->
	<?php 
	if( $webnus_options->webnus_footer_bottom_enable() )
		get_template_part('parts/footer','bottom'); 

	?>
	<!-- end-footbot -->
	</footer>
    <?php if (!is_front_page()): ?>
    <section id="newsletter-bar">
        <div id="newsletter-bar-container" class="container">
            <label><?php echo $newsletterLabel; ?></label>
            <?php echo $widgetNL->widget(array('form' => $idNewsletter, 'form_type' => 'php')); ?>
        </div>
    </section>
    <?php endif; ?>
	<!-- end-footer -->
<?php endif; ?>


<span id="scroll-top"><a class="scrollup"><i class="fa-chevron-up"></i></a></span></div>
<!-- end-wrap -->
<!-- End Document
================================================== -->
<?php

	echo $webnus_options->webnus_space_before_body();
	wp_footer(); 
	
?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.select-multiple.js?ver=1.0.0"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/onready.js?ver=1.0.0"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/certification.js"></script>
<!-- Azalead tracking code --> 
<script src="//b2btagmgr.azalead.com/tag?az=aWRTdWJzY3JpYmVyPTE4NjcmaWRTaXRlPTE5NjEmb3JpZ2luPXd3dy5jZXJpYi5jb20vJmN1c3RvbT1ub25l" type="application/javascript"> </script> 
<!-- End Azalead tracking code -->
</body>
</html>