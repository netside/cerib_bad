UPDATE pzebfiv_options SET option_value = replace(option_value, 'http://ns37211.ovh.net/~ceribw', 'http://www.cerib.com') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE pzebfiv_posts SET guid = replace(guid, 'http://ns37211.ovh.net/~ceribw','http://www.cerib.com');

UPDATE pzebfiv_posts SET post_content = replace(post_content, 'http://ns37211.ovh.net/~ceribw', 'http://www.cerib.com');