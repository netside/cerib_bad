 <?php
/******************/
/**  Single Post
/******************/

get_header();
if (is_single()){
	global $blogpost_meta;
	$post_meta = $blogpost_meta->the_meta();
	if(!empty($post_meta)){
		if($post_meta['style_type']=="postshow1" && $thumbnail_id = get_post_thumbnail_id()){
			$background = wp_get_attachment_image_src( $thumbnail_id, 'full' ); ?>
			<div class="postshow1" style="background-image: url(<?php echo $background[0]; ?> );">
				<div class="postshow-overlay"></div>
				<div class="container"><h1 class="post-title-ps1"><?php the_title() ?></h1></div>
			</div> <?php
		}
	}
} ?>
<div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<section id="main-content" class="container interne">
<!-- Start Page Content -->
<div class="row-wrapper-x">
	<div class="vc_col-md-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 
		</div> 
	</div> 
</div></section>
<section id="carousel-interne" class="wpb_row vc_row-fluid full-row">
	<div class="vc_col-md-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">            
            <?php echo do_shortcode( '[layerslider id="23"]' ); ?>
		</div> 
	</div> 
</section>
<section class="blox bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
    <div class="max-overlay" style="background-color:"></div>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="container">
            <div class="vc_col-md-3 wpb_column vc_column_container custom-aside-widget ">
                <aside class="col-md-3 sidebar leftside custom-bloc-wiget formation-widget">
                    <?php 
                    dynamic_sidebar( 'Left Sidebar' );
                    ?>
            		<?php if( 1 == $webnus_options->webnus_blog_social_share() ) { ?>	
            		<div class="widget widget-full">
                		<h4 class="subtitle">Partager</h4>
                		<?php
                        $linkS = urlencode(get_the_permalink());
                        $titleS = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
                        ?>
                		<ul class="social-share-icons">
                    		<li><a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $linkS ?>&amp;t=<?php echo $titleS ?>" target="blank"><i class="fa-facebook"></i></a></li>
                    		<li><a class="google" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php echo $linkS ?>" target="_blank"><i class="fa-google"></i></a></li>
                    		<li><a class="twitter" href="https://twitter.com/intent/tweet?original_referer=<?php echo $linkS ?>&amp;text=<?php echo $titleS ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?><?php echo isset( $twitter_user ) ? '&amp;via='.$twitter_user : ''; ?>" target="_blank"><i class="fa-twitter"></i></a></li>
                    		<li><a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $linkS ?>&amp;title=<?php echo $titleS ?>&amp;source=<?php bloginfo( 'name' ); ?>" target="blank"><i class="fa-linkedin"></i></a></li>
                    		<li><a class="email" href="mailto:?subject=<?php echo get_the_title() ?>&amp;body=<?php echo $linkS ?>"><i class="fa-envelope"></i></a></li>
                		</ul>
            		</div>
            		<?php } ?>
        		</aside>
            </div>
            <div class="vc_col-md-9 wpb_column vc_column_container">
                <div class="wpb_wrapper">
                    <div class="wpb_tabs wpb_content_element  onglet-nombre-4" data-interval="0">
                        <div class="wpb_wrapper webnus-tabs wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">
                            <?php /*
                            <ul id="formation-detail-onglets" class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="0" aria-controls="tab-tous" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                                    <a href="<?php echo home_url('/'); ?>prestations/former/#tab-le-centre-de-formation-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Le Centre de Formation</a>
                                </li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-matinales" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                                    <a href="<?php echo home_url('/'); ?>prestations/former/#tab-la-pedagogie-active-2" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">La pédagogie active</a>
                                </li>
                                <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="-1" aria-controls="tab-petits-dejeuners" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                                    <a href="<?php echo home_url('/'); ?>prestations/former/#tab-sessions-3" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">Les sessions</a>
                                </li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-soirees" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false">
                                    <a href="<?php echo home_url('/'); ?>prestations/former/#tab-temoignages-4" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">Témoignages</a>
                                </li>
                            </ul>
                            */ ?>
                            <div id="formation-detail" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="display: block;">
                                <?php
                                $arrAllTags = array();
                                $arrAllTags = get_the_terms(get_the_ID(), 'formation_category', 'fields=names');
                                $arrTags = array();
                                if ($arrAllTags != "") {
                                    foreach ($arrAllTags as $tag) $arrTags[] = $tag->name;
                                }
                                $datedebut = rwmb_meta('formation_datedebut');
                                $datefin = rwmb_meta('formation_datefin');
                                ?>
                                <div class="wpb_row vc_row-fluid full-row">
                                    <div class="col-xs-6 formation-retour-liste">
                                        <a href="<?php echo home_url('/'); ?>prestations/former/sessions/"><i class="fa-chevron-left"></i> Retour à la liste</a>
                                    </div>
                                    <div class="col-xs-6 formation-reserver">
                                        <a href="#" class="lbp-inline-link-1"><i class="fa-chevron-right"></i> Pré-inscription</a>
                                        <div style="display: none;">
                                            <div id="lbp-inline-href-1" style="padding:10px; background: #fff;">
                                            	<?php echo do_shortcode(' [ajaxformation id="'.get_the_id().'"]' ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_row vc_row-fluid full-row row-formation">
                                    <div class="col-xs-4 col-md-2 formation-date">
                                        <span class="jour"><?php echo date("d", strtotime($datedebut)); ?></span>
                                        <span class="mois"><?php echo date("m", strtotime($datedebut)); ?></span>
                                    </div>
                                    <div class="col-xs-8 col-md-10 formation-item">
                                        <h4>
                                            <?php the_title() ?>
                                        </h4>
                            			<p class="formation-theme-lieu">
                                			<span class="formation-theme-label"><i class="fa fa-tag"></i> Thème</span> <?php echo implode(", ", $arrTags); ?>
                                			<br>
                                			<span class="formation-date-label"><i class="fa fa-calendar-o"></i> Date</span> 
                                			<?php
                                    			if ($datefin != "") {
                                        			if ($datedebut == $datefin) {
                                            			echo date("d/m/Y", strtotime($datedebut));
                                                    } else {
                                                        echo "Du ".date("d/m/Y", strtotime($datedebut))." au ".date("d/m/Y", strtotime($datefin));
                                                    }
                                                } else {
                                                    echo date("d/m/Y", strtotime($datedebut));
                                                }
                                            ?>
                                			<br>
                                			<span class="formation-contact-label"><i class="fa fa-envelope-o"></i> Contact</span> <?php echo rwmb_meta('formation_contact'); ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="wpb_row vc_row-fluid full-row row-formation">
                                    <div class="col-xs-12 formation-content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
  <?php 
  get_footer();
  ?>