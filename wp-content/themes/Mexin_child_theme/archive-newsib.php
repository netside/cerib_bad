<?php

get_header();
GLOBAL $webnus_options;
?>
<div class="breadcrumbs-w"><div class="container"><div id="crumbs"><a href="http://www.cerib.com">Accueil</a> &gt; <span class="current">News IB</span></div></div></div>
<section class="blox   bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
    <div class="max-overlay" style="background-color:"></div>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="container">
            <div class="vc_col-sm-3 wpb_column vc_column_container bloc-content-full without-carousel">
                <aside class="col-md-3 sidebar leftside custom-bloc-wiget">
                    <?php 
                    dynamic_sidebar( 'News IB' );
                    ?>
        		</aside>
            </div>
            <div class="vc_col-sm-9 wpb_column vc_column_container white-bg bloc-content-full">
                <div class="wpb_wrapper">
                    <h1><?php
    					if ( is_day() ) :
    						printf(''. __( 'Daily Archives', 'WEBNUS_TEXT_DOMAIN' ) . ' : <strong>%s</strong>', get_the_date() );
    					elseif ( is_month() ) :
    						printf(''. __( 'Monthly Archives', 'WEBNUS_TEXT_DOMAIN' ) . ' : <strong>%s</strong>', get_the_date( _x( 'F Y', 'monthly archives date format', 'WEBNUS_TEXT_DOMAIN' ) ) );
    					elseif ( is_year() ) :
    						printf(''. __( 'Yearly Archives', 'WEBNUS_TEXT_DOMAIN' ) .' : <strong>%s</strong>', get_the_date( _x( 'Y', 'yearly archives date format', 'WEBNUS_TEXT_DOMAIN' ) ) );
    						
    					elseif ( is_category() ):
    						printf(  '%s', single_cat_title( '', false ) );
    					elseif ( is_tag() ):
    						printf(''. __( 'Tag', 'WEBNUS_TEXT_DOMAIN' ) .' : <strong>%s</strong>', single_tag_title( '', false ) );
    
    					else :
    						echo "News IB";
    					endif;
    				?></h1>
                	<!-- begin-main-content -->
                    <section class="list-article">
                     <?php
                 	 if(have_posts()):
                		while( have_posts() ): the_post();
                			
                			if( 'both' == $webnus_options->webnus_blog_sidebar() )
                			{
                				get_template_part('parts/blogloop','bothsidebar');
                			}
                			else{
                				switch( $webnus_options->webnus_blog_template() )
                				{
                					case 1:
                						get_template_part('parts/blogloop');
                						break;
                					case 2:
                						get_template_part('parts/blogloop','type2');
                						break;
                					default:
                						get_template_part('parts/blogloop');
                						break;
                					
                					
                				}
                			}
                		endwhile;
                	 else:
                		get_template_part('blogloop-none');
                	 endif;
                	
                	 ?>
                       
                      <br class="clear">
                   
                	  <?php 
                		if(function_exists('wp_pagenavi'))
                		{
                			wp_pagenavi();
                		}
                	  ?>
                	  <hr class="vertical-space">
                    </section>
                    <!-- end-main-content -->
                </div>
            </div>
        </div>
    </div>
</section>
  <?php 
  get_footer();
  ?>