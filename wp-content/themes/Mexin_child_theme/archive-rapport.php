<?php

get_header();
GLOBAL $webnus_options;

# utilisateur
global $current_user;
get_currentuserinfo();
$roleUtilisateur = get_user_role();

# langue
$currentLang =  get_locale();
if ($currentLang == "fr_FR") {
    $lireLaSuite = "Lire la suite";
    $telecharger = "Télécharger";
    $keywordss = "Mots-clés :";
    $resultats = "Résultats de recherche pour :";
    $archives = "Archives pour :";
    $archivesA = "Archives pour l'année :";
    $par = "Par";
} else {
    $lireLaSuite = "See more";
    $telecharger = "Download";
    $keywordss = "Keywords:";
    $resultats = "Search results for:";
    $archives = "Archives for:";
    $archivesA = "Archives by Year for:";
    $par = "By";
}

# résultat de recherche de la ged
function getLstGed () {
    global $wpdb;
    
    include_once("inc/ged/verticrawl.class.php");
    
    if (isset($_GET["r"])) $_GET["q"] = $_GET["r"];
    
    $verticrawl = new verticrawl;
    $URL = $verticrawl->getResults();
    
	$results=array();
	for($i=1, $n=count($URL); $i<=$n; $i++){
		$row =& $URL[$i];
		
		$row['body']      = utf8_encode($row['body']);
		$row['url-title'] = utf8_encode($row['url-title']);
		$row['url']       = utf8_encode($row['url']);
		
		$result = new stdClass;
		$result->title    = $row['url-title'];
		$result->extract  = $row['body'];
		$result->url      = $row['url'];
		$results[] = $result;
	}
    $arrLstRapport = array();
	if (count($results)>0) {
    	foreach ($results as $result) {
    		$query  = " SELECT p.ID ";
    		$query .= " FROM pzebfiv_posts p, pzebfiv_postmeta pm ";
            $query .= " WHERE p.ID = pm.post_id AND p.post_status = 'publish' AND pm.meta_key = 'rapport_documentged' AND pm.meta_value = '".$result->url."' ";
    		$data = $wpdb->get_results($query);
    		
    		if ($data[0]->ID != "") $arrLstRapport[] = $data[0]->ID;
        }
    }
    return $arrLstRapport;
}

# résultat de recherche du post
function getLstSearch () {
    global $wpdb;
    
    $arrLstRapport = array();
    
	$query  = " SELECT p.ID ";
	$query .= " FROM pzebfiv_posts p ";
    $query .= " WHERE p.post_type = 'rapport' AND p.post_status = 'publish' AND (p.post_title LIKE '%".$_GET["r"]."%' OR p.post_content LIKE '%".$_GET["r"]."%') ";
	$data = $wpdb->get_results($query);
	foreach ($data as $val) 
	    if ($val->ID>0)
            $arrLstRapport[] = $val->ID;
            
    return $arrLstRapport;
}

# résultat de recherche des mots-clés
function getLstMotsCles () {
    global $wpdb;
    
    $arrLstRapport = array();
    
	$query  = " SELECT p.ID ";
	$query .= " FROM pzebfiv_posts p, pzebfiv_term_relationships r, pzebfiv_term_taxonomy tt, pzebfiv_terms t ";
    $query .= " WHERE p.ID = r.object_id ";
    $query .= "   AND p.post_status = 'publish' ";
    $query .= "   AND r.term_taxonomy_id = tt.term_taxonomy_id ";
    $query .= "   AND tt.taxonomy = 'rapport_tag' ";
    $query .= "   AND tt.term_id = t.term_id";
    $query .= "   AND t.name LIKE '%".$_GET["r"]."%' ";
	$data = $wpdb->get_results($query);
	foreach ($data as $val) 
	    if ($val->ID>0)
            $arrLstRapport[] = $val->ID;
            
    return $arrLstRapport;
}
?>
<?php if ($currentLang == "fr_FR") { ?>
<div class="breadcrumbs-w"><div class="container"><div id="crumbs"><a href="<?php echo home_url('/'); ?>">Accueil</a> &gt; <a href="#">Innovation</a> &gt; <span class="current">Études et Projets</span></div></div></div>
<?php } else { ?>  
<div class="breadcrumbs-w"><div class="container"><div id="crumbs"><a href="<?php echo home_url('/'); ?>">Home</a> &gt; <a href="#">Innovation</a> &gt; <span class="current">Development and Projects</span></div></div></div>
<?php } ?>
<div class="row-wrapper-x">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 
		</div> 
	</div> 
</div></section>
<section id="carousel-interne" class="wpb_row vc_row-fluid full-row">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">            
            <?php echo do_shortcode( '[layerslider id="24"]' ); ?>
		</div> 
	</div> 
</section>
<section class="blox   bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
    <div class="max-overlay" style="background-color:"></div>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="container">
            <div class="vc_col-sm-3 bloc-sidebar-left wpb_column vc_column_container">
                <aside class="col-md-3 sidebar leftside">
                    <?php 
                    dynamic_sidebar( 'Rapport' );
                    ?>
        		</aside>
            </div>
            <div class="vc_col-sm-9 wpb_column vc_column_container">
                <div class="wpb_wrapper">
                    <div class="wpb_tabs wpb_content_element  onglet-nombre-<?php echo ($currentLang == "fr_FR")?"4":"3" ?>" data-interval="0">
                        <div class="wpb_wrapper webnus-tabs wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">
                            <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                <?php if ($currentLang == "fr_FR") { ?>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-etudes-recherches" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/etudes-recherches/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Études &amp; Recherches</a></li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-2015" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/programme/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">Programme E&amp;R</a></li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-doctoral" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/etudes-et-projets/theses/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">Thèses</a></li>
                                <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tab-rapports-er" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true"><a href="http://www.cerib.com/rapport/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">Rapports E&amp;R</a></li>
                                <?php } else { ?>                                
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-etudes-recherches" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/development-and-projects/research-development/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Research & Development</a></li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-programme-2015" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="http://www.cerib.com/innovation/development-and-projects/rd-programme/" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">R&D programme</a></li>
                                <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tab-rapports-er" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true"><a href="http://www.cerib.com/rapport/?lang=en" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">R&D reports</a></li>
                                <?php } ?>
                            </ul>
                            <div id="tab-rapports-er" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="false" style="display: block;">
                                <?php
                                # si recherche
                                if (isset($_GET["r"]) && $_GET["r"] != "") {
                                    ?>
                                    <h2><?php echo $resultats; ?> <strong><?php echo $_GET["r"]; ?></strong></h2>
                                    <?php
                                }
            					if ( is_month() ) {
            						?>
                                    <h2><?php echo $archives; ?> <strong><?php echo get_the_date( _x( 'F Y', 'monthly archives date format', 'WEBNUS_TEXT_DOMAIN' ) ); ?></strong></h2>
            						<?php
                				}
            					if ( is_year() ) {
            						?>
                                    <h2><?php echo $archivesA; ?> <strong><?php echo get_the_date( _x( 'Y', 'yearly archives date format', 'WEBNUS_TEXT_DOMAIN' ) ); ?></strong></h2>
            						<?php
                				}
                				
                				# ged
                				$arrLstGed = array();
                                $arrLstSearch = array();
                                $arrLstKeyword = array();
                                $arrLstID = array();
                				if (isset($_GET["r"]) && $_GET["r"] != "") {
                    				# ged
                                    $arrLstGed = getLstGed(); #print_r($arrLstGed);
                                    # search
                                    $arrLstSearch = getLstSearch(); #print_r($arrLstSearch);
                                    # mots-clés
                                    $arrLstKeyword = getLstMotsCles(); #print_r($arrLstKeyword);
                                    # combine unique
                                    $arrLstID = array_unique(array_merge($arrLstGed, $arrLstSearch)); #print_r($arrLstID);
                                    $arrLstID = array_unique(array_merge($arrLstID, $arrLstKeyword)); #print_r($arrLstID);
                                    
                                    if (count($arrLstID)>0) {
                                        $args = array( 
                                            'post_type' => 'rapport', 
                                            'post__in' => $arrLstID, 
                                            'posts_per_page' => 10, 
                                            'paged' => $paged,
                                        );
                                    }
                                } else if ( is_month() ) {
                                    $args = array( 
                                        'post_type' => 'rapport', 
                                        'posts_per_page' => 10, 
                                        'paged' => $paged,
                                    	'date_query' => array(
                                    		array(
                                    			'year'  => get_the_date("Y"),
                                    			'month' => get_the_date("m"),
                                    		),
                                    	),
                                    );
                                } else if ( is_year() ) {
                                    $args = array( 
                                        'post_type' => 'rapport', 
                                        'posts_per_page' => -1, 
                                    	'date_query' => array(
                                    		array(
                                    			'year'  => get_the_date("Y"),
                                    		),
                                    	),
                                    );
                                } else {
                                    $args = array( 
                                        'post_type' => 'rapport', 
                                        'posts_per_page' => 10, 
                                        'paged' => $paged,
                                    );
                                }
                                
                                # liste des rapports
                                if ($args == "") {
                                    ?>
                                    Aucun rapport trouvé.
                                    <?php
                                } else {
                                    $loop = new WP_Query( $args );
                                    while ( $loop->have_posts() ) : $loop->the_post();
                                    
                                		# accès extranet
                                        $acces = 1;
                                        $extranet = rwmb_meta('rapport_extranet');
                                        if ($extranet == 0) {
                                            $acces = 1;
                                        } else if ( @$extranet == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
                                            $acces = 1;
                                        } else {
                                            $acces = 0;
                                        }
                                        
                                        if ($acces == 1) {
                                    		# auteur
                                            $arrAllAut = get_the_terms(get_the_ID(), 'rapport_auteur', 'fields=names');
                                            $arrAut = array();
                                            if ($arrAllAut[0]->name != "") foreach ($arrAllAut as $aut) $arrAut[] = $aut->name;
                                            # numéro
                                            $arrAllNum = get_the_terms(get_the_ID(), 'rapport_numero', 'fields=names');
                                            $arrNum = array();
                                            if ($arrAllNum[0]->name != "") foreach ($arrAllNum as $num) $arrNum[] = $num->name;
                                            # tags
                                            $arrAllTags = get_the_terms(get_the_ID(), 'rapport_tag', 'fields=names');
                                            $arrTags = array();
                                            if ($arrAllTags[0]->name != "") foreach ($arrAllTags as $tag) $arrTags[] = $tag->name;
                                            ?>
                                            <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post rapport-list'); ?>>
                                                <div class="wpb_row vc_row-fluid full-row">
                                                    <div class="col-md-12">
                                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
                                                    </div>
                                                </div>
                                                <div class="wpb_row vc_row-fluid full-row">
                                                    <div class="col-md-2">
                                                        <img src="http://admin.verticrawl.com/search/screenshot.php?DATABASE=3f4e781dbe09a35fcfbfb0c08de85684&amp;url=<?php echo rwmb_meta('rapport_documentged'); ?>">
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="wpb_row vc_row-fluid full-row">
                                                            <div class="col-md-12">
                                                    			<h6 class="blog-time">
                                                        			<?php if ($currentLang == "fr_FR") { ?>
                                                        			    <?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?> 
                                                        			 <?php } else { ?>
                                                        			    <?php the_time('m') ?>/<?php the_time('d') ?>/<?php the_time('Y') ?> 
                                                        			 <?php } ?>
                                                                </h6>
                                                    			<?php if ($arrAllNum[0]->name != "") { ?><h6 class="blog-number"><strong>réf. </strong> <?php echo implode(", ", $arrNum); ?> </h6><?php } ?>
                                                    			<?php if ($arrAllAut[0]->name != "") { ?><h6 class="blog-author"><strong><?php echo $par; ?> </strong> <?php echo implode(", ", $arrAut); ?> </h6><?php } ?>
                                                    			<p>
                                                                <?php echo mb_substr( strip_tags( preg_replace( '/\[[^\]]+\]/', '', get_the_content()) ), 0, 150, "UTF-8" ).'&hellip;'; ?>
                                                    			</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_row vc_row-fluid full-row">
                                                    <div class="col-md-6 mots-cles"><?php if ($arrAllTags[0]->name != "") { ?><strong><?php echo $keywordss; ?></strong> <?php echo implode(", ", $arrTags); ?><?php } ?></div>
                                                    <div class="col-md-6 lire-suite">
                                                        <ul>
                                                            <li><a href="<?php the_permalink(); ?>"><?php echo $lireLaSuite; ?> <i class="fa-chevron-right"></i></a></li>
                                                            <li><a href="<?php echo do_shortcode('[download url="'.rwmb_meta('rapport_documentged').'"]'); ?>" target="_blank"><?php echo $telecharger; ?> <i class="fa-chevron-right"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </article>
                                            <?php
                                        }
                                    endwhile;
                                    ?>
                                    <br class="clear">
                                    <?php 
                                    if(function_exists('wp_pagenavi')) wp_pagenavi( array( 'query' => $loop ) );
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
  <?php 
  get_footer();
  ?>