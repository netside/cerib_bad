<?php
$post_type = get_post_type( $post );
$icone = "";
switch ($post_type) {
    case "post":
        $icone = '<i class="fa fa-newspaper-o"></i>';
    break;
    case "page":
        $icone = '<i class="fa fa-file-text-o"></i>';
    break;
    case "rapport":
        $icone = '<i class="fa fa-bar-chart"></i>';
    break;
    case "job_listing":
        $icone = '<i class="fa fa-briefcase"></i>';
    break;
    case "evenements":
        $icone = '<i class="fa fa-calendar"></i>';
    break;
    case "formation":
        $icone = '<i class="fa fa-graduation-cap"></i>';
    break;
    case "espace-presse":
        $icone = '<i class="fa fa-newspaper-o"></i>';
    break;
    case "marquage":
        $icone = '<i class="fa fa-certificate"></i>';
    break;
    case "certification":
        $icone = '<i class="fa fa-certificate"></i>';
    break;
    default:
        $icone = '<i class="fa fa-newspaper-o"></i>';
    break;
}

# vérification accès extranet pour rapport
global $current_user;
get_currentuserinfo();
$roleUtilisateur = get_user_role();

# langue
$currentLang =  get_locale();
if ($currentLang == "fr_FR") {
    $lireSuite = "Lire la suite";
} else {
    $lireSuite = "Read more";
}

$acces = 1;
$extranet = rwmb_meta('rapport_extranet');
if ($extranet == 0) {
    $acces = 1;
} else if ( @$extranet == 1 && ($roleUtilisateur == "administrator" || $roleUtilisateur == "cerib" || $roleUtilisateur == "apegib" || $roleUtilisateur == "extranet") ) {
    $acces = 1;
} else {
    $acces = 0;
}

if ($acces == 1) {
    ?>
    <article id="post-<?php the_ID(); ?>" class="blog-post"> 
    <?php
    
    global $webnus_options;
    
    ?>
    	<?php /*if( 1 == $webnus_options->webnus_blog_meta_date_enable() ) { ?>
    	<div class="col-md-2 alpha">
    	  <div class="blog-date-sec">
    		<h3><?php the_time('d') ?></h3>
    		<span><?php the_time('M Y') ?></span> </div>
    	</div>
    	<?php }*/ ?>
    	<div class="col-md-12 omega">
    
    	  <h4><a href="<?php the_permalink(); ?>"><?php echo $icone; ?> <?php the_title() ?></a></h4>
    	 <p>
            <?php
                // si on utilise le résumé alors on l'affiche
                if( get_the_excerpt() != '' ) {
                    echo mb_substr( get_the_excerpt(), 0, 200, "UTF-8" ).'&hellip;';
                    ?>
                    <div class="lire-suite">
                        <ul>
                            <li><a href="<?php the_permalink(); ?>"><?php echo $lireSuite; ?> <i class="fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                    <?php
                // sinon on le crée à partir du contenu de l'article
                } else {
                    $content = get_the_content();
                    if ($content != "") {
                        echo mb_substr( strip_tags( preg_replace( '/\[[^\]]+\]/', '', get_the_content()) ), 0, 200, "UTF-8" ).'&hellip;' ;
                    }
                    ?>
                    <div class="lire-suite">
                        <ul>
                            <li><a href="<?php the_permalink(); ?>"><?php echo $lireSuite; ?> <i class="fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                    <?php
                }
             ?>
    	  </p>
    	  </div>
    	<hr class="vertical-space">
    </article>
    <?php
}
?>