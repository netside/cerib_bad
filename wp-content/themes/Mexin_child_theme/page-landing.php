<?php

/**
 * Template Name: Landing Page
 */

wp_enqueue_style('landing');
wp_enqueue_script('landing_modal');
wp_enqueue_script('landing');

get_header('landing');

$fields = get_fields();

?>
<div id="wrap" class="landing">
<div class="container">
	<header class="landing-header">
		<a href="<?php echo home_url(); ?>" class="landing-header__logo">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cerib.png" alt="Cerib">
		</a>
		<h1 class="landing-header__baseline">
			Prévenir, évaluer, Modéliser, Diagnostiquer et Former
			<span>Nous vous accompagnons dans vos besoins en Métrologie</span>
		</h1>
	</header>
</div>

<div class="landing-cover" style="background-image: url('<?php echo $fields['landing_cover']; ?>');">
	<section class="container" >
		<div class="landing-form">
			<?php echo do_shortcode('[contact-form-7 id="7439" title="Formulaire landing"]'); ?>
		</div>
	</section>
</div>

<?php if ( $fields['landing_info'] ): ?>
<section class="landing-info container">
	<h2 class="landing-info__title">Le CERIB c'est</h2>
	<div class="landing-info__bloc">
		<?php foreach ($fields['landing_info'] as $landing_info): ?>
		<div class="landing-info__bloc__item">
			<div class="landing-info__bloc__inner" style="background-image: url('<?php echo $landing_info['landing_info_pic']; ?>')">
				<p class="landing-info__bloc__title"><?php echo $landing_info['landing_info_title']; ?></p>
				<div class="landing-info__bloc__caption">
					<div class="landing-info__bloc__caption__inner">
						<?php echo $landing_info['landing_info_desc']; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>

<?php if( $fields['landing_content'] ): ?>
<section class="landing-content container">
	<?php foreach( $fields['landing_content'] as $landing_content ): ?>
	<div class="landing-content__table">
		<div class="landing-content__table__cell pic" style="background-image: url('<?php echo $landing_content['landing_info_pic']; ?>');"></div>
		<div class="landing-content__table__cell<?php echo ($landing_content['citation']) ? ' quotes' : ''; ?>">
			<?php echo $landing_content['landing_info_content']; ?>
		</div>
	</div>
	<?php endforeach; ?>
	<div class="landing-content__button">
		<span class="landing-button" id="scrollToForm">Contactez-nous</span>
	</div>
</section>
<?php endif; ?>

<div class="landing-modal" id="bio_ep">
	<div class="landing-modal__inner">
		<div class="landing-modal__header">
			<h3 class="landing-modal__title">Besoin de plus d'information ? Visitez le site du CERIB !</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
		</div>
		<div class="landing-modal__content">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/landing/landing-preview-website.jpg" alt="Voir le site CERIB">
			<a class="landing-button" href="<?php echo home_url(); ?>">Visitez notre site web </a>
		</div>
	</div>
</div>

<?php

get_footer();
