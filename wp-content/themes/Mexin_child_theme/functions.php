<?php
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
error_reporting(0);


# thème Mexin
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 15 );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
	//DTA - Font Awesome
	wp_enqueue_style( 'font-awesome-style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' );
}

// Déclaration des styles pour la landing
add_action('init', function(){
	wp_register_style('landing', get_stylesheet_directory_uri() . '/css/landing.css', '', '1.0.0', 'all');
	wp_register_script( 'landing_modal', get_stylesheet_directory_uri() . '/js/bioep.js', '', '1.0.0', true );
	wp_register_script( 'landing', get_stylesheet_directory_uri() . '/js/landing.js', '', '1.0.0', true );
});


/**
 * Chargement des configs ACF contenues dans le dossier THEME/inc/acf_fields
 * --------------------------------------------
 *  => Les fichiers dont le nom commence pas un '_' sont ignorés
 */

$folders = array('inc/acf_fields');

foreach( $folders as $folder ){
	foreach( scandir(dirname(__FILE__) . '/' . $folder) as $file ){
		if( in_array($file, array('.', '..')) || $file[0] == '_' ){
			continue;
		}

		$path = dirname(__FILE__) . '/' . $folder . '/' . $file;

		if( is_file( $path ) ){
			require_once($path);
		}
	}
}



# admin - profile
require_once("inc/profile-inc.php");

# front - affiche Google Maps itinéraire
require_once("inc/itineraire-inc.php");

# admin - connexion à la GED
require_once("inc/ged/ged_db.class.php");

# admin - custom post type : presse
require_once("inc/espace-presse-custom-inc.php");

# admin - custom post type : rapports
require_once("inc/rapports-custom-inc.php");

# admin - custom meta boxes : docs & liens
require_once("inc/docs-liens-meta-boxes-inc.php");

# admin - custom post type : évènements
require_once("inc/evenements-custom-inc.php");

# admin - custom post type : formation
require_once("inc/formations-custom-inc.php");

# admin - custom post type : équipe
require_once("inc/equipe-custom-inc.php");

# admin - espace client
require_once("inc/espace-client-inc.php");

# admin - recherche
require_once("inc/recherche-inc.php");

# admin - custom styles
require_once("inc/custom-styles-inc.php");

# admin - postes
require_once("inc/postes-custom-inc.php");

# admin - newsletter
require_once("inc/newsletters-custom-inc.php");

# admin - news IB
require_once("inc/newsib-custom-inc.php");

# front - lance les shortcodes dans les widgets texte
add_filter('widget_text', 'do_shortcode');

# front - affiche la left sidebar
require_once("inc/sidebar-inc.php");

# front - formulaire d'inscription pour évènement JEC
require_once("inc/jec-inscription-inc.php");

# admin - place la box yoast en dernier
function yoast_bottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoast_bottom');

# admin - formate le nom des fichiers uploadés
add_filter( 'sanitize_file_name', 'remove_accents' );

# admin - enlève le numéro de version
remove_action("wp_head", "wp_generator");
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

# RSS
function myfeed_request($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'espace-presse', 'evenements', 'formation', 'job_listing');
	return $qv;
}
add_filter('request', 'myfeed_request');

# protection brute force multicall
function mmx_remove_xmlrpc_methods( $methods ) {
unset( $methods['system.multicall'] );
return $methods;
}
add_filter( 'xmlrpc_methods', 'mmx_remove_xmlrpc_methods');

# admin - custom post type : demo group
#require_once("inc/demo-group.php");

# admin - custom post type : demo
#require_once("inc/demo.php");

// remove notifications in CSS
add_action('admin_head', 'remove_update_on_css');

function remove_update_on_css() {
  echo '<style>.update-count,.plugin-count,.update-nag,#wp-admin-bar-updates{display:none!important}</style>';
}
