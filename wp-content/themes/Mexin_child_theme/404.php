<?php
get_header();
GLOBAL $webnus_options;
?>
<section class="blox   bloc-content" style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; "><div class="max-overlay" style="background-color:"></div><div class="wpb_row vc_row-fluid full-row"><div class="container">
	<div class="vc_col-sm-12 bloc-content-full wpb_column vc_column_container">
		<div class="wpb_wrapper">
            
            <div class="wpb_text_column wpb_content_element">
                <div class="wpb_wrapper">
                    <h2><?php _e('404','WEBNUS_TEXT_DOMAIN'); ?> - <?php _e('Page Not Found','WEBNUS_TEXT_DOMAIN'); ?></h2>
                    
                    <?php echo $webnus_options->webnus_404_text(); ?> 
                    Rechercher sur le site : &nbsp;&nbsp;<?php get_search_form(); ?>
                    <br class="clear">
                </div> 
            </div> 
        </div>
    </div>
</section>

<?php get_footer(); ?>