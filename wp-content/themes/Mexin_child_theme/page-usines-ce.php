<?php

if(!isset($_GET["idCertif"]) && empty($_GET["idCertif"]))
{
	wp_redirect(home_url());
	exit;
}
else
{
	global $wpdb;
	global $post;
			
	$idFamilleCertif = $_GET['idCertif'];
	
	$preparedQuery = $wpdb->prepare("SELECT id FROM marquagece_certif WHERE id_famille_produit = %d", $idFamilleCertif);
	
	$idCertif = $wpdb->get_row($preparedQuery);
	
	$idCertif = $idCertif->id;
			
	$tablePostTypeIdName = $wpdb->prefix . "uepc_post_type_id";
	$preparedQuery = $wpdb->prepare("SELECT id_post_type FROM $tablePostTypeIdName WHERE type_produit = 'ce' AND id_certif = '%d'", $idFamilleCertif);
	$idPost = $wpdb->get_row($preparedQuery);
	
	if(is_null($idPost))
	{
		wp_redirect(home_url());
		exit();
	}
}

get_header();

$pdfDir = WP_CONTENT_URL . "/pdf/";
$pdfDirFile = WP_CONTENT_DIR . "/pdf/";

GLOBAL $webnus_options;

$last_time = get_the_time(' F Y');


GLOBAL $webnus_page_options_meta;


$show_titlebar = null;
$titlebar_bg = null;
$titlebar_fg = null;
$have_sidebar = false;
$sidebar_pos = null;

$meta = $webnus_page_options_meta->the_meta();

if(!empty($meta)){
$show_titlebar =  isset($meta['webnus_page_options'][0]['show_page_title_bar'])?$meta['webnus_page_options'][0]['show_page_title_bar']:null;
$titlebar_bg =  isset($meta['webnus_page_options'][0]['title_background_color'])?$meta['webnus_page_options'][0]['title_background_color']:null;
$titlebar_fg =  isset($meta['webnus_page_options'][0]['title_text_color'])?$meta['webnus_page_options'][0]['title_text_color']:null;
$titlebar_fs =  isset($meta['webnus_page_options'][0]['title_font_size'])?$meta['webnus_page_options'][0]['title_font_size']:null;
$sidebar_pos =  isset($meta['webnus_page_options'][0]['sidebar_position'])?$meta['webnus_page_options'][0]['sidebar_position']:'right';
$have_sidebar = !( 'none' == $sidebar_pos )? true : false;

}
/*
if('hide' != $show_titlebar):
?>
<section id="headline" style="<?php if(!empty($titlebar_bg)) echo ' background-color:'.$titlebar_bg.';'; ?>">
    <div class="container">
      <h3 style="<?php if(!empty($titlebar_fg)) echo ' color:'.$titlebar_fg.';'; if(!empty($titlebar_fs)) echo ' font-size:'.$titlebar_fs.';';  ?>"><?php the_title(); ?></h3>
    </div>
</section>
<?php if( 1 == $webnus_options->webnus_enable_breadcrumbs() ) { ?>
      <div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<?php } ?>
<?php
endif;
*/
?>
<?php if (!is_front_page()): ?>
      <!--<div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>-->
<?php endif; ?>
<?php if (!is_front_page()): ?>
<section id="main-content" class="container interne-cerfif">
<?php else: ?>
<section id="main-content" class="container">
<?php endif; ?>
<!-- Start Page Content -->
<?php
/*
	LEFT SIDEBAR
*/

if( ('left' == $sidebar_pos) || ('both' == $sidebar_pos ) ) get_sidebar('left');
/*
if( $have_sidebar ) {
?>
<section class="<?php  echo('both' == $sidebar_pos )?'col-md-6 cntt-w':'col-md-9 cntt-w'; ?>">
	<article>
	
<?php 
}
*/				 


	$args = array('post_type' => 'marquage', 'p' => $idPost->id_post_type);
	$query = new WP_Query($args);
	
	echo '<div class="row-wrapper-x">';
		if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post();
		
			
		
			echo '<div id="certif-title"><h3>';
			echo 'Liste des Usines - '. $post->post_title;
			echo '</h3></div>';
			
			echo '<ul id="nb-usines-liste">';
	?>
			<li><a href="<?php the_permalink(); ?>">
			
	<?php
			echo '<div class="nb-menu">';
			echo '<i class="fa fa-2x fa-chevron-circle-left"></i> Retour aux caractéristiques';
			echo '</div>';
			echo '</a></li>';
			echo '</ul>';
	
	//Liste Usines
	
	$sSQLSiege = "SELECT siege.* FROM 
				marquagece_etablissements as usine, 
				marquagece_entreprises as siege, 
				marquagece_nn_etablissements_produits_certif as nnepc 
				WHERE 
				nnepc.id_certif = '%d' AND 
				nnepc.id_etablissements = usine.id AND 
				usine.id_entreprises = siege.id 
				ORDER BY usine.cp, usine.id";
				
	$sSQLUsine = "SELECT usine.*, nnepc.id_produits, nnepc.filename FROM 
				marquagece_etablissements as usine, 
				marquagece_entreprises as siege, 
				marquagece_nn_etablissements_produits_certif as nnepc 
				WHERE 
				nnepc.id_certif = '%d' AND 
				nnepc.id_etablissements = usine.id AND 
				usine.id_entreprises = siege.id 
				ORDER BY usine.cp, usine.id";
	
	$wpdb->show_errors();
	$preparedQuery = $wpdb->prepare($sSQLSiege, $idCertif);
	$listSieges = $wpdb->get_results($preparedQuery);
	
	$preparedQuery = $wpdb->prepare($sSQLUsine, $idCertif);
	$listUsines = $wpdb->get_results($preparedQuery);
	
	/*for($i = 0; $i < count($listUsines); $i++)
	{
		var_dump($listUsines[$i]);
		echo '<br>';
		var_dump($listSieges[$i]);
		echo '<hr>';
	}*/
	
	//Liste Département
	$arrayDpt = array();
	$arrayPays = array();
	for($i = 0; $i < count($listUsines); $i++)
	{	
		$cp = $listUsines[$i]->cp;
		$pays = $listUsines[$i]->pays;
		
		if(trim(strtolower($pays)) == "france")
		{
			$dpt = substr($listUsines[$i]->cp, 0, 2);
			$listUsines[$i]->dpt = $dpt;
			
			if(!in_array($dpt, $arrayDpt))
				array_push($arrayDpt, $dpt);
		}
		else
		{
			$pays = trim($listUsines[$i]->pays);
			$listUsines[$i]->dpt = $pays;
			
			if(!in_array($pays, $arrayPays))
				array_push($arrayPays, $pays);
		}
	}
	$arrayFilter = array_merge($arrayDpt, $arrayPays);
	
	?>
	
		<div class="filter">
			<div class="small-12">
			<table id="formDept" class="filter-list">
				<tr>
				<td>Filtrer les résultats par département : </td>
				<td>
					<select id="dptFilterCE">
						<option value="all">Tous les départements</option>
						<?php for($d = 0; $d < count($arrayFilter); $d++): ?>
						<option value="<?php echo $arrayFilter[$d]; ?>"><?php echo $arrayFilter[$d]; ?></option>
						<?php endfor; ?>
					</select>
				</td>
				</tr>
			</table>
			</div>
		</div>
		
		<div id="usine-list">
			<table id="table-usine">
				<tr>
					<th class="small-1">Departement</th>
					<th class="small-5">Usine</th>
					<th class="small-5">Siège</th>
					<th class="small-1">Certificat</th>
				</tr>
				<?php for($i = 0; $i < count($listUsines); $i++): 
				
					$listUsines[$i]->adr_2 = trim($listUsines[$i]->adr_2);
					$listUsines[$i]->adr_3 = trim($listUsines[$i]->adr_3);
					$listUsines[$i]->tel   = trim($listUsines[$i]->tel);
					$listUsines[$i]->fax   = trim($listUsines[$i]->fax);
				
					$listSieges[$i]->adr_2 = trim($listSieges[$i]->adr_2);
					$listSieges[$i]->adr_3 = trim($listSieges[$i]->adr_3);
					$listSieges[$i]->tel   = trim($listSieges[$i]->tel);
					$listSieges[$i]->fax   = trim($listSieges[$i]->fax);
				?>
				<tr class="usine-info" data-dpt="<?php echo $listUsines[$i]->dpt; ?>">
					<td><?php echo $listUsines[$i]->dpt; ?></td>
					<td><h5 class="usine-name"><?php echo $listUsines[$i]->nom ?></h5><p><?php echo $listUsines[$i]->adr_1; ?><br><?php if(!empty($listUsines[$i]->adr_2)) echo $listUsines[$i]->adr_2.'<br>'; ?><?php if(!empty($listUsines[$i]->adr_3)) echo $listUsines[$i]->adr_3.'<br>'; ?><?php echo $listUsines[$i]->cp; ?> - <?php echo $listUsines[$i]->ville; ?><br><?php echo $listUsines[$i]->pays; ?><br><?php if(!empty($listUsines[$i]->tel)) echo 'TEL : '.$listUsines[$i]->tel.'<br>'; ?><?php if(!empty($listUsines[$i]->fax)) echo 'FAX : '.$listUsines[$i]->fax; ?></p></td>
					<td><h5><?php echo $listSieges[$i]->nom ?></h5><p><?php echo $listSieges[$i]->adr_1; ?><br><?php if(!empty($listSieges[$i]->adr_2)) echo $listSieges[$i]->adr_2.'<br>'; ?><?php if(!empty($listSieges[$i]->adr_3)) echo $listSieges[$i]->adr_3.'<br>'; ?><?php echo $listSieges[$i]->cp; ?> - <?php echo $listSieges[$i]->ville; ?><br><?php echo $listSieges[$i]->pays; ?><br><?php if(!empty($listSieges[$i]->tel)) echo 'TEL : '.$listSieges[$i]->tel.'<br>'; ?><?php if(!empty($listSieges[$i]->fax)) echo 'FAX : '.$listSieges[$i]->fax; ?></p></td>
					<td><?php if(!empty($listUsines[$i]->filename) && file_exists($pdfDirFile.'ce/'.$listUsines[$i]->filename)): ?><a href="<?php echo $pdfDir.'ce/'.$listUsines[$i]->filename; ?>" target="_blank"><i class="fa fa-2x fa-file-pdf-o"></i><?php else: echo '&nbsp;'; endif; ?></td>
				</tr>
				<?php endfor; ?>
			</table>
		</div>
	<?php
			
			endwhile;
			endif;
	echo '</div>';
		
	wp_link_pages();
	if ( comments_open() && ( $webnus_options->webnus_allow_comments_on_page() == 1) )

	comments_template();


/*
		  if( $have_sidebar ) {
?>
	</article>
</section>	

<?php
}
*/
/*
	RIGHT SIDEBAR
*/

if( ('right' == $sidebar_pos) || ('both' == $sidebar_pos) ) get_sidebar('right');

?>

</section>

<?php get_footer(); ?>