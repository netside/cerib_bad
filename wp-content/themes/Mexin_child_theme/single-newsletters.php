 <?php
/******************/
/**  Single Post
/******************/

get_header();
if (is_single()){
	global $blogpost_meta;
	$post_meta = $blogpost_meta->the_meta();
	if(!empty($post_meta)){
		if($post_meta['style_type']=="postshow1" && $thumbnail_id = get_post_thumbnail_id()){
			$background = wp_get_attachment_image_src( $thumbnail_id, 'full' ); ?>
			<div class="postshow1" style="background-image: url(<?php echo $background[0]; ?> );">
				<div class="postshow-overlay"></div>
				<div class="container"><h1 class="post-title-ps1"><?php the_title() ?></h1></div>
			</div> <?php
		}
	}
} ?>
<div class="breadcrumbs-w"><div class="container"><?php if('webnus_breadcrumbs') webnus_breadcrumbs(); ?></div></div>
<section class="blox bloc-content " style=" padding-top:px; padding-bottom:px; background-size: cover; min-height:px; ">
    <div class="max-overlay" style="background-color:"></div>
    <div class="wpb_row vc_row-fluid full-row">
        <div class="container">
            <div class="vc_col-md-3 wpb_column vc_column_container bloc-content-full without-carousel">
                <aside class="col-md-3 sidebar leftside custom-bloc-wiget">
                    <?php 
                    dynamic_sidebar( 'Newsletters' );
                    ?>
            		<?php if( 1 == $webnus_options->webnus_blog_social_share() ) { ?>	
            		<div class="widget widget-full">
                		<h4 class="subtitle">Partager</h4>
                		<?php
                        $linkS = urlencode(get_the_permalink());
                        $titleS = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
                        ?>
                		<ul class="social-share-icons">
                    		<li><a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $linkS ?>&amp;t=<?php echo $titleS ?>" target="blank"><i class="fa-facebook"></i></a></li>
                    		<li><a class="google" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php echo $linkS ?>" target="_blank"><i class="fa-google"></i></a></li>
                    		<li><a class="twitter" href="https://twitter.com/intent/tweet?original_referer=<?php echo $linkS ?>&amp;text=<?php echo $titleS ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?><?php echo isset( $twitter_user ) ? '&amp;via='.$twitter_user : ''; ?>" target="_blank"><i class="fa-twitter"></i></a></li>
                    		<li><a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $linkS ?>&amp;title=<?php echo $titleS ?>&amp;source=<?php bloginfo( 'name' ); ?>" target="blank"><i class="fa-linkedin"></i></a></li>
                    		<li><a class="email" href="mailto:?subject=<?php echo get_the_title() ?>&amp;body=<?php echo $linkS ?>"><i class="fa-envelope"></i></a></li>
                		</ul>
            		</div>
            		<?php } ?>
        		</aside>
            </div>
            <div class="vc_col-md-9 wpb_column vc_column_container white-bg bloc-content-full">
                <div class="wpb_wrapper">

                		<?php if( have_posts() ): while( have_posts() ): the_post();  ?>
                      <article class="blog-single-post">
                	  
                		<?php
                			
                			setViews(get_the_ID());
                			GLOBAL $webnus_options;
                			
                			
                		$post_format = get_post_format(get_the_ID());
                	
                		$content = get_the_content(); ?>
                		<div class="post-trait-w"> <?php
                		if(!isset($background)) { ?>
                		 <h1><?php the_title() ?></h1> 
                	  <?php if( 1 == $webnus_options->webnus_blog_meta_date_enable() ) { ?>
                		<h6 class="blog-date"><?php the_date() ?></h6>
                		<?php } ?><?php
                		}
                	if(  $webnus_options->webnus_blog_sinlge_featuredimage_enable() && !isset($background) ){
                	
                		global $featured_video;
                		
                		$meta_video = $featured_video->the_meta();
                		
                		if( 'video'  == $post_format || 'audio'  == $post_format)
                		{
                			
                		$pattern =
                		  '\\['                              // Opening bracket
                		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                		. "(video|audio)"                     // 2: Shortcode name
                		. '(?![\\w-])'                       // Not followed by word character or hyphen
                		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                		.     '(?:'
                		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                		.     ')*?'
                		. ')'
                		. '(?:'
                		.     '(\\/)'                        // 4: Self closing tag ...
                		.     '\\]'                          // ... and closing bracket
                		. '|'
                		.     '\\]'                          // Closing bracket
                		.     '(?:'
                		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                		.             '[^\\[]*+'             // Not an opening bracket
                		.             '(?:'
                		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                		.                 '[^\\[]*+'         // Not an opening bracket
                		.             ')*+'
                		.         ')'
                		.         '\\[\\/\\2\\]'             // Closing shortcode tag
                		.     ')?'
                		. ')'
                		. '(\\]?)';  			
                		
                			
                			preg_match('/'.$pattern.'/s', $post->post_content, $matches);
                			
                			
                			if( (is_array($matches)) && (isset($matches[3])) && ( ($matches[2] == 'video') || ('audio'  == $post_format)) && (isset($matches[2])))
                			{
                				$video = $matches[0];
                				
                				echo do_shortcode($video);
                				
                				$content = preg_replace('/'.$pattern.'/s', '', $content);
                				
                			}else				
                			if( (!empty( $meta_video )) && (!empty($meta_video['the_post_video'])) )
                			{
                				echo do_shortcode($meta_video['the_post_video']);
                			}
                				
                	
                			
                			
                		}else
                		if( 'gallery'  == $post_format)
                		{
                			
                						
                			$pattern =
                		  '\\['                              // Opening bracket
                		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                		. "(gallery)"                     // 2: Shortcode name
                		. '(?![\\w-])'                       // Not followed by word character or hyphen
                		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                		.     '(?:'
                		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                		.     ')*?'
                		. ')'
                		. '(?:'
                		.     '(\\/)'                        // 4: Self closing tag ...
                		.     '\\]'                          // ... and closing bracket
                		. '|'
                		.     '\\]'                          // Closing bracket
                		.     '(?:'
                		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                		.             '[^\\[]*+'             // Not an opening bracket
                		.             '(?:'
                		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                		.                 '[^\\[]*+'         // Not an opening bracket
                		.             ')*+'
                		.         ')'
                		.         '\\[\\/\\2\\]'             // Closing shortcode tag
                		.     ')?'
                		. ')'
                		. '(\\]?)';  			
                
                			preg_match('/'.$pattern.'/s', $post->post_content, $matches);
                			
                			
                			if( (is_array($matches)) && (isset($matches[3])) && ($matches[2] == 'gallery') && (isset($matches[2])))
                			{
                				
                				$atts = shortcode_parse_atts($matches[3]);
                				
                				$ids = $gallery_type = '';
                				
                				if(isset($atts['ids']))
                				{
                					$ids = $atts['ids'];
                				}
                				if(isset($atts['webnus_gallery_type']))
                				{
                					$gallery_type = $atts['webnus_gallery_type'];
                				}
                
                					echo do_shortcode('[vc_gallery img_size= "full" type="flexslider_fade" interval="3" images="'.$ids.'" onclick="link_image" custom_links_target="_self"]');
                				
                				$content = preg_replace('/'.$pattern.'/s', '', $content);
                			}
                				
                	
                			
                			
                		}else
                		if( (!empty( $meta_video )) && (!empty($meta_video['the_post_video'])) )
                		{
                			echo do_shortcode($meta_video['the_post_video']);
                		}
                		else
                			get_the_image( array( 'meta_key' => array( 'Full', 'Full' ), 'size' => 'Full' ) ); 
                	}
                			
                		?>
                
                		</div>
                	    <div <?php post_class('post'); ?>>
                          
                           <div class="postmetadata">
                
                		<?php if( 1 == $webnus_options->webnus_blog_meta_gravatar_enable() ) { ?>	
                		<div class="au-avatar"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 90 ); ?></div>
                		<?php } ?>
                		<?php if( 1 == $webnus_options->webnus_blog_meta_author_enable() ) { ?>	
                		<h6 class="blog-author"><strong><?php _e('by','WEBNUS_TEXT_DOMAIN'); ?></strong> <?php the_author(); ?> </h6>
                		<?php } ?>
                		<?php if( 1 == $webnus_options->webnus_blog_meta_category_enable() ) { ?>
                		<h6 class="blog-cat"><strong><?php _e('in','WEBNUS_TEXT_DOMAIN'); ?></strong> <?php the_category(', ') ?> </h6>
                		<?php } ?>
                        <h6 class="blog-cat"><?php the_terms( $post->ID, 'presse_category', 'Catégories : ', ', ' ); ?></h6>
                	  </div>
                          
                          
                		
                			<?php 
                			
                			if( 'quote' == $post_format  ) echo '<blockquote>';
                			echo apply_filters('the_content',$content); 
                			if( 'quote' == $post_format  ) echo '</blockquote>';
                			
                			?>
                          <br class="clear">
                            <div class="next-prev-posts">
                              <div class="vc_col-sm-6 alignleft"><?php previous_post_link('%link', '<i class="fa fa-chevron-left"></i> Précédent') ?></div>
                              <div class="vc_col-sm-6 alignright"><?php next_post_link('%link', 'Suivant <i class="fa fa-chevron-right"></i>') ?></div>
                            </div>
                            <div class="vertical-space"></div>
                        </div>
                		<?php 
                		
                		  ?>
                		
                      </article>
                      <?php 
                       endwhile;
                		 endif;
                      ?>
                        <!-- end-main-conten -->
                </div>
            </div>
        </div>
    </div>
</section>
  <?php 
  get_footer();
  ?>