<?php
function  recentworks_shortcode($attributes, $content) {	
	extract(shortcode_atts(array(
		'type'  		=>  '',
		'title'      	=>	'',
		'text'      	=>	'',
		'subtitle'		=>	'',
		'link_text'		=>	'',
		'link_url'		=>	'',
		'show_text'		=>	'',
		'show_category' =>	'',
		'show_archive'	=>	'',
		'icon'			=>	'',
		'work_id'		=>  '',
		"count" 		=>  '12',
		'cols'			=>  '4',
		'hfilters'		=>  '',
		'full'			=>  '',
		'space'			=>  '',
	), $attributes));
	ob_start();

if ( $type=='1' ) { ?>
	
	<div class="col-md-3 alpha">
		<div class="rw-left-sidebar">
        <?php if( 'true' == $show_text ): ?>
		    <h3><strong><?php echo $title; ?></strong></h3>
	        <p><?php echo $text; ?></p>
	        <a href="<?php echo $link_url; ?>" class="magicmore"><?php echo $link_text; ?></a>
	        <hr>
	    <?php endif; ?>
	    
	    <?php if( 'true' == $show_category ): ?>
	        <h5><strong>Categories</strong></h5>
	        <div class="listbox1">
			    <ul> <?php
				  // Get the portfolio categories
				  $terms = get_terms("filter");
				  if( is_array($terms) ) :
					foreach( $terms as $term ) {
						echo '<li><a href="'.get_term_link($term, 'filter'). '">'. $term->name . '</a></li>';
					}
				  endif; ?>
				</ul>
		    </div>
		<?php endif; ?>
		</div>
    </div> <?php
    
	$sidebar = ob_get_contents();
	ob_end_clean();
	
	
	$wpbp = new WP_Query(array( 'post_type' => 'portfolio', 'posts_per_page'=>4,'paged'=>1));
	
	
	$i=0;
	$temp_out = '<div class="col-md-9 omega recent-works-items">';
	if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post();
	ob_start();
	?>
 
  <figure class="col-md-6 columns <?php echo ( ($i % 2)==0 ) ? 'alpha' : 'omega'; ?>">
  <a href="<?php the_permalink(); ?>"><?php get_the_image( array( 'meta_key' => array( 'Full', 'Full' ), 'size' => 'portfolio_full' ) ); ?></a>
	<figcaption><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
	<p><?php echo get_the_date('d M Y');?> - <?php 

	$terms = get_the_terms(get_the_id(), 'filter' );
	$terms_slug_str = '';
	//var_dump($terms);
	if ($terms && ! is_wp_error($terms)) :
		$term_slugs_arr = array();
		foreach ($terms as $term) {
			$term_slugs_arr[] = '<a href="'. get_term_link($term, 'filter') .'">' . $term->name . '</a>';
		}
		$terms_slug_str = join( ", ", $term_slugs_arr);
	endif;
	echo $terms_slug_str;
	

	

	?></p></figcaption>
  </figure>
 
	
	
	<?php
	++$i;
	
	
		$temp_out .= ob_get_contents();
	ob_end_clean();
	
	endwhile; endif;

	$temp_out .='</div>';
	
	
	$out = do_shortcode($sidebar . $temp_out);
	wp_reset_query();
	return $out;

} elseif ( $type=='2' ) {

	GLOBAL $webnus_options;
	$global_terms_array = array(); ?>

	<section class="latest-works <?php echo 'col'. $cols . '-w '; echo !empty($space)? 'with-space-w' : ''; ?>">   	
		<hr class="vertical-space1">
		<div class="icon-top-title aligncenter"> <?php
			echo "<i class=\"{$icon}\"></i>";
			echo '<hr class="vertical-space1">';
			echo "<h1>{$title}</h1>";
			echo "<h4>{$subtitle}</h4>";
			echo '<br>';
			if(empty($hfilters)) : ?>
			<nav class="primary clearfix">
				<div class="portfolioFilters">
				<a href="#" class="selected" data-filter="*"><?php _e('All','WEBNUS_TEXT_DOMAIN'); ?></a>
				{term_filter}
				</div>	
			</nav>
			<?php endif; ?>
		</div> <!-- end icon-top-title -->
		<hr class="vertical-space2">

		<div class="portfolio <?php echo empty($full)?'container':''; ?>"> <?php
		$works = null;
		if(!empty($work_id)) {
			$works = explode(',', $work_id);
		}

		if(!empty($works)) {
			$args = array(
				'orderby'=>'date',
				'order'=>'desc',
				'post_type'=>'portfolio',
				'nopaging ' => true,
				'posts_per_page'=>$count,
				'post__in'=>$works
			); 
		} else {
			$args = array(
				'orderby'=>'date',
				'order'=>'desc',
				'post_type'=>'portfolio',
				'nopaging ' => true,
				'posts_per_page'=>$count
				
			);
		}

		$query = new WP_Query($args);
		if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
			//get Filter Terms
			$terms = get_the_terms( get_the_ID(), 'filter' );
			if(!is_array($terms)) $terms=array(); ?>

			<figure class="portfolio-item  <?php foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->slug)). ' '; } ?> ">
			<div class="img-item"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'portfolio_thumb' ) ); ?>
			<div class="zoomex2"><h6><?php the_title(); ?><br><small><?php echo get_the_date('d M Y');?></small></h6>
			<a href="<?php $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' ); 
			$large_image = $large_image[0]; echo $large_image; ?>" class="prettyPhoto zoomlink1" ><i class="fa-plus"></i></a><a href="<?php the_permalink(); ?>" class="zoomlink2" ><i class="fa-arrow-right"></i></a></div></div>
			</figure>  <!-- end portfolio-item --> <?php

			$terms = get_the_terms(get_the_id(), 'filter' );
			if ($terms && ! is_wp_error($terms)) :
				
				foreach ($terms as $term) {
					
					if(!isset($global_terms_array[$term->slug]))
						$global_terms_array[$term->slug] = $term->name;
				}
				
			endif;

		endwhile; // end loop
		endif; ?>
		</div>	
	</section><?php 

	$category_str = '';
	if(count($global_terms_array) > 0) {
		foreach($global_terms_array as $slug=> $name)
		$category_str .= '<a href="#" class="" data-filter=".' . strtolower(preg_replace('/\s+/', '-', $slug)) . '">' . $name . '</a>';
	}
	 
	$output = ob_get_contents();

	$output = str_replace('{term_filter}', $category_str, $output);

	ob_end_clean();
	return $output;

	wp_reset_query(); // Reset the Query Loop
}
}

add_shortcode("recentworks", 'recentworks_shortcode');
?>