<?php

/*

Template Name: Portfolio

*/


get_header();

GLOBAL $webnus_options;
ob_start();
$global_terms_array = array();

?>
<section id="headline">
<div class="container">
  <h3>Portfolio</h3>
</div>
</section>
<section class="container page-content" >
<hr class="vertical-space1">
<?php 
if( 1 == $webnus_options->webnus_portfolio_isotope_enable() ){

?>
<nav class="primary old clearfix">
<div class="portfolioFilters">
  <ul>
	<li><a href="#" class="selected" data-filter="*"><?php _e('All','WEBNUS_TEXT_DOMAIN'); ?></a></li>
	{term_filter}
</ul>
</div>
</nav>
<?php } ?>
<div class="portfolio old">
<?php
			
			$portfolio_per_page = ($webnus_options->webnus_portfolio_count() > 0 ) ? $webnus_options->webnus_portfolio_count() : 1000;
			
			$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				   'orderby'=>'date',
				   'order'=>'desc',
				   'post_type'=>'portfolio',
				   'paged' => $page,
				   'posts_per_page'=>$portfolio_per_page
			); 
			query_posts($args);
			 
			if (have_posts()) : while (have_posts()) : the_post();
$terms = get_the_terms( get_the_ID(), 'filter' );
if(!is_array($terms)) $terms=array();
	
	$col_md = $webnus_options->webnus_portfolio_columns();
	switch ( $col_md ) {
		case '2':
			$col_md = '6';
			break;

		case '3':
			$col_md = '4';
			break;	
		
		case '4':
			$col_md = '3';
			break;

		case '5':
			$col_md = '3';
			break;

		case '6':
			$col_md = '3';
			break;
	}
?>
  <figure class="portfolio-item  <?php echo 'col-md-'. $col_md. ' '; foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->slug)). ' '; } ?> ">
	<div class="img-item tooltips"> <?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'portfolio_thumb' ) ); ?><span class="zoomex2"><h6><?php the_title(); ?></h6><a href="<?php $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' ); 
					$large_image = $large_image[0]; echo $large_image; ?>"  rel="help" title="Zoom" class="prettyPhoto zoomlink1" ><i class="fa-plus"></i></a><a href="<?php the_permalink(); ?>" rel="help" title="Go to Page" class="zoomlink2" ><i class="fa-arrow-right"></i></a></span> </div>
	<figcaption><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
	<p><?php echo get_the_date('d M Y');?> - <?php 

	$terms = get_the_terms(get_the_id(), 'filter' );
	$terms_slug_str = '';
	//var_dump($terms);
	if ($terms && ! is_wp_error($terms)) :
		$term_slugs_arr = array();
		foreach ($terms as $term) {
			$term_slugs_arr[] = '<a href="'. get_term_link($term, 'filter') .'">' . $term->name . '</a>';
			if(!isset($global_terms_array[$term->slug]))
				$global_terms_array[$term->slug] = $term->name;
		}
		$terms_slug_str = join( ", ", $term_slugs_arr);
	endif;
	echo $terms_slug_str;

	?></p></figcaption>
  </figure>
  <!-- end-portfolio-item-->
<?php
			endwhile;
			endif;

?>
</div>
<!-- end-portfolio -->
<div class="sixteen columns">
   <?php 
		if(function_exists('wp_pagenavi'))
		{
			wp_pagenavi();
			
		}
	  ?>
</div>
<hr class="vertical-space2">

</section>
  <!-- container -->

 <?php
$category_str = '';
 if(count($global_terms_array) > 0)
 {
	foreach($global_terms_array as $slug=> $name)
	 $category_str .= '<li><a href="#" class="" data-filter=".' . strtolower(preg_replace('/\s+/', '-', $slug)) . '">' . $name . '</a></li>';
	
 }
 
 $output = ob_get_contents();
 
 $output = str_replace('{term_filter}', $category_str, $output);
 
 ob_end_clean();
 echo $output;
 wp_reset_query(); // Reset the Query Loop 
get_footer();
?>